<?php
use Fuel\Core\Model_Crud;

/**
 * Model アプリ審査リジェクトステータス
 *
 * @author     Kouji Itahana
 */

class Model_Master_Inspect_Status_Reject extends Model_Crud_Master
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'M_INSPECT_STATUS_REJECT';

    /**
     * リジェクト理由の取得
     *
     * @return String
     */
    public static function get_reject($reject_id)
    {
        $reject = self::find_one_by('id', $reject_id);
        return is_null($reject) ? '' : $reject->name;
    }
}