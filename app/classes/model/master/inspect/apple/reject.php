<?php
use Fuel\Core\Model_Crud;

/**
 * Model Apple審査NG理由マスター
 *
 * @author     Kouji Itahana
 */

class Model_Master_Inspect_Apple_Reject extends Model_Crud_Master
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'M_INSPECT_APPLE_REJECT';

    /**
     * Select用Array生成
     *
     * @return array
     */
    public static function list_opt_array()
    {
        $opt = array('' => '----- リジェクト理由を選択 -----');
        $list = self::find_all();
        foreach ($list as $entry)
        {
            $opt[$entry->id] = $entry->title;
        }
        return $opt;
    }
}