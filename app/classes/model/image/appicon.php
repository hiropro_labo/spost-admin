<?php
/**
 * Model アプリ内ステータスバー表示用画像
 *
 * @author     Kouji Itahana
 */

class Model_Image_Appicon extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'IMAGE_APPICON';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'app_icon';

    /**
     * カラム[link]がある場合はtrue
     * @var bolean
     */
    protected static $_column_link = false;
}