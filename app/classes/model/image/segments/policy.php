<?php
/**
 * Model 事業内容ページ：Policy画像
 *
 * @author     Kouji Itahana
 */

class Model_Image_Segments_Policy extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'IMAGE_SEGMENTS_POLICY';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'segments_policy';

}