<?php
/**
 * Model 事業内容ページ：Work画像
 *
 * @author     Kouji Itahana
 */

class Model_Image_Segments_Work extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'IMAGE_SEGMENTS_WORK';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'segments_work';

}