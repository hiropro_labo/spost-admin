<?php
/**
 * Model 事業内容ページ：Message画像
 *
 * @author     Kouji Itahana
 */

class Model_Image_Segments_Message extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'IMAGE_SEGMENTS_MESSAGE';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'segments_message';

}