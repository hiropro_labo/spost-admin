<?php
/**
 * Model アプリアイコン画像
 *
 * @author     Kouji Itahana
 */

class Model_Image_Banner extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'IMAGE_BANNER';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'banner';

    /**
     * カラム[link]がある場合はtrue
     * @var bolean
     */
    protected static $_column_link = false;

    /**
     * 表示順にソートされたリストを取得
     *
     * @param string $id
     * @return Array|NULL
     */
    public static function get_member_list_all($id)
    {
        $list = self::find_by(array(static::$_user_id_column => $id));
        if ( ! is_null($list))
        {
            usort($list, function($a, $b){
                return $a['position'] > $b['position'];
            });
        }

        return $list;
    }

    /**
     * 表示順にソートされたリストを取得（※表示設定が有効もののみ）
     *
     * @param string $id
     * @return boolean|unknown
     */
    public static function get_member_list($id)
    {
        $list = self::find_by(array(
            static::$_user_id_column => $id,
            'enable'    => 1
        ));
        if ( ! is_null($list))
        {
            usort($list, function($a, $b){
                return $a['position'] > $b['position'];
            });
        }

        return static::order_by_asc($list, 'position');
    }
}