<?php
/**
 * Model スクリーンショット
 *
 * @author     Kouji Itahana
 */

class Model_Image_Screen_Shot extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'IMAGE_SCREEN_SHOT';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'screen_shot1';

    /**
     * カラム[link]がある場合はtrue
     * @var bolean
     */
    protected static $_column_link = false;

    /**
     * Imageデータ取得
     *
     * @Override
     * @param string $client_id
     * @param string $position
     * @return
     */
    public static function get_image($id, $position=null)
    {
        $user_col = static::$_user_id_column;
        $where = array($user_col => $id);
        if ( ! is_null($position) and ! empty($position))
        {
            $where['position'] = $position;
        }

        $res = self::find_one_by($where);
        if (is_null($res))
        {
            $res = self::forge();
            $res->$user_col = $id;
            $res->file_name = '';

            if ( ! is_null($position) and ! empty($position))
            {
                $res->position = $position;
            }
        }
        return $res;
    }


}