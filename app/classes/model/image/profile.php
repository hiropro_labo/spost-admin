<?php
/**
 * Model プロフィール用画像（※議員アプリ）
 *
 * @author     Kouji Itahana
 */

class Model_Image_Profile extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'IMAGE_PROFILE';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'profile';
}