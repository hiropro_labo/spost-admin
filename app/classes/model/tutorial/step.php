<?php
/**
 * Model チュートリアル進行状況
 *
 * @author Yoshitaka Kitagawa
 */

class Model_Tutorial_Step extends Model_Crud_Shard
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'TUTORIAL_STEP';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';
}