<?php
use Fuel\Core\Model_Crud;

/**
 * Model 販売商品の報酬付与ログ
 *
 * @author     Kouji Itahana
 */

class Model_Log_Reward extends Model_Crud
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'LOG_REWARD';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

}