<?php
/**
 * Model PUSH通知送信予約キュー
 *
 * @author     Kouji Itahana
 */

class Model_Queue_Notification extends Model_Crud
{
    /**
     * 実行ステータス：未配信
     * @var int
     */
    const QUEUE_STATUS_PRE     = 0;

    /**
     * 実行ステータス：配信処理中
     * @var int
     */
    const QUEUE_STATUS_RUNNING = 1;

    /**
     * 実行ステータス：配信完了
     * @var int
     */
    const QUEUE_STATUS_END     = 2;

    /**
     * 実行ステータス：配信エラー(※配信中断)
     * @var int
     */
    const QUEUE_STATUS_ERROR   = 99;

    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'QUEUE_NOTIFICATION';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';
}