<?php
/**
 * Model 予約カレンダーの表示
 *
 * @author     Yoshitaka Kitagawa
 */

class Model_Appoint_Item extends Model_Crud_Shard
{
    /**
     * 表示設定：非表示
     * @var number
     */
    const APPOINT_FLG_OFF = 0;

    /**
     * 表示設定：表示
     * @var number
     */
    const APPOINT_FLG_ON = 1;

    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'APPOINT_ITEM';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    /**
     * 表示のフラグを、「する」「しない」に変更
     *
     * @param $client_id
     * @return String
     */
    public static function getFlgString($client_id)
    {
        $appoint = '表示しない';
        $model = self::find_one_by('client_id', $client_id);
        if ( ! is_null($model)) {
            if ($model->appoint_flg == self::APPOINT_FLG_ON) {
                $appoint = '表示する';
            }
        }
        return $appoint;
    }
}