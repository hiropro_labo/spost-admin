<?php
/**
 * Model 採用情報タイトル コンテンツ情報
 *
 * @author     Kouji Itahana
 */

class Model_Recruit_Title extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'RECRUIT_TITLE';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

}