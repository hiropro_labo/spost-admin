<?php
use Fuel\Core\Model_Crud;

/**
 * Model クライアントクーポン
 *
 * @author     Kouji Itahana
 */

class Model_Coupon_Client extends Model_Crud
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'COUPON_CLIENT';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

}