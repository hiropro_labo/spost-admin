<?php
use Fuel\Core\Model_Crud;

/**
 * Model エージェントクーポン
 *
 * @author     Kouji Itahana
 */

class Model_Coupon_Agent extends Model_Crud
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'COUPON_AGENT';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    /**
     * クーポンコード取得
     * @param string $agent_id
     * @param string $sale_id
     * @return string $code
     */
    public static function code($agent_id, $sale_id)
    {
        $model = self::find_one_by(array(
            'agent_id' => $agent_id,
            'sale_id'  => $sale_id
        ));
        if (is_null($model))
        {
            try
            {
                DB::start_transaction();

                $conf   = \Config::load('original', true);
                $length = $conf['coupon_code_length'];

                $model = self::forge();
                $model->agent_id = $agent_id;
                $model->sale_id  = $sale_id;
                $model->code     = \Str::random('alnum', $length);

                if ( ! $model->save(false))
                {
                    throw new Exception('regist error: coupon agent');
                }

                DB::commit_transaction();
            }
            catch (\Exception $e)
            {
                DB::rollback_transaction();
                Log::error($e->getMessage());
            }
        }

        return $model->code;
    }
}