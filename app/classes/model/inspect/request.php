<?php
/**
 * Model: アプリ審査申請エントリ
 *
 * @author     Kouji Itahana
 */

class Model_Inspect_Request extends Model_Crud
{
   /**
     * @var int 未申請状態（※初期値：リジェクト時に本ステータスになる）
     */
    const INSPECT_STATUS_PRE = 0;

    /**
     * @var int 申請受付
     */
    const INSPECT_STATUS_INIT  = 1;

    /**
     * @var int 簡易審査中
     */
    const INSPECT_STATUS_START = 2;

    /**
     * @var int 簡易審査終了（※入金待機）
     */
    const INSPECT_STATUS_END   = 3;

    /**
     * @var int Androidリリース完了
     */
    const INSPECT_STATUS_ANDROID_RELEASE = 4;

    /**
     * @var int Apple審査申請受け付け
     */
    const INSPECT_STATUS_IOS_REQUEST = 5;

    /**
     * @var int Apple審査中(※審査申請完了)
     */
    const INSPECT_STATUS_IOS_INSPECT = 6;
    /**
     * @var int Apple審査リジェクト
     */
    const INSPECT_STATUS_IOS_REJECT = 7;

    /**
     * @var int Appple審査通過
     */
    const INSPECT_STATUS_IOS_PASS = 8;

    /**
     * @var int iOSリリース完了
     */
    const INSPECT_STATUS_IOS_RELEASE = 9;

        /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'INSPECT_REQUEST';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    /**
     * リクエスト一覧取得
     *   ・指定ステータスのエントリーを返す
     *   ・エントリーに担当者氏名を追加する
     *
     * @param int $status
     * @param string $per_page
     * @param string $offset
     * @return NULL|array
     */
    public static function get_list_by_status($status, $per_page=null, $offset=null)
    {
        $list = self::find_by(
            'status',
            $status,
            '=',
            $per_page,
            $offset
        );

        if (is_null($list)) return null;

        $list = static::add_master($list);
        $list = static::add_status_name($list);

        return $list;
    }

    /**
     * リクエスト一覧取得
     *   ・指定ステータス以外のエントリーを返す
     *   ・エントリーに担当者氏名を追加する
     *
     * @param int $status
     * @param string $per_page
     * @param string $offset
     * @return NULL|array
     */
    public static function get_list($status, $per_page=null, $offset=null)
    {
        $list  = self::find_by(
            array(array('status', '!=', $status)),
            null,
            null,
            $per_page,
            $offset);
        if (is_null($list)) return null;

        $list = static::add_master($list);
        return $list;
    }

    /**
     * 指定IDのレコード取得（※ステータス名称を付与して返す）
     *
     * @param string $id
     * @return NULL|Model_Inspect_Request
     */
    public static function get_one($id)
    {
        $model = self::find_by_pk($id);
        if (is_null($model))
        {
            return null;
        }

        $status_name = '---';
        $status = \Model_Master_Inspect_Status_Request::find_by_pk($model->status);
        if (is_null($model))
        {
            $model->status_name = $status_name;
            $model->elapsed_time = '--- 不明 ---';
        }
        else
        {
            $model->status_name  = $status->name;
            $model->elapsed_time = static::elapsed_time($model->created_at);
        }
        return $model;
    }

    /**
     * 審査中エントリの存在チェック
     *
     * @param string $client_id
     * @return boolean true:エントリあり/false:エントリなし
     */
    public static function is_exists_in_inspect($client_id)
    {
        $rs = self::find_by(array(
            'client_id' => $client_id
        ));
        if (is_null($rs))
        {
            return false;
        }

        foreach ($rs as $model)
        {
            if (intval($model->status) != 3)
            {
                return true;
            }
        }
        return false;
    }

    private static function elapsed_time($start_datetime)
    {
        $start   = strtotime($start_datetime);
        $current = time();
        $e_sec   = $current - $start;
        if ($e_sec < 0)
        {
            $e_sec = '--- 不明 ---';
            return $e_sec;
        }

        $dd = (int)($e_sec / (60 * 60 * 24));
        if ($dd < 1)
        {
            $dd = '';
        }

        $e_sec2 = $e_sec - ($dd * 60 * 60 * 24);
        $ss = $e_sec2 % 60;
        $mm = (int)($e_sec2 / 60) % 60;
        $hh = (int)($e_sec2 / (60 * 60));

        $time_str = '';
        if ( ! empty($dd))
        {
            $time_str = '%d日 + %02d:%02d:%02d 経過';
            return sprintf($time_str, $dd, $hh, $mm, $ss);
        }
        else
        {
            $time_str = '%02d:%02d:%02d 経過';
            return sprintf($time_str, $hh, $mm, $ss);
        }
    }

    public static function get_app_disp_name($client_id)
    {
        $shop = new \Shop($client_id);
        $shop->init_models();
        if (is_null($shop))
        {
            throw new Exception('shop object is null.');
        }

        $model = Model_Store_Info::find_one_by('client_id', $client_id);

        return $model->app_disp_name;
    }

    /**
     * Select用Array生成
     *
     * @return array
     */
    public static function list_opt_array()
    {
        $list = array('' => '----- 検索条件 -----');
        $master = \Model_Master_Inspect_Status_Request::find_all();

        foreach ($master as $rec)
        {
            $list[$rec->id] = $rec->name;
        }

        return $list;
    }

    /**
     * 審査ステータス名称取得
     *
     * @param string $status
     * @return string
     */
    private static function status_name($status)
    {
        $opt = static::list_opt_array();
        return $opt[$status];
    }

    /**
     * 各レコードにステータス名称を追加
     * @param array $list 本クラスのオブジェクトが格納された配列
     * @return array
     */
    private static function add_status_name($list)
    {
        if (is_null($list) or empty($list)) return $list;

        foreach ($list as $rec)
        {
            if ( ! property_exists($rec, 'status')) continue;
            $rec->status_name = static::status_name($rec->status);
        }

        return $list;
    }

    /**
     * 各レコードに担当者氏名を追加
     *
     * @param array $list 本クラスのオブジェクトが格納された配列
     * @return array
     */
    private static function add_master($list)
    {
        foreach ($list as $rec)
        {
            if ( ! property_exists($rec, 'master_id'))
            {
                $rec->master_name = '--- 未設定 ---';
                continue;
            }

            $master = Model_User_Master::find_by_pk($rec->master_id);
            if (is_null($master))
            {
                $rec->master_name = '--- 未設定 ---';
                continue;
            }

            $rec->master_name = $master->username;
        }

        return $list;
    }


}