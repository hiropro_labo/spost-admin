<?php
/**
 * Model: アプリ審査：Appleリジェクト履歴
 *
 * @author     Kouji Itahana
 */

class Model_Inspect_Reject_Apple extends Model_Crud
{
    /**
     * @var int ステータス値：未
     */
    const STATUS_OFF = 0;

    /**
     * @var int ステータス値：済s
     */
    const STATUS_ON  = 1;

    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'INSPECT_REJECT_APPLE';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

}