<?php
/**
 * Model: アプリ審査：リジェクト履歴
 *
 * @author     Kouji Itahana
 */

class Model_Inspect_Reject_History extends Model_Crud
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'INSPECT_REJECT_HISTORY';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

}