<?php
/**
 * Model: アプリ審査：リジェクトエントリ
 *
 * @author     Kouji Itahana
 */

class Model_Inspect_Reject extends Model_Crud
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'INSPECT_REJECT';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    /**
     * リジェクト理由の取得
     *
     * @return String
     */
    public static function get_reject_name($reject_id)
    {
        return Model_Master_Inspect_Status_Reject::get_reject($reject_id);
    }
}