<?php
/**
 * Model プロフィール略歴 コンテンツ情報
 *
 * @author     Kouji Itahana
 */

class Model_Profile_History extends Model_Crud_Shard
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'PROFILE_HISTORY';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

}