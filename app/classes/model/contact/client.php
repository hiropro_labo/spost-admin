<?php
use Fuel\Core\Model_Crud;
//データベースを参照する場合にはフォルダ名とファイル名は一緒でなければならない。
class Model_Contact_Client extends Model_Crud
{
	/**
	 * 既読ステータス：未読
	 * @var int
	 */
	const STATUS_UNREAD = 0;
	
	/**
	 * 既読ステータス：既読
	 * @var int
	 */
	const STATUS_READ   = 1;
	
	protected static $_table_name = 'CONTACT_CLIENT'; //データベースの読み込むテーブルを指定
	/**
	 * @var  string  fieldname of created_at field, uncomment to use.
	 */
	protected static $_created_at = 'created_at'; //日時を自動で反映

	/**
	 * @var  string  fieldname of updated_at field, uncomment to use.
	 */
	protected static $_updated_at = 'updated_at'; //更新日時を自動で反映
   
}