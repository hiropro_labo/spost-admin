<?php
use Fuel\Core\Model_Crud;

/**
 * Model 決済結果OKログ
 *
 * ・J-Paymentからのキックバック(決済結果)を保存
 *
 * @author     Kouji Itahana
 */

class Model_Payment_Result_Success extends Model_Crud_Shard_Payment
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'PAYMENT_RESULT_SUCCESS';

    /**
     * @var  string primary key
     */
    protected static $_primary_key = 'seq';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';
}