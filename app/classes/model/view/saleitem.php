<?php
use Fuel\Core\Model_Crud;

/**
 * Model ゴールドパートナーユーザ
 *
 * @author     Kouji Itahana
 */

class Model_View_Saleitem extends Model_Readonly
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'VIEW_SALEITEM';
}