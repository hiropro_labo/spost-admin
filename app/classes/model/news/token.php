<?php
/**
 * Model News: PUSH通知用デバイストークン
 *
 * @author     Kouji Itahana
 */

class Model_News_Token extends Model_Crud_Shard
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'NEWS_TOKEN';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

}