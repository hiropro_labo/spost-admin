<?php
/**
 * Model News: ニュース情報
 *
 * @author     Kouji Itahana
 */

class Model_News_Info extends Model_Crud_Shard_Image
{
    /**
     * PUSH通知配信フラグ：通知なし
     * @var int
     */
    const NOTICE_SETTING_OFF = 0;

    /**
     * PUSH通知配信フラグ：通知なし
     * @var int
     */
    const NOTICE_SETTING_ON  = 1;

    /**
     * PUSH通知配信完了フラグ：未配信
     * @var int
     */
    const NOTICE_STATUS_OFF = 0;

    /**
     * PUSH通知配信完了フラグ：配信済
     * @var int
     */
    const NOTICE_STATUS_ON  = 1;

    /**
     * @var string
     */
    protected static $_user_id_column = 'client_id';

    /**
    * @var  string  $_table_name  The table name
    */
    protected static $_table_name = 'NEWS_INFO';

    /**
    * @var  string  fieldname of created_at field, uncomment to use.
    */
    protected static $_created_at = 'created_at';

    /**
    * @var  string  fieldname of updated_at field, uncomment to use.
    */
    protected static $_updated_at = 'updated_at';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'news';

    protected static $_datetime_columns = array(
        'updated_at', 'created_at', 'send_at'
    );

    /**
     * 開催日時を年月日時分にそれぞれ分解
     *
     * @param Model_Crud $model
     * @return Model_Crud
     */
    public function analyze_reserved_at()
    {
        static::reserved_at_year();
        static::reserved_at_month();
        static::reserved_at_day();
        static::reserved_at_hour();
        static::reserved_at_min();
    }

    /**
     * 試合開催日時
     *
     * @return \DateTime
     */
    public function reserved_at()
    {
        $dt = null;
        if (array_key_exists('reserved_at', $this->_data))
        {
            $dt = $this->reserved_at == '0000-00-00 00:00:00' ? null : $this->reserved_at;
        }

        return new \DateTime($dt);
    }

    /**
     * 試合開催日時：年
     *
     * @return string
     */
    public function reserved_at_year()
    {
        $this->reserved_at_year = static::reserved_at()->format('Y');
        return $this->reserved_at_year;
    }

    /**
     * 試合開催日時：月
     *
     * @return string
     */
    public function reserved_at_month()
    {
        $this->reserved_at_month = static::reserved_at()->format('n');
        return $this->reserved_at_month;
    }

    /**
     * 試合開催日時：日
     *
     * @return string
     */
    public function reserved_at_day()
    {
        $this->reserved_at_day = static::reserved_at()->format('j');
        return $this->reserved_at_day;
    }

    /**
     * 試合開催日時：時
     *
     * @return string
     */
    public function reserved_at_hour()
    {
        $this->reserved_at_hour = static::reserved_at()->format('H');
        return $this->reserved_at_hour;
    }

    /**
     * 試合開催日時：分
     *
     * @return string
     */
    public function reserved_at_min()
    {
        $this->reserved_at_min = static::reserved_at()->format('i');
        return $this->reserved_at_min;
    }

}