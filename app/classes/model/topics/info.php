<?php
/**
 * Model TOPICS: トピックス情報
 *
 * @author     Kouji Itahana
 */

class Model_Topics_Info extends Model_Crud_Shard_Image
{
    /**
     * PUSH通知配信フラグ：通知なし
     * @var int
     */
    const NOTICE_SETTING_OFF = 0;

    /**
     * PUSH通知配信フラグ：通知なし
     * @var int
     */
    const NOTICE_SETTING_ON  = 1;

    /**
     * PUSH通知配信完了フラグ：未配信
     * @var int
     */
    const NOTICE_STATUS_OFF = 0;

    /**
     * PUSH通知配信完了フラグ：配信済
     * @var int
     */
    const NOTICE_STATUS_ON  = 1;

    /**
     * @var string
     */
    protected static $_user_id_column = 'client_id';

    /**
    * @var  string  $_table_name  The table name
    */
    protected static $_table_name = 'TOPICS_INFO';

    /**
    * @var  string  fieldname of created_at field, uncomment to use.
    */
    protected static $_created_at = 'created_at';

    /**
    * @var  string  fieldname of updated_at field, uncomment to use.
    */
    protected static $_updated_at = 'updated_at';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'topics';
}