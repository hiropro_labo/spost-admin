<?php
/**
 * Model 各種機能の利用設定
 *
 * @author     Kouji Itahana
 */

class Model_Switcher_Info extends Model_Crud_Shard
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'SWITCHER_INFO';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    protected static $_mysql_timestamp = true;
}