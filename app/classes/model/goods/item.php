<?php
/**
 * Model GOODS: 商品情報
 *
 * @author     Kouji Itahana
 */

class Model_Goods_Item extends Model_Crud_Shard_Image
{
    /**
     * 表示フラグ：非表示
     * @var int
     */
    const ENABLE_FLG_OFF = 0;

    /**
     * 表示フラグ：表示
     * @var int
     */
    const ENABLE_FLG_ON  = 1;

    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'GOODS_ITEM';

    /**
     * 設定ファイルでのキー値（※継承先で設定必須）
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'goods_item';

    /**
     * カラム[link]がある場合はtrue
     * @var bolean
     */
    protected static $_column_link = false;

    /**
     * @var string
     */
    protected static $_user_id_column = 'client_id';

    /**
     * 表示順にソートされたリストを取得
     *
     * @param string $id User-ID
     * @param string $parent_id
     * @param int $enable [0:非表示/1:表示]
     * @return array|NULL
     */
    public static function get_goods_list($id, $parent_id, $enable=null)
    {
        $where = array(
            static::$_user_id_column => $id,
            'parent_id' => $parent_id
        );

        if ( ! is_null($enable))
        {
            $where['enable'] = $enable;
        }

        $list = self::find_by($where);

        if ( ! is_null($list))
        {
            usort($list, function($a, $b){
                return $a['position'] > $b['position'];
            });
        }

        return $list;
    }

    /**
     * 商品ポジション（※新規追加時のポジション取得）
     *
     * @param string $id User-ID
     * @return number
     */
    public function new_position_by_parent_id($id, $parent_id)
    {
        $cnt = self::count('id', null, array(static::$_user_id_column => $id, 'parent_id' => $parent_id));
        return $cnt + 1;
    }

    /**
     * T画像表示: 順序変更 順序ダウン
     *
     * @param string $id User-ID
     * @param string $current_position
     * @param int    $max
     * @throws Exception
     * @return boolean
     */
    public static function item_position_down($id, $current_position, $max, $parent_id)
    {
        try
        {
            DB::start_transaction();

            $image = self::find_one_by(array(
                static::$_user_id_column => $id,
                'position'  => $current_position,
                'parent_id' => $parent_id
            ));
            if (is_null($image))
            {
                throw new Exception('Error: coupon image position change failed.');
            }

            $new_position = null;
            if ($image->position < $max)
            {
                $new_position = $image->position + 1;
            }
            else
            {
                throw new Exception('Error: coupon image position change failed.');
            }

            $image_other = self::find_one_by(array(
                static::$_user_id_column => $id,
                'position'  => $new_position,
                'parent_id' => $parent_id
            ));
            if (! is_null($image_other))
            {
                $image_other->position  = $image->position;
                if (! $image_other->save(false))
                {
                    throw new Exception('Error: coupon image position change failed.');
                }
            }

            $image->position = $new_position;
            if (! $image->save(false))
            {
                throw new Exception('Error: coupon image position change failed.');
            }

            DB::commit_transaction();
        }
        catch(Exception $e)
        {
            DB::rollback_transaction();
            throw new Exception('Error: coupon image position change failed.');
        }

        return true;
    }

    /**
     * 画像表示: 順序変更 順序アップ
     *
     * @param string $id User-ID
     * @param string $current_position
     * @param int    $max
     * @throws Exception
     * @return boolean
     */
    public static function item_position_up($id, $current_position, $parent_id)
    {
        try
        {
            DB::start_transaction();

            $image = self::find_one_by(array(
                static::$_user_id_column => $id,
                'position'  => $current_position,
                'parent_id' => $parent_id
            ));
            if (is_null($image))
            {
                throw new Exception('Error: coupon image position change failed.');
            }

            $new_position = null;
            if ($current_position > 1)
            {
                $new_position = $image->position - 1;
            }

            $image_other = self::find_one_by(array(
                static::$_user_id_column => $id,
                'position'  => $new_position,
                'parent_id' => $parent_id
            ));
            if (! is_null($image_other))
            {
                $image_other->position = $image->position;
                if (! $image_other->save(false))
                {
                    throw new Exception('Error: coupon image position change failed.');
                }
            }

            $image->position  = $new_position;
            if (! $image->save(false))
            {
                throw new Exception('Error: coupon image position change failed.');
            }

            DB::commit_transaction();
        }
        catch(Exception $e)
        {
            DB::rollback_transaction();
            throw new Exception('Error: coupon image position change failed.');
        }

        return true;
    }

}