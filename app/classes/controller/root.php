<?php
/**
 * Root Controller
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Root extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'root/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'home';

    /**
     * @var Inspect
     */
    protected static $_status = null;

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
    	Lang::load('root');
    }

    /**
     * TOP
     *
     * @access  public
     * @return  Response
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $infomation = new \Support\Root\Infomation(self::get_user_id());
        $category = self::$_sponsor->contents()->theme()->model()->category;

        $view->set('category',   $category);
        $view->set('infomation', $infomation);
        return $view;
    }

    private function popup()
    {
        return '<script type="text/javascript" language="javascript">popup_open("cp");</script>';
    }

    private function home_bar($status)
    {
        if( ! $status->shop())
        {
            $bar_num = 1;
        }
        if($status->alive())
        {
            $bar_num = 2;
        }
        if($status->inspect() && $status->inspect_pass())
        {
            $bar_num = 3;
        }
        if($status->shop())
        {
            $bar_num = 4;
        }
        return '<img src="/assets/img/common/index/home_bar'. $bar_num .'.png" class="fl">';
    }

    private function app_status()
    {
                // 公開状態
        $app_status = '<img src="/assets/img/common/index/app_yellow.png">アプリ準備中';
        if(static::$_shop->is_opened())
        {
            $app_status = '<img src="/assets/img/common/index/app_green.png">アプリ公開中';
        }
        else if(static::$_shop->status() > 500)
        {
            $app_status = '<img src="/assets/img/common/index/app_red.png">アプリ公開停止中';
        }

        return $app_status;
    }
}
