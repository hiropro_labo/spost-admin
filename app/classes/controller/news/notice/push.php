<?php
use Fuel\Tasks\Notification;

/**
 * NEWS: ニュース編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_News_Notice_Push extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'news/notice/';
    const POST_URI_UPDATE_EXE = 'news/notice/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'news';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        if ( ! self::$_shop->is_opened())
        {
            return Response::redirect('exception/411');
        }

        static::$_support = \Support\Api\News::instance(self::get_user_id());
    }

    /**
     * ニュース配信：確認画面
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $id = $this->param('id');
        if (empty($id) or ! is_numeric($id))
        {
            return Response::redirect('exception/404');
        }

        $client = Model_User_Client::find_by_pk(self::get_user_id());
        $news   = Model_News_Info::find_one_by(array(
            'id'            => $id,
            'client_id'     => self::get_user_id(),
            'notice_status' => 0
        ));

        if (is_null($news))
        {
            return Response::redirect('exception/404');
        }

        $fieldset = $this->get_fieldset();
        $fieldset->populate($news);

        $view->set('news',      $news);
        $view->set('client',    $client);
        $view->set('fieldset',  $fieldset);
        return $view;
    }

    /**
     * ニュース配信：配信処理
     *
     * ここでは配信キューへ登録するのみで実際の配信処理はCronジョブで処理される
     *
     * @return View
     */
    public function action_exe()
    {
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $id = $this->param('id');
	    if (empty($id) or ! is_numeric($id))
	    {
	        return Response::redirect('excpetion/404');
	    }

	    $news = Model_News_Info::find_by_pk($id);
	    if (is_null($news))
	    {
	        Response::redirect('exception/404');
	    }

//         $news = Model_News_Info::find_one_by(array(
//             'id'            => $id,
//             'client_id'     => self::get_user_id(),
//             'notice'        => 1,
//             'notice_status' => 0
//         ));
	    if (intval($news->notice) == 1)
	    {
	        // ニュースPUSH通知処理
	        $notification = Notification::instance(self::get_user_id(), $id);
            if ( ! $notification->push_queue())
            {
                $view->set('message', '配信登録の処理に失敗しました');
                $view->set_filename('exception/503');
                return $view;
            }
	    }

	    try
	    {
	        DB::start_transaction();
	        $news->notice_status = 1;
            $news->send_at = Date::forge()->format('mysql');
            if ( ! $news->save(false))
            {
                throw new Exception('update error: news push notice [id: '.$id.']');
            }
	        DB::commit_transaction();
	    }
	    catch (\Exception $e)
	    {
	        DB::rollback_transaction();
	        Log::error($e->getMessage());
	        $view->set('message', '配信登録の更新処理に失敗しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

        // キャッシュクリア
        static::support()->clear_cache();

	    return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // タイトル
        $fieldset->add('title', 'タイトル')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 本文
        $fieldset->add('body', '本文')
            ->set_type('textarea')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'));

        // プッシュ通知
        $opt = array('0' => 'しない', '1' => 'する');
        $fieldset->add('notice', 'プッシュ通知',
            array('options' => $opt, 'type' => 'radio'))
            ->add_rule('required');

        return $fieldset;
    }
}
