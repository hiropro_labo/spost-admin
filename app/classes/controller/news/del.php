<?php
/**
 * NEWS: ニュース削除
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_News_Del extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'news/del/';
    const POST_URI_UPDATE_EXE = 'news/del/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'news';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('news');
        static::$_support = \Support\Api\News::instance(self::get_user_id());
    }

    /**
     * ニュース削除：入力画面/確認画面
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $id = $this->param('id');
        if (empty($id) or ! is_numeric($id))
        {
            return Response::redirect('excpetion/404');
        }

        $news = self::$_sponsor->contents()->news_info()->model_for_delete($id);
        if (is_null($news))
        {
            return Response::redirect('exception/404');
        }
        $news->analyze_reserved_at();

        $fieldset = $this->get_fieldset();
        $fieldset->populate($news);

        $view->set('news',      $news);
        $view->set('fieldset',  $fieldset);
        return $view;
    }

    /**
     * ニュース削除：更新処理
     *
     * @return View
     */
    public function action_exe()
    {
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $id = $this->param('id');
	    if (empty($id) or ! is_numeric($id))
	    {
	        return Response::redirect('excpetion/404');
	    }

        $news = self::$_sponsor->contents()->news_info()->model_for_delete($id);
        if (is_null($news))
        {
            return Response::redirect('exception/404');
        }

	    $conf_image = \Config::load('original_image', true);
	    $save_dir   = $conf_image['save_dir']['news'].self::get_user_id().DS;
	    $file_name  = $news->file_name;

	    try
	    {
	        DB::start_transaction();

	        // 画像ファイル削除
	        $file_upload = new \Support\File_Upload(self::get_user_id(), 'news');
	        $file_upload->delete_file($news->file_name, true);
	        // DBレコード削除
	        $news->delete();
	        // キャッシュクリア
	        static::support()->clear_cache();

	        DB::commit_transaction();
	    }
	    catch (Exception $e)
	    {
	        Debug::dump($e->getMessage());
            DB::rollback_transaction();
            Log::error(__METHOD__.'news delete error: '.$e->getMessage());
            $view->set('message', 'ニュース削除の処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
	    }

	    return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // タイトル
        $fieldset->add('title', 'タイトル')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 本文
        $fieldset->add('body', '本文')
            ->set_type('textarea')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'));

        // 配信対象
        $opt = array(1 => '球団アプリユーザーへも配信', 2 => '球団アプリユーザーへは配信しない');
        $fieldset->add('target', '配信対象',array('type' => 'checkbox', 'options' => $opt));

        // 配信予約日時：年
        $fieldset->add('reserved_at_year', '試合開催日', array('options' => $this->opt_year(), 'type' => 'select', 'class' => 'w_90'))
            ->add_rule('required')
            ->add_rule('valid_string', array('numeric', 'utf8'));

        // 配信予約日時：月
        $fieldset->add('reserved_at_month', '試合開催日', array('options' => $this->opt_month(), 'type' => 'select', 'class' => 'w_60'))
            ->add_rule('required')
            ->add_rule('valid_string', array('numeric', 'utf8'));

        // 配信予約日時：日
        $fieldset->add('reserved_at_day', '試合開催日', array('options' => $this->opt_day(), 'type' => 'select', 'class' => 'w_60'))
            ->add_rule('required')
            ->add_rule('valid_string', array('numeric', 'utf8'));

        // 配信予約日時：時
        $fieldset->add('reserved_at_hour', '試合開始時刻', array('options' => $this->opt_hour(), 'type' => 'select', 'class' => 'w_60'))
            ->add_rule('required')
            ->add_rule('valid_string', array('numeric', 'utf8'));

        // 配信予約日時：分
        $fieldset->add('reserved_at_min', '試合開始時刻', array('options' => $this->opt_min(), 'type' => 'select', 'class' => 'w_60'))
            ->add_rule('required')
            ->add_rule('valid_string', array('numeric', 'utf8'));

        return $fieldset;
    }

    private function opt_year()
    {
        $opt = array();
        for ($i=2013; $i < 2020; $i++)
        {
            $opt[$i] = $i;
        }
        return $opt;
    }

    private function opt_month()
    {
        $opt = array();
        for ($i=1; $i < 13; $i++)
        {
            $opt[$i] = $i;
        }
        return $opt;
    }

    private function opt_day()
    {
        $opt = array();
        for ($i=1; $i < 32; $i++)
        {
            $opt[$i] = $i;
        }
        return $opt;
    }

    private function opt_hour()
    {
        $opt = array();
        for ($i=0; $i < 25; $i++)
        {
            $hour = sprintf("%02d", $i);
            $opt[$hour] = $hour;
        }
        return $opt;
    }

    private function opt_min()
    {
        $opt = array();
        for ($i=0; $i < 60; $i++)
        {
            $min = sprintf("%02d", $i);
            $opt[$min] = $min;
        }
        return $opt;
    }

}
