<?php
use Fuel\Core\Pagination;

/**
 * NEWS: ニュース一覧
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_News_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'news/';
    const PAGINATION_URL   = 'news';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'news';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('news');
    }

    /**
     * ニュース一覧
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $pager = self::$_sponsor->contents()->news_info()->list_news_pager(self::PAGINATION_URL);
        $list  = self::$_sponsor->contents()->news_info()->list_news($pager->per_page, $pager->offset);

//         // チュートリアルの進行状況
//         $tutorial = \Support\Tutorial\Step::instance(self::get_user_id());
//         $view->set('tutorial_js', $tutorial->step_js('news'), false);

        $view->set('list',   $list);
        $view->set('pager',  $pager->render(), false);
        return $view;
    }

    /**
     * ニュース詳細（※個別表示）
     *
     * @return View
     */
    public function action_info()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'info.tpl');

        $id = $this->param('id');
        if (empty($id) or ! is_numeric($id))
        {
            return Response::redirect('excpetion/404');
        }

        $news = self::$_sponsor->contents()->news_info()->model_for_info($id);
        if (is_null($news))
        {
            return Response::redirect('excpetion/404');
        }

        $view->set('news',   $news);
        return $view;
    }
}
