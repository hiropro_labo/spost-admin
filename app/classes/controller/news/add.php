<?php
/**
 * NEWS: ニュース新規作成
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_News_Add extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'news/add/';
    const POST_URI_UPDATE_EXE = 'menu/item/add/exe';

    /**
    * @var ログイン済ページフラグ
    */
    protected static $_logined_page = true;

    /**
    * @var カレントページ（※UI操作に使用)
    */
    protected static $_current_page = 'news';

    /**
    * コントローラ前処理
    */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('news');
    }

    /**
    * ニュース新規作成：入力画面/確認画面
    *
    * @return View
    */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $fieldset = $this->get_fieldset();
        $news = self::$_sponsor->contents()->news_info()->model_new();
        $news->analyze_reserved_at();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $filedset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));

                $file_upload = new \Support\File_Upload(self::get_user_id(), 'news');
                if ($file_upload->upload_file_exists())
                {
                    try
                    {
                        // 画像一時保存
                        $file_upload->save_tmp_file();
                    }
                    catch (\Exception $e)
                    {
                        Log::error($e->getMessage());
                        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                        $view->set_filename('exception/503');
                        return $view;
                     }
                     $view->set('img_upload_flg', true);
                }
                else
                {
                    $file_upload->remove_tmp_files();
                    $view->set('img_upload_flg', false);
                }
            }
        }
        else
        {
            $fieldset->populate($news);
        }

        $this->set_view_js($view, Asset::js('news/form.js'));
        $this->set_view_js($view, Asset::js('form/disable.js'));

        $view->set('news',      $news);
        $view->set('fieldset',  $fieldset);
        return $view;
    }

    /**
    * ニュース新規作成：更新処理
    *
    * @return View
    */
    public function action_exe()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');
        $fieldset = $this->get_fieldset();
        $valid    = $fieldset->validation();
        $news     = self::$_sponsor->contents()->news_info()->model_new();

        if ($valid->run())
        {
            $fields = $fieldset->validated();
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'news');

            try
            {
                DB::start_transaction();

                $tmp_file_name = '';
                $tmp_files = $file_upload->get_tmp_files();
                if ( ! empty($tmp_files))
                {
                    $tmp_file_name = basename(array_shift($tmp_files));
                }

                if ( ! empty($tmp_file_name))
                {
                    $file_upload->save_file($tmp_file_name);
                }

                // 予約配信日時：データ整形
                $year  = $fields['reserved_at_year'];
                $month = sprintf("%02d", $fields['reserved_at_month']);
                $day   = sprintf("%02d", $fields['reserved_at_day']);
                $hour  = $fields['reserved_at_hour'];
                $min   = $fields['reserved_at_min'];

                // DBレコード登録
                $news->title          = $fields['title'];
                $news->body           = $fields['body'];
                $news->tmp_file_name  = '';
                $news->file_name      = $file_upload->file_name($tmp_file_name);
                $news->target         = $fields['target'];
                $news->reserved_at    = "{$year}-{$month}-{$day} {$hour}:{$min}";
                if ( ! $news->save(false))
                {
                  throw new Exception('news registration error.');
                }

                DB::commit_transaction();
            }
            catch (\Exception $e)
            {
                DB::rollback_transaction();
                Log::error($e->getMessage());
                $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                $view->set_filename('exception/503');
                return $view;
            }
        }
        else
        {
            Log::error(__METHOD__.'news update error: '.$e->getMessage());
            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

        return $view;
    }

    /**
    * Fieldset取得: 登録用
    *
    * @return Fuel\Core\Fieldset
    */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // タイトル
        $fieldset->add('title', 'タイトル')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 本文
        $fieldset->add('body', '本文')
            ->set_type('textarea')
            ->add_rule('required');

        // 配信対象
        $opt = array(1 => '球団アプリユーザーへも配信', 2 => '球団アプリユーザーへは配信しない');
        $fieldset->add('target', '配信対象',array('type' => 'checkbox', 'options' => $opt));

        // 配信予約日時：年
        $fieldset->add('reserved_at_year', '試合開催日', array('options' => $this->opt_year(), 'type' => 'select', 'class' => 'w_90'))
            ->add_rule('required')
            ->add_rule('valid_string', array('numeric', 'utf8'));

        // 配信予約日時：月
        $fieldset->add('reserved_at_month', '試合開催日', array('options' => $this->opt_month(), 'type' => 'select', 'class' => 'w_60'))
            ->add_rule('required')
            ->add_rule('valid_string', array('numeric', 'utf8'));

        // 配信予約日時：日
        $fieldset->add('reserved_at_day', '試合開催日', array('options' => $this->opt_day(), 'type' => 'select', 'class' => 'w_60'))
            ->add_rule('required')
            ->add_rule('valid_string', array('numeric', 'utf8'));

        // 配信予約日時：時
        $fieldset->add('reserved_at_hour', '試合開始時刻', array('options' => $this->opt_hour(), 'type' => 'select', 'class' => 'w_60'))
            ->add_rule('required')
            ->add_rule('valid_string', array('numeric', 'utf8'));

        // 配信予約日時：分
        $fieldset->add('reserved_at_min', '試合開始時刻', array('options' => $this->opt_min(), 'type' => 'select', 'class' => 'w_60'))
            ->add_rule('required')
            ->add_rule('valid_string', array('numeric', 'utf8'));

        return $fieldset;
    }

    private function opt_year()
    {
        $opt = array();
        for ($i=2013; $i < 2020; $i++)
        {
            $opt[$i] = $i;
        }
        return $opt;
    }

    private function opt_month()
    {
        $opt = array();
        for ($i=1; $i < 13; $i++)
        {
            $opt[$i] = $i;
        }
        return $opt;
    }

    private function opt_day()
    {
        $opt = array();
        for ($i=1; $i < 32; $i++)
        {
            $opt[$i] = $i;
        }
        return $opt;
    }

    private function opt_hour()
    {
        $opt = array();
        for ($i=0; $i < 25; $i++)
        {
            $hour = sprintf("%02d", $i);
            $opt[$hour] = $hour;
        }
        return $opt;
    }

    private function opt_min()
    {
        $opt = array();
        for ($i=0; $i < 60; $i++)
        {
            $min = sprintf("%02d", $i);
            $opt[$min] = $min;
        }
        return $opt;
    }

}
