<?php
/**
 * URL: URL編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Url_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'url/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'url';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('url');
    }

    /**
     * カテゴリー編集：TOP
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $model = self::$_sponsor->contents()->url_info()->model();

        $view->set('model', $model);
        return $view;
    }
}
