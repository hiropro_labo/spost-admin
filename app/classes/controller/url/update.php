<?php
/**
 * URL: URL編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Url_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'url/update/';
    const POST_URI_UPDATE_EXE = 'url/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'url';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('url');
    }

    /**
     * カテゴリー編集：TOP
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $model = self::$_sponsor->contents()->url_info()->model();
        if (is_null($model))
        {
            self::$_sponsor->contents()->url_info()->model_new();
        }
        $fieldset = $this->get_fieldset();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $filedset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));
            }
        }
        else
        {
            $fieldset->populate($model);
        }

        $view->set('model',    $model);
        $view->set('fieldset', $fieldset);
        return $view;
    }

    /**
     * カテゴリー編集：更新処理
     */
    public function action_exe()
    {
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

        $model = self::$_sponsor->contents()->url_info()->model();
        if (is_null($model))
        {
            $model = self::$_sponsor->contents()->url_info()->model_new();
        }

	    $fieldset = $this->get_fieldset();
	    $valid    = $fieldset->validation();

	    if ($valid->run())
	    {
	        $fields = $fieldset->validated();

	        try
	        {
	            DB::start_transaction();

	            $model->url1 = $fields['url1'];
	            $model->url2 = $fields['url2'];
	            $model->url3 = $fields['url3'];
	            $model->url_title1 = $fields['url_title1'];
	            $model->url_title2 = $fields['url_title2'];
	            $model->url_title3 = $fields['url_title3'];
                if ( ! $model->save(false))
	            {
	                throw new Exception('update urls failed.');
	            }

	            DB::commit_transaction();
	        }
	        catch (Exception $e)
	        {
	            DB::rollback_transaction();
	            Log::error(__METHOD__.'urls update error: '.$e->getMessage());
	            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	            $view->set_filename('exception/503');
	            return $view;
	        }
	    }
	    else
	    {
	        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

	    $view->set('model', $model);
	    return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // URL1
        $fieldset->add('url1', 'URL1')
            ->add_rule('valid_url');

        // URL2
        $fieldset->add('url2', 'URL1')
            ->add_rule('valid_url');

        // URL3
        $fieldset->add('url3', 'URL1')
            ->add_rule('valid_url');

        // URLタイトル1
        $fieldset->add('url_title1', 'タイトル１')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '8');

        // URLタイトル2
        $fieldset->add('url_title2', 'タイトル２')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '8');

        // URLタイトル3
        $fieldset->add('url_title3', 'タイトル３')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '8');


        return $fieldset;
    }
}
