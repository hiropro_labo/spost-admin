<?php
/**
 * COUPON: クーポン情報更新
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Coupon_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'coupon/update/';
    const POST_URI_UPDATE_EXE = 'coupon/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'coupon';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('coupon');
    }

    /**
	 * クーポン情報更新
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

	    $c_id = $this->param('c_id');
        if (empty($c_id) or ! is_numeric($c_id))
        {
            return Response::redirect('excpetion/404');
        }

        $coupon = self::$_sponsor->contents()->coupon()->model_by_id($c_id);
        if (is_null($coupon))
        {
            Response::redirect('exception/404');
        }

        $fieldset = $this->get_fieldset();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $filedset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));
            }
        }
        else
        {
            $fieldset->populate($coupon);
            $start_datetime = $coupon->datetime_to_array($coupon->start_datetime, 'start_');
            $end_datetime   = $coupon->datetime_to_array($coupon->end_datetime,   'end_');
            $fieldset->populate($start_datetime);
            $fieldset->populate($end_datetime);
        }

        $view->set('coupon',    $coupon);
        $view->set('fieldset',  $fieldset);
        return $view;
	}

	/**
	 * クーポン情報：更新処理(※DB更新処理)
	 *
	 * @return unknown
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

		$c_id = $this->param('c_id');
        if (empty($c_id) or ! is_numeric($c_id))
        {
            return Response::redirect('excpetion/404');
        }

        $coupon = self::$_sponsor->contents()->coupon()->model_by_id($c_id);
        if (is_null($coupon))
        {
            Response::redirect('exception/404');
        }

	    $fieldset = $this->get_fieldset();
	    $valid    = $fieldset->validation();

	    if ($valid->run())
	    {
	        $fields = $fieldset->validated();

            try
            {
                DB::start_transaction();

	            $coupon->title  = $fields['title'];
	            $coupon->body   = $fields['body'];
	            $coupon->policy = $fields['policy'];
                $coupon->start_datetime = sprintf("%d-%d-%d 00:00:00", $fields['start_year'], $fields['start_month'], $fields['start_day']);
                $coupon->end_datetime   = sprintf("%d-%d-%d 00:00:00", $fields['end_year'],   $fields['end_month'],   $fields['end_day']);
                $coupon->term_flg       = $fields['term_flg'];
                if ( ! $coupon->save(false))
                {
                    throw new Exception('coupon registration error.');
                }

    	        DB::commit_transaction();
            }
            catch (Exception $e)
            {
                Debug::dump($e->getMessage());
                DB::rollback_transaction();
                Log::error(__METHOD__.'coupon update error: '.$e->getMessage());
                $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                $view->set_filename('exception/503');
                return $view;
            }
	    }
	    else
	    {
	        Log::error(__METHOD__.'coupon update error: '.$e->getMessage());
	        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

	    $view->set('coupon', $coupon);

	    return $view;
	}

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // タイトル
        $fieldset->add('title', 'タイトル')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // クーポン内容
        $fieldset->add('body', 'クーポン内容')
            ->set_type('textarea')
            ->add_rule('required')
            ->add_rule('max_length', '1000');

        // 利用条件
        $fieldset->add('policy', '利用条件')
            ->set_type('textarea')
            ->add_rule('max_length', '1000');

        // 利用期間の利用有無
        $opt = array('0' => '指定しない', '1' => '指定する');
        $fieldset->add('term_flg', '利用期間の指定',
            array('options' => $opt, 'type' => 'radio'))
            ->add_rule('required');

        // 利用期間: 開始日
        $opt_year  = $this->get_year();
        $fieldset->add('start_year', '年', array('options' => $opt_year, 'type' => 'select', 'class' => 'w_90'))
            ->add_rule('valid_string', array('numeric'));
        $opt_month = $this->get_month();
        $fieldset->add('start_month', '月', array('options' => $opt_month, 'type' => 'select', 'class' => 'w_60'))
            ->add_rule('valid_string', array('numeric'));
        $opt_day   = $this->get_day();
        $fieldset->add('start_day', '日', array('options' => $opt_day, 'type' => 'select', 'class' => 'w_60'))
            ->add_rule('valid_string', array('numeric'));

        // 利用期間: 終了日
        $opt_year  = $this->get_year();
        $fieldset->add('end_year', '年', array('options' => $opt_year, 'type' => 'select', 'class' => 'w_90'))
            ->add_rule('valid_string', array('numeric'));
        $opt_month = $this->get_month();
        $fieldset->add('end_month', '月', array('options' => $opt_month, 'type' => 'select', 'class' => 'w_60'))
            ->add_rule('valid_string', array('numeric'));
        $opt_day   = $this->get_day();
        $fieldset->add('end_day', '日', array('options' => $opt_day, 'type' => 'select', 'class' => 'w_60'))
            ->add_rule('valid_string', array('numeric'));

        return $fieldset;
    }

    private function get_year()
    {
        $list = array();
        $year = intval(date('Y'));
        $max  = $year + 3;
        for ($i = $year; $i < $max; $i++)
        {
            $list[(string)$i] = $i;
        }
        return $list;
    }

    private function get_month()
    {
        $list = array();
        for ($i = 1; $i < 13; $i++)
        {
        $list[(string)$i] = $i;
        }
        return $list;
    }

    private function get_day()
    {
        $list = array();
        for ($i = 1; $i < 32; $i++)
        {
        $list[(string)$i] = $i;
        }
        return $list;
    }
}