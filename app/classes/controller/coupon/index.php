<?php
/**
 * COUPON: 一覧表示
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Coupon_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'coupon/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'coupon';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('coupon');
    }

    /**
     * クーポン表示
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $list = self::$_sponsor->contents()->coupon()->list_order_by_position();

        $view->set('list',   $list);
        return $view;
    }
}
