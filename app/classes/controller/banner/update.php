<?php
/**
 * BANNER: バナー編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Banner_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'banner/update/';
    const POST_URI_UPDATE_EXE = 'banner/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'banner';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('banner');
    }

    /**
     * カテゴリー編集：TOP
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $c_id = $this->param('c_id');
        if (empty($c_id) or ! is_numeric($c_id))
        {
            return Response::redirect('excpetion/404');
        }

        $model = self::$_sponsor->contents()->image_banner()->model_by_id($c_id);
        if (is_null($model))
        {
            return Response::redirect('excpetion/404');
        }
        $fieldset = $this->get_fieldset();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $filedset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));

                $file_upload = new \Support\File_Upload(self::get_user_id(), 'banner');
                if ($file_upload->upload_file_exists())
                {
                    try
                    {
                        DB::start_transaction();

                        $file_upload->save_tmp_file();

                        $model->tmp_file_name  = $file_upload->tmp_file_name();
                        if ( ! $model->save(false))
                        {
                            throw new Exception('banner registration error.');
                        }

                        DB::commit_transaction();

                    }
                    catch (\Exception $e)
                    {
                        Log::error($e->getMessage());
                        DB::rollback_transaction();
                        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                        $view->set_filename('exception/503');
                        return $view;
                    }

                    $view->set('img_upload_flg', true);
                }
                else
                {
                    $view->set('img_upload_flg', false);

                    if (empty($model->file_name))
                    {
                        $view->set('message', 'バナー登録の際には画像を必ず指定してください');
                        $view->set_filename('exception/503');
                        return $view;
                    }

                    try
                    {
                        DB::start_transaction();

                        $model->tmp_file_name = '';
                        if ( ! $model->save(false))
                        {
                            throw new Exception('banner registration error.');
                        }
                        DB::commit_transaction();

                    }
                    catch (\Exception $e)
                    {
                        DB::rollback_transaction();
                        Log::error(__METHOD__.'banner registration error: '.$e->getMessage());
                        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                        $view->set_filename('exception/503');
                        return $view;
                    }
                }
            }
            else
            {
                $fieldset->populate($model);
            }
        }
        else
        {
            $fieldset->populate($model);
        }

        $view->set('model',    $model);
        $view->set('c_id',     $c_id);
        $view->set('fieldset', $fieldset);
        return $view;
    }

    /**
     * カテゴリー編集：更新処理
     */
    public function action_exe()
    {
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

        $c_id = $this->param('c_id');
        if (empty($c_id) or ! is_numeric($c_id))
        {
            return Response::redirect('excpetion/404');
        }

        $model = self::$_sponsor->contents()->image_banner()->model_by_id($c_id);
	    if (is_null($model))
	    {
	        return Response::redirect('excpetion/404');
	    }

	    $fieldset = $this->get_fieldset();
	    $valid    = $fieldset->validation();

	    if ($valid->run())
	    {
	        $fields = $fieldset->validated();
	        $file_upload = new \Support\File_Upload(self::get_user_id(), 'banner');

	        try
	        {
	            DB::start_transaction();

	            if ( ! empty($model->tmp_file_name))
	            {
	                $file_upload->save_file($model->tmp_file_name);
	                $model->file_name = $file_upload->file_name($model->tmp_file_name);
	            }
	            $model->url             = $fields['url'];
	            $model->tmp_file_name   = '';
	            $model->enable          = $fields['enable'];
                if ( ! $model->save(false))
	            {
	                throw new Exception('banner registration error.');
	            }

    	        // キャッシュクリア
    	        //static::support()->clear_cache();

	            DB::commit_transaction();
	        }
	        catch (Exception $e)
	        {
	            DB::rollback_transaction();
	            Log::error(__METHOD__.'banner update error: '.$e->getMessage());
	            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	            $view->set_filename('exception/503');
	            return $view;
	        }
	    }
	    else
	    {
	        Log::error(__METHOD__.'banner update error: '.$e->getMessage());
	        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

	    $view->set('model', $model);
	    return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // URL
        $fieldset->add('url', 'url')
            ->add_rule('valid_url');

        // 表示
        $opt = array('0' => '非表示', '1' => '表示');
        $fieldset->add('enable', '表示設定',
            array('options' => $opt, 'type' => 'radio'))
            ->add_rule('required');

        return $fieldset;
    }
}
