<?php
/**
 * TEAM：チアメンバー削除
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Banner_Del extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'banner/del/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'banner';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('banner');
    }

    /**
     * カテゴリー削除：TOP
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $c_id = $this->param('c_id');
        if (empty($c_id) or ! is_numeric($c_id))
        {
            return Response::redirect('excpetion/404');
        }

        $model = self::$_sponsor->contents()->image_banner()->model_by_id($c_id);
        if (is_null($model))
        {
            return Response::redirect('exception/404');
        }
        $fieldset = $this->get_fieldset();
        $fieldset->populate($model);

        $view->set('model',    $model);
        $view->set('fieldset', $fieldset);
        return $view;
    }

    /**
     * カテゴリー削除
     */
    public function action_exe()
    {
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $this->check_only_post();

        $c_id = $this->param('c_id');
        if (empty($c_id) or ! is_numeric($c_id))
        {
            return Response::redirect('excpetion/404');
        }

        $model = self::$_sponsor->contents()->image_banner()->model_by_id($c_id);
        if (is_null($model))
        {
            $view->set('message', 'チアメンバー削除の処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

        try
        {
            DB::start_transaction();

            // 画像ファイル削除
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'banner');
            $file_upload->delete_file($model->file_name);

            // DBレコード削除
            $model->delete();

            // ポジション再セット
            self::$_sponsor->contents()->image_banner()->recreate_position();

	        DB::commit_transaction();
        }
        catch (Exception $e)
        {
            DB::rollback_transaction();
            Log::error(__METHOD__.'banner delete error: '.$e->getMessage());
            $view->set('message', 'チアメンバー削除の処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

	    return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // URL
        $fieldset->add('url', 'url')
            ->add_rule('valid_url');

        // 表示
        $opt = array('0' => '非表示', '1' => '表示');
        $fieldset->add('enable', '表示設定',
            array('options' => $opt, 'type' => 'radio'))
            ->add_rule('required');

        return $fieldset;
    }
}
