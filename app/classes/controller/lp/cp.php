<?php
/**
 * LandingPage Campaign
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Lp_Cp extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'lp/cp/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'lp';

    /**
     * POP Index
     *
     * @access  public
     * @return  Response
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        // $this->set_view_css($view, Asset::css('path/lp/cp.css'));

        $client = Model_User_Client::find_by_pk(self::get_user_id());

        $view->set('client', $client);
        return $view;
    }

    /**
     * POP Index
     *
     * @access  public
     * @return  Response
     */
    public function action_bank()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'bank.tpl');

        $client = Model_User_Client::find_by_pk(self::get_user_id());

        // 購入可能商品アイテム取得
        $item = static::$_shop->get_sale_item();
        if (is_null($item))
        {
            Response::redirect('exception/404');
        }

        $view->set('item',   $item);
        $view->set('client', $client);
        return $view;
    }
}

