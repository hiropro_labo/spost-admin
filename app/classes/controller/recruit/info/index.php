<?php
/**
 * RECRUIT: 採用情報の編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Recruit_Info_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'recruit/info/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'recruit';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('recruit');
    }

    /**
     * TOPページ
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $c_id = $this->param('c_id');
        if (empty($c_id) or ! is_numeric($c_id))
        {
            return Response::redirect('excpetion/404');
        }

        $title = self::$_sponsor->contents()->recruit_title()->model_by_id($c_id);
        if (is_null($title))
        {
            Response::redirect('exception/404');
        }

        $list = self::$_sponsor->contents()->recruit_info()->list_info($c_id);

        $this->set_view_css($view, Asset::css('path/menu.css'));

        $view->set('title', $title);
        $view->set('list',  $list);
        return $view;
    }
}
