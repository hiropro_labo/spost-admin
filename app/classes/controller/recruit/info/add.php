<?php
use Fuel\Core\Controller_Rest;

/**
 * Recruit: 非同期更新#コンテンツ追加
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Recruit_Info_Add extends Basecontroller_Rest
{
    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
	    $data = array(true, "");

	    try
	    {
	        DB::start_transaction();

	        $p_id  = Input::json('p_id');
	        $title = Input::json('title');
            $body  = Input::json('body');

            $this->check_p_id($p_id);
            $this->check_title($title);
            $this->check_body($body);

            $model = self::$_sponsor->contents()->recruit_info()->model_new();

            $model->parent_id = $p_id;
            $model->title     = $title;
            $model->body      = $body;
            if ( ! $model->save(false))
            {
                throw new Exception("更新データの保存に失敗しました");
            }

	        DB::commit_transaction();
	    }
	    catch (\Exception $e)
	    {
            DB::rollback_transaction();
            Log::error("company data regist failed.   ".$e->getMessage());
            $data = array(false, $e->getMessage());
	    }

	    $json = json_encode($data);

	    $this->response->set_header('Content-Type', 'application/json; charset=utf-8');
	    return $this->response->body($json);
	}

	/**
	 * 入力値バリデーション
	 *
	 * @param string $title
	 * @throws Exception
	 */
	private function check_p_id($str)
	{
	    if (empty($str))
	    {
	        throw new Exception("不正なアクセスです");
	    }

	    if ( ! is_numeric($str))
	    {
            throw new Exception("不正なアクセスです");
	    }

	    if ( ! self::$_sponsor->contents()->recruit_info()->is_exists_parent_id($str))
	    {
	        throw new Exception("不正なアクセスです");
	    }
	}

	/**
	 * 入力値バリデーション
	 *
	 * @param string $title
	 * @throws Exception
	 */
	private function check_title($str)
	{
	    if (empty($str))
	    {
	        throw new Exception("タイトルが入力されていません");
	    }

	    if (mb_strlen($str, 'UTF-8') > 255)
	    {
            throw new Exception("タイトルは255文字以下で入力してください");
	    }
	}

	/**
	 * 入力値バリデーション: 本文
	 *
	 * @param string $body
	 * @throws Exception
	 */
	private function check_body($str)
	{
	    if (empty($str))
	    {
	        throw new Exception("本文が入力されていません");
	    }

	    if (mb_strlen($str, 'UTF-8') > 1000)
	    {
            throw new Exception("本文は1000文字以下で入力してください");
	    }
	}
}
