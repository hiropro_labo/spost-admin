<?php
/**
 * RECRUIT: 採用情報の新規追加
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Recruit_Title_Add extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'recruit/title/add/';
    const POST_URI_UPDATE_EXE = 'recruit/title/add/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'recruit';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('recruit');
    }

    /**
     * カテゴリー新規作成：TOP
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $fieldset = $this->get_fieldset();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();

            if ($valid->run())
            {
                $filedset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));
                $view->set('selected_pref', $this->get_pref_name($fieldset->validated('pref')));
            }
        }

        $view->set('fieldset', $fieldset);
        return $view;
    }

    /**
     * カテゴリー新規作成：登録処理
     */
    public function action_exe()
    {
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $fieldset = $this->get_fieldset();
	    $valid    = $fieldset->validation();

	    if ( ! $valid->run())
	    {
	        Response::redirect('exception/503');
	    }

	    $title = self::$_sponsor->contents()->recruit_title()->model_new();
	    $fields = $fieldset->validated();

	    try
	    {
            DB::start_transaction();

            $title->title  = $fields['title'];
            $title->enable = $fields['enable'];
            $title->position = $title->new_position(self::get_user_id());
            if ( ! $result = $title->save(false))
            {
                throw new Exception('recruit title registration faield.');
            }

            DB::commit_transaction();
	    }
	    catch (Exception $e)
	    {
	        DB::rollback_transaction();
	        Log::error($e->getMessage());
	        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

        $view->set('title', $title);
	    return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // タイトル
        $fieldset->add('title', 'タイトル')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');


        // 表示
        $opt = array('0' => '非表示', '1' => '表示');
        $fieldset->add('enable', '表示設定',
            array('options' => $opt, 'type' => 'radio'))
            ->add_rule('required');

        return $fieldset;
    }

}
