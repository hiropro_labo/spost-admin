<?php
/**
 * SEGMENTS: 画像更新 Message
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Recruit_Title_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'recruit/title/update/';
    const POST_URI_UPDATE_EXE = 'recruit/title/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'recruit';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('recruit');
    }

    /**
	 * TOP画像：更新
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $c_id = $this->param('c_id');
        if (empty($c_id))
        {
            Response::redirect('exception/503');
        }

        $model = self::$_sponsor->contents()->recruit_title()->model_by_id($c_id);
        if (is_null($model))
        {
            Response::redirect('exception/503');
        }

        $fieldset = $this->get_fieldset();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $filedset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));
            }
        }
        else
        {
            $fieldset->populate($model);
        }

        $view->set('c_id',      $c_id);
        $view->set('model',     $model);
        $view->set('fieldset',  $fieldset);
        return $view;
    }

	/**
	 * アイコン画像：更新処理
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $c_id = $this->param('c_id');
        if (empty($c_id))
        {
            Response::redirect('exception/503');
        }

        $model = self::$_sponsor->contents()->recruit_title()->model_by_id($c_id);
        if (is_null($model))
        {
            Response::redirect('exception/503');
        }

	    $fieldset = $this->get_fieldset();
	    $valid    = $fieldset->validation();

	    if ($valid->run())
	    {
	        $fields = $fieldset->validated();

            try
            {
                DB::start_transaction();

                $model->title = $fields['title'];
                if ( ! $model->save(false))
                {
                    throw new Exception('recruit title update failed.');
                }

    	        DB::commit_transaction();
            }
            catch (Exception $e)
            {
                Debug::dump($e->getMessage());
                DB::rollback_transaction();
                Log::error(__METHOD__.'recruit title update failed : '.$e->getMessage());
                $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                $view->set_filename('exception/503');
                return $view;
            }
	    }
	    else
	    {
	        Log::error(__METHOD__.'recruit title update failed : '.$e->getMessage());
	        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

	    return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // タイトル
        $fieldset->add('title', 'タイトル')
        ->add_rule('required');

        return $fieldset;
    }

}