<?php
/**
 * Tutorial Step Controller
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Tutorial_Step extends Basecontroller
{
    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
    }

    /**
     * TUTORIAL STEP
     *
     * @access  public
     * @return  Response
     */
    public function action_index()
    {
        $client_id = Input::post('client');
        $step = Input::post('step');

        $this->step_check($client_id, $step);
    }


    /**
     * Tutorial Step Check
     *
     * @access  private
     * @param string Client ID
     * @param string Tutorial Step
     **/
    private function step_check($client_id, $step)
    {
        // チュートリアルの進捗状況のテーブル
        $tutorial_step = Model_Tutorial_Step::find_one_by('client_id', $client_id);

        if(is_null($tutorial_step))
        {
            $tutorial_step = Model_Tutorial_Step::forge();
            $tutorial_step->client_id = $client_id;
        }

        switch($step)
        {
            case 1: $tutorial_step->root = 1;
                break;
            case 2: $tutorial_step->top = 1;
                break;
            case 3: $tutorial_step->top = 2;
                break;
            case 4: $tutorial_step->top = 3;
                break;
            case 5: $tutorial_step->top = 4;
                break;
            case 6: $tutorial_step->coupon = 1;
                break;
            case 7: $tutorial_step->news = 1;
                break;
            case 8: $tutorial_step->menu = 1;
                break;
            case 9: $tutorial_step->menu = 2;
                break;
            case 10: $tutorial_step->menu = 3;
                break;
            case 11: $tutorial_step->store = 1;
                break;
            case 12: $tutorial_step->store = 2;
                break;
            case 13: $tutorial_step->store = 3;
                break;
            case 14: $tutorial_step->store = 4;
                break;
        }

        // チュートリアルテーブルへ書き込み
        try
        {
            DB::start_transaction();
            if ( ! $tutorial_step->save(false))
            {
                throw new Exception('tutorial step registration error.');
            }
            DB::commit_transaction();
        }
        catch (\Exception $e)
        {
            Log::error($e->getMessage());
            DB::rollback_transaction();
        }
    }
}
