<?php
/**
 * Store: アプリカテゴリー選択
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Store_Category_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'store/category/update/';
    const POST_URI_UPDATE_EXE = 'store/category/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'store';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('store');
    }

    /**
	 * TOP画像：更新
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $fieldset = $this->get_fieldset();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $fieldset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));
            }
        }
        else
        {
            $model = self::$_sponsor->contents()->store_info()->model();
            $fieldset->populate($model);
        }

        $this->set_view_js($view, Asset::js('store/preview.js'));

        $view->set('fieldset', $fieldset);
        return $view;
	}

	/**
	 * アイコン画像：更新処理
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $fieldset = $this->get_fieldset();
	    $fieldset->repopulate();
	    $valid    = $fieldset->validation();

	    if ($valid->run())
	    {
            $fields = $fieldset->validated();

            $model = self::$_sponsor->contents()->store_info()->model();
            if (is_null($model))
            {
                $model = self::$_sponsor->contents()->store_info()->model_new();
            }

            $model->category_iphone1 = $fields['category_iphone1'];
            $model->category_iphone2 = $fields['category_iphone2'];
            $model->category_android = $fields['category_android'];

            try
            {
                DB::start_transaction();
                if ( ! $model->save(false))
                {
                    throw new Exception('update error: store info.');
                }
                DB::commit_transaction();
            }
            catch (\Exception $e)
            {
                DB::rollback_transaction();
                Log::error($e->getMessage());
                $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                $view->set_filename('exception/503');
                return $view;
            }

	    }
	    else
	    {
	        Log::error(__METHOD__.'store info update error: '.$e->getMessage());
	        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

	    return $view;
	}

	/**
	 * Fieldset取得
	 */
	private function get_fieldset()
	{
	    Lang::load('validationdesc');

	    $fieldset   = Exfieldset::forge();
	    $validation = $fieldset->validation();
	    $validation->add_callable('ExValidation');
	    $fieldset->validation($validation);

	    // アプリカテゴリー：iPhone１
	    $conf = \Config::load('category', true);
	    $opt_iphone = $conf['iphone'];
	    $fieldset->add('category_iphone1', 'iPhone１',
	        array('options' => $opt_iphone, 'type' => 'select', 'class' => 'w_200'))
	        ->add_rule('required');

	    // アプリカテゴリー：iPhone１
	    $fieldset->add('category_iphone2', 'iPhone１',
	        array('options' => $opt_iphone, 'type' => 'select', 'class' => 'w_200'))
	        ->add_rule('required');

	    // アプリカテゴリー：Android
	    $opt_android = $conf['android'];
	    $fieldset->add('category_android', 'Android',
	        array('options' => $opt_android, 'type' => 'select', 'class' => 'w_200'))
	        ->add_rule('required');

	    return $fieldset;
	}

}