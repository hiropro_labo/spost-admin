<?php
/**
 * Store: アプリアイコン画像更新
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Store_Icon_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'store/icon/update/';
    const POST_URI_UPDATE_EXE = 'store/icon/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'store';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('store');
    }

    /**
	 * TOP画像：更新
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $image_icon = self::$_sponsor->contents()->icon()->model();
        if (is_null($image_icon))
        {
            $image_icon = self::$_sponsor->contents()->icon()->model_new();
        }

        if (Input::method() == 'POST')
        {
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'icon');
            if ($file_upload->upload_file_exists())
            {
                try
                {
                    DB::start_transaction();

                    $file_upload->save_icon_tmp_file();

                    $image_icon->client_id     = self::get_user_id();
                    $image_icon->tmp_file_name = $file_upload->tmp_file_name();
                    if ( ! $image_icon->save(false))
                    {
                        throw new Exception('icon registration error.');
                    }

                    DB::commit_transaction();
                }
                catch (\Exception $e)
                {
                    DB::rollback_transaction();
                    Log::error($e->getMessage());
                    $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                    $view->set_filename('exception/503');
                    return $view;
                }
                $view->set('img_upload_flg', true);
            }
            else
            {
                $view->set('img_upload_flg', false);
                if (empty($image_icon->file_name))
                {
                    $view->set('message', '画像を指定してください');
                    $view->set_filename('exception/503');
                    return $view;
                }
            }

            $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
        }

        $this->set_view_js($view, Asset::js('store/preview.js'));

        $view->set('image_icon', $image_icon);
        return $view;
	}

	/**
	 * アイコン画像：更新処理
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $image_icon = self::$_sponsor->contents()->icon()->model();
        if (is_null($image_icon))
        {
            $image_icon = self::$_sponsor->contents()->icon()->model_new();
        }

	    $file_upload = new \Support\File_Upload(self::get_user_id(), 'icon');

	    try
        {
            DB::start_transaction();

            if ( ! empty($image_icon->tmp_file_name))
            {
                $file_upload->save_file($image_icon->tmp_file_name);
                $image_icon->file_name = $file_upload->file_name($image_icon->tmp_file_name);
                $image_icon->tmp_file_name = '';
            }

            if ( ! $image_icon->save(false))
            {
                throw new Exception('icon registration error.');
            }

            DB::commit_transaction();
        }
        catch (Exception $e)
        {
            DB::rollback_transaction();
            Log::error(__METHOD__.'icon update error: '.$e->getMessage());
            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

	    return $view;
	}
}