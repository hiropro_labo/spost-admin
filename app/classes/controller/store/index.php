<?php
/**
 * アプリ登録時の基本情報
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Store_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'store/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'store';

    /**
     * Storeインスタンス
     * @var Store
     */
    protected static $_store = null;

    /**
     * action前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('store');
        self::$_store = self::$_sponsor->contents()->store();
    }

    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		$view   = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

		$image_icon    = self::$_sponsor->contents()->icon()->get_image();
		$image_appicon = self::$_sponsor->contents()->appicon()->get_image();
		$list_sshot1   = self::$_sponsor->contents()->screen_shot()->get_image_set(self::conf_get('screen_shot1_image_max'));
		$list_sshot2   = self::$_sponsor->contents()->screen_shot_old()->get_image_set(self::conf_get('screen_shot2_image_max'));
		$image_splash  = self::$_sponsor->contents()->splash()->get_image();

        $this->set_view_js($view, Asset::js('store/preview.js'));

        $view->set('image_icon',    $image_icon);
        $view->set('image_appicon', $image_appicon);
        $view->set('list_sshot1',   $list_sshot1);
        $view->set('list_sshot2',   $list_sshot2);
        $view->set('image_splash',  $image_splash);
		return $view;
	}

}
