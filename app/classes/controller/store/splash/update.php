<?php
/**
 * Store: 起動画面用の画像更新
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Store_Splash_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'store/splash/update/';
    const POST_URI_UPDATE_EXE = 'store/splash/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'store';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('store');
    }

    /**
	 * TOP画像：更新
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $image = self::$_sponsor->contents()->splash()->model();
        if (is_null($image))
        {
            $image = self::$_sponsor->contents()->splash()->model_new();
        }

        if (Input::method() == 'POST')
        {
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'splash');
            if ($file_upload->upload_file_exists())
            {
                try
                {
                    DB::start_transaction();
                    $file_upload->save_icon_tmp_file();

                    $image->tmp_file_name = $file_upload->tmp_file_name();
                    if ( ! $image->save(false))
                    {
                        throw new Exception('splash image registration error.');
                    }
                    DB::commit_transaction();
                }
                catch (\Exception $e)
                {
                    Log::error($e->getMessage());
                    DB::rollback_transaction();
                    $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                    $view->set_filename('exception/503');
                    return $view;
                }
                $view->set('img_upload_flg', true);
            }
            else
            {
                $view->set('img_upload_flg', false);
                if (empty($image->file_name))
                {
                    $view->set('message', '画像を指定してください');
                    $view->set_filename('exception/503');
                    return $view;
                }
            }

            $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
        }

        $this->set_view_js($view, Asset::js('store/preview.js'));

        $view->set('image', $image);
        return $view;
	}

	/**
	 * 起動画面用の画像：更新処理
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $image = self::$_sponsor->contents()->splash()->model();
        if (is_null($image))
        {
            $image = self::$_sponsor->contents()->splash()->model_new();
        }

	    $file_upload = new \Support\File_Upload(self::get_user_id(), 'splash');

	    try
        {
            DB::start_transaction();

            if ( ! empty($image->tmp_file_name))
            {
                $file_upload->save_file($image->tmp_file_name);
                $image->file_name = $file_upload->file_name($image->tmp_file_name);
                $image->tmp_file_name = '';
            }

            if ( ! $image->save(false))
            {
                throw new Exception('splash image registration error.');
            }

            // 不要ファイル削除
            $file_upload->cleanup_files($image->file_name);

            DB::commit_transaction();
        }
        catch (Exception $e)
        {
            DB::rollback_transaction();
            Log::error(__METHOD__.'splash image update error: '.$e->getMessage());
            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

	    return $view;
	}

}