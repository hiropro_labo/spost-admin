<?php
/**
 * Store: アプリストア(App Store/Google Play)表示項目の更新
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Store_Text_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'store/text/update/';
    const POST_URI_UPDATE_EXE = 'store/text/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'store';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('store');
    }

    /**
	 * TOP画像：更新
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $fieldset = $this->get_fieldset();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $fieldset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));
            }
        }
        else
        {
            $model = self::$_sponsor->contents()->store_info()->model();
            $fieldset->populate($model);
        }

        $this->set_view_js($view, Asset::js('store/preview.js'));

        $view->set('fieldset', $fieldset);
        return $view;
	}

	/**
	 * アイコン画像：更新処理
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $fieldset = $this->get_fieldset();
	    $fieldset->repopulate();
	    $valid    = $fieldset->validation();

	    if ($valid->run())
	    {
            $fields = $fieldset->validated();

            $model = self::$_sponsor->contents()->store_info()->model();
            if (is_null($model))
            {
                $model = self::$_sponsor->contents()->store_info()->model_new();
            }

            $model->app_name      = $fields['app_name'];
            $model->app_disp_name = $fields['app_disp_name'];
            $model->description   = $fields['description'];

            try
            {
                DB::start_transaction();

                if ( ! $model->save(false))
                {
                    throw new Exception('update error: store info.');
                }

                DB::commit_transaction();
            }
            catch (\Exception $e)
            {
                DB::rollback_transaction();
                Log::error($e->getMessage());
                $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                $view->set_filename('exception/503');
                return $view;
            }

	    }
	    else
	    {
	        Log::error(__METHOD__.'store info update error: '.$e->getMessage());
	        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

	    return $view;
	}

	/**
	 * Fieldset取得
	 */
	private function get_fieldset()
	{
	    Lang::load('validationdesc');

	    $fieldset   = Exfieldset::forge();
	    $validation = $fieldset->validation();
	    $validation->add_callable('ExValidation');
	    $fieldset->validation($validation);

	    // アプリ名
	    $fieldset->add('app_name', 'アプリ名')
	        ->add_rule('required')
	        ->add_rule('valid_string', array('utf8'))
	        ->add_rule('max_length', '112');

	    // アプリ内表示名
        $fieldset->add('app_disp_name', '表示名')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '6');

	    // 説明文
	    $fieldset->add('description', '説明文', array('id' => 'form_description'))
	        ->set_type('textarea')
	        ->add_rule('required')
	        ->add_rule('min_length', '100')
	        ->add_rule('max_length', '2000');

	    return $fieldset;
	}

}