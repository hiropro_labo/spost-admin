<?php
/**
 * TOP: TOPページのカルーセル画像操作
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Store_Sshot_Delete extends Basecontroller
{
    const VIEW_FILE_PREFIX     = 'store/sshot/delete/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'store';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('store');
    }

    /**
     * TOPカルーセル画像：削除#確認画面
     * @return unknown
     */
	public function action_index()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

	    $position = $this->param('position');
	    if (empty($position) or ! is_numeric($position))
	    {
	        return Response::redirect('excpetion/404');
	    }

	    $image = self::$_sponsor->contents()->screen_shot()->get_image($position);
	    if (is_null($image))
	    {
	        return Response::redirect('exception/404');
	    }

        $this->set_view_js($view, Asset::js('store/preview.js'));

	    $view->set('position', $position);
	    $view->set('image',    $image);
	    return $view;
	}

	/**
	 * TOPカルーセル画像：削除
	 * @return unknown
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $this->check_only_post();

	    $position = $this->param('position');
	    if (empty($position) or ! is_numeric($position))
	    {
	        return Response::redirect('excpetion/404');
	    }

	    $image = self::$_sponsor->contents()->screen_shot()->get_image($position);
	    if (is_null($image))
	    {
	        return Response::redirect('exception/404');
	    }

	    try
	    {
	        DB::start_transaction();

	        // DBレコード削除
	        $image->delete();
            $image->recreate_position(self::get_user_id());

	        // 画像ファイル削除
    	    $file_upload = new \Support\File_Upload(self::get_user_id(), 'screen_shot1');
    	    $rs = $file_upload->delete_file($image->file_name);

    	    DB::commit_transaction();
	    }
	    catch (\Exception $e)
	    {
	        DB::rollback_transaction();
	        Log::error($e->getMessage());
	        $view->set('message', 'スクリーンショット画像の削除処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

	    return $view;
	}
}