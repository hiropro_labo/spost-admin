<?php
/**
 * アプリストア：スクリーンショット更新
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Store_Sshot2_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'store/sshot2/update/';
    const POST_URI_UPDATE_EXE = 'store/sshot2/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'store';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('store');
    }

    /**
	 * TOP画像：更新
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $position = $this->param('position');
        if (empty($position) or ! is_numeric($position))
        {
            return Response::redirect('excpetion/404');
        }

        $image = self::$_sponsor->contents()->screen_shot_old()->get_image($position);

        if (Input::method() == 'POST')
        {
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'screen_shot2');
            if ($file_upload->upload_file_exists())
            {
                try
                {
                    DB::start_transaction();

                    $file_upload->save_tmp_file();

                    $image->tmp_file_name = $file_upload->tmp_file_name();
                    $image->position      = $position;
                    if ( ! $image->save(false))
                    {
                        throw new Exception('screen shot registration error.');
                    }

                    DB::commit_transaction();
                }
                catch (\Exception $e)
                {
                    Log::error($e->getMessage());
                    DB::rollback_transaction();
                    $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                    $view->set_filename('exception/503');
                    return $view;
                }
                $view->set('img_upload_flg', true);
            }
            else
            {
                $view->set('img_upload_flg', false);
                if (empty($image->file_name))
                {
                    $view->set('message', '画像を指定してください');
                    $view->set_filename('exception/503');
                    return $view;
                }
            }

            $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
        }

        $this->set_view_js($view, Asset::js('store/preview.js'));

        $view->set('image',     $image);
        $view->set('position',  $position);
        return $view;
	}

	/**
	 * TOP画像：更新＃確認画面
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $position = $this->param('position');
	    if (empty($position) or ! is_numeric($position))
	    {
	        return Response::redirect('excpetion/404');
	    }

	    $image = self::$_sponsor->contents()->screen_shot_old()->get_image($position);
	    if (is_null($image))
	    {
	        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

	    $file_upload = new \Support\File_Upload(self::get_user_id(), 'screen_shot2');

	    try
        {
            DB::start_transaction();

            if ( ! empty($image->tmp_file_name))
            {
                $file_upload->save_file($image->tmp_file_name);
                $image->file_name = $file_upload->file_name($image->tmp_file_name);
                $image->tmp_file_name = '';
            }

            if ( ! $image->save(false))
            {
                throw new Exception('screen shot registration error.');
            }

            DB::commit_transaction();
        }
        catch (Exception $e)
        {
            DB::rollback_transaction();
            Log::error(__METHOD__.'screen shot update error: '.$e->getMessage());
            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

	    return $view;
	}
}