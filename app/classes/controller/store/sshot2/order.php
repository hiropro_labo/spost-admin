<?php
/**
 * TOP: TOPページの画像表示順の変更
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Store_Sshot2_Order extends Basecontroller
{

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'store';

    /**
    * スクリーンショット2画像：表示順更新
    *
    * @access  public
    * @return  Response
    */
    public function action_index()
    {
        $positions = Input::post('positions');

            try
        {
            DB::start_transaction();

            $list = array();
            $idx  = 1;

            foreach ($positions as $posi)
            {
                if (is_null($posi) or empty($posi) or ! is_numeric($posi))
                {
                    continue;
                }

                $image = self::$_sponsor->contents()->screen_shot_old()->get_image(intval($posi));
                $image->position = $idx;

                array_push($list, $image);
                $idx++;
            }

            foreach ($list as $rec)
            {
                if ( ! $rec->save(false))
                {
                    throw new Exception('Error: Screen Shot image position change failed.');
                }
            }

            DB::commit_transaction();
        }
        catch (Exception $e)
        {
            DB::rollback_transaction();
            Log::error($e->getMessage());

            return Response::forge(null, 503);
        }
    }
}