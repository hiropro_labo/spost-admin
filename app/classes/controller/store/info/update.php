<?php
/**
 * Store: TEL/MAIL
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Store_Info_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'store/info/update/';
    const POST_URI_UPDATE_EXE = 'store/info/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'store';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('store');
    }

    /**
	 * TOP画像：更新
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $fieldset = $this->get_fieldset();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $fieldset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));
            }
        }
        else
        {
            $model = self::$_sponsor->contents()->store_info()->model();
            $fieldset->populate($model);
        }

        $this->set_view_js($view, Asset::js('store/preview.js'));

        $view->set('fieldset', $fieldset);
        return $view;
	}

	/**
	 * アイコン画像：更新処理
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $fieldset = $this->get_fieldset();
	    $fieldset->repopulate();
	    $valid    = $fieldset->validation();

	    if ($valid->run())
	    {
            $fields = $fieldset->validated();

            $model = self::$_sponsor->contents()->store_info()->model();
            if (is_null($model))
            {
                $model = self::$_sponsor->contents()->store_info()->model_new();
            }

            $model->email        = $fields['email'];
            $model->tel1         = $fields['tel1'];
            $model->tel2         = $fields['tel2'];
            $model->tel3         = $fields['tel3'];

            try
            {
                DB::start_transaction();

                if ( ! $model->save(false))
                {
                    throw new Exception('update error: store info.');
                }

                DB::commit_transaction();
            }
            catch (\Exception $e)
            {
                DB::rollback_transaction();
                Log::error($e->getMessage());
                $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                $view->set_filename('exception/503');
                return $view;
            }

	    }
	    else
	    {
	        Log::error(__METHOD__.'store info update error: '.$e->getMessage());
	        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

	    return $view;
	}

	/**
	 * Fieldset取得
	 */
	private function get_fieldset()
	{
	    Lang::load('validationdesc');

	    $fieldset   = Exfieldset::forge();
	    $validation = $fieldset->validation();
	    $validation->add_callable('ExValidation');
	    $fieldset->validation($validation);

        // メールアドレス
        $fieldset->add('email', 'メールアドレス')
            ->add_rule('valid_email')
            ->add_rule('max_length', '255');

        // TEL
        $fieldset->add('tel1', 'TEL', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '5');
        $fieldset->add('tel2', 'TEL(2)', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '5');
        $fieldset->add('tel3', 'TEL(3)', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '4');

	    return $fieldset;
	}

}