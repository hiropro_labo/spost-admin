<?php
use Fuel\Core\Pagination;

/**
 * TOPICS: トピックス一覧
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Topics_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'topics/';
    const PAGINATION_URL   = 'topics';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'topics';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('topics');
    }

    /**
     * ニュース一覧
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $pager = self::$_sponsor->contents()->topics_info()->list_topics_pager(self::PAGINATION_URL);
        $list  = self::$_sponsor->contents()->topics_info()->list_topics($pager->per_page, $pager->offset);

        $view->set('list',   $list);
        $view->set('pager',  $pager->render(), false);
        return $view;
    }

    /**
     * ニュース詳細（※個別表示）
     *
     * @return View
     */
    public function action_info()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'info.tpl');

        $id = $this->param('id');
        if (empty($id) or ! is_numeric($id))
        {
            return Response::redirect('excpetion/404');
        }

        $topics = self::$_sponsor->contents()->topics_info()->model_for_info($id);
        if (is_null($topics))
        {
            return Response::redirect('excpetion/404');
        }

        $view->set('topics',   $topics);
        return $view;
    }
}
