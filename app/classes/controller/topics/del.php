<?php
/**
 * TOPICS: トピックス削除
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Topics_Del extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'topics/del/';
    const POST_URI_UPDATE_EXE = 'topics/del/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'topics';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('topics');
    }

    /**
     * トピックス削除：入力画面/確認画面
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $id = $this->param('id');
        if (empty($id) or ! is_numeric($id))
        {
            return Response::redirect('excpetion/404');
        }

        $topics = self::$_sponsor->contents()->topics_info()->model_for_delete($id);
        if (is_null($topics))
        {
            return Response::redirect('exception/404');
        }

        $fieldset = $this->get_fieldset();
        $fieldset->populate($topics);

        $view->set('topics',      $topics);
        $view->set('fieldset',  $fieldset);
        return $view;
    }

    /**
     * トピックス削除：更新処理
     *
     * @return View
     */
    public function action_exe()
    {
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $id = $this->param('id');
	    if (empty($id) or ! is_numeric($id))
	    {
	        return Response::redirect('excpetion/404');
	    }

        $topics = self::$_sponsor->contents()->topics_info()->model_for_delete($id);
        if (is_null($topics))
        {
            return Response::redirect('exception/404');
        }

	    $conf_image = \Config::load('original_image', true);
	    $save_dir   = $conf_image['save_dir']['topics'].self::get_user_id().DS;
	    $file_name  = $topics->file_name;

	    try
	    {
	        DB::start_transaction();

	        // 画像ファイル削除
	        $file_upload = new \Support\File_Upload(self::get_user_id(), 'topics');
	        $file_upload->delete_file($topics->file_name, true);
	        // DBレコード削除
	        $topics->delete();

	        DB::commit_transaction();
	    }
	    catch (Exception $e)
	    {
	        Debug::dump($e->getMessage());
            DB::rollback_transaction();
            Log::error(__METHOD__.'topics delete error: '.$e->getMessage());
            $view->set('message', 'トピックス削除の処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
	    }

	    return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // タイトル
        $fieldset->add('title', 'タイトル')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 本文
        $fieldset->add('body', '本文')
            ->set_type('textarea')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'));

        return $fieldset;
    }
}
