<?php
/**
 * TOPICS: ニュース新規作成
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Topics_Add extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'topics/add/';
    const POST_URI_UPDATE_EXE = 'menu/item/add/exe';

    /**
    * @var ログイン済ページフラグ
    */
    protected static $_logined_page = true;

    /**
    * @var カレントページ（※UI操作に使用)
    */
    protected static $_current_page = 'topics';

    /**
    * コントローラ前処理
    */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('topics');
    }

    /**
    * ニュース新規作成：入力画面/確認画面
    *
    * @return View
    */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $fieldset = $this->get_fieldset();
        $topics     = self::$_sponsor->contents()->topics_info()->model_new();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $filedset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));

                $file_upload = new \Support\File_Upload(self::get_user_id(), 'topics');
                if ($file_upload->upload_file_exists())
                {
                    try
                    {
                        // 画像一時保存
                        $file_upload->save_tmp_file();
                    }
                    catch (\Exception $e)
                    {
                        Log::error($e->getMessage());
                        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                        $view->set_filename('exception/503');
                        return $view;
                     }
                     $view->set('img_upload_flg', true);
                }
                else
                {
                    $file_upload->remove_tmp_files();
                    $view->set('img_upload_flg', false);
                }
            }
        }

        $this->set_view_js($view, Asset::js('news/form.js'));
        $this->set_view_js($view, Asset::js('form/disable.js'));

        $view->set('topics',      $topics);
        $view->set('fieldset',  $fieldset);
        return $view;
    }

    /**
    * ニュース新規作成：更新処理
    *
    * @return View
    */
    public function action_exe()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');
        $fieldset = $this->get_fieldset();
        $valid    = $fieldset->validation();
        $topics     = self::$_sponsor->contents()->topics_info()->model_new();

        if ($valid->run())
        {
            $fields = $fieldset->validated();
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'topics');

            try
            {
                DB::start_transaction();

                $tmp_file_name = '';
                $tmp_files = $file_upload->get_tmp_files();
                if ( ! empty($tmp_files))
                {
                    $tmp_file_name = basename(array_shift($tmp_files));
                }

                if ( ! empty($tmp_file_name))
                {
                    $file_upload->save_file($tmp_file_name);
                }

                // DBレコード登録
                $topics->title          = $fields['title'];
                $topics->body           = $fields['body'];
                $topics->tmp_file_name  = '';
                $topics->file_name      = $file_upload->file_name($tmp_file_name);
                if ( ! $topics->save(false))
                {
                  throw new Exception('topics registration error.');
                }

                DB::commit_transaction();
            }
            catch (\Exception $e)
            {
                DB::rollback_transaction();
                Log::error($e->getMessage());
                $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                $view->set_filename('exception/503');
                return $view;
            }
        }
        else
        {
            Log::error(__METHOD__.'topics update error: '.$e->getMessage());
            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

        return $view;
    }

    /**
    * Fieldset取得: 登録用
    *
    * @return Fuel\Core\Fieldset
    */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // タイトル
        $fieldset->add('title', 'タイトル')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 本文
        $fieldset->add('body', '本文')
            ->set_type('textarea')
            ->add_rule('required');

        return $fieldset;
    }
}
