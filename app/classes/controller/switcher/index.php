<?php
/**
 * アプリ各種機能の利用設定
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Switcher_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'switcher/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'switcher';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        static::$_support = \Support\Api\Switcher::instance(self::get_user_id());

        Lang::load('navigation');
        Lang::load('switcher');
    }

    /**
     * 利用設定の確認
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $category = self::$_sponsor->contents()->theme()->model()->category;

        $view->set('category', $category);
        return $view;
    }

    /**
     * 機能利用の設定更新：利用開始
     *
     * @return view
     */
    public function action_update()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'dummy.tpl');
        $key = $this->param('key');
        if (is_null($key) or empty($key))
        {
            throw new Exception('key is null.');
        }

        $switcher = self::$_sponsor->contents()->switcher();
        if ($switcher->is_active($key)) {
            if( ! $switcher->inactive($key))
            {
                throw new Exception('switcher update failed.');
            }
        }
        else
        {
            if( ! $switcher->active($key))
            {
                throw new Exception('switcher update failed.');
            }
        }

        //Response::redirect('/switcher');
    }

    /**
     * 機能利用の設定更新：利用停止
     *
     * @return view
     */
    public function action_off()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'dummy.tpl');

        $key = $this->param('key');
        if (is_null($key) or empty($key))
        {
            throw new Exception('key is null.');
        }

        $switcher = self::$_sponsor->contents()->switcher();
        if( ! $switcher->inactive($key))
        {
            throw new Exception('switcher update failed.');
        }

        return $view;
    }
}
