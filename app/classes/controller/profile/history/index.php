<?php
/**
 * PROFILE: 略歴の編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Profile_History_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'profile/history/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'profile';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('profile');
    }

    /**
     * TOPページ
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $list = self::$_sponsor->contents()->profile_history()->model_list();

        $this->set_view_css($view, Asset::css('path/menu.css'));

        $view->set('list',  $list);
        return $view;
    }
}
