<?php
use Fuel\Core\Controller_Rest;

/**
 * PROFILE: 非同期更新#コンテンツ追加
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Profile_History_Add extends Basecontroller_Rest
{
    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
	    $data = array(true, "");

	    try
	    {
	        DB::start_transaction();

	        $title = Input::json('title');
            $body  = Input::json('body');

            $this->check_title($title);
            $this->check_body($body);

            $model = self::$_sponsor->contents()->profile_history()->model_new();

            $model->title     = $title;
            $model->body      = $body;
            if ( ! $model->save(false))
            {
                throw new Exception("更新データの保存に失敗しました");
            }

	        DB::commit_transaction();
	    }
	    catch (\Exception $e)
	    {
            DB::rollback_transaction();
            Log::error("profile history data regist failed.   ".$e->getMessage());
            $data = array(false, $e->getMessage());
	    }

	    $json = json_encode($data);

	    $this->response->set_header('Content-Type', 'application/json; charset=utf-8');
	    return $this->response->body($json);
	}

	/**
	 * 入力値バリデーション
	 *
	 * @param string $title
	 * @throws Exception
	 */
	private function check_title($str)
	{
	    if (empty($str))
	    {
	        throw new Exception("タイトルが入力されていません");
	    }

	    if (mb_strlen($str, 'UTF-8') > 255)
	    {
            throw new Exception("タイトルは255文字以下で入力してください");
	    }
	}

	/**
	 * 入力値バリデーション: 本文
	 *
	 * @param string $body
	 * @throws Exception
	 */
	private function check_body($str)
	{
	    if (empty($str))
	    {
	        throw new Exception("本文が入力されていません");
	    }

	    if (mb_strlen($str, 'UTF-8') > 1000)
	    {
            throw new Exception("本文は1000文字以下で入力してください");
	    }
	}
}
