<?php
/**
 * PROFILE: プロフィール編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Profile_Info_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'profile/info/update/';
    const POST_URI_UPDATE_EXE = 'profile/info/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'profile';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('profile');
    }

    /**
     * カテゴリー編集：TOP
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $model = self::$_sponsor->contents()->profile_info()->model();
        if (is_null($model))
        {
            $model = self::$_sponsor->contents()->profile_info()->model_new();
        }
        $fieldset = $this->get_fieldset();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $filedset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));
            }
        }
        else
        {
            $fieldset->populate($model);
        }

        $view->set('model',    $model);
        $view->set('fieldset', $fieldset);
        return $view;
    }

    /**
     * カテゴリー編集：更新処理
     */
    public function action_exe()
    {
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

        $model = self::$_sponsor->contents()->profile_info()->model();
        if (is_null($model))
        {
            $model = self::$_sponsor->contents()->profile_info()->model_new();
        }
	    $fieldset = $this->get_fieldset();
	    $valid    = $fieldset->validation();

	    if ($valid->run())
	    {
	        $fields = $fieldset->validated();

	        try
	        {
	            DB::start_transaction();

	            $model->name        = $fields['name'];
	            $model->position    = $fields['position'];
	            $model->birth_year  = $fields['birth_year'];
	            $model->birth_month = $fields['birth_month'];
	            $model->birth_date  = $fields['birth_date'];
	            $model->blood       = $fields['blood'];
	            $model->hobby       = $fields['hobby'];
                if ( ! $model->save(false))
	            {
	                throw new Exception('profile registration error.');
	            }

	            DB::commit_transaction();
	        }
	        catch (Exception $e)
	        {
	            DB::rollback_transaction();
	            Log::error(__METHOD__.'profile update error: '.$e->getMessage());
	            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	            $view->set_filename('exception/503');
	            return $view;
	        }
	    }
	    else
	    {
	        Log::error(__METHOD__.'profile update error: '.$e->getMessage());
	        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

	    $view->set('model', $model);
	    return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // お名前
        $fieldset->add('name', 'お名前')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 肩書き
        $fieldset->add('position', '肩書き')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 生年月日：年
        $fieldset->add('birth_year', '生年月日', array('class' => 'w_60', 'type' => 'number'))
            ->add_rule('valid_string', array('utf8', 'numeric'))
            ->add_rule('max_length', '2');

        // 生年月日：月
        $fieldset->add('birth_month', '生年月日', array('class' => 'w_60'))
            ->add_rule('valid_string', array('utf8', 'numeric'))
            ->add_rule('max_length', '2');

        // 生年月日：日
        $fieldset->add('birth_date', '生年月日', array('class' => 'w_60'))
            ->add_rule('valid_string', array('utf8', 'numeric'))
            ->add_rule('max_length', '2');

        // 血液型
        $fieldset->add('blood', '血液型')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 趣味
        $fieldset->add('hobby', '趣味')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        return $fieldset;
    }

}
