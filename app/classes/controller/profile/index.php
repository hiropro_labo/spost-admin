<?php
/**
 * PROFILE: プロフィール編集（※議員アプリ用）
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Profile_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'profile/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'profile';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('profile');
    }

    /**
     * TOPページ
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        // プロフィール画像
        $image = self::$_sponsor->contents()->image_profile()->model();
        if (is_null($image))
        {
            $image = self::$_sponsor->contents()->image_profile()->model_new();
        }

        // プロフィール情報
        $profile_info = self::$_sponsor->contents()->profile_info()->model();

        // 略歴
        $profile_history = self::$_sponsor->contents()->profile_history()->model_list();

        // バナー
        $list_banner = self::$_sponsor->contents()->image_banner()->get_member_list_all();

        $view->set('image',           $image);
        $view->set('profile_info',    $profile_info);
        $view->set('profile_history', $profile_history);
        $view->set('list_banner',     $list_banner);

        $this->set_view_css($view, Asset::css('path/menu.css'));
        return $view;
    }
}
