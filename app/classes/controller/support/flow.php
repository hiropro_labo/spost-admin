<?php
/**
 * SUPPORT: サポートページ
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Support_Flow extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'support/flow/';

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'support';

    /**
	 * SUPPORT
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
    return View::forge(self::VIEW_FILE_PREFIX.'index.tpl');
	}
}
