<?php
/**
 * Reject Controller
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Support_Reject extends Basecontroller
{
        const VIEW_FILE_PREFIX = 'support/reject/';

        /**
         * @var ログイン済ページフラグ
         */
        protected static $_logined_page = true;

        /**
         * @var カレントページ（※UI操作に使用)
         */
        protected static $_current_page = 'reject';

    /**
     * TOP
     *
     * @access  public
     * @return  Response
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $id = $this->param('id');

        // クライアント
        $client = Model_User_Client::find_by_pk(self::get_user_id());
        $infomation = new \Support\Root\Infomation($client->id);

        $list = array();
        if (empty($id) or ! is_numeric($id))
        {
            // リジェクト
            $reject = $infomation->get_reject();
        }
        else
        {
            // リジェクト
            $reject = $infomation->get_reject_by($id);
        }

        $this->set_view_css($view, Asset::css('path/sub.css'));
        $this->set_view_css($view, Asset::css('path/info.css'));

        $view->set('client',     $client);
        $view->set('list',       $reject);
        return $view;
    }
}
