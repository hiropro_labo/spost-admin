<?php
/**
 * Info Controller
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Support_Info extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'support/info/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'info';

    /**
     * TOP
     *
     * @access  public
     * @return  Response
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        // クライアント
        $client = Model_User_Client::find_by_pk(self::get_user_id());

        $id = $this->param('id');

        $list = array();
        if (empty($id) or ! is_numeric($id))
        {
            // お知らせ
            $info = Model_Info_Admin::find_all(static::conf_get('info_limit'));
        }
        else
        {
            // お知らせ１件だけ
            $info = Model_Info_Admin::find_one_by('id', $id);
        }


        $this->set_view_css($view, Asset::css('path/sub.css'));
        $this->set_view_css($view, Asset::css('path/info.css'));

        $view->set('client',  $client);
        $view->set('id',      $id);
        $view->set('list',    $info);
        return $view;
    }
}
