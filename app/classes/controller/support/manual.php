<?php
/**
 * 製作マニュアル
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Support_Manual extends Basecontroller
{
  const VIEW_FILE_PREFIX = 'support/manual/';

  /**
   * @var カレントページ（※UI操作に使用)
   */
  protected static $_current_page = 'manual';

  /**
   * Manual Index
   *
   * @access  public
   * @return  Response
   */
  public function action_index()
  {
    $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');
    return $view;
  }

  /**
   * Manual Top
   *
   * @access  public
   * @return  Response
   */
  public function action_top()
  {
    $view = View::forge(self::VIEW_FILE_PREFIX.'top.tpl');
    return $view;
  }

  /**
   * Manual Coupon
   *
   * @access  public
   * @return  Response
   */
  public function action_coupon()
  {
    $view = View::forge(self::VIEW_FILE_PREFIX.'coupon.tpl');
    return $view;
  }

  /**
   * Manual News
   *
   * @access  public
   * @return  Response
   */
  public function action_news()
  {
    $view = View::forge(self::VIEW_FILE_PREFIX.'news.tpl');
    return $view;
  }

  /**
   * Manual Menu
   *
   * @access  public
   * @return  Response
   */
  public function action_menu()
  {
    $view = View::forge(self::VIEW_FILE_PREFIX.'menu.tpl');
    return $view;
  }

  /**
   * Manual Store
   *
   * @access  public
   * @return  Response
   */
  public function action_store()
  {
    $view = View::forge(self::VIEW_FILE_PREFIX.'store.tpl');
    return $view;
  }
}

