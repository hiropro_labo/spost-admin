<?php
/**
 * HELP：よくある質問・Q&A
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Support_Help extends Basecontroller
{
  const VIEW_FILE_PREFIX = 'support/help/';

  /**
   * @var カレントページ（※UI操作に使用)
   */
  protected static $_current_page = 'support_help';

  /**
   * コントローラ前処理
   */
  protected function before_controller()
  {
      Lang::load('navigation');
  }

  /**
   * HELP Q&A
   *
   * @access  public
   * @return  Response
   */
  public function action_index()
  {
    $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

    $g_id = $this->param('g_id');

    if (empty($g_id) or ! is_numeric($g_id))
    {
      $list = Model_Support_Help::get_faq('3');
    }
    else{
      $list = Model_Support_Help::get_help('3', $g_id);
    }
    $ipost_list   = Model_Support_Help::get_help_ipost($g_id);
    $sub_menu     = Model_Master_Support_Help_Genre::find_all();
    $current_name = Model_Master_Support_Help_Genre::get_type_name($g_id);

    $this->set_view_css($view, Asset::css('path/help.css'));
    $view->set('current',      $g_id);
    $view->set('current_name', $current_name);
    $view->set('sub_menu',     $sub_menu);
    $view->set('list',         $list);
    $view->set('ipost_list',   $ipost_list);
    return $view;
  }
}

