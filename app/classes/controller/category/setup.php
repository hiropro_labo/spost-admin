<?php
/**
 * カテゴリー選択
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Category_Setup extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'category/setup/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'category';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        if (self::$_sponsor->contents()->theme()->is_exsits_category())
        {
            //Response::redirect('/');
        }

        Lang::load('navigation');
        Lang::load('category');
    }

    /**
     * カテゴリー選択
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        return $view;
    }

    /**
     * カテゴリー選択：選択内容の確認
     */
    public function action_confirm()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'confirm.tpl');

        $category = $this->param('category');
        if (empty($category) or ! is_numeric($category))
        {
            return Response::redirect('excpetion/404');
        }

        $view->set('category', $category);
        return $view;
    }

    /**
     * カテゴリー選択：DB更新
     */
    public function action_exe()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

        $category = $this->param('category');
        if (empty($category) or ! is_numeric($category))
        {
            return Response::redirect('excpetion/404');
        }

        try
        {
            DB::start_transaction();

            $model = self::$_sponsor->contents()->theme()->model();
            if (is_null($model))
            {
                $model = self::$_sponsor->contents()->theme()->model_new();
            }

            $model->category = intval($category);
            if ( ! $model->save(false))
            {
                throw new Exception('update category failed.');
            }

            DB::commit_transaction();
        }
        catch (Exception $e)
        {
            DB::rollback_transaction();
            Log::error('[ Update Error ] '.$e->getMessage());
            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

        Response::redirect('/');
    }
}
