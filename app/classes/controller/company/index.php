<?php
/**
 * COMPANY: 企業概要の編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Company_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'company/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'company';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('company');
    }

    /**
     * TOPページ
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        // チームトップ画像
        $image = self::$_sponsor->contents()->image_company()->model();
        if (is_null($image))
        {
            $image = self::$_sponsor->contents()->image_company()->model_new();
        }

        // 表示項目リスト
        $list = self::$_sponsor->contents()->company()->model_list();

        $this->set_view_css($view, Asset::css('path/menu.css'));

        $view->set('image',  $image);
        $view->set('list',   $list);
        return $view;
    }
}
