<?php
/**
 * Store: アプリストア(App Store/Google Play)表示項目の更新
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Company_Name_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'company/name/update/';
    const POST_URI_UPDATE_EXE = 'company/name/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'company';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('company');
    }

    /**
	 * TOP画像：更新
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $fieldset = $this->get_fieldset();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $fieldset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));
            }
        }
        else
        {
            $model = self::$_sponsor->contents()->company_name()->model();
            $fieldset->populate($model);
        }

        $view->set('fieldset', $fieldset);
        return $view;
	}

	/**
	 * アイコン画像：更新処理
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $fieldset = $this->get_fieldset();
	    $fieldset->repopulate();
	    $valid    = $fieldset->validation();

	    if ($valid->run())
	    {
            $fields = $fieldset->validated();

            $model = self::$_sponsor->contents()->company_name()->model();
            if (is_null($model))
            {
                $model = self::$_sponsor->contents()->company_name()->model_new();
            }

            $model->name_ja = $fields['name_ja'];
            $model->name_en = $fields['name_en'];

            try
            {
                DB::start_transaction();

                if ( ! $model->save(false))
                {
                    throw new Exception('update error: company name.');
                }

                DB::commit_transaction();
            }
            catch (\Exception $e)
            {
                DB::rollback_transaction();
                Log::error($e->getMessage());
                $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                $view->set_filename('exception/503');
                return $view;
            }

	    }
	    else
	    {
	        Log::error(__METHOD__.'company name update error: '.$e->getMessage());
	        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

	    return $view;
	}

	/**
	 * Fieldset取得
	 */
	private function get_fieldset()
	{
	    Lang::load('validationdesc');

	    $fieldset   = Exfieldset::forge();
	    $validation = $fieldset->validation();
	    $validation->add_callable('ExValidation');
	    $fieldset->validation($validation);

	    // 日本語
	    $fieldset->add('name_ja', '日本語')
	        ->add_rule('required');

	    // 英語
	    $fieldset->add('name_en', '英語');

	    return $fieldset;
	}

}