<?php
use Fuel\Core\Controller_Rest;

/**
 * Company: 非同期更新#コンテンツ削除
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Company_Del extends Basecontroller_Rest
{
    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
	    $data = array(true, "");

	    try
	    {
	        DB::start_transaction();

	        $c_id = $this->param('c_id');
	        if (empty($c_id))
	        {
	            throw new Exception('不正なアクセスです');
	        }

            $model = self::$_sponsor->contents()->company()->model_by_id($c_id);
            if (is_null($model))
            {
                throw new Exception('削除対象データがみつかりませんでした');
            }
            $model->delete();

	        DB::commit_transaction();
	    }
	    catch (\Exception $e)
	    {
            DB::rollback_transaction();
            Log::error("company data delete failed.   ".$e->getMessage());
            $data = array(false, $e->getMessage());
	    }

	    $json = json_encode($data);

	    $this->response->set_header('Content-Type', 'application/json; charset=utf-8');
	    return $this->response->body($json);
	}
}
