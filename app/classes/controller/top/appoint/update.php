<?php
/**
 * TOP: 予約カレンダーの表示の編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Top_Appoint_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'top/appoint/update/';
    const POST_URI_UPDATE_EXE = 'top/appoint/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'appoint';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('top');
        static::$_support = \Support\Api\Top::instance(self::get_user_id());
    }

    /**
     * 予約カレンダーの表示：編集
     *
     * @access  public
     * @return  Response
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $fieldset = $this->get_fieldset();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $fieldset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));
            }
        }
        else
        {
            $appoint = self::$_sponsor->contents()->appoint_item()->model();
            if (is_null($appoint))
            {
                $appoint = self::$_sponsor->contents()->appoint_item()->model_new();
                $appoint->appoint_flg = Model_Appoint_Item::APPOINT_FLG_OFF;
            }
            $view->set('appoint', $appoint);
            $fieldset->populate($appoint);
        }

        $view->set('fieldset', $fieldset);
        return $view;
    }

    /**
     * 予約カレンダーの表示：更新処理(DB更新)
     *
     * @return View
     */
    public function action_exe()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

        $fieldset = $this->get_fieldset();
        $valid    = $fieldset->validation();
        $appoint = self::$_sponsor->contents()->appoint_item()->model();
        if (is_null($appoint))
        {
            $appoint = self::$_sponsor->contents()->appoint_item()->model_new();
        }

        if ($valid->run())
        {
            $fields = $fieldset->validated();

            try
            {
                DB::start_transaction();

                $appoint->appoint_flg = $fields['appoint'];
                if ( ! $appoint->save(false))
                {
                    throw new Exception('appoint setting update failed.');
                }

                // キャッシュクリア
                static::support()->clear_cache();

                DB::commit_transaction();
            }
            catch (Exception $e)
            {
                DB::rollback_transaction();
                Log::error(__METHOD__.'appoint update save error: '.$e->getMessage());
                $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                $view->set_filename('exception/503');
                return $view;
            }
        }
        else
        {
            Log::error(__METHOD__.'appoint update error: '.$e->getMessage());
            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

        return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // トピック情報
        $ops = array(
            '0' => '表示しない',
            '1' => '表示する',
        );
        $fieldset->add('appoint', '予約カレンダー', array(
                    'options' => $ops,
                    'type' => 'radio'
                ))
            ->add_rule('required');

        return $fieldset;
    }

}
