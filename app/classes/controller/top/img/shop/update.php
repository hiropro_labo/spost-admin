<?php
/**
 * TOP: 店舗画像の操作
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Top_Img_Shop_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX     = 'top/img/shop/update/';
    const SHOP_IMAGE_FILE_NAME = 'shop_top.jpg';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'shop';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('shop');
        static::$_support = \Support\Api\Top::instance(self::get_user_id());
    }

    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		$view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        if (Input::method() == 'POST')
        {
            $shop_image = self::$_sponsor->contents()->shop()->model();
            if (is_null($shop_image))
            {
                $shop_image = self::$_sponsor->contents()->shop()->model_new();
            }
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'shop');
            if ($file_upload->upload_file_exists())
            {
                try
                {
                    DB::start_transaction();
                    $file_upload->save_tmp_file();

                    $shop_image->tmp_file_name = $file_upload->tmp_file_name();
                    if ( ! $shop_image->save(false))
                    {
                        throw new Exception('shop image registration error.');
                    }
                    DB::commit_transaction();
                }
                catch (\Exception $e)
                {
                    Log::error($e->getMessage());
                    DB::rollback_transaction();
                    $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                    $view->set_filename('exception/503');
                    return $view;
                }

                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
            }
            else
            {
                $view->set('message', '画像を指定してください');
                $view->set_filename('exception/503');
                return $view;
            }
        }

		return $view;
	}

	/**
	 * TOP
	 *
	 * @throws Exception
	 * @return unknown
	 */
	public function action_exe()
	{
	    $this->check_only_post();

	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

        try
        {
            DB::start_transaction();

            $shop_image = self::$_sponsor->contents()->shop()->model();
            if (is_null($shop_image))
            {
                $shop_image = self::$_sponsor->contents()->shop()->model_new();
            }
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'shop');

            // ファイル名変更
            $file_upload->save_file($shop_image->tmp_file_name);

            // DBレコード更新
            $shop_image->file_name = $file_upload->file_name($shop_image->tmp_file_name);
            $shop_image->tmp_file_name = '';
            if ( ! $shop_image->save(false))
            {
                throw new Exception('shop image registration error.');
            }

            // キャッシュクリア
    	    static::support()->clear_cache();

            DB::commit_transaction();
        }
        catch (\Exception $e)
        {
            DB::rollback_transaction();
            Log::error(__METHOD__.'shop image registration error: '.$e->getMessage());
            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

		return $view;
	}
}
