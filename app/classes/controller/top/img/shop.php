<?php
/**
 * TOP: 店舗画像の操作
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Top_Img_Shop extends Basecontroller
{
    const VIEW_FILE_PREFIX     = 'top/img/shop/';
    const SHOP_IMAGE_FILE_NAME = 'shop_top.jpg';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'top';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        static::$_support = \Support\Api\Top::instance(self::get_user_id());
    }

    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_update()
	{
		$view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $client     = Model_User_Client::find_by_pk(self::get_user_id());
        $shop_image = Model_Image_Shop::get_image(self::get_user_id());

		if (Input::method() == 'POST')
		{
            $config = \Config::load('original_upload', true);
            $config = $config['shop'];
            $config['suffix'] .= self::get_user_id();

            Upload::process($config);
            if (Upload::is_valid())
            {
                Upload::save();
                $file = Upload::get_files(0);
                $save_tmp_file = $file['saved_to'].$file['saved_as'];

                $conf_image = \Config::load('original_image', true);
                $save_dir = $conf_image['save_dir']['shop'].self::get_user_id().DS;

                // ディレクトリ生成
                // 注意・・・$save_dirのパーミッション777必須
                if (! is_dir($save_dir))
                {
                    File::create_dir($conf_image['save_dir']['shop'], self::get_user_id());
                }

                // 画像処理
                Image::load($save_tmp_file)
                    ->crop_resize(640, 420)
                    ->save($save_dir.self::SHOP_IMAGE_FILE_NAME);

                // DB処理：画像ファイル名等をDBへ保存
                $shop_image = Model_Image_Shop::find_one_by('client_id', self::get_user_id());
                if (is_null($shop_image))
                {
                    $shop_image = Model_Image_Shop::forge();
                }

                $shop_image->client_id = self::get_user_id();
                $shop_image->file_name = self::SHOP_IMAGE_FILE_NAME;

                try
                {
                    DB::start_transaction();

                    if (! $shop_image->save(false))
                    {
                        throw new Exception('shop image registration error.');
                    }

                    // キャッシュクリア
                    static::support()->clear_cache();

                    DB::commit_transaction();
                }
                catch (Exception $e)
                {
                    DB::rollback_transaction();
                    Log::error(__METHOD__.'shop image registration error: '.$e->getMesage());
                    $veiw->set('message', 'ショップ画像の登録に失敗しました');
                    $view->set_filename('exception/503');
                    return $view;
                }
            }
            else
            {
                Log::error(__METHOD__.'file uploading error: shop image');
                $view->set('message', '登録処理中に予期せぬエラーが発生しました<br />アップロードできるイメージの種類は png/jpg/jpeg/gif です');
                $view->set_filename('exception/503');
            }
		}

		$view->set('client',     $client);
		$view->set('shop_image', $shop_image);
		return $view;
	}
}
