<?php
/**
 * TOP: TOPページのカルーセル画像操作
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Top_Img_Top_Delete extends Basecontroller
{
    const VIEW_FILE_PREFIX     = 'top/img/top/delete/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'top';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('top');
        static::$_support = \Support\Api\Top::instance(self::get_user_id());
    }

    /**
     * TOPカルーセル画像：削除#確認画面
     * @return unknown
     */
	public function action_index()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

	    $position = $this->param('position');
	    if (empty($position) or ! is_numeric($position))
	    {
	        return Response::redirect('excpetion/404');
	    }

	    $top_image = self::$_sponsor->contents()->recommend()->get_image($position);

	    $view->set('position',  $position);
	    $view->set('top_image', $top_image);
	    return $view;
	}

	/**
	 * TOPカルーセル画像：削除
	 * @return unknown
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $this->check_only_post();

	    $position = $this->param('position');
	    if (empty($position) or ! is_numeric($position))
	    {
	        return Response::redirect('excpetion/404');
	    }

	    $top_image = self::$_sponsor->contents()->recommend()->get_image($position);

	    try
	    {
	        DB::start_transaction();

	        // DBレコード削除
	        $top_image->delete();
            $top_image->recreate_position(self::get_user_id());

	        // 画像ファイル削除
    	    $file_upload = new \Support\File_Upload(self::get_user_id(), 'top_recommend');
    	    $rs = $file_upload->delete_file($top_image->file_name);

    	    // キャッシュクリア
    	    static::support()->clear_cache();

    	    DB::commit_transaction();
	    }
	    catch (\Exception $e)
	    {
	        DB::rollback_transaction();
	        Log::error($e->getMessage());
	        $view->set('message', 'TOP画像の削除処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

	    return $view;
	}
}