<?php
/**
 * TOP: TOPページのカルーセル画像操作
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Top_Img_Top_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'top/img/top/update/';
    const POST_URI_UPDATE_EXE = 'top/img/top/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'top';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('top');
        static::$_support = \Support\Api\Top::instance(self::get_user_id());
    }

    /**
	 * TOP画像：更新
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $position = $this->param('position');
        if (empty($position) or ! is_numeric($position))
        {
            return Response::redirect('excpetion/404');
        }

        $top_image = self::$_sponsor->contents()->recommend()->get_image($position);

        if (Input::method() == 'POST')
        {
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'top_recommend');
            if ($file_upload->upload_file_exists())
            {
                try
                {
                    DB::start_transaction();
                    $file_upload->save_tmp_file();

                    $top_image->client_id     = self::get_user_id();
                    $top_image->tmp_file_name = $file_upload->tmp_file_name();
                    $top_image->position      = $position;
                    if ( ! $top_image->save(false))
                    {
                        throw new Exception('top recommend registration error.');
                    }
                    DB::commit_transaction();
                }
                catch (\Exception $e)
                {
                    Log::error($e->getMessage());
                    DB::rollback_transaction();
                    $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                    $view->set_filename('exception/503');
                    return $view;
                }
                $view->set('img_upload_flg', true);
            }
            else
            {
                $view->set('img_upload_flg', false);
                if (empty($top_image->file_name))
                {
                    $view->set('message', '画像を指定してください');
                    $view->set_filename('exception/503');
                    return $view;
                }
            }

            $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
        }

        $view->set('top_image', $top_image);
        $view->set('position',  $position);
        return $view;
	}

	/**
	 * TOP画像：更新＃確認画面
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $position = $this->param('position');
	    if (empty($position) or ! is_numeric($position))
	    {
	        return Response::redirect('excpetion/404');
	    }

	    $top_image = self::$_sponsor->contents()->recommend()->get_image($position);
	    $file_upload = new \Support\File_Upload(self::get_user_id(), 'top_recommend');

	    try
        {
            DB::start_transaction();

            if ( ! empty($top_image->tmp_file_name))
            {
                $file_upload->save_file($top_image->tmp_file_name);
                $top_image->file_name = $file_upload->file_name($top_image->tmp_file_name);
                $top_image->tmp_file_name = '';
            }

            if ( ! $top_image->save(false))
            {
                throw new Exception('coupon registration error.');
            }

	        // キャッシュクリア
	        static::support()->clear_cache();

            DB::commit_transaction();
        }
        catch (Exception $e)
        {
            DB::rollback_transaction();
            Log::error(__METHOD__.'shop update error: '.$e->getMessage());
            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

	    return $view;
	}
}