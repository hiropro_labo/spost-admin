<?php
/**
 * TOP: トピック情報の編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Top_Topic_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'top/topic/update/';
    const POST_URI_UPDATE_EXE = 'top/topic/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'top';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('top');
        static::$_support = \Support\Api\Top::instance(self::get_user_id());
    }

    /**
	 * トピック情報：更新＃情報入力
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		$view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

		$fieldset = $this->get_fieldset();

		if (Input::method() == 'POST')
	    {
	        $fieldset->repopulate();
	        $valid = $fieldset->validation();
	        if ($valid->run())
	        {
	            $fieldset = $this->convert_fieldset_confirm($fieldset);
	            $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
	            $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));
	        }
	    }
	    else
	    {
	        $topic = self::$_sponsor->contents()->top_topic()->model();
	        if (is_null($topic))
	        {
	            $topic = self::$_sponsor->contents()->top_topic()->model_new();
	        }
	        $view->set('topic', $topic);
	        $fieldset->populate($topic);
	    }

	    // jsロード：住所自動入力
	    $this->load_js_auto_address($view);

		$view->set('fieldset', $fieldset);
		return $view;
	}

	/**
	 * ショップ情報：更新処理(DB更新)
	 *
	 * @return View
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $fieldset = $this->get_fieldset();
        $valid    = $fieldset->validation();

        if ($valid->run())
        {
        	$topic = self::$_sponsor->contents()->top_topic()->model();
            if (is_null($topic))
            {
                $topic = self::$_sponsor->contents()->top_topic()->model_new();
            }
            $fields = $fieldset->validated();

            try
            {
                DB::start_transaction();

                $topic->message   = $fields['message'];
                if ( ! $topic->save(false))
                {
                    throw new Exception('topic message update failed.');
                }

                // キャッシュクリア
                static::support()->clear_cache();

                DB::commit_transaction();
            }
            catch (Exception $e)
            {
                DB::rollback_transaction();
                Log::error(__METHOD__.'topic message update error: '.$e->getMessage());
                $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                $view->set_filename('exception/503');
                return $view;
            }
        }
        else
        {
            Log::error(__METHOD__.'topic update error: '.$e->getMessage());
            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

	    return $view;
	}

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // トピック情報
        $fieldset->add('message', 'トピックメッセージ')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        return $fieldset;
    }
}
