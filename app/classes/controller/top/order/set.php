<?php
/**
 * TOP: TOPページの画像表示順の変更
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Top_Order_Set extends Basecontroller
{

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        static::$_support = \Support\Api\Top::instance(self::get_user_id());
    }

    /**
     * TOP画像：表示順更新
     *
     * @access  public
     * @return  Response
     */
    public function action_index()
    {
        $positions = Input::post('positions');
        try
        {
            DB::start_transaction();

            $list = array();
            $idx  = 1;

            foreach($positions as $posi)
            {
                if (is_null($posi) or empty($posi) or ! is_numeric($posi)){
                    continue;
                }

                $image = self::$_sponsor->contents()->recommend()->get_image(intval($posi));
                $image->position = $idx;

                array_push($list, $image);
                $idx++;
            }

            foreach ($list as $rec)
            {
                if ( ! $rec->save(false))
                {
                    throw new Exception('Error: top_recommend image position change failed.');
                }
            }

            // キャッシュクリア
            static::support()->clear_cache();

            DB::commit_transaction();
        }
        catch (Exception $e)
        {
            DB::rollback_transaction();
            Log::error($e->getMessage());

            return Response::forge(null, 503);
        }
    }
}