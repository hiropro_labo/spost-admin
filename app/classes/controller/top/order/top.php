<?php
/**
 * TOP: TOPページの画像表示順の変更
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Top_Order_Top extends Basecontroller
{
  const VIEW_FILE_PREFIX     = 'top/order/top/';

  /**
   * @var ログイン済ページフラグ
   */
  protected static $_logined_page = true;

  /**
   * @var カレントページ（※UI操作に使用)
   */
  protected static $_current_page = 'top';

  /**
   * TOP画像：表示順UP
   *
   * @access  public
   * @return  Response
   */
  public function action_up()
  {
    $position = $this->param('position');
    if (empty($position) or ! is_numeric($position))
    {
      return Response::redirect('excpetion/404');
    }

    try
    {
      Model_Image_Recommend::position_up(self::get_user_id(), $position);
    }
    catch (Exception $e)
    {
      return Response::redirect('excpetion/404');
    }

    Response::redirect('/top');
  }

  /**
   * TOP画像：表示順DOWN
   *
   * @access  public
   * @return  Response
   */
  public function action_down()
  {
    $position = $this->param('position');
    if (empty($position) or ! is_numeric($position))
    {
      return Response::redirect('excpetion/404');
    }

    try
    {
      Model_Image_Recommend::position_down(self::get_user_id(), $position, self::conf_get('top_image_max'));
    }
    catch (Exception $e)
    {
      return Response::redirect('excpetion/404');
    }

    Response::redirect('/top');
  }
}
