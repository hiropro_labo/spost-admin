<?php
/**
 * TOP: TOPカルーセル画像/店舗画像/店舗情報の表示
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Top_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'top/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'top';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('top');
    }

    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		$view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        // 予約
 		$appoint = Model_Appoint_Item::getFlgString(self::get_user_id());
 		$view->set('appoint', $appoint);

        // チュートリアルの進行状況
        //$tutorial = \Support\Tutorial\Step::instance(self::get_user_id());
        //$view->set('tutorial_js', $tutorial->step_js('top'), false);

        // カルーセル画像
        $image_list = static::$_sponsor->contents()->recommend()->get_image_set(self::conf_get('top_image_max'));
        $view->set('image_list', $image_list);

        // トピックメッセージ
        $topic_message = '現在トピックメッセージは未設定です';
        $topic_model   = static::$_sponsor->contents()->top_topic()->model();
        if ( ! is_null($topic_model))
        {
            $topic_message = $topic_model->message;
        }
        $view->set('topic_message', $topic_message);

		return $view;
	}
}
