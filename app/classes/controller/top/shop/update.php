<?php
/**
 * TOP: 店舗情報の編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Top_Shop_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'top/shop/update/';
    const POST_URI_UPDATE_EXE = 'top/shop/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'shop';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('shop');
        static::$_support = \Support\Api\Top::instance(self::get_user_id());
    }

    /**
	 * ショップ情報：更新＃情報入力
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		$view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

		$fieldset = $this->get_fieldset();
		$shop     = self::$_sponsor->contents()->shop_profile()->model();

		if (Input::method() == 'POST')
	    {
	        $fieldset->repopulate();
	        $valid = $fieldset->validation();
	        if ($valid->run())
	        {
	            $fieldset = $this->convert_fieldset_confirm($fieldset);
	            $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
	            $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));
	            $view->set('selected_pref', $this->get_pref_name($fieldset->validated('pref')));
	        }
	    }
	    else
	    {
	        $fieldset->populate($shop);
	    }

	    // jsロード：住所自動入力
	    $this->load_js_auto_address($view);

		$view->set('fieldset', $fieldset);
		$view->set('shop',     $shop);
		return $view;
	}

	/**
	 * ショップ情報：更新処理(DB更新)
	 *
	 * @return View
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $shop     = self::$_sponsor->contents()->shop_profile()->model();
	    $fieldset = $this->get_fieldset();
        $valid    = $fieldset->validation();

        if ($valid->run())
        {
            $fields = $fieldset->validated();

            // Shop: GPS
            $address = '';
            if ( ! empty($fields['city']) and ! empty($fields['address_opt1']))
            {
                $pref = $this->get_pref_name($fields['pref']);
                $address = $pref.$fields['city'].$fields['address_opt1'];
            }
            $geo = self::$_sponsor->geocode($address);
            // Shop: プロフィール情報
            $shop->shop_name    = $fields['shop_name'];
            $shop->email        = $fields['email'];
            $shop->tel1         = $fields['tel1'];
            $shop->tel2         = $fields['tel2'];
            $shop->tel3         = $fields['tel3'];
            $shop->fax1         = $fields['fax1'];
            $shop->fax2         = $fields['fax2'];
            $shop->fax3         = $fields['fax3'];
            $shop->zip_code1    = $fields['zip_code1'];
            $shop->zip_code2    = $fields['zip_code2'];
            $shop->pref         = $fields['pref'];
            $shop->city         = $fields['city'];
            $shop->address_opt1 = $fields['address_opt1'];
            $shop->address_opt2 = $fields['address_opt2'];
            $shop->url          = $fields['url'];
            $shop->online_shop  = $fields['online_shop'];
            $shop->open_hours   = $fields['open_hours'];
            $shop->holiday      = $fields['holiday'];
            $shop->lat          = $geo['lat'];
            $shop->lng          = $geo['lng'];

            try
            {
                DB::start_transaction();

                if ( ! $shop->save(false))
                {
                    throw new Exception('shop profile update failed.');
                }
                // キャッシュクリア
                static::support()->clear_cache();

                DB::commit_transaction();
            }
            catch (Exception $e)
            {
                DB::rollback_transaction();
                Log::error(__METHOD__.'shop update error: '.$e->getMessage());
                $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                $view->set_filename('exception/503');
                return $view;
            }
        }
        else
        {
            Log::error(__METHOD__.'shop update error: '.$e->getMessage());
            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

        $view->set('shop',   $shop);
	    return $view;
	}

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        //------------------------------------------
        // Required Entry
        //------------------------------------------
        // ショップ名称
        $fieldset->add('shop_name', 'お店の名前')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        //------------------------------------------
        // Optional Entry
        //------------------------------------------
        // メールアドレス
        $fieldset->add('email', 'メールアドレス')
            ->add_rule('valid_email')
            ->add_rule('max_length', '255');
        // TEL
        $fieldset->add('tel1', 'TEL', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '5');
        $fieldset->add('tel2', 'TEL(2)', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '5');
        $fieldset->add('tel3', 'TEL(3)', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '4');

        // FAX
        $fieldset->add('fax1', 'FAX', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '5');
        $fieldset->add('fax2', 'FAX(2)', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '5');
        $fieldset->add('fax3', 'FAX(3)', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '4');

        // 郵便番号
        $fieldset->add('zip_code1', '郵便番号', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('max_length', '3');
        $fieldset->add('zip_code2', '郵便番号(2)',
                array('onKeyUp' => 'AjaxZip2.zip2addr(\'zip_code1\',\'pref\',\'city\',\'zip_code2\',\'address_opt2\',\'address_opt1\');',
                    'class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('max_length', '4');

        // 住所：県(コード)
        $opt_pref = Model_Master_Prefectures::list_opt_array();
        $fieldset->add('pref', '都道府県', array('options' => $opt_pref, 'type' => 'select', 'class' => 'w_120'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('numeric_min', 1)
            ->add_rule('numeric_max', 47);

        // 市区町村
        $fieldset->add('city', '市区町村')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 住所（番地）
        $fieldset->add('address_opt1', '住所（番地）')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 住所（建物）
        $fieldset->add('address_opt2', '住所（建物）')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // ホームページURL
        $fieldset->add('url', 'ホームページアドレス')
            ->add_rule('valid_url');

        // オンラインショップURL
        $fieldset->add('online_shop', 'オンラインショップURL')
            ->add_rule('valid_url');

        // 営業時間
        $fieldset->add('open_hours', '営業時間')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 定休日
        $fieldset->add('holiday', '定休日')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        return $fieldset;
    }
}
