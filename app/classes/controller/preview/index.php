<?php
/**
 * PREVIEW: プレビュー表示
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Preview_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'preview/';

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'preview';

    /**
     * INDEX
     *
     * @access  public
     * @return  Response
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');
        return $view;
    }

    /**
     * TOP
     *
     * @access  public
     * @return  Response
     */
    public function action_top()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'top.tpl');

        // カレントページ指定
        $view->set('current', 'top');

        // カルーセル画像
        $image_list = static::$_sponsor->contents()->recommend()->get_image_set(self::conf_get('top_image_max'));
        $view->set('image_list', $image_list);

        // トピックメッセージ
        $topic_message = '現在トピックメッセージは未設定です';
        $topic_model   = static::$_sponsor->contents()->top_topic()->model();
        if ( ! is_null($topic_model))
        {
            $topic_message = $topic_model->message;
        }
        $view->set('topic_message', $topic_message);

        return $view;
    }

    /**
     * SHOPDETAIL
     *
     * @access  public
     * @return  Response
     */
    public function action_shopdetail()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'shopdetail.tpl');

        $shop_profile = self::$_sponsor->contents()->shop_profile()->model();
        $shop_image   = self::$_sponsor->contents()->shop()->get_image();

        $view->set('current',     'top');
        $view->set('shop',        $shop_profile);
        $view->set('shop_pref',   $this->get_pref_name($shop_profile->pref));
        $view->set('shop_image',  $shop_image);

        return $view;
    }

    /**
     * WEBVIEW
     *
     * @access  public
     * @return  Response
     */
    public function action_webview()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'webview.tpl');

        $shop_profile = self::$_sponsor->contents()->shop_profile()->model();

        $view->set('current', 'top');
        $view->set('url',      $shop_profile->url);
        return $view;
    }

    /**
     * ONLINE SHOP
     *
     * @access  public
     * @return  Response
     */
    public function action_onlineshop()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'webview.tpl');

        $shop_profile = Model_Shop_Profile::find_one_by('client_id', self::get_user_id());

        $url = $shop_profile->online_shop;

        $view->set('current', 'top');
        $view->set('tabname',  $this->get_tabname());
        $view->set('url', $url);
        return $view;
    }

    /**
     * COUPON
     *
     * @access  public
     * @return  Response
     */
    public function action_coupon()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'coupon.tpl');

        $list = self::$_sponsor->contents()->coupon()->get_image_set(self::conf_get('coupon_image_max'));

        $view->set('current',  'coupon');
        $view->set('list',     $list);
        return $view;
    }

    /**
     * NEWS
     *
     * @access  public
     * @return  Response
     */
    public function action_news()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'news.tpl');

        $list  = self::$_sponsor->contents()->news_info()->list_news();

        $view->set('current',  'news');
        $view->set('list',     $list);

        return $view;
    }

    /**
     * MENU
     *
     * @access  public
     * @return  Response
     */
    public function action_menu()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'menu.tpl');

        $parent_id = 0;
        $image     = null;

        // カテゴリーID
        $c_id = $this->param('c_id');
        $p_id = $this->param('p_id');

        $list = array();

        // MENUトップ
        if(empty($c_id) or ! is_numeric($c_id))
        {
            // メニュートップ画像表示
            $image = self::$_sponsor->contents()->menu()->get_image();

            // メニュー管理：カテゴリー一覧表示
            $category = self::$_sponsor->contents()->menu_category()->get_category_list();
            if ( ! is_null($category) and is_array($category))
            {
                foreach ($category as $cate)
                {
                    $menu = self::$_sponsor->contents()->menu_item()->get_menu_list($cate->id, true);
                    if (is_null($menu)) continue;
                    array_push($list, $cate);
                }
            }
        }
        // カテゴリー一覧
        else
        {
            // カテゴリーの画像の取得
            $image = self::$_sponsor->contents()->menu_category()->get_category($c_id);

            // 商品一覧
            $list      = self::$_sponsor->contents()->menu_item()->get_menu_list($c_id, true);
            $parent_id = $c_id;
        }

        $view->set('current',   'menu');
        $view->set('image',     $image);
        $view->set('parent_id', $parent_id);
        $view->set('list',      $list);

        return $view;
    }

    /**
     * MENU ITEM
     *
     * @access  public
     * @return  Response
     */
    public function action_menudetail()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'menudetail.tpl');

        // メニューID
        $c_id   = $this->param('c_id');

        // メニュー管理：メニュー一覧表示
        $item = self::$_sponsor->contents()->menu_item()->get_item($c_id);

        $view->set('current', 'menu');
        $view->set('item',    $item);

        return $view;
    }

    /**
     * SETTING
     *
     * @access  public
     * @return  Response
     */
    public function action_setting()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'setting.tpl');
        $view->set('current',  'setting');
        $view->set('tabname',  $this->get_tabname());
        return $view;
    }

    /**
     * Apple Store
     *
     * @access  public
     * @return  Response
     */
    public function action_store()
    {
        static::$_current_page = 'store';
        $view = View::forge(self::VIEW_FILE_PREFIX.'store.tpl');

        $image_icon  = self::$_sponsor->contents()->icon()->get_image();
        $list_sshot1 = self::$_sponsor->contents()->screen_shot()->get_image_set(self::conf_get('screen_shot1_image_max'));

        $view->set('current',    'store');
        $view->set('image_icon',  $image_icon);
        $view->set('list_sshot1', $list_sshot1);
        return $view;
    }

    /**
     * Google Play
     *
     * @access  public
     * @return  Response
     */
    public function action_play()
    {
        static::$_current_page = 'play';
        $view = View::forge(self::VIEW_FILE_PREFIX.'play.tpl');

        $image_icon  = self::$_sponsor->contents()->icon()->get_image();
        $list_sshot1 = self::$_sponsor->contents()->screen_shot()->get_image_set(self::conf_get('screen_shot1_image_max'));

        $view->set('current',    'store');
        $view->set('image_icon',  $image_icon);
        $view->set('list_sshot1', $list_sshot1);
        return $view;
    }

    /**
     * タブ名の取得
     */
    public function get_tabname()
    {
        $array = array('top' => 'TOP', 'coupon' => 'COUPON','news' => 'NEWS','menu'    => 'MENU', 'setting' => 'SETTING');

        $tab_item = Model_Tab_Item::find_by('client_id', self::get_user_id());

        if( ! is_null($tab_item))
        {
            foreach($tab_item as $item) {
                if( ! is_null($item->tab_name) && ! empty($item->tab_name))
                {
                    if($item->tab_id == 1) $array['top']     = $item->tab_name;
                    if($item->tab_id == 2) $array['coupon']  = $item->tab_name;
                    if($item->tab_id == 3) $array['news']    = $item->tab_name;
                    if($item->tab_id == 4) $array['menu']    = $item->tab_name;
                    if($item->tab_id == 5) $array['setting'] = $item->tab_name;
                }
            }
        }

        return $array;
    }
}
