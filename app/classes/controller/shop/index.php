<?php
/**
 * SHOP: 店舗情報の表示
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Shop_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'shop/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'shop';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('shop');
    }

    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		return View::forge(self::VIEW_FILE_PREFIX.'index.tpl');
	}
}
