<?php
/**
 * SEGMENTS: 事業内容の編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Segments_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'segments/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'segments';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('segments');
    }

    /**
     * TOPページ
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        // 画像: Work
        $image_work = self::$_sponsor->contents()->image_segments_work()->model();
        if (is_null($image_work))
        {
            $image_work = self::$_sponsor->contents()->image_segments_work()->model_new();
        }
        $view->set('image_work', $image_work);

        // 画像: Policy
        $image_policy = self::$_sponsor->contents()->image_segments_policy()->model();
        if (is_null($image_policy))
        {
            $image_policy = self::$_sponsor->contents()->image_segments_policy()->model_new();
        }
        $view->set('image_policy', $image_policy);

        // 画像: Message
        $image_message = self::$_sponsor->contents()->image_segments_message()->model();
        if (is_null($image_message))
        {
            $image_message = self::$_sponsor->contents()->image_segments_message()->model_new();
        }
        $view->set('image_message', $image_message);

        // コンテンツ
        $segments = self::$_sponsor->contents()->segments()->model();
        $view->set('segments', $segments);

        $this->set_view_css($view, Asset::css('path/menu.css'));

        return $view;
    }
}
