<?php
/**
 * SEGMENTS: テキスト編集 Work
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Segments_Text_Work_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'segments/text/work/update/';
    const POST_URI_UPDATE_EXE = 'segments/text/work/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'segments';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('segments');
    }

    /**
	 * クーポン情報更新
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $model = self::$_sponsor->contents()->segments()->model();
        if (is_null($model))
        {
            $model = self::$_sponsor->contents()->segments()->model_new();
        }

        $fieldset = $this->get_fieldset();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $filedset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));
            }
        }
        else
        {
            $fieldset->populate($model);
        }

        $view->set('model',     $model);
        $view->set('fieldset',  $fieldset);
        return $view;
	}

	/**
	 * クーポン情報：更新処理(※DB更新処理)
	 *
	 * @return unknown
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $model = self::$_sponsor->contents()->segments()->model();
        if (is_null($model))
        {
            $model = self::$_sponsor->contents()->segments()->model_new();
        }

	    $fieldset = $this->get_fieldset();
	    $valid    = $fieldset->validation();

	    if ($valid->run())
	    {
	        $fields = $fieldset->validated();

            try
            {
                DB::start_transaction();

                $model->title_work = $fields['title_work'];
                $model->body_work  = $fields['body_work'];
                if ( ! $model->save(false))
                {
                    throw new Exception('segments work update failed.');
                }

    	        DB::commit_transaction();
            }
            catch (Exception $e)
            {
                Debug::dump($e->getMessage());
                DB::rollback_transaction();
                Log::error(__METHOD__.'segments work update failed : '.$e->getMessage());
                $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                $view->set_filename('exception/503');
                return $view;
            }
	    }
	    else
	    {
	        Log::error(__METHOD__.'segments work update failed : '.$e->getMessage());
	        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

	    $view->set('model', $model);
	    return $view;
	}

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // タイトル
        $fieldset->add('title_work', 'タイトル')
            ->add_rule('required');

        // 本文
        $fieldset->add('body_work', '本文')
            ->set_type('textarea')
            ->add_rule('required');

        return $fieldset;
    }
}