<?php
/**
 * SEGMENTS: 画像更新 Work
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Segments_Img_Policy_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'segments/img/policy/update/';
    const POST_URI_UPDATE_EXE = 'segments/img/policy/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'segments';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('segments');
    }

    /**
	 * TOP画像：更新
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $model = self::$_sponsor->contents()->image_segments_policy()->model();
        if (is_null($model))
        {
            $model = self::$_sponsor->contents()->image_segments_policy()->model_new();
        }

        if (Input::method() == 'POST')
        {
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'segments_policy');
            if ($file_upload->upload_file_exists())
            {
                try
                {
                    DB::start_transaction();

                    $file_upload->save_tmp_file();

                    $model->client_id     = self::get_user_id();
                    $model->tmp_file_name = $file_upload->tmp_file_name();
                    if ( ! $model->save(false))
                    {
                        throw new Exception('segments image[policy] registration error.');
                    }

                    DB::commit_transaction();
                }
                catch (\Exception $e)
                {
                    DB::rollback_transaction();
                    Log::error($e->getMessage());
                    $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                    $view->set_filename('exception/503');
                    return $view;
                }
                $view->set('img_upload_flg', true);
            }
            else
            {
                $view->set('img_upload_flg', false);
                if (empty($model->file_name))
                {
                    $view->set('message', '画像を指定してください');
                    $view->set_filename('exception/503');
                    return $view;
                }
            }

            $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
        }

        $view->set('model', $model);
        return $view;
	}

	/**
	 * アイコン画像：更新処理
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $model = self::$_sponsor->contents()->image_segments_policy()->model();
	    if (is_null($model))
	    {
	        $model = self::$_sponsor->contents()->image_segments_policy()->model_new();
	    }

	    $file_upload = new \Support\File_Upload(self::get_user_id(), 'segments_policy');

	    try
        {
            DB::start_transaction();

            if ( ! empty($model->tmp_file_name))
            {
                $file_upload->save_file($model->tmp_file_name);
                $model->file_name = $file_upload->file_name($model->tmp_file_name);
                $model->tmp_file_name = '';
            }

            if ( ! $model->save(false))
            {
                throw new Exception('segments image[policy] registration error.');
            }

            DB::commit_transaction();
        }
        catch (Exception $e)
        {
            DB::rollback_transaction();
            Log::error(__METHOD__.'segments image[policy] registration error: '.$e->getMessage());
            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

	    return $view;
	}
}