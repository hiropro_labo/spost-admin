<?php
/**
 * POP：ポスター・POP サンプル
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Pop_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'pop/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'pop';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('pop');
    }

    /**
     * POP Index
     *
     * @access  public
     * @return  Response
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $this->set_view_css($view, Asset::css('path/pop.css'));

        $client = Model_User_Client::find_by_pk(self::get_user_id());

        $view->set('client', $client);
        return $view;
    }
}

