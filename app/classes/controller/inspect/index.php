<?php
/**
 * INSPECT：アプリ審査申請
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Inspect_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'inspect/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'inspect';

    /**
     * @var Inspect
     */
    protected static $_inspect = null;

    /**
     * @var Status
     */
    protected static $_status = null;

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('inspect');

        static::$_inspect = self::$_sponsor->app()->inspect();
    }

    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		$view = View::forge($this->get_index_view_path());

		if (Input::method() == 'POST')
		{
		    // アプリ審査申請処理
            if ( ! static::$_inspect->request())
            {
                $view->set('message', 'アプリ審査申請の処理に失敗しました');
                $view->set_filename('exception/503');
                return $view;
            }

            $view->set_filename(self::VIEW_FILE_PREFIX.'exe');
		}

		return $view;
	}

	/**
	 * Apple審査申請処理
	 *
	 * @access public
	 * @return リダイレクト
	 */
	public function action_ios()
	{
	    $this->check_only_post();

	    if ( ! static::$_inspect->request_ios_by_client())
	    {
	        Response::redirect('exception/503');
	    }

	    Response::redirect('/inspect');
	}

	/**
	 * Indexページのテンプレートパス取得
	 *   ・Inspect Status に応じてテンプレート切り替え
	 *
	 * @return string
	 */
	private function get_index_view_path()
	{
	    $path = self::VIEW_FILE_PREFIX;

	    switch (static::$_inspect->status())
	    {
	        case Model_Inspect_Request::INSPECT_STATUS_PRE:
                $path .= 'index_pre.tpl';
	            break;
	        case Model_Inspect_Request::INSPECT_STATUS_INIT:
	            $path .= 'index_init.tpl';
	            break;
	        case Model_Inspect_Request::INSPECT_STATUS_START:
	            $path .= 'index_start.tpl';
	            break;
	        case Model_Inspect_Request::INSPECT_STATUS_END:
	            $path .= 'index_end.tpl';
	            break;
	        case Model_Inspect_Request::INSPECT_STATUS_ANDROID_RELEASE:
	            $path .= 'index_android_release.tpl';
	            break;
	        case Model_Inspect_Request::INSPECT_STATUS_IOS_REQUEST:
	            $path .= 'index_ios_request.tpl';
	            break;
	        case Model_Inspect_Request::INSPECT_STATUS_IOS_INSPECT:
	            $path .= 'index_ios_inspect.tpl';
	            break;
	        case Model_Inspect_Request::INSPECT_STATUS_IOS_REJECT:
	            $path .= 'index_ios_reject.tpl';
	            break;
	        case Model_Inspect_Request::INSPECT_STATUS_IOS_PASS:
	            $path .= 'index_ios_pass.tpl';
	            break;
	        case Model_Inspect_Request::INSPECT_STATUS_IOS_RELEASE:
	            $path .= 'index_ios_release.tpl';
	            break;
	    }

	    return $path;
	}
}
