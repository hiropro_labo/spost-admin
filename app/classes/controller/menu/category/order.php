<?php
/**
 * MENU：カテゴリー表示順の変更
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Menu_Category_Order extends Basecontroller
{
    const VIEW_FILE_PREFIX     = 'menu/category/order/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'menu';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        static::$_support = \Support\Api\Menu::instance(self::get_user_id());
    }

    /**
	 * TOP画像：表示順UP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_up()
	{
        $position = $this->param('position');
        if (empty($position) or ! is_numeric($position))
        {
            return Response::redirect('excpetion/404');
        }

        try
        {
            self::$_sponsor->contents()->menu_category()->position_up($position);

	        // キャッシュクリア
	        static::support()->clear_cache();
        }
        catch (Exception $e)
        {
            return Response::redirect('excpetion/404');
        }

        Response::redirect('/menu');
	}

	/**
	 * TOP画像：表示順DOWN
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_down()
	{
        $position  = $this->param('position');
	    if (empty($position) or ! is_numeric($position))
        {
            return Response::redirect('excpetion/404');
        }

        try
        {
            $max_cnt = self::$_sponsor->contents()->menu_category()->count();
            self::$_sponsor->contents()->menu_category()->position_down($position, $max_cnt);

	        // キャッシュクリア
	        static::support()->clear_cache();
        }
        catch (Exception $e)
        {
            return Response::redirect('excpetion/404');
        }

        Response::redirect('/menu');
	}

}