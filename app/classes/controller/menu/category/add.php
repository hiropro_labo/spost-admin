<?php
/**
 * MENU: カテゴリー新規作成
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Menu_Category_Add extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'menu/category/add/';
    const POST_URI_UPDATE_EXE = 'menu/category/add/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'menu';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('menu');
        static::$_support = \Support\Api\Menu::instance(self::get_user_id());
    }

    /**
     * カテゴリー新規作成：TOP
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $category = self::$_sponsor->contents()->menu_category()->model_new();
        $fieldset = $this->get_fieldset();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();

            if ($valid->run())
            {
                $filedset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));

                $file_upload = new \Support\File_Upload(self::get_user_id(), 'menu_category');
                if ($file_upload->upload_file_exists())
                {
                    try
                    {
                        // 画像一時保存
                        $file_upload->save_tmp_file();
                    }
                    catch (\Exception $e)
                    {
                        Log::error($e->getMessage());
                        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                        $view->set_filename('exception/503');
                        return $view;
                    }
                    $view->set('img_upload_flg', true);
                }
                else
                {
                    $view->set('message', 'カテゴリー登録の際には画像を必ず指定してください');
                    $view->set_filename('exception/503');
                    return $view;
                }
            }
        }

        $view->set('category', $category);
        $view->set('fieldset', $fieldset);
        return $view;
    }

    /**
     * カテゴリー新規作成：登録処理
     */
    public function action_exe()
    {
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $category = self::$_sponsor->contents()->menu_category()->model_new();

	    $fieldset = $this->get_fieldset();
	    $valid    = $fieldset->validation();

	    if ($valid->run())
	    {
	        $fields      = $fieldset->validated();
	        $file_upload = new \Support\File_Upload(self::get_user_id(), 'menu_category');

	        try
	        {
	            DB::start_transaction();

	            $tmp_file_name = '';
	            $tmp_files = $file_upload->get_tmp_files();
	            if ( ! empty($tmp_files))
	            {
	                $tmp_file_name = basename(array_shift($tmp_files));
	            }

	            if ( ! empty($tmp_file_name))
	            {
	                $file_upload->save_file($tmp_file_name);
	            }

	            // DBレコード登録
	            $category->client_id      = self::get_user_id();
	            $category->title          = $fields['title'];
	            $category->sub_title      = $fields['sub_title'];
	            $category->tmp_file_name  = '';
	            $category->file_name      = $file_upload->file_name($tmp_file_name);
	            $category->position       = $category->new_position(self::get_user_id());
	            $category->enable         = $fields['enable'];
	            if ( ! $category->save(false))
	            {
	                throw new Exception('menu category registration error.');
	            }

    	        // キャッシュクリア
    	        static::support()->clear_cache();

    	        DB::commit_transaction();
	        }
	        catch (\Exception $e)
	        {
	            DB::rollback_transaction();
	            Log::error($e->getMessage());
	            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	            $view->set_filename('exception/503');
	            return $view;
	        }
	    }
	    else
	    {
	        Log::error(__METHOD__.'menu category registration error: '.$e->getMessage());
	        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

	    $view->set('category',  $category);
        $view->set('fieldset',  $fieldset);
	    return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // カテゴリー名
        $fieldset->add('title', 'カテゴリー名')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // サブタイトル名
        $fieldset->add('sub_title', 'サブタイトル名')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 表示
        $opt = array('0' => '非表示', '1' => '表示');
        $fieldset->add('enable', '表示設定',
            array('options' => $opt, 'type' => 'radio'))
            ->add_rule('required');

        return $fieldset;
    }

}
