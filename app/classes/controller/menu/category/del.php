<?php
/**
 * MENU: カテゴリー削除
 *
 * ・紐付いている商品もすべて削除する
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Menu_Category_Del extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'menu/category/del/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'menu';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('menu');
        static::$_support = \Support\Api\Menu::instance(self::get_user_id());
    }

    /**
     * カテゴリー削除：TOP
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $c_id = $this->param('c_id');
        if (empty($c_id) or ! is_numeric($c_id))
        {
            return Response::redirect('excpetion/404');
        }

        $category = self::$_sponsor->contents()->menu_category()->get_category($c_id);
        if (is_null($category))
        {
            return Response::redirect('exception/404');
        }
        $fieldset = $this->get_fieldset();
        $fieldset->populate($category);

        $view->set('category',  $category);
        $view->set('fieldset',  $fieldset);
        return $view;
    }

    /**
     * カテゴリー削除
     */
    public function action_exe()
    {
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $this->check_only_post();

        $c_id = $this->param('c_id');
        if (empty($c_id) or ! is_numeric($c_id))
        {
            return Response::redirect('excpetion/404');
        }

        $category = self::$_sponsor->contents()->menu_category()->get_category($c_id);
        if (is_null($category))
        {
            $view->set('message', 'カテゴリー削除の処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

        try
        {
            DB::start_transaction();

            // 画像ファイル削除
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'menu_category');
            $file_upload->delete_file($category->file_name);

            // DBレコード削除
            $category->delete();

            // ポジション再セット
            self::$_sponsor->contents()->menu_category()->recreate_position();

	        // キャッシュクリア
	        static::support()->clear_cache();

	        DB::commit_transaction();
        }
        catch (Exception $e)
        {
            Debug::dump($e->getMessage());
            exit();
            DB::rollback_transaction();
            Log::error(__METHOD__.'menu category delete error: '.$e->getMessage());
            $view->set('message', 'カテゴリー削除の処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

	    return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // カテゴリー
        $opt_cate = Model_Menu_Category::list_opt_array(self::get_user_id());
        $fieldset->add('parent_id', 'カテゴリー',
        array('options' => $opt_cate, 'type' => 'select'))
        ->add_rule('required')
        ->add_rule('valid_string', array('numeric'));

        // メニュー名
        $fieldset->add('title', 'メニュー名')
        ->add_rule('required')
        ->add_rule('valid_string', array('utf8'))
        ->add_rule('max_length', '255');

        // サブタイトル名
        $fieldset->add('sub_title', 'サブタイトル')
        ->add_rule('required')
        ->add_rule('valid_string', array('utf8'))
        ->add_rule('max_length', '255');

        // メニュー説明
        $fieldset->add('description', 'メニュー説明')
        ->add_rule('required')
        ->add_rule('valid_string', array('utf8'));

        // 価格
        $fieldset->add('price', '価格')
        ->add_rule('required')
        ->add_rule('valid_string', array('numeric'));

        // URL
        $fieldset->add('url', 'URL')
        ->add_rule('valid_url');

        // 表示
        $opt = array('0' => '非表示', '1' => '表示');
        $fieldset->add('enable', '表示設定',
        array('options' => $opt, 'type' => 'radio'))
        ->add_rule('required');

        return $fieldset;
    }

}
