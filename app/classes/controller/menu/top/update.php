<?php
/**
 * MENU: メニュートップ画像編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Menu_Top_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX     = 'menu/top/update/';
    const MENU_IMAGE_FILE_NAME = 'menu_top.jpg';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'menu';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('menu');
    }

    /**
	 * MENUトップ画像：画像選択
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		$view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

		$image = self::$_sponsor->contents()->menu()->model();
		if (is_null($image))
		{
		    $image = self::$_sponsor->contents()->menu()->model_new();
		}

		if (Input::method() == 'POST')
		{
		    $file_upload = new \Support\File_Upload(self::get_user_id(), 'menu_top');
		    if ($file_upload->upload_file_exists())
		    {
		        try
		        {
		            DB::start_transaction();
		            $file_upload->save_tmp_file();

		            $image->tmp_file_name = $file_upload->tmp_file_name();
		            if ( ! $image->save(false))
		            {
		                throw new Exception('menu top image registration error.');
		            }
		            DB::commit_transaction();
		        }
		        catch (\Exception $e)
		        {
		            Log::error($e->getMessage());
		            DB::rollback_transaction();
		            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
		            $view->set_filename('exception/503');
		            return $view;
		        }

		        $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
		    }
		    else
		    {
		        $view->set('message', '画像を指定してください');
		        $view->set_filename('exception/503');
		        return $view;
		    }
		}

		$view->set('image', $image);
		return $view;
	}

	/**
	 * MENUトップ画像：登録処理
	 *
	 * @throws Exception
	 * @return unknown
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

        $this->check_only_post();

        try
        {
            DB::start_transaction();

            $image = self::$_sponsor->contents()->menu()->model();
            if (is_null($image))
            {
                $image = self::$_sponsor->contents()->menu->model_new();
            }

            // ファイル名変更
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'menu_top');
            $file_upload->save_file($image->tmp_file_name);

            // DBレコード更新
            $image->file_name = $file_upload->file_name($image->tmp_file_name);
            $image->tmp_file_name = '';
            if ( ! $image->save(false))
            {
                throw new Exception('menu top image registration error.');
            }

    	    DB::commit_transaction();
        }
        catch (\Exception $e)
        {
            DB::rollback_transaction();
            Log::error(__METHOD__.'menu top image registration error: '.$e->getMessage());
            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

		$view->set('image',  $image);
		return $view;
	}
}
