<?php
/**
 * BRANCH: 支店編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Branch_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'branch/update/';
    const POST_URI_UPDATE_EXE = 'branch/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'branch';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('branch');
    }

    /**
     * カテゴリー編集：TOP
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $b_id = $this->param('b_id');
        if (empty($b_id) or ! is_numeric($b_id))
        {
            return Response::redirect('excpetion/404');
        }

        $branch = self::$_sponsor->contents()->branch_profile()->model_by_id($b_id);
        if (is_null($branch))
        {
            return Response::redirect('excpetion/404');
        }

        $fieldset = $this->get_fieldset();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $filedset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));
                $view->set('selected_pref', $this->get_pref_name($fieldset->validated('pref')));

                $file_upload = new \Support\File_Upload(self::get_user_id(), 'branch');
                if ($file_upload->upload_file_exists())
                {
                    try
                    {
                        DB::start_transaction();

                        $file_upload->save_tmp_file();

                        $branch->client_id      = self::get_user_id();
                        $branch->tmp_file_name  = $file_upload->tmp_file_name();
                        if ( ! $branch->save(false))
                        {
                            throw new Exception('branch registration error.');
                        }

                        DB::commit_transaction();

                    }
                    catch (\Exception $e)
                    {
                        Log::error($e->getMessage());
                        DB::rollback_transaction();
                        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                        $view->set_filename('exception/503');
                        return $view;
                    }

                    $view->set('img_upload_flg', true);
                }
                else
                {
                    $view->set('img_upload_flg', false);

                    if (empty($branch->file_name))
                    {
                        $view->set('message', '支店登録の際には画像を必ず指定してください');
                        $view->set_filename('exception/503');
                        return $view;
                    }

                    try
                    {
                        DB::start_transaction();

                        $branch->tmp_file_name = '';
                        if ( ! $branch->save(false))
                        {
                            throw new Exception('branch registration error.');
                        }
                        DB::commit_transaction();

                    }
                    catch (\Exception $e)
                    {
                        DB::rollback_transaction();
                        Log::error(__METHOD__.'branch registration error: '.$e->getMessage());
                        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                        $view->set_filename('exception/503');
                        return $view;
                    }
                }
            }
            else
            {
                $fieldset->populate($branch);
            }
        }
        else
        {
            $fieldset->populate($branch);
        }

        // jsロード：住所自動入力
        $this->load_js_auto_address($view);

        $view->set('branch',    $branch);
        $view->set('b_id',      $b_id);
        $view->set('fieldset',  $fieldset);
        return $view;
    }

    /**
     * カテゴリー編集：更新処理
     */
    public function action_exe()
    {
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

        $b_id = $this->param('b_id');
        if (empty($b_id) or ! is_numeric($b_id))
        {
            return Response::redirect('excpetion/404');
        }

        $branch = self::$_sponsor->contents()->branch_profile()->model_by_id($b_id);
        if (is_null($branch))
        {
            return Response::redirect('excpetion/404');
        }

        $fieldset = $this->get_fieldset();
	    $valid    = $fieldset->validation();

	    if ($valid->run())
	    {
	        $fields = $fieldset->validated();
	        $file_upload = new \Support\File_Upload(self::get_user_id(), 'branch');

	        try
	        {
	            DB::start_transaction();

	            if ( ! empty($branch->tmp_file_name))
	            {
	                $file_upload->save_file($branch->tmp_file_name);
	                $branch->file_name = $file_upload->file_name($branch->tmp_file_name);
	            }

                // Shop: GPS
                $address = '';
                if ( ! empty($fields['city']) and ! empty($fields['address_opt1']))
                {
                    $pref = $this->get_pref_name($fields['pref']);
                    $address = $pref.$fields['city'].$fields['address_opt1'];
                }
                $geo = self::$_sponsor->geocode($address);
	            // DBレコード登録
	            $branch->client_id     = self::get_user_id();
                $branch->shop_name     = $fields['shop_name'];
                $branch->email         = $fields['email'];
                $branch->tel1          = $fields['tel1'];
                $branch->tel2          = $fields['tel2'];
                $branch->tel3          = $fields['tel3'];
                $branch->fax1          = $fields['fax1'];
                $branch->fax2          = $fields['fax2'];
                $branch->fax3          = $fields['fax3'];
                $branch->zip_code1     = $fields['zip_code1'];
                $branch->zip_code2     = $fields['zip_code2'];
                $branch->pref          = $fields['pref'];
                $branch->city          = $fields['city'];
                $branch->address_opt1  = $fields['address_opt1'];
                $branch->address_opt2  = $fields['address_opt2'];
                $branch->url           = $fields['url'];
                $branch->online_shop   = $fields['online_shop'];
                $branch->open_hours    = $fields['open_hours'];
                $branch->holiday       = $fields['holiday'];
                $branch->lat           = $geo['lat'];
                $branch->lng           = $geo['lng'];
                $branch->tmp_file_name = '';
                $branch->enable        = $fields['enable'];
	            if ( ! $branch->save(false))
	            {
	                throw new Exception('branch registration error.');
	            }

	            DB::commit_transaction();
	        }
	        catch (Exception $e)
	        {
	            DB::rollback_transaction();
	            Log::error(__METHOD__.'branch update error: '.$e->getMessage());
	            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	            $view->set_filename('exception/503');
	            return $view;
	        }
	    }
	    else
	    {
	        Log::error(__METHOD__.'branch update error: '.$e->getMessage());
	        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
	        $view->set_filename('exception/503');
	        return $view;
	    }

	    $view->set('branch', $branch);
	    return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        //------------------------------------------
        // Required Entry
        //------------------------------------------
        // ショップ名称
        $fieldset->add('shop_name', 'お店の名前')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        //------------------------------------------
        // Optional Entry
        //------------------------------------------
        // メールアドレス
        $fieldset->add('email', 'メールアドレス')
            ->add_rule('valid_email')
            ->add_rule('max_length', '255');
        // TEL
        $fieldset->add('tel1', 'TEL', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '5');
        $fieldset->add('tel2', 'TEL(2)', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '5');
        $fieldset->add('tel3', 'TEL(3)', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '4');

        // FAX
        $fieldset->add('fax1', 'FAX', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '5');
        $fieldset->add('fax2', 'FAX(2)', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '5');
        $fieldset->add('fax3', 'FAX(3)', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '4');

        // 郵便番号
        $fieldset->add('zip_code1', '郵便番号', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('max_length', '3');
        $fieldset->add('zip_code2', '郵便番号(2)',
                array('onKeyUp' => 'AjaxZip2.zip2addr(\'zip_code1\',\'pref\',\'city\',\'zip_code2\',\'address_opt2\',\'address_opt1\');',
                    'class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('max_length', '4');

        // 住所：県(コード)
        $opt_pref = Model_Master_Prefectures::list_opt_array();
        $fieldset->add('pref', '都道府県', array('options' => $opt_pref, 'type' => 'select', 'class' => 'w_120'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('numeric_min', 1)
            ->add_rule('numeric_max', 47);

        // 市区町村
        $fieldset->add('city', '市区町村')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 住所（番地）
        $fieldset->add('address_opt1', '住所（番地）')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 住所（建物）
        $fieldset->add('address_opt2', '住所（建物）')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // ホームページURL
        $fieldset->add('url', 'ホームページアドレス')
            ->add_rule('valid_url');

        // オンラインショップURL
        $fieldset->add('online_shop', 'オンラインショップURL')
            ->add_rule('valid_url');

        // 営業時間
        $fieldset->add('open_hours', '営業時間')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 定休日
        $fieldset->add('holiday', '定休日')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 表示
        $opt = array('0' => '非表示', '1' => '表示');
        $fieldset->add('enable', '表示設定',
            array('options' => $opt, 'type' => 'radio'))
            ->add_rule('required');

        return $fieldset;
    }

}
