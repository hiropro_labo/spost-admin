<?php
/**
 * BRANCH: 支店の削除
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Branch_Del extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'branch/del/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'branch';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('branch');
    }

    /**
     * カテゴリー削除：TOP
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $b_id = $this->param('b_id');
        if (empty($b_id) or ! is_numeric($b_id))
        {
            return Response::redirect('excpetion/404');
        }

        $branch = self::$_sponsor->contents()->branch_profile()->model_by_id($b_id);
        if (is_null($branch))
        {
            return Response::redirect('excpetion/404');
        }

        $fieldset = $this->get_fieldset();
        $fieldset->populate($branch);

        $view->set('selected_pref', $this->get_pref_name($branch->pref));
        $view->set('branch',  $branch);
        $view->set('fieldset',  $fieldset);
        return $view;
    }

    /**
     * カテゴリー削除
     */
    public function action_exe()
    {
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

	    $this->check_only_post();

        $b_id = $this->param('b_id');
        if (empty($b_id) or ! is_numeric($b_id))
        {
            return Response::redirect('excpetion/404');
        }

        $branch = self::$_sponsor->contents()->branch_profile()->model_by_id($b_id);
        if (is_null($branch))
        {
            $view->set('message', '店舗情報の削除処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

        try
        {
            DB::start_transaction();

            // 画像ファイル削除
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'branch');
            $file_upload->delete_file($branch->file_name);

            // DBレコード削除
            $branch->delete();

            // ポジション再セット
            Model_Branch_Profile::recreate_position(self::get_user_id());

	        DB::commit_transaction();
        }
        catch (Exception $e)
        {
            DB::rollback_transaction();
            Log::error(__METHOD__.'branch delete error: '.$e->getMessage());
            $view->set('message', '支店データ削除の処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

	    return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        //------------------------------------------
        // Required Entry
        //------------------------------------------
        // ショップ名称
        $fieldset->add('shop_name', 'お店の名前')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        //------------------------------------------
        // Optional Entry
        //------------------------------------------
        // メールアドレス
        $fieldset->add('email', 'メールアドレス')
            ->add_rule('valid_email')
            ->add_rule('max_length', '255');
        // TEL
        $fieldset->add('tel1', 'TEL', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '5');
        $fieldset->add('tel2', 'TEL(2)', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '5');
        $fieldset->add('tel3', 'TEL(3)', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '4');

        // FAX
        $fieldset->add('fax1', 'FAX', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '5');
        $fieldset->add('fax2', 'FAX(2)', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '5');
        $fieldset->add('fax3', 'FAX(3)', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('min_length', '1')
            ->add_rule('max_length', '4');

        // 郵便番号
        $fieldset->add('zip_code1', '郵便番号', array('class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('max_length', '3');
        $fieldset->add('zip_code2', '郵便番号(2)',
                array('onKeyUp' => 'AjaxZip2.zip2addr(\'zip_code1\',\'pref\',\'city\',\'zip_code2\',\'address_opt2\',\'address_opt1\');',
                    'class' => 'w_60'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('max_length', '4');

        // 住所：県(コード)
        $opt_pref = Model_Master_Prefectures::list_opt_array();
        $fieldset->add('pref', '都道府県', array('options' => $opt_pref, 'type' => 'select', 'class' => 'w_120'))
            ->add_rule('valid_string', array('numeric', 'utf8'))
            ->add_rule('numeric_min', 1)
            ->add_rule('numeric_max', 47);

        // 市区町村
        $fieldset->add('city', '市区町村')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 住所（番地）
        $fieldset->add('address_opt1', '住所（番地）')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 住所（建物）
        $fieldset->add('address_opt2', '住所（建物）')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // ホームページURL
        $fieldset->add('url', 'ホームページアドレス')
            ->add_rule('valid_url');

        // オンラインショップURL
        $fieldset->add('online_shop', 'オンラインショップURL')
            ->add_rule('valid_url');

        // 営業時間
        $fieldset->add('open_hours', '営業時間')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 定休日
        $fieldset->add('holiday', '定休日')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 表示
        $opt = array('0' => '非表示', '1' => '表示');
        $fieldset->add('enable', '表示設定',
            array('options' => $opt, 'type' => 'radio'))
            ->add_rule('required');

        return $fieldset;
    }

}
