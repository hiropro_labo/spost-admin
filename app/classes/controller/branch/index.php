<?php
/**
 * MENU: 店舗情報の編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Branch_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'branch/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'branch';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('branch');
    }

    /**
     * TOPページ
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $list = self::$_sponsor->contents()->branch_profile()->list_all();

        $this->set_view_css($view, Asset::css('path/menu.css'));

        $view->set('list',   $list);
        return $view;
    }
}
