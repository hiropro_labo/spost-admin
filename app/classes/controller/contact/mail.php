<?php
use Fuel\Core\FuelException;
use Fuel\Core\Fieldset;

/**
 * クライアント：新規登録ページ
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Contact_Mail extends Basecontroller
{
    const VIEW_FILE_PREFIX        = 'contact/';
    const POST_URI_REGIST_CONFIRM = 'contact/mail';
    const POST_URI_REGIST_EXE     = 'contact/mail/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('contact');
    }

    /**
     * 登録情報入力ページ
     *
     * @access  public
     * @return  Response
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $fieldset    = $this->get_fieldset();
        $form_action = self::POST_URI_REGIST_CONFIRM;

        $agent = Model_User_Client::find_one_by('id', self::get_user_id());//パートナー管理のデータベースからログインしてる方の情報を取得①

        $view->set('agent',         $agent);
        //$view->set('fieldset',      $this->get_fieldset());

        $token_key = \Config::get('security.csrf_token_key');
        $token = \Security::fetch_token();

        $view->set('token_key', $token_key);
        $view->set('token', $token);

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $fieldset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $form_action = self::POST_URI_REGIST_EXE;

                //$panf_sub = 60 * Input::post('circulation');
                //$view->set('panf_sub',  $panf_sub);
            }
            else
            {
                $fieldset->repopulate();
            }
        }

        $view->set('fieldset', $fieldset);
        return $view;
    }

    /**
     * フォーム関連テスト用アクション
     */
    public function action_exe()
    {
        $this->check_only_post();

        $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

        $fieldset = $this->get_fieldset();
        $valid    = $fieldset->validation();

        if( ! \Security::check_token()){
            $view->set('message', 'すでに１度発行されています。');
            $view->set_filename('exception/503');
            return $view;
        }

        if ($valid->run())
        {
            $fields = $fieldset->validated();

            $agent = Model_User_Client::find_one_by('id', self::get_user_id());
            $panf_order = Model_Contact_Client::forge();//パートナーの管理情報のテーブル指定②
            $panf_order->client_id    = self::get_user_id();//パートナーID セッションで保存したログイン取得
            $panf_order->address    = $agent->email;//アドレス 指定したデータベーステーブルのどこの部分に保存するか指定
            $panf_order->title    = $fields['title'];//件名
            $panf_order->body    = $fields['body'];//本文
            $panf_order->status    = Model_Contact_Client::STATUS_UNREAD;//既読

            try
            {
                DB::start_transaction();

                if ( ! $result = $panf_order->save())
                {
                    throw new Exception('panf_order registratoin error');
                }

                DB::commit_transaction();

                // [ パンフレット発行手続きメール送信 ]
                // パートナー
                //$agent_model = Model_User_Agent::find_by_pk(self::get_user_id());
                $mail_title = $this->get_mail_title_regist('agent');
                $mail_body  = $this->get_mail_body_regist('agent');
                $mail_body  = str_replace('%%%username%%%', $agent->username, $mail_body);

                // メール送信 メールアドレス
                $this->send_mail($agent->email, $mail_title, $mail_body);//(アドレス,件名,本文) メールの作成

                // ヒロ企画
                $mail_title = $this->get_mail_title_regist('master');
                $mail_body  = $this->get_mail_body_regist('master');
                $mail_body  = str_replace('%%%agent_id%%%',   $agent->id,         $mail_body);//検索、置き換え、検索対象
                $mail_body  = str_replace('%%%username%%%',   $agent->username,   $mail_body);
                $mail_body  = str_replace('%%%body%%%',       $panf_order->body,   $mail_body);
                $mail_body  = str_replace('%%%title%%%',       $panf_order->title,   $mail_body);

                // メール送信
                $this->send_mail('support@hiropro.co.jp', $mail_title, $mail_body);//サポート担当に送信させるメールアドレス
            }
            catch (Exception $e)
            {
                DB::rollback_transaction();
                Log::error(__METHOD__.'panf_order registratoin error: '.$e->getMessage());
                $view->set('message', '登録処理中に予期せぬエラーが発生しました');
                $view->set_filename('exception/503');
                return $view;
            }



            $view->set('panf_order', $panf_order);
        }
        else
        {
            Log::error(__METHOD__.'panf_order registratoin error: '.$e->getMessage());
            $view->set('message', '登録処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

        return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        //------------------------------------------
        // Required Entry
        //------------------------------------------
        ////fieldsetの箱の中にフォームの内容を受け取る場所を作成して上記所でデータベースに挿入させてる
        $fieldset->add('title', '件名')
        ->add_rule('required')
        //->add_rule('valid_string', array('utf8'))
        ->add_rule('max_length', '255');

        $fieldset->add('body', '本文')
        ->add_rule('required')
        //->add_rule('valid_string', array('utf8'))//未入力だった時のエラー処理
        ->add_rule('max_length', '2000');
        //add_rule('パラメータで与えられた条件に満たすか検証', array('数字の文字列', 'utf8'));//未入力だった時のエラー処理

        return $fieldset;
    }


    /**
     * メール送信関係
     */
    private function send_mail($address, $title, $body)
    {
        // メール送信
        $email = \Email::forge();
        $email->from('support@hiropro.co.jp', 'iPost運営事務局');
        $email->to($address);
        $email->subject($title);
        $email->body($body);

        try
        {
            $email->send();
        }
        catch (EmailValidationFailedException $e)
        {
            Log::error($e->getMessage());
        }
        catch (EmailSendingFailedException $e)
        {
            Log::error($e->getMessage());
        }
    }

    private function get_mail_title_regist($type)
    {
        $title_agent  = "iPost運営事務局";
        $title_master = "お問い合わせが届きました";

        $valname = "title_".$type;
        return $$valname;
    }

    private function get_mail_body_regist($type)
    {
        $body_agent = <<< EOF
iPost運営事務局でございます。

氏名：%%%username%%% 様

お問い合わせ頂き有難うございます。

お問い合わせメールの送信が完了致しました。

-------------------------
 iPost運営事務局
 support@hiropro.co.jp

EOF;

        $body_master = <<< EOF
お問い合わせ内容

ID：%%%agent_id%%%
氏名：%%%username%%% 様

件名：%%%title%%%

本文：%%%body%%%

http://mgr.i-post.jp/

EOF;
        $valname = "body_".$type;
        return $$valname;
    }
}