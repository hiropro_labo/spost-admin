<?php
/**
 * Login Controller
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Auth extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'auth/';

    /**
     * This method gets called before the action is called
     */
    public function before_controller()
    {
        Lang::load('auth');
    }

    /**
     * ログインページ
     *
     * @access  public
     * @return  Response
     */
    public function action_login()
    {
        // ログイン済の場合はリダイレクト
        if ($this->login_check())
        {
            Response::redirect('http://admin.spost.jp/');
        }

        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $fieldset = $this->get_fieldset_login();

        // ログイン認証
        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $fields = $valid->validated();
                $auth   = LoginAuth\Auth::instance();
                if ($auth->login($fields['login_id'], $fields['password']))
                {
                    Response::redirect('/');
                }
                else
                {
                    $message = 'ログインID or パスワードが間違っています';
                    $view->set('message', $message, false);
                }
            }
        }

        $this->set_view_css($view, Asset::css('path/login.css'));
        $view->set('fieldset', $fieldset);
        return $view;
    }

    /**
     * ログアウト
     *
     * @access public
     * @return Response
     */
    public function action_logout()
    {
        if ($this->login_check())
        {
            LoginAuth\Auth::instance()->logout();
        }
        Response::redirect('/auth/login');
    }

    /**
     * パスワードリマインダー
     *
     * @access public
     * @return Response
     */
    public function action_reminder()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'reminder.tpl');

        $fieldset = $this->get_fieldset_reminder();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();
            if ($valid->run())
            {
                $fields = $valid->validated();
            }
        }

        return $view;
    }

    /**
     * Fieldset取得: ログイン認証用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset_login()
    {
        Lang::load('validationdesc');
        $fieldset   = Exfieldset::forge();

        // ログインID
        $fieldset->add('login_id', __('lbl_login_id'))
            ->add_rule('required')
            ->add_rule('valid_email')
            //->add_rule('valid_string', array('numeric', 'alpha', 'utf8', 'dots', 'dashes', 'punctuation'))
            //->add_rule('min_length', '6')
            ->add_rule('max_length', '255');
        // パスワード
        $fieldset->add('password', __('lbl_secret'))
            ->set_type('password')
            ->add_rule('required')
            ->add_rule('valid_string', array('numeric', 'alpha', 'utf8', 'dots', 'dashes', 'punctuation'))
            ->add_rule('min_length', '6')
            ->add_rule('max_length', '64');

        return $fieldset;
    }

    /**
     * Fieldset取得: パスワードリマインダー用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset_reminder()
    {
        Lang::load('validationdesc');
        $fieldset   = Exfieldset::forge();

        // メールアドレス
        $fieldset->add('email', 'メールアドレス')
            ->add_rule('required')
            ->add_rule('valid_email')
            ->add_rule('max_length', '255');

        return $fieldset;
    }
}