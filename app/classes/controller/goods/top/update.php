<?php
/**
 * GOODS: 商品トップ画像編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Goods_Top_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX      = 'goods/top/update/';
    const GOODS_IMAGE_FILE_NAME = 'goods_top.jpg';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'goods';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('goods');
    }

    /**
	 * GOODSトップ画像：画像選択
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		$view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

		$image = self::$_sponsor->contents()->goods()->model();
		if (is_null($image))
		{
		    $image = self::$_sponsor->contents()->goods()->model_new();
		}

		if (Input::method() == 'POST')
		{
		    $file_upload = new \Support\File_Upload(self::get_user_id(), 'goods_top');
		    if ($file_upload->upload_file_exists())
		    {
		        try
		        {
		            DB::start_transaction();
		            $file_upload->save_tmp_file();

		            $image->tmp_file_name = $file_upload->tmp_file_name();
		            if ( ! $image->save(false))
		            {
		                throw new Exception('goods top image registration error.');
		            }
		            DB::commit_transaction();
		        }
		        catch (\Exception $e)
		        {
		            Log::error($e->getMessage());
		            DB::rollback_transaction();
		            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
		            $view->set_filename('exception/503');
		            return $view;
		        }

		        $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
		    }
		    else
		    {
		        $view->set('message', '画像を指定してください');
		        $view->set_filename('exception/503');
		        return $view;
		    }
		}

		$view->set('image', $image);
		return $view;
	}

	/**
	 * GOODSトップ画像：登録処理
	 *
	 * @throws Exception
	 * @return unknown
	 */
	public function action_exe()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

        $this->check_only_post();

        try
        {
            DB::start_transaction();

            $image = self::$_sponsor->contents()->goods()->model();
            if (is_null($image))
            {
                $image = self::$_sponsor->contents()->goods()->model_new();
            }

            // ファイル名変更
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'goods_top');
            $file_upload->save_file($image->tmp_file_name, self::GOODS_IMAGE_FILE_NAME);

            // DBレコード更新
            $image->file_name = self::GOODS_IMAGE_FILE_NAME;
            if ( ! $image->save(false))
            {
                throw new Exception('goods top image registration error.');
            }

    	    DB::commit_transaction();
        }
        catch (\Exception $e)
        {
            DB::rollback_transaction();
            Log::error(__METHOD__.'goods top image registration error: '.$e->getMessage());
            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

		$view->set('image',  $image);
		return $view;
	}
}
