<?php
/**
 * GOODS：カテゴリー表示順の変更
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Goods_Category_Order extends Basecontroller
{
    const VIEW_FILE_PREFIX     = 'goods/category/order/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'goods';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        //static::$_support = \Support\Api\Menu::instance(self::get_user_id());
    }

    /**
	 * TOP画像：表示順UP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_up()
	{
        $position = $this->param('position');
        if (empty($position) or ! is_numeric($position))
        {
            return Response::redirect('excpetion/404');
        }

        try
        {
            self::$_sponsor->contents()->goods_category()->position_up($position);
        }
        catch (Exception $e)
        {
            return Response::redirect('excpetion/404');
        }

        Response::redirect('/goods');
	}

	/**
	 * TOP画像：表示順DOWN
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_down()
	{
        $position  = $this->param('position');
	    if (empty($position) or ! is_numeric($position))
        {
            return Response::redirect('excpetion/404');
        }

        try
        {
            $max_cnt = self::$_sponsor->contents()->goods_category()->count();
            self::$_sponsor->contents()->goods_category()->position_down($position, $max_cnt);
        }
        catch (Exception $e)
        {
            return Response::redirect('excpetion/404');
        }

        Response::redirect('/goods');
	}

}