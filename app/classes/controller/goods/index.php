<?php
/**
 * GOODS: 商品情報の編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Goods_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'goods/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'goods';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('goods');
    }

    /**
     * TOPページ
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        // メニュートップ画像
        $image = self::$_sponsor->contents()->goods()->model();
        if (is_null($image))
        {
            $image = self::$_sponsor->contents()->goods()->model_new();
        }

        // メニュー管理：カテゴリー一覧
        $category = self::$_sponsor->contents()->goods_category()->get_category_list_all();
        if (is_null($category)) $category = array();

        $list = array();
        foreach ($category as $cate)
        {
            $goods = self::$_sponsor->contents()->goods_item()->get_goods_list($cate->id);
            $cate->goods_max_cnt = self::$_sponsor->contents()->goods_item()->count_by_parent_id($cate->id);

            $entry = array();
            $entry['category'] = $cate;
            $entry['goods']    = $goods;
            array_push($list, $entry);
        }

        $this->set_view_css($view, Asset::css('path/menu.css'));

//         // チュートリアルの進行状況
//         $tutorial = \Support\Tutorial\Step::instance(self::get_user_id());
//         $view->set('tutorial_js', $tutorial->step_js('menu'), false);

        $view->set('image',  $image);
        $view->set('list',   $list);
        return $view;
    }
}
