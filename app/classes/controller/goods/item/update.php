<?php
/**
 * GOODS: 商品の編集
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Goods_Item_Update extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'goods/item/update/';
    const POST_URI_UPDATE_EXE = 'goods/item/update/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'goods';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('goods');
    }

    /**
     * 商品の編集：TOP
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $c_id = $this->param('c_id');
        if (empty($c_id) or ! is_numeric($c_id))
        {
            return Response::redirect('excpetion/404');
        }

        $item = self::$_sponsor->contents()->goods_item()->get_item($c_id);
        if (is_null($item))
        {
            return Response::redirect('excpetion/404');
        }

        $fieldset = $this->get_fieldset();

        if (Input::method() == 'POST')
        {
            $fieldset->repopulate();
            $valid = $fieldset->validation();

            if ($valid->run())
            {
                $filedset = $this->convert_fieldset_confirm($fieldset);
                $view->set_filename(self::VIEW_FILE_PREFIX.'confirm');
                $view->set_safe('html_fieldset', $fieldset->build(self::POST_URI_UPDATE_EXE));

                $category = self::$_sponsor->contents()->goods_category()->get_category($item->parent_id);
                $view->set('category', $category);

                $file_upload = new \Support\File_Upload(self::get_user_id(), 'goods_item');
                if ($file_upload->upload_file_exists())
                {
                    try
                    {
                        DB::start_transaction();

                        $file_upload->save_tmp_file();

                        $item->tmp_file_name  = $file_upload->tmp_file_name();
                        if ( ! $item->save(false))
                        {
                            throw new Exception('goods item update error.');
                        }

                        DB::commit_transaction();

                    }
                    catch (\Exception $e)
                    {
                        Log::error($e->getMessage());
                        DB::rollback_transaction();
                        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                        $view->set_filename('exception/503');
                        return $view;
                    }

                    $view->set('img_upload_flg', true);
                }
                else
                {
                    $view->set('img_upload_flg', false);

                    if (empty($item->file_name))
                    {
                        $view->set('message', '商品登録の際には画像を必ず指定してください');
                        $view->set_filename('exception/503');
                        return $view;
                    }

                    try
                    {
                        DB::start_transaction();

                        $item->tmp_file_name = '';
                        if ( ! $item->save(false))
                        {
                            throw new Exception('goods item registration error.');
                        }

                        DB::commit_transaction();
                    }
                    catch (\Exception $e)
                    {
                        DB::rollback_transaction();
                        Log::error(__METHOD__.'goods item registration error: '.$e->getMessage());
                        $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                        $view->set_filename('exception/503');
                        return $view;
                    }
                }
            }
        }
        else
        {
            $fieldset->populate($item);
            $fieldset->populate(array('parent_id' => $item->parent_id));
        }

        $this->set_view_js($view, Asset::js('menu/form.js'));

        $view->set('c_id',     $c_id);
        $view->set('item',     $item);
        $view->set('fieldset', $fieldset);
        return $view;
    }

    /**
     * 商品の編集：更新処理
     */
    public function action_exe()
    {
      $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

        $c_id = $this->param('c_id');
        if (empty($c_id) or ! is_numeric($c_id))
        {
            return Response::redirect('excpetion/404');
        }
        $item = self::$_sponsor->contents()->goods_item()->get_item($c_id);
        if (is_null($item))
        {
            return Response::redirect('excpetion/404');
        }

        $fieldset = $this->get_fieldset();
        $valid    = $fieldset->validation();

        if ($valid->run())
        {
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'goods_item');
            $fields = $fieldset->validated();

            try
            {
                DB::start_transaction();

                if ( ! empty($item->tmp_file_name))
                {
                    $file_upload->save_file($item->tmp_file_name);
                    $item->file_name = $file_upload->file_name($item->tmp_file_name);
                }

                // DBレコード登録
                $item->parent_id      = $fields['parent_id'];
                $item->title          = $fields['title'];
                $item->sub_title      = $fields['sub_title'];
                $item->description    = $fields['description'];
                $item->url            = $fields['url'];
                $item->tmp_file_name  = '';
                $item->price          = $fields['price'];
                $item->enable         = $fields['enable'];

                if ( ! $item->save(false))
                {
                    throw new Exception('goods item update error.');
                }

                DB::commit_transaction();
            }
            catch (Exception $e)
            {
                DB::rollback_transaction();
                Log::error(__METHOD__.'goods item update error: '.$e->getMessage());
                $view->set('message', '更新処理中に予期せぬエラーが発生しました');
                $view->set_filename('exception/503');
                return $view;
            }
        }
        else
        {
            Log::error(__METHOD__.'goods item update error: '.$e->getMessage());
            $view->set('message', '更新処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

        $view->set('item', $item);

        return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // カテゴリー
        $opt_cate = Model_Goods_Category::list_opt_array(self::get_user_id());
        $fieldset->add('parent_id', 'カテゴリー',
            array('options' => $opt_cate, 'type' => 'select'))
            ->add_rule('required')
            ->add_rule('valid_string', array('numeric'));

        // 商品名
        $fieldset->add('title', '商品名')
            ->add_rule('required')
            ->add_rule('max_length', '255');

        // サブタイトル名
        $fieldset->add('sub_title', 'サブタイトル')
            ->add_rule('required')
            ->add_rule('max_length', '255');

        // 商品説明
        $fieldset->add('description', '商品説明')
            ->set_type('textarea')
            ->add_rule('required');

        // 価格
        $fieldset->add('price', '価格', array('class' => 'w_100 txt_r'))
            ->add_rule('required')
            ->add_rule('valid_string', array('numeric'));

        // URL
        $fieldset->add('url', 'URL')
            ->add_rule('valid_url');

        // 表示
        $opt = array('0' => '非表示', '1' => '表示');
        $fieldset->add('enable', '表示設定',
            array('options' => $opt, 'type' => 'radio'))
            ->add_rule('required');

        return $fieldset;
    }
}
