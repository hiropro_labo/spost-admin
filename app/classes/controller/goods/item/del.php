<?php
/**
 * GOODS: 商品の削除
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Goods_Item_Del extends Basecontroller
{
    const VIEW_FILE_PREFIX    = 'goods/item/del/';
    const POST_URI_UPDATE_EXE = 'goods/del/exe';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'goods';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('goods');
    }

    /**
     * カテゴリー新規作成：TOP
     *
     * @return View
     */
    public function action_index()
    {
        $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $c_id = $this->param('c_id');
        if (empty($c_id) or ! is_numeric($c_id))
        {
            return Response::redirect('excpetion/404');
        }

        $item = self::$_sponsor->contents()->goods_item()->get_item($c_id);
        if (is_null($item))
        {
            return Response::redirect('exception/404');
        }

        $category = self::$_sponsor->contents()->goods_category()->get_category($item->parent_id);
        $view->set('category', $category);

        $fieldset = $this->get_fieldset();
        $fieldset->populate($item);

        $view->set('item',      $item);
        $view->set('fieldset',  $fieldset);
        return $view;
    }

    /**
     * カテゴリー新規作成：登録処理
     */
    public function action_exe()
    {
	    $view = View::forge(self::VIEW_FILE_PREFIX.'exe.tpl');

        $c_id = $this->param('c_id');
        if (empty($c_id) or ! is_numeric($c_id))
        {
            return Response::redirect('excpetion/404');
        }

        $item = self::$_sponsor->contents()->goods_item()->get_item($c_id);
        if (is_null($item))
        {
            $view->set('message', 'グッズ商品の削除処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

        try
        {
            DB::start_transaction();

            $parent_id = $item->parent_id;

            // 画像ファイル削除
            $file_upload = new \Support\File_Upload(self::get_user_id(), 'goods_item');
            $file_upload->delete_file($item->file_name);

            // DBレコード削除
            $item->delete();

            // ポジション再セット
            self::$_sponsor->contents()->goods_item()->item_recreate_position(self::get_user_id(), $parent_id);

            DB::commit_transaction();
        }
        catch (Exception $e)
        {
            DB::rollback_transaction();
            Log::error(__METHOD__.'goods item delete error: '.$e->getMessage());
            $view->set('message', 'グッズ商品の削除処理中に予期せぬエラーが発生しました');
            $view->set_filename('exception/503');
            return $view;
        }

	    return $view;
    }

    /**
     * Fieldset取得: 登録用
     *
     * @return Fuel\Core\Fieldset
     */
    private function get_fieldset()
    {
        Lang::load('validationdesc');

        $fieldset   = Exfieldset::forge();
        $validation = $fieldset->validation();
        $validation->add_callable('ExValidation');
        $fieldset->validation($validation);

        // カテゴリー
        $opt_cate = Model_Goods_Category::list_opt_array(self::get_user_id());
        $fieldset->add('parent_id', 'カテゴリー',
            array('options' => $opt_cate, 'type' => 'select'))
            ->add_rule('required')
            ->add_rule('valid_string', array('numeric'));

        // 商品名
        $fieldset->add('title', '商品名')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // サブタイトル名
        $fieldset->add('sub_title', 'サブタイトル')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'))
            ->add_rule('max_length', '255');

        // 商品説明
        $fieldset->add('description', '商品説明')
            ->add_rule('required')
            ->add_rule('valid_string', array('utf8'));

        // 価格
        $fieldset->add('price', '価格')
            ->add_rule('required')
            ->add_rule('valid_string', array('numeric'));

        // URL
        $fieldset->add('url', 'URL')
            ->add_rule('valid_url');

        // 表示
        $opt = array('0' => '非表示', '1' => '表示');
        $fieldset->add('enable', '表示設定',
            array('options' => $opt, 'type' => 'radio'))
            ->add_rule('required');

        return $fieldset;
    }
}
