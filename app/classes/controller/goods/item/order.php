<?php
/**
 * GOODS: GOODSページの商品画像表示順の変更
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Goods_Item_Order extends Basecontroller
{
    const VIEW_FILE_PREFIX     = 'goods/item/order/';

    /**
     * @var ログイン済ページフラグ
     */
    protected static $_logined_page = true;

    /**
     * @var カレントページ（※UI操作に使用)
     */
    protected static $_current_page = 'goods';

    /**
     * コントローラ前処理
     */
    protected function before_controller()
    {
        Lang::load('navigation');
        Lang::load('goods');
    }

    /**
	 * TOP画像：表示順UP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_up()
	{
        $position = $this->param('position');
        if (empty($position) or ! is_numeric($position))
        {
            return Response::redirect('excpetion/404');
        }

        $parent_id = $this->param('parent_id');
        if (empty($parent_id) or ! is_numeric($parent_id))
        {
            return Response::redirect('exception/404');
        }

        try
        {
            self::$_sponsor->contents()->goods_item()->item_position_up($parent_id, $position);
        }
        catch (Exception $e)
        {
            return Response::redirect('excpetion/404');
        }

        Response::redirect('/goods');
	}

	/**
	 * TOP画像：表示順DOWN
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_down()
	{
        $position  = $this->param('position');
	    if (empty($position) or ! is_numeric($position))
        {
            return Response::redirect('excpetion/404');
        }

        $parent_id = $this->param('parent_id');
        if (empty($parent_id) or ! is_numeric($parent_id))
        {
            return Response::redirect('excpetion/404');
        }

        try
        {
            $max_cnt = self::$_sponsor->contents()->goods_item()->count_by_parent_id($parent_id);
            self::$_sponsor->contents()->goods_item()->item_position_down($parent_id, $position, $max_cnt);
        }
        catch (Exception $e)
        {
            return Response::redirect('excpetion/404');
        }

        Response::redirect('/goods');
	}
}