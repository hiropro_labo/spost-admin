<?php
/**
 * GOODS: おすすめ処理
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Goods_Item_Pickup extends Basecontroller_Rest
{
    /**
	 * メニュー商品：おすすめ処理
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
	    $data = array(true, "");

	    try
	    {
	        DB::start_transaction();

	        $id = $this->param('c_id');
	        if (empty($id) or ! is_numeric($id))
	        {
	            return Response::redirect('excpetion/404');
	        }

	        $model = self::$_sponsor->contents()->goods_item()->get_item($id);
	        if (is_null($model))
	        {
	            throw new Exception('menu pickup update failed. model not found.');
	        }

	        $flg = intval($model->pickup) == 0 ? 1 : 0;

	        $model->pickup = $flg;
	        if ( ! $model->save(false))
	        {
	            throw new Exception("更新データの保存に失敗しました");
	        }

	        $data[1] = $flg == 0 ? '☆' : '★';

	        DB::commit_transaction();
	    }
	    catch (\Exception $e)
	    {
	        DB::rollback_transaction();
	        Log::error("company data regist failed.   ".$e->getMessage());
	        $data = array(false, $e->getMessage());
	    }

	    $json = json_encode($data);

	    $this->response->set_header('Content-Type', 'application/json; charset=utf-8');
	    return $this->response->body($json);
	}
}