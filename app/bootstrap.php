<?php

// Load in the Autoloader
require COREPATH.'classes'.DIRECTORY_SEPARATOR.'autoloader.php';
class_alias('Fuel\\Core\\Autoloader', 'Autoloader');

// Bootstrap the framework DO NOT edit this
require COREPATH.'bootstrap.php';


Autoloader::add_classes(array(
	// Add classes you want to override here
	// Example: 'View' => APPPATH.'classes/view.php',
    'Basecontroller'               => APPPATH.'classes/lib/basecontroller.php',
    'Basecontroller_Rest'          => APPPATH.'classes/lib/basecontroller_rest.php',
	'ExValidation'                 => SUPPORTPATH.'exvalidation.php',
	'Fuel\\Core\\Exfieldset'       => SUPPORTPATH.'fieldset.php',
	'Model_Crud_Shard'             => SUPPORTPATH.'models/model_crud_shard.php',
	'Model_Crud_Shard_Payment'     => SUPPORTPATH.'models/model_crud_shard_payment.php',
	'Model_Crud_Shard_Image'       => SUPPORTPATH.'models/model_crud_shard_image.php',
	'Model_Crud_Shard_Token'       => SUPPORTPATH.'models/model_crud_shard_token.php',
    'Model_Crud_Master'            => SUPPORTPATH.'models/model_crud_master.php',
	'Model_Readonly'               => SUPPORTPATH.'models/model_readonly.php',
	'Jpayment'                     => SUPPORTPATH.'jpayment.php',
	'Support\\File_Upload'         => SUPPORTPATH.'file_upload.php',
    'Support\\Post_Mail'           => SUPPORTPATH.'post_mail.php',

	// Supported Classes
    'Notification'                 => SUPPORTPATH.'notification.php',
    'Support\\Payment'             => SUPPORTPATH.'payment.php',
    'Support\\Shard'               => SUPPORTPATH.'shard.php',
    'Branch'                       => SUPPORTPATH.'branch.php',
    'Theme'                        => SUPPORTPATH.'theme.php',
    'Sponsor'                      => SUPPORTPATH.'sponsor.php',
    'Team'                         => SUPPORTPATH.'team.php',

    // Supported Classes: Contents
    'Support\\Contents\\Base'             => SUPPORTPATH.'contents/base.php',
    'Support\\Contents\\Image'            => SUPPORTPATH.'contents/image.php',
    'Support\\Contents\\Banner'           => SUPPORTPATH.'contents/banner.php',
    'Support\\Contents\\Branch'           => SUPPORTPATH.'contents/branch.php',
    'Support\\Contents\\Coupon'           => SUPPORTPATH.'contents/coupon.php',
    'Support\\Contents\\Icon'             => SUPPORTPATH.'contents/icon.php',
    'Support\\Contents\\News'             => SUPPORTPATH.'contents/news.php',
    'Support\\Contents\\Topics'           => SUPPORTPATH.'contents/topics.php',
    'Support\\Contents\\Theme'            => SUPPORTPATH.'contents/theme.php',
    'Support\\Contents\\Url'              => SUPPORTPATH.'contents/url.php',
    'Support\\Contents\\Profile'          => SUPPORTPATH.'contents/profile.php',
    'Support\\Contents\\Profile\\History' => SUPPORTPATH.'contents/profile/history.php',
    'Support\\Contents\\Company'          => SUPPORTPATH.'contents/company.php',
    'Support\\Contents\\Company\\Name'    => SUPPORTPATH.'contents/company/name.php',
    'Support\\Contents\\Shop\\Profile'    => SUPPORTPATH.'contents/shop/profile.php',
    'Support\\Contents\\Menu\\Category'   => SUPPORTPATH.'contents/menu/category.php',
    'Support\\Contents\\Menu\\Item'       => SUPPORTPATH.'contents/menu/item.php',
    'Support\\Contents\\Goods\\Category'  => SUPPORTPATH.'contents/goods/category.php',
    'Support\\Contents\\Goods\\Item'      => SUPPORTPATH.'contents/goods/item.php',
    'Support\\Contents\\Recruit\\Info'    => SUPPORTPATH.'contents/recruit/info.php',
    'Support\\Contents\\Recruit\\Title'   => SUPPORTPATH.'contents/recruit/title.php',

    // Supported Classes: Contents for Sponsor
    'Support\\Contents\\Status\\Sponsor'           => SUPPORTPATH.'contents/status/sponsor.php',
    'Support\\Contents\\Status\\Sponsor\\Branch'   => SUPPORTPATH.'contents/status/sponsor/branch.php',
    'Support\\Contents\\Status\\Sponsor\\Company'  => SUPPORTPATH.'contents/status/sponsor/company.php',
    'Support\\Contents\\Status\\Sponsor\\Coupon'   => SUPPORTPATH.'contents/status/sponsor/coupon.php',
    'Support\\Contents\\Status\\Sponsor\\Goods'    => SUPPORTPATH.'contents/status/sponsor/goods.php',
    'Support\\Contents\\Status\\Sponsor\\Menu'     => SUPPORTPATH.'contents/status/sponsor/menu.php',
    'Support\\Contents\\Status\\Sponsor\\News'     => SUPPORTPATH.'contents/status/sponsor/news.php',
    'Support\\Contents\\Status\\Sponsor\\Profile'  => SUPPORTPATH.'contents/status/sponsor/profile.php',
    'Support\\Contents\\Status\\Sponsor\\Recruit'  => SUPPORTPATH.'contents/status/sponsor/recruit.php',
    'Support\\Contents\\Status\\Sponsor\\Segments' => SUPPORTPATH.'contents/status/sponsor/segments.php',
    'Support\\Contents\\Status\\Sponsor\\Shop'     => SUPPORTPATH.'contents/status/sponsor/shop.php',
    'Support\\Contents\\Status\\Sponsor\\Store'    => SUPPORTPATH.'contents/status/sponsor/store.php',
    'Support\\Contents\\Status\\Sponsor\\Top'      => SUPPORTPATH.'contents/status/sponsor/top.php',
    'Support\\Contents\\Status\\Sponsor\\Topics'   => SUPPORTPATH.'contents/status/sponsor/topics.php',

    // Supported Classes: Sponsor
    'Support\\Sponsor\\App'                            => SUPPORTPATH.'sponsor/app.php',
    'Support\\Sponsor\\App\\Inspect'                   => SUPPORTPATH.'sponsor/app/inspect.php',
    'Support\\Sponsor\\App\\Contents'                  => SUPPORTPATH.'sponsor/app/contents.php',
    'Support\\Sponsor\\App\\Contents\\Status\\Base'    => SUPPORTPATH.'sponsor/app/contents/status/base.php',
    'Support\\Sponsor\\App\\Contents\\Status\\Status1' => SUPPORTPATH.'sponsor/app/contents/status/status1.php',
    'Support\\Sponsor\\App\\Contents\\Status\\Status2' => SUPPORTPATH.'sponsor/app/contents/status/status2.php',
    'Support\\Sponsor\\App\\Contents\\Status\\Status3' => SUPPORTPATH.'sponsor/app/contents/status/status3.php',
    'Support\\Sponsor\\App\\Contents\\Status\\Status4' => SUPPORTPATH.'sponsor/app/contents/status/status4.php',
    'Support\\Sponsor\\App\\Contents\\Store'           => SUPPORTPATH.'sponsor/app/contents/store.php',
    'Support\\Sponsor\\App\\Contents\\Switcher'        => SUPPORTPATH.'sponsor/app/contents/switcher.php',


    // Supported Classes: API
    'Support\\Api\\Base'           => SUPPORTPATH.'api/base.php',
    'Support\\Api\\Top'            => SUPPORTPATH.'api/top.php',
    'Support\\Api\\Menu'           => SUPPORTPATH.'api/menu.php',
    'Support\\Api\\Menu_Item'      => SUPPORTPATH.'api/menu_item.php',
    'Support\\Api\\Coupon'         => SUPPORTPATH.'api/coupon.php',
    'Support\\Api\\News'           => SUPPORTPATH.'api/news.php',
    'Support\\Api\\Setting'        => SUPPORTPATH.'api/setting.php',
    'Support\\Api\\Global_Info'    => SUPPORTPATH.'api/global_info.php',
    'Support\\Api\\Branch'         => SUPPORTPATH.'api/branch.php',
    'Support\\Api\\Theme'          => SUPPORTPATH.'api/theme.php',
    'Support\\Api\\Switcher'       => SUPPORTPATH.'api/switcher.php',

    // Supported Classes: Inspect
    'Support\\Inspect\\Base'     => SUPPORTPATH.'inspect/base.php',
    'Support\\Inspect\\Top'      => SUPPORTPATH.'inspect/top.php',
    'Support\\Inspect\\Coupon'   => SUPPORTPATH.'inspect/coupon.php',
    'Support\\Inspect\\Menu'     => SUPPORTPATH.'inspect/menu.php',
    'Support\\Inspect\\Reject\\Apple' => SUPPORTPATH.'inspect/reject/apple.php',

    // Supported Classes: Root
    'Support\\Root\\Status'      => SUPPORTPATH.'root/status.php',
    'Support\\Root\\Infomation'  => SUPPORTPATH.'root/infomation.php',
    'Support\\Tutorial\\Step'    => SUPPORTPATH.'tutorial/step.php',

    // ApnsPHP
    'ApnsPHP_Log_Interface' => APPPATH.'vendor/ApnsPHP/Log/Interface.php',
    'ApnsPHP_Log_Embedded'  => APPPATH.'vendor/ApnsPHP/Log/Embedded.php',
    'ApnsPHP_Abstract'      => APPPATH.'vendor/ApnsPHP/Abstract.php',
    'ApnsPHP_Exception'     => APPPATH.'vendor/ApnsPHP/Exception.php',
    'ApnsPHP_Push'          => APPPATH.'vendor/ApnsPHP/Push.php',
    'ApnsPHP_Message'       => APPPATH.'vendor/ApnsPHP/Message.php',
));

// Register the autoloader
Autoloader::register();

/**
 * Your environment.  Can be set to any of the following:
 *
 * Fuel::DEVELOPMENT
 * Fuel::TEST
 * Fuel::STAGING
 * Fuel::PRODUCTION
 */
Fuel::$env = (isset($_SERVER['FUEL_ENV']) ? $_SERVER['FUEL_ENV'] : Fuel::STAGING);

// Initialize the framework with the config file.
Fuel::init('config.php');

// Languages
$languages = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
if (isset($languages[0]) && preg_match('/^en/i', $languages[0])) {
    Config::set('language', 'en');
} else  {
    Config::set('language', 'ja');
}
