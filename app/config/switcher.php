<?php
/**
 * 各種設定ファイル: ショップ関連
 *
 * ・アプリ内の各所で利用する汎用的な設定項目は本ファイルへ記述
 * ・config.phpと分ける目的で本ファイルを設置
 * ・設定内容に特に決まりはないがパッケージ化したものなどはそちらの専用ファイルへ記述
 */

return array(
    //=================================
    // カテゴリー1: 飲食
    //=================================
    "category_1" => array(
        // TOP
        array(
            // コントローラキー値(※アプリサイドで参照）
            'controller' => 'top',
            // 表示順
            'position'   => 100,
            // 利用必須: 0:任意/1:必須
            'require'    => 1,
            // メニュー名称
            'name'       => 'TOP',
            // メニューアイコンファイル名
            'icon'       => 'icon_top.png',
            // ビュータイプ：0:Webview/1:ネイティブ
            'view_type'  => 0
        ),
        // NEWS
        array(
            'controller' => 'news',
            'position'   => 200,
            'require'    => 1,
            'name'       => 'NEWS',
            'icon'       => 'icon_news.png',
            'view_type'  => 1
        ),
        // SHOP
        array(
            'controller' => 'shop',
            'position'   => 300,
            'require'    => 1,
            'name'       => 'SHOP',
            'icon'       => 'icon_shop.png',
            'view_type'  => 0
        ),
        // BRANCH
        array(
            'controller' => 'branch',
            'position'   => 310,
            'require'    => 0,
            'name'       => 'BRANCH',
            'icon'       => 'icon_branch.png',
            'view_type'  => 0
        ),
        // COUPON
        array(
            'controller' => 'coupon',
            'position'   => 400,
            'require'    => 0,
            'name'       => 'COUPON',
            'icon'       => 'icon_coupon.png',
            'view_type'  => 0
        ),
        // MENU
        array(
            'controller' => 'menu',
            'position'   => 500,
            'require'    => 1,
            'name'       => 'MENU',
            'icon'       => 'icon_menu.png',
            'view_type'  => 0
        ),
        // URL
        // ※動的設定の為、記述していない

        // RECRUIT
        array(
            'controller' => 'recruit',
            'position'   => 800,
            'require'    => 0,
            'name'       => 'RECRUIT',
            'icon'       => 'icon_recruit.png',
            'view_type'  => 0
        ),
        // TEL
        // ※動的設定の為、記述していない

        // MAIL
        // ※動的設定の為、記述していない

        // SETTING
        array(
            'controller' => 'setting',
            'position'   => 9999,
            'require'    => 1,
            'name'       => 'SETTING',
            'icon'       => 'icon_setting.png',
            'view_type'  => 1
        )
    ),
    //=================================
    // カテゴリー2: 物販
    //=================================
    "category_2" => array(
        // TOP
        array(
            // コントローラキー値(※アプリサイドで参照）
            'controller' => 'top',
            // 表示順
            'position'   => 100,
            // 利用必須: 0:任意/1:必須
            'require'    => 1,
            // メニュー名称
            'name'       => 'TOP',
            // メニューアイコンファイル名
            'icon'       => 'icon_top.png',
            // ビュータイプ：0:Webview/1:ネイティブ
            'view_type'  => 0
        ),
        // NEWS
        array(
            'controller' => 'news',
            'position'   => 200,
            'require'    => 1,
            'name'       => 'NEWS',
            'icon'       => 'icon_news.png',
            'view_type'  => 1
        ),
        // SHOP
        array(
            'controller' => 'shop',
            'position'   => 300,
            'require'    => 1,
            'name'       => 'SHOP',
            'icon'       => 'icon_shop.png',
            'view_type'  => 0
        ),
        // BRANCH
        array(
            'controller' => 'branch',
            'position'   => 310,
            'require'    => 0,
            'name'       => 'BRANCH',
            'icon'       => 'icon_branch.png',
            'view_type'  => 0
        ),
        // COUPON
        array(
            'controller' => 'coupon',
            'position'   => 400,
            'require'    => 0,
            'name'       => 'COUPON',
            'icon'       => 'icon_coupon.png',
            'view_type'  => 0
        ),
        // GOODS
        array(
            'controller' => 'goods',
            'position'   => 510,
            'require'    => 1,
            'name'       => 'GOODS',
            'icon'       => 'icon_goods.png',
            'view_type'  => 0
        ),
        // URL
        // ※動的設定の為、記述していない

        // RECRUIT
        array(
            'controller' => 'recruit',
            'position'   => 800,
            'require'    => 0,
            'name'       => 'RECRUIT',
            'icon'       => 'icon_recruit.png',
            'view_type'  => 0
        ),
        // TEL
        // ※動的設定の為、記述していない

        // MAIL
        // ※動的設定の為、記述していない

        // SETTING
        array(
            'controller' => 'setting',
            'position'   => 9999,
            'require'    => 1,
            'name'       => 'SETTING',
            'icon'       => 'icon_setting.png',
            'view_type'  => 1
        )
    ),
    //=================================
    // カテゴリー3: 企業
    //=================================
    "category_3" => array(
        // TOP
        array(
            // コントローラキー値(※アプリサイドで参照）
            'controller' => 'top',
            // 表示順
            'position'   => 100,
            // 利用必須: 0:任意/1:必須
            'require'    => 1,
            // メニュー名称
            'name'       => 'TOP',
            // メニューアイコンファイル名
            'icon'       => 'icon_top.png',
            // ビュータイプ：0:Webview/1:ネイティブ
            'view_type'  => 0
        ),
        // NEWS
        array(
            'controller' => 'news',
            'position'   => 200,
            'require'    => 1,
            'name'       => 'NEWS',
            'icon'       => 'icon_news.png',
            'view_type'  => 1
        ),
        // TOPICS
        array(
            'controller' => 'topics',
            'position'   => 210,
            'require'    => 0,
            'name'       => 'TOPICS',
            'icon'       => 'icon_topics.png',
            'view_type'  => 0
        ),
        // COMPANY
        array(
            'controller' => 'company',
            'position'   => 300,
            'require'    => 1,
            'name'       => 'COMPANY',
            'icon'       => 'icon_company.png',
            'view_type'  => 0
        ),
        // SEGMENTS
        array(
            'controller' => 'segments',
            'position'   => 310,
            'require'    => 1,
            'name'       => 'SEGMENTS',
            'icon'       => 'icon_segments.png',
            'view_type'  => 0
        ),
        // URL
        // ※動的設定の為、記述していない

        // RECRUIT
        array(
            'controller' => 'recruit',
            'position'   => 800,
            'require'    => 0,
            'name'       => 'RECRUIT',
            'icon'       => 'icon_recruit.png',
            'view_type'  => 0
        ),
        // TEL
        // ※動的設定の為、記述していない

        // MAIL
        // ※動的設定の為、記述していない

        // SETTING
        array(
            'controller' => 'setting',
            'position'   => 9999,
            'require'    => 1,
            'name'       => 'SETTING',
            'icon'       => 'icon_setting.png',
            'view_type'  => 1
        )
    ),
    //=================================
    // カテゴリー4: 議員
    //=================================
    "category_4" => array(
        // TOP
        array(
        // コントローラキー値(※アプリサイドで参照）
        'controller' => 'top',
        // 表示順
        'position'   => 100,
        // 利用必須: 0:任意/1:必須
        'require'    => 1,
        // メニュー名称
        'name'       => 'TOP',
        // メニューアイコンファイル名
        'icon'       => 'icon_top.png',
        // ビュータイプ：0:Webview/1:ネイティブ
        'view_type'  => 0
        ),
        // NEWS: 活動・行事予定
        array(
            'controller' => 'news',
            'position'   => 200,
            'require'    => 1,
            'name'       => 'NEWS',
            'icon'       => 'icon_news.png',
            'view_type'  => 1
        ),
        // PROFILE: プロフィール
        array(
            'controller' => 'profile',
            'position'   => 320,
            'require'    => 1,
            'name'       => 'PROFILE',
            'icon'       => 'icon_profile.png',
            'view_type'  => 0
        ),
        // URL
        // ※動的設定の為、記述していない

        // TEL
        // ※動的設定の為、記述していない

        // MAIL
        // ※動的設定の為、記述していない

        // SETTING
        array(
            'controller' => 'setting',
            'position'   => 9999,
            'require'    => 1,
            'name'       => 'SETTING',
            'icon'       => 'icon_setting.png',
            'view_type'  => 1
        )
    ),
);
