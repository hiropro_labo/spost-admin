<?php
/**
 * 各種設定ファイル
 *
 * ・アプリ内の各所で利用する汎用的な設定項目は本ファイルへ記述
 * ・config.phpと分ける目的で本ファイルを設置
 * ・設定内容に特に決まりはないがパッケージ化したものなどはそちらの専用ファイルへ記述
 */

return array(
    /**
     * SSH:設定
     *     host: domain or IP Address
     */
    'not_preview' => array(
        'pop',
        'payment',
        'inspect',
        'help',
        'branch',
        'appoint',
        'support_help',
        'switcher',
        'category',
        'shop',
        'url',
        'recruit',
        'company',
        'segments',
        'topics',
        'profile'
    ),
);
