<?php
/**
 * ファイルアップロード設定ファイル
 * ※Upload::processへセットする為の設定用Arrayを定義
 */
return array(
    /*
     * アイコン画像用
     */
    'icon' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/store/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.%%%extension%%%',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 1024, 'h' => 1024),
    ),

    /*
     * アプリステータスバー表示用アイコン
     */
    'app_icon' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/icon/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.%%%extension%%%',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('h' => 64),
    ),

    /*
     * ショップ画像用
     */
    'shop' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/shop/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 640, 'h' => 420),
    ),

    /*
     * TOPカルーセル画像
     */
    'top_recommend' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/top/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 640, 'h' => 420),
    ),

    /*
     * クーポン画像
     */
    'coupon' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/coupon/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 560, 'h' => 600),
    ),

    /*
     * 企業概要画像
     */
    'company_top' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/company/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 640, 'h' => 280),
    ),

    /*
     * 事業内容: Work
     */
    'segments_work' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/segments/work/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 640, 'h' => 160),
    ),

    /*
     * 事業内容: Policy
     */
    'segments_policy' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/segments/policy/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 640, 'h' => 160),
    ),

        /*
     * 事業内容: Message
     */
    'segments_message' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/segments/message/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 640, 'h' => 160),
    ),

/*
     * MENU: トップ画像
     */
    'menu_top' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/menu/top/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 640, 'h' => 350),
    ),

    /*
     * MENU: カテゴリー画像
     */
    'menu_category' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/menu/cate/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 640, 'h' => 350),
    ),

    /*
     * MENU: 商品画像
     */
    'menu_item' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/menu/item/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 560, 'h' => 320),
    ),

    /*
     * GOODS: トップ画像
     */
    'goods_top' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/goods/top/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 288, 'h' => 288),
    ),

    /*
     * GOODS: カテゴリー画像
     */
    'goods_category' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/goods/cate/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 288, 'h' => 288),
        // サムネイル画像サイズ
        'thumb_size'    => array('w' => 288, 'h' => 288),
    ),

    /*
     * GOODS: 商品画像
     */
    'goods_item' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/goods/item/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 288, 'h' => 288),
        // サムネイル画像サイズ
        'thumb_size'    => array('w' => 288, 'h' => 288),
    ),

    /*
     * NEWS
     */
    'news' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/news/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 640, 'h' => 280),
        // サムネイル画像サイズ
        'thumb_size'    => array('w' => 240, 'h' => 240),
    ),

    /*
     * TOPICS
     */
    'topics' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/topics/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 640, 'h' => 280),
        // サムネイル画像サイズ
        'thumb_size'    => array('w' => 240, 'h' => 240),
    ),

    /*
     * スクリーンショット画像（１）※最大５枚まで
    */
    'screen_shot1' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/screen_shot1/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 640, 'h' => 1136),
    ),

    /*
     * スクリーンショット画像（２）※最大５枚まで
    */
    'screen_shot2' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/screen_shot2/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 640, 'h' => 960),
    ),

    /*
     * スプラッシュ画像
    */
    'splash' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/splash/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 640, 'h' => 1136),
    ),

    /*
     * 支店画像用
    */
    'branch' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/branch/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.jpg',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 640, 'h' => 350),
    ),

    /*
     * プロフィール用画像（※議員アプリ用）
     */
    'profile' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/profile/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.%%%extension%%%',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 240, 'h' => 240),
    ),

    /*
     * バナー画像（※議員アプリ用）
    */
    'banner' => array(
        // アップロードキー(form)
        'upload_key'    => 'upload',
        // アップ画像：保存先ディレクトリ
        'save_dir'      => DOCROOT.'assets/img/app/banner/%%%client_id%%%/',
        // 保存ファイル名
        'file_name'     => '%%%time_stamp%%%.%%%extension%%%',
        // MAXサイズ(byte)
        'max_size'      => 1024 * 1000 * 3,
        // リサイズ
        'resize'        => array('w' => 640, 'h' => 142),
    ),
);

