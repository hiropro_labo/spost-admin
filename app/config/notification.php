<?php
/**
 * 設定ファイル：PUSH通知
 */

return array(

    /**
     * Entrust Root Certification Authority　保存パス
     */
    'entrust_root_cert_pem' => '/home/spost/admin/app/pem/root/entrust_root_certification_authority.pem',

    /**
     * GCM Send URL
     */
    'gcm_send_url' => 'https://android.googleapis.com/gcm/send',

    /**
     * GCM API Key
     */
    'gcm_api_key' => 'AIzaSyC9X9KJyxTVepx7DXOFmD_pn4VN7UGOwH4',

    /**
     * クライアント用pemファイル保存ディレクトリ
     */
    'pem_save_dir' => '/home/spost/admin/app/pem/client/',

    /**
     * 1回のPUSH通知で処理する件数
     */
    'per_limit'   => 10000,
);
