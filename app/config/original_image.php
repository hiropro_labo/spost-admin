<?php
/**
 * 画像処理の設定値
 */
return array(
    /**
     * アプリ参照画像：保存先ルートディレクトリパス
     */
    'app_img_root' => array(
        // ショップ
        'shop'             => '/assets/img/app/shop/',
        // TOP: スライド画像
        'top_recommend'    => '/assets/img/app/top/',
        // COUPON: クーポン画像
        'coupon'           => '/assets/img/app/coupon/',
        // COMPANY: 企業概要画像
        'company_top'      => '/assets/img/app/company/',
        // SEGMENTS: 事業内容 Work
        'segments_work'    => '/assets/img/app/segments/work/',
        // SEGMENTS: 事業内容 Policy
        'segments_policy'  => '/assets/img/app/segments/policy/',
        // SEGMENTS: 事業内容 Message
        'segments_message' => '/assets/img/app/segments/message/',
        // MENU: トップ画像
        'menu_top'         => '/assets/img/app/menu/top/',
        // MENU: カテゴリー画像
        'menu_category'    => '/assets/img/app/menu/cate/',
        // MENU: 商品詳細画像
        'menu_item'        => '/assets/img/app/menu/item/',
        // GOODS: トップ画像
        'goods_top'        => '/assets/img/app/goods/top/',
        // GOODS: カテゴリー画像
        'goods_category'   => '/assets/img/app/goods/cate/',
        // GOODS: 商品詳細画像
        'goods_item'       => '/assets/img/app/goods/item/',
        // NEWS
        'news'             => '/assets/img/app/news/',
        // TOPICS
        'topics'           => '/assets/img/app/topics/',
        // ICON(※アプリ内タイトルバー表示）
        'app_icon'         => '/assets/img/app/icon/',
        // ICON
        'icon'             => '/assets/img/app/store/',
        // スクーンショット１
        'screen_shot1'     => '/assets/img/app/screen_shot1/',
        // スクーンショット１
        'screen_shot2'     => '/assets/img/app/screen_shot2/',
        // スプラッシュ画像
        'splash'           => '/assets/img/app/splash/',
        // 支店
        'branch'           => '/assets/img/app/branch/',
        // PROFILE: プロフィール用画像（※議員アプリ用）
        'profile'          => '/assets/img/app/profile/',
        // BANNER
        'banner'           => '/assets/img/app/banner/',
    ),

    /**
     * imageパス: common
     */
    'img_common_path' => '/assets/img/common/',

    /**
     * アップロード画像保存先ディレクトリ
     * ※実際には以下のパス配下のクライアントIDのディレクトリ配下へ画像が保存される
     * ・・・/shop/[client-id]/◯◯◯.jpg
     */
    'save_dir' => array(
        // ショップ
        'shop'             => DOCROOT.'assets/img/app/shop/',
        // TOP: スライド画像
        'top_recommend'    => DOCROOT.'assets/img/app/top/',
        // COUPON: クーポン画像
        'coupon'           => DOCROOT.'assets/img/app/coupon/',
        // COMPANY: 企業概要画像
        'company_top'      => DOCROOT.'assets/img/app/company/',
        // SEGMENTS: 事業内容 Work
        'segments_work'    => DOCROOT.'assets/img/app/segments/work/',
        // SEGMENTS: 事業内容 Policy
        'segments_policy'  => DOCROOT.'assets/img/app/segments/policy/',
        // SEGMENTS: 事業内容 Message
        'segments_message' => DOCROOT.'assets/img/app/segments/message/',
        // MENU: トップ画像
        'menu_top'         => DOCROOT.'assets/img/app/menu/top/',
        // MENU: カテゴリー画像
        'menu_category'    => DOCROOT.'assets/img/app/menu/cate/',
        // MENU: 商品詳細画像
        'menu_item'        => DOCROOT.'assets/img/app/menu/item/',
        // GOODS: トップ画像
        'goods_top'        => DOCROOT.'assets/img/app/goods/top/',
        // GOODS: カテゴリー画像
        'goods_category'   => DOCROOT.'assets/img/app/goods/cate/',
        // GOODS: 商品詳細画像
        'goods_item'       => DOCROOT.'assets/img/app/goods/item/',
        // NEWS
        'news'             => DOCROOT.'assets/img/app/news/',
        // TOPICS
        'topics'           => DOCROOT.'assets/img/app/topics/',
        // ICON(※アプリ内タイトルバー表示）
        'app_icon'         => DOCROOT.'assets/img/app/icon/',
        // ICON
        'icon'             => DOCROOT.'assets/img/app/store/',
        // 支店
        'branch'           => DOCROOT.'assets/img/app/branch/',
        // PROFILE: プロフィール用画像（※議員アプリ用）
        'pofile'           => DOCROOT.'assets/img/app/profile/',
        // BANNER
        'banner'           => DOCROOT.'assets/img/app/banner/',
        ),

    /**
     * 各種ブランク画像ファイル名
     * ※保存先ディレクトリ：img_common_path直下
     */
    'img_blank' => array(
        'shop'             => 'noimage/noimage_top_shop.png',
        'top'              => 'noimage/noimage_top_shop.png',
        'top_recommend'    => 'noimage/noimage_top_shop.png',
        'coupon'           => 'noimage/noimage_coupon.png',
        'company_top'      => 'noimage/noimage_companytop.png',
        'segments_work'    => 'noimage/noimage_segments_work.png',
        'segments_policy'  => 'noimage/noimage_segments_policy.png',
        'segments_message' => 'noimage/noimage_segments_message.png',
        'menu_top'         => 'noimage/noimage_menutop.png',
        'menu_category'    => 'noimage/noimage_menu_news.png',
        'menu_item_thumb'  => 'noimage/noimage_menu_news.png',
        'menu_item'        => 'noimage/noimage_menu_news.png',
        'goods_top'        => 'noimage/noimage_goodstop.png',
        'goods_category'   => 'noimage/noimage_goods_news.png',
        'goods_item_thumb' => 'noimage/noimage_goods_news.png',
        'goods_item'       => 'noimage/noimage_goods_news.png',
        'news'             => 'noimage/noimage_menu_news.png',
        'topics'           => 'noimage/noimage_menu_topics.png',
        'app_icon'         => 'noimage/noimage_app_icon.png',
        'icon'             => 'noimage/noimage_icon.png',
        'screen_shot1'     => 'noimage/noimage_screen_shot.png',
        'screen_shot2'     => 'noimage/noimage_screen_shot2.png',
        'splash'           => 'noimage/noimage_splash.png',
        'branch'           => 'noimage/noimage_top_shop.png',
        'profile'          => 'noimage/noimage_profile.png',
        'banner'           => 'noimage/noimage_banner.png',
    ),

);

