<?php
/**
 * JPayment 決済関連設定ファイル
 */

return array(
    /**
     * JPayment決済データ送信先URL(※相対パス)
     */
    'send_url' => 'https://credit.j-payment.co.jp/gateway/payform.aspx',
    //'send_url' => 'http://admin.i-post.local/payment/apply/confirm',

    /**
     * 店舗ID
     */
    'aid' => '105505',

    /**
     * FORMパラメータ: カード決済画面表示
     */
    'pt' => '1',

    /**
     * 有効期限切れ警告期日
     *
     * @var int 有効期限切れ前◯sec
     */
    'expired_warning' => 3600 * 24 * 7,

    /**
     * JPayment送信元ID（※kickback時）
     */
    'allow_ip' => array(
        '59.106.45.225',
        '115.30.22.70'
    ),

    /**
     * 販売商品：デフォルトiid
     * ※クーポン適用がない場合はこちらで指定したiidの商品が購入対象商品となる
     */
    'default_iid' => 'basic_1month_auto',

    /**
     * 販売商品：デフォルトiid(月後半用)
     * ※クーポン適用がない場合はこちらで指定したiidの商品が購入対象商品となる
     */
    'default_iid_latter_half' => 'basic_1month_auto2',

    /**
     * 販売商品：変換
     * ※２回目払いで、１ヶ月無料を消す用
     */
    'convert_sale_item' => array(
        'basic_1month_auto'        => 'basic_1month_auto',
        'basic_1month_auto2'       => 'basic_1month_auto2',
        'cp_1month_auto'           => 'cp_1month_auto',
        'cp_1month_free_auto'      => 'basic_1month_auto',
        'cp_1month_free_plus_auto' => 'cp_1month_auto',
        'cp_1month_free_auto2'     => 'basic_1month_auto2',
    ),

    /**
     * ユーザー毎に強制的に販売アイテム指定
     */
    'force_sale_item' => array(
        // ユーザーID => 販売アイテムID
        //'53' => '1',
        '40' => '4',
        '142' => '5',
        '225' => '4',
        '382' => '4',
        '383' => '4',
        '385' => '4',
        '386' => '4',

    ),
);
