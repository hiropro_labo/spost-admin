<?php
/**
 * チュートリアル設定ファイル
 */

return array(
    /**
     * DB: TUTORIAL_STEP数
     */
    'step_root'   => 1,
    'step_top'    => 4,
    'step_coupon' => 1,
    'step_news'   => 1,
    'step_menu'   => 3,
    'step_store'  => 4,
);
