<?php
/**
 * アプリコンテンツ：登録制限値
 *
 * ・アプリ内の各所で利用する汎用的な設定項目は本ファイルへ記述
 * ・config.phpと分ける目的で本ファイルを設置
 * ・設定内容に特に決まりはないがパッケージ化したものなどはそちらの専用ファイルへ記述
 */

return array(
    /**
     * TOP
     */
    'top' => array(
        'carousel_img' => 1,
        'topic'        => 1,
        'shop_img'     => 1,
        'shop_prof'    => 1
    ),

    /**
     * COUPON
     */
    'coupon' => array(
        'entry_cnt' => 1,
    ),

    /**
     * NEWS
     */
    'news' => array(
        'entry_cnt' => 1
    ),

    /**
     * TOPICS
     */
    'topics' => array(
        'entry_cnt' => 1
    ),

    /**
     * MENU
     */
    'menu' => array(
        'top_img'      => 1,
        'category_cnt' => 3,
        'item_cnt'     => 1
    ),

    /**
     * GOODS
     */
    'goods' => array(
        'top_img'      => 1,
        'category_cnt' => 3,
        'item_cnt'     => 1
    ),

    /**
     * STORE
     */
    'store' => array(
        'icon_img' => 1,
        'app_info' => 1,
        'screen_shot_img'     => 1,
        'screen_shot_old_img' => 1,
        'splash_img'   => 1,
        'app_category' => 1
    ),

    /**
     * SHOP
     */
    'shop' => array(
        'top_img'   => 1,
        'shop_prof' => 1
    ),

    /**
     * BRANCH
     */
    'branch' => array(
        'entry_cnt' => 1
    ),

    /**
     * COMPANY
     */
    'company' => array(
        'entry_cnt' => 1
    ),

    /**
     * SEGMENTS
     */
    'segments' => array(
        'required' => array(
            'title_work', 'title_policy', 'title_message',
            'body_work',  'body_policy',  'body_message'
            )
    ),

    /**
     * RECRUIT
     */
    'recruit' => array(
        'entry_cnt' => 1
    ),

    /**
     * PROFILE
     */
    'profile' => array(
        'entry_cnt' => 1
    ),
);
