<?php
/**
 * 設定ファイル：テーマ設定値
 *
 * ・キー値はカテゴリーID
 * ・アプリ内の各所で利用する汎用的な設定項目は本ファイルへ記述
 * ・config.phpと分ける目的で本ファイルを設置
 * ・設定内容に特に決まりはないがパッケージ化したものなどはそちらの専用ファイルへ記述
 */

return array(
    // 1. 飲食
    '1' => array(
        'status_bar_color'    => '#FF0000',
        'status_bar_btn_menu' => 'btn_menu_1.png',
        'status_bar_btn_back' => 'btn_back_1.png'
    ),

    // 2. 物販
    '2' => array(
        'status_bar_color'    => '#FFFF00',
        'status_bar_btn_menu' => 'btn_menu_2.png',
        'status_bar_btn_back' => 'btn_back_2.png'
    ),

    // 3. 企業
    '3' => array(
        'status_bar_color'    => '#FFFFFF',
        'status_bar_btn_menu' => 'btn_menu_3.png',
        'status_bar_btn_back' => 'btn_back_3.png'
    ),

    // 4. 議員
    '4' => array(
        'status_bar_color'    => '#4C2DEA',
        'status_bar_btn_menu' => 'btn_menu_4.png',
        'status_bar_btn_back' => 'btn_back_4.png'
    ),
);
