<?php
return array(
	'_root_'  => 'root/index',       // The default route
	'_404_'   => 'exception/404',    // The main 404 route

//     'regist/exe'                              => 'regist/exe',
//     'regist/send'                             => 'regist/send',
//     'regist/(:agent_id)'                      => 'regist',
//     'payment/apply/confirm/(:item_id)'        => 'payment/apply/confirm',

//     'reg/confirm/(:hash)'                     => 'reg/confirm',

    //--------------------------------------
    // category/*
    //--------------------------------------
    'category/setup/exe/(:category)'     => 'category/setup/exe',
    'category/setup'                     => 'category/setup/index',

    //--------------------------------------
    // top/*
    //--------------------------------------
    'top'                                     => 'top/index',
    'top/shop'                                => 'top/shop',
    'top/img/top/update/exe/(:position)'      => 'top/img/top/update/exe',
    'top/img/top/update/(:position)'          => 'top/img/top/update/index',
    'top/img/top/delete/exe/(:position)'      => 'top/img/top/delete/exe',
    'top/img/top/delete/(:position)'          => 'top/img/top/delete',
    'top/img/shop/update'                     => 'top/img/shop/update/index',
    'top/img/shop/update/exe/'                => 'top/img/shop/update/exe',
    'top/order/up/(:position)'                => 'top/order/up/index',
    'top/order/down/(:position)'              => 'top/order/down/index',

    //--------------------------------------
    // shop
    //--------------------------------------
    'shop' => 'shop/index',

    //--------------------------------------
    // banner/*
    //--------------------------------------
    'banner/add/exe'            => 'banner/add/exe',
    'banner/add'                => 'banner/add/index',
    'banner/update/exe/(:c_id)' => 'banner/update/exe',
    'banner/update/(:c_id)'     => 'banner/update/index',
    'banner/del/exe/(:c_id)'    => 'banner/del/exe',
    'banner/del/(:c_id)'        => 'banner/del',
    'banner/order/up/(:position)'   => 'banner/order/up',
    'banner/order/down/(:position)' => 'banner/order/down',

    //--------------------------------------
    // coupon/*
    //--------------------------------------
    'coupon'                        => 'coupon/index',
    'coupon/update/exe/(:c_id)'     => 'coupon/update/exe',
    'coupon/update/(:c_id)'         => 'coupon/update',
    'coupon/del/exe/(:c_id)'        => 'coupon/del/exe',
    'coupon/del/(:c_id)'            => 'coupon/del',
    'coupon/order/up/(:position)'   => 'coupon/order/up',
    'coupon/order/down/(:position)' => 'coupon/order/down',

    //--------------------------------------
    // company/*
    //--------------------------------------
    'company' => 'company/index',
    'company/update/(:c_id)'  => 'company/update/index',
    'company/add'             => 'company/add/index',
    'company/del/(:c_id)'     => 'company/del/index',
    'company/top/update/exe'  => 'company/top/update/exe',
    'company/top/update'      => 'company/top/update',
    'company/name/update'     => 'company/name/update',
    'company/name/update/exe' => 'company/name/update/exe',

    //--------------------------------------
    // recruit/*
    //--------------------------------------
    'recruit'                          => 'recruit/index',
    'recruit/info/add'                 => 'recruit/info/add/index',
    'recruit/info/update/(:c_id)'      => 'recruit/info/update/index',
    'recruit/info/del/(:c_id)'         => 'recruit/info/del/index',
    'recruit/info/(:c_id)'             => 'recruit/info/index',
    'recruit/title/update/exe/(:c_id)' => 'recruit/title/update/exe',
    'recruit/title/update/(:c_id)'     => 'recruit/title/update',

    //--------------------------------------
    // profile/*
    //--------------------------------------
    'profile'                         => 'profile/index',
    'profile/img/update/exe'          => 'profile/img/update/exe',
    'profile/img/update'              => 'profile/img/update/index',
    'profile/info/update/exe'         => 'profile/info/update/exe',
    'profile/info/update'             => 'profile/info/update',
    'profile/history'                 => 'profile/history/index',
    'profile/history/add'             => 'profile/history/add/index',
    'profile/history/update/(:c_id)'  => 'profile/history/update/index',
    'profile/history/del/(:c_id)'     => 'profile/history/del/index',

    //--------------------------------------
    // segments/*
    //--------------------------------------
    'segments'                    => 'segments/index',
    'segments/work/update/exe'    => 'segments/work/update/exe',
    'segments/work/update'        => 'segments/work/update/index',
    'segments/policy/update/exe'  => 'segments/policy/update/exe',
    'segments/policy/update'      => 'segments/policy/update/index',
    'segments/message/update/exe' => 'segments/message/update/exe',
    'segments/message/update'     => 'segments/message/update/index',
    'segments/img/work/update/exe'    => 'segments/img/work/update/exe',
    'segments/img/work/update'        => 'segments/img/work/update',
    'segments/img/policy/update/exe'  => 'segments/img/policy/update/exe',
    'segments/img/policy/update'      => 'segments/img/policy/update',
    'segments/img/message/update/exe' => 'segments/img/message/update/exe',
    'segments/img/message/update'     => 'segments/img/message/update',
    'segments/text/work/update/exe' => 'segments/text/work/update/exe',
    'segments/text/work/update'     => 'segments/text/work/update',
    'segments/text/policy/update/exe' => 'segments/text/policy/update/exe',
    'segments/text/policy/update'     => 'segments/text/policy/update',
    'segments/text/message/update/exe' => 'segments/text/message/update/exe',
    'segments/text/message/update'     => 'segments/text/message/update',

    //--------------------------------------
    // url/*
    //--------------------------------------
    'url'            => 'url/index',
    'url/update/exe' => 'url/update/exe',
    'url/update'     => 'url/update',

    //--------------------------------------
    // menu/*
    //--------------------------------------
    'menu'                                          => 'menu/index',
    'menu/top/update'                               => 'menu/top/update',
    'menu/category/add'                             => 'menu/category/add',
    'menu/category/add/exe'                         => 'menu/category/add/exe',
    'menu/category/update/exe/(:c_id)'              => 'menu/category/update/exe',
    'menu/category/update/(:c_id)'                  => 'menu/category/update',
    'menu/category/order/up/(:position)'            => 'menu/category/order/up',
    'menu/category/order/down/(:position)'          => 'menu/category/order/down',
    'menu/category/del/exe/(:c_id)'                 => 'menu/category/del/exe',
    'menu/category/del/(:c_id)'                     => 'menu/category/del',
    'menu/item/add/exe/(:c_id)'                     => 'menu/item/add/exe',
    'menu/item/add/(:c_id)'                         => 'menu/item/add',
    'menu/item/update/exe/(:c_id)'                  => 'menu/item/update/exe',
    'menu/item/update/(:c_id)'                      => 'menu/item/update',
    'menu/item/order/up/(:position)/(:parent_id)'   => 'menu/item/order/up',
    'menu/item/order/down/(:position)/(:parent_id)' => 'menu/item/order/down',
    'menu/item/del/exe/(:c_id)'                     => 'menu/item/del/exe',
    'menu/item/del/(:c_id)'                         => 'menu/item/del',
    'menu/item/pickup/(:c_id)'                      => 'menu/item/pickup',

    //--------------------------------------
    // goods/*
    //--------------------------------------
    'goods'                                          => 'goods/index',
    'goods/top/update'                               => 'goods/top/update',
    'goods/category/add'                             => 'goods/category/add',
    'goods/category/add/exe'                         => 'goods/category/add/exe',
    'goods/category/update/exe/(:c_id)'              => 'goods/category/update/exe',
    'goods/category/update/(:c_id)'                  => 'goods/category/update',
    'goods/category/order/up/(:position)'            => 'goods/category/order/up',
    'goods/category/order/down/(:position)'          => 'goods/category/order/down',
    'goods/category/del/exe/(:c_id)'                 => 'goods/category/del/exe',
    'goods/category/del/(:c_id)'                     => 'goods/category/del',
    'goods/item/add/exe/(:c_id)'                     => 'goods/item/add/exe',
    'goods/item/add/(:c_id)'                         => 'goods/item/add',
    'goods/item/update/exe/(:c_id)'                  => 'goods/item/update/exe',
    'goods/item/update/(:c_id)'                      => 'goods/item/update',
    'goods/item/order/up/(:position)/(:parent_id)'   => 'goods/item/order/up',
    'goods/item/order/down/(:position)/(:parent_id)' => 'goods/item/order/down',
    'goods/item/del/exe/(:c_id)'                     => 'goods/item/del/exe',
    'goods/item/del/(:c_id)'                         => 'goods/item/del',
    'goods/item/pickup/(:c_id)'                      => 'goods/item/pickup',

    //--------------------------------------
    // news/*
    //--------------------------------------
    'news'                                   => 'news/index',
    'news/add'                               => 'news/add',
    'news/add/exe'                           => 'news/add/exe',
    'news/update/exe/(:id)'                  => 'news/update/exe',
    'news/update/(:id)'                      => 'news/update',
    'news/del/exe/(:id)'                     => 'news/del/exe',
    'news/del/(:id)'                         => 'news/del',
    'news/notice/push/exe/(:id)'             => 'news/notice/push/exe',
    'news/notice/push/(:id)'                 => 'news/notice/push',
    'news/info/(:id)'                        => 'news/index/info',
    'news/(:p)'                              => 'news/index',

    //--------------------------------------
    // topics/*
    //--------------------------------------
    'topics'                                   => 'topics/index',
    'topics/add'                               => 'topics/add',
    'topics/add/exe'                           => 'topics/add/exe',
    'topics/update/exe/(:id)'                  => 'topics/update/exe',
    'topics/update/(:id)'                      => 'topics/update',
    'topics/del/exe/(:id)'                     => 'topics/del/exe',
    'topics/del/(:id)'                         => 'topics/del',
    'topics/info/(:id)'                        => 'topics/index/info',
    'topics/(:p)'                              => 'topics/index',

    //--------------------------------------
    // payment/*
    //--------------------------------------
    'payment/apply/confirm/(:item_id)'       => 'payment/apply/confirm',
    'payment/apply/complete'                 => 'payment/apply/complete/index',
    'payment/apply'                          => 'payment/apply',
    'payment/apply/transfer'                 => 'payment/apply/transfer',
    'payment/apply/transfer/exe'             => 'payment/apply/transfer/exe',
    'payment/kickback'                       => 'payment/kickback',

    //--------------------------------------
    // inspect/*
    //--------------------------------------
    'inspect/ios' => 'inspect/index/ios',
    'inspect'     => 'inspect/index',

    //--------------------------------------
    // support/*
    //--------------------------------------
    'support'              => 'support/help/index',
    'support/help/(:g_id)' => 'support/help/index',
    'support/policy'       => 'support/policy/index',
    'support/law'          => 'support/law/index',
    'support/info'         => 'support/info/index',
    'support/info/(:id)'   => 'support/info/index',
    'support/reject/(:id)' => 'support/reject/index',

    //--------------------------------------
    // pop/*
    //--------------------------------------
    'pop'            => 'pop/index',

    //--------------------------------------
    // preview/*
    //--------------------------------------
    'preview'                       => 'preview/index/top',
    'preview/top'                   => 'preview/index/top',
    'preview/shopdetail'            => 'preview/index/shopdetail',
    'preview/webview'               => 'preview/index/webview',
    'preview/onlineshop'            => 'preview/index/onlineshop',
    'preview/coupon'                => 'preview/index/coupon',
    'preview/news'                  => 'preview/index/news',
    'preview/menu'                  => 'preview/index/menu',
    'preview/menu/(:c_id)'          => 'preview/index/menu',
    'preview/menudetail/(:c_id)'    => 'preview/index/menudetail',
    'preview/setting'               => 'preview/index/setting',
    'preview/store'                 => 'preview/index/store',
    'preview/play'                  => 'preview/index/play',

    //--------------------------------------
    // store/*
    //--------------------------------------
    'store'                                   => 'store/index',
    'store/sshot/update/exe/(:position)'      => 'store/sshot/update/exe',
    'store/sshot/update/(:position)'          => 'store/sshot/update/index',
    'store/sshot/delete/exe/(:position)'      => 'store/sshot/delete/exe',
    'store/sshot/delete/(:position)'          => 'store/sshot/delete',
    'store/sshot/order/(:positions)'          => 'store/sshot/order/index',
    'store/sshot/delete/exe/(:position)'      => 'store/sshot/delete/exe',
    'store/sshot/delete/(:position)'          => 'store/sshot/delete',
    'store/sshot2/update/exe/(:position)'     => 'store/sshot2/update/exe',
    'store/sshot2/update/(:position)'         => 'store/sshot2/update/index',
    'store/sshot2/delete/exe/(:position)'     => 'store/sshot2/delete/exe',
    'store/sshot2/delete/(:position)'         => 'store/sshot2/delete',
    'store/sshot2/order/(:positions)'         => 'store/sshot2/order/index',
    'store/sshot2/delete/exe/(:position)'     => 'store/sshot2/delete/exe',
    'store/sshot2/delete/(:position)'         => 'store/sshot2/delete',

    //--------------------------------------
    // branch/*
    //--------------------------------------
    'branch'                        => 'branch/index',
    'branch/add'                    => 'branch/add',
    'branch/add/exe'                => 'branch/add/exe',
    'branch/update/exe/(:b_id)'     => 'branch/update/exe',
    'branch/update/(:b_id)'         => 'branch/update',
    'branch/order/up/(:position)'   => 'branch/order/up',
    'branch/order/down/(:position)' => 'branch/order/down',
    'branch/del/exe/(:b_id)'        => 'branch/del/exe',
    'branch/del/(:b_id)'            => 'branch/del',

    //--------------------------------------
    // switcher/*
    //--------------------------------------
    'switcher'                   => 'switcher/index',
    'switcher/update/(:key)'  => 'switcher/index/update',

);