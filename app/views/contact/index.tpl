{include "common/header_error.tpl"}

<link href="/assets/img/common/favicon/ipost.png" rel="shortcut icon" type="image/png" />

<link rel="stylesheet" type="text/css" href="/assets/css/bass.css">
<link rel="stylesheet" type="text/css" href="/assets/css/original.css">
<link rel="stylesheet" type="text/css" href="/assets/css/path/sub.css">
<link rel="stylesheet" type="text/css" href="/assets/css/tooltipster.css">
<link type="text/css" rel="stylesheet" href="http://admin.hirolabo.net/assets/css/path/menu.css?1384054704" />

<div id="pan">
  <ul id="topicpath">
    <li><a href="/">{__('breadlist.bread_home')}</a></li>
    <li>{__('breadlist.bread_support')}</li>
    <li>{__('breadlist.bread_contact')}</li>
  </ul>
</div>

<div id="contents" class="no_preview">

    <div id="title_wrap">
      <div id="title_bar" class="info_color"></div>
         <div id="title_box">
            <h2>{__('header.title')}</h2>
            <p id="title_sub">{__('header.sub')}</p>
         </div>
      <div id="help_pre_box"></div>
    </div>

    <!---->
    <div class="contents_box original">
      <div class="contents_box_head">メール送信画面</div>
         <div class="split_right">
          {* ------- Form : HTML [ Start ] ----------- *}
          <form method="POST" accept-charset="utf-8" action="/contact/mail">
          <div class="contents_form_or">
            <ul>
            <li>
            <BR><BR>
               <label>件名:</label>
               <input name="title" type="text" id="title" maxlength="255" class="original"/>
               <p class="contact_error">{$fieldset->description('title')} {$fieldset->error_msg('title')}</p>
            </li>
            <li>
               <label>本文:</label>
               <textarea name="body" id="body" rows="20" cols="50" class="original" ></textarea>
               <p class="contact_error">{$fieldset->description('body')} {$fieldset->error_msg('body')}</p>
            </li>
            </ul>
               <input type="hidden" name="{$token_key}" value="{$token}" />{* トークンキーの発行 発行しないと本文を受信するときにエラーになる *}
               <p class="contact_btn">
               <input class="blue_btn" type="submit" name="submit" value="内容確認"/>
               </p>
            </div>
            </form>
           {* ------- Form : HTML [ End ] ------------- *}
        </div>

        <div class="scroll original" id = "policy">
            <p>
              個人情報取得における告知・同意文<br />
              <br />
              弊社では、個人情報保護法並びにプライバシーマーク（ＪＩＳＱ１５００１）に基づき皆様の個人情報保護に努めています。皆様から個人情報を収集する際は、事前に通知し、同意を得た後でなければ個人情報を皆様から頂くことはございません。下記の事項をお読みになり、ご同意を頂ける方は同意ボタンを押して下さい。尚、不明な点は下記管理者にお尋ね下さい。<br />
              <br />
              １．当社の個人情報に関する管理者<br />
                株式会社ヒロ企画<br />
                個人情報保護管理者　統括本部　統括本部長<br />
                〒700-0913 岡山県岡山市北区大供2-2-3　プランドール大供1F<br />
              電話：086-212-0077<br />
              メール：support@hiropro.co.jp<br />
              <br />
              ２．取得・利用目的<br />
              氏名、メールアドレス、問い合わせ内容を お問い合わせに対応、連絡のために取得、利用致します。<br />
              尚、本人が容易に認識出来ない方法によって個人情報を取得する事はございません。<br />
              <br />
              ３．第三者への提供<br />
              頂いた個人情報は第三者への提供は致しません。<br />
              ただし、刑事訴訟法、地方税法、所得税法、商法などに基づく場合、ご本人様の同意なく個人情報の利用・提供を行うことがあります。<br />
              <br />
              ４．個人情報の委託<br />
              レンタルサーバー会社に委託をしています。<br />
              尚、業務の委託にあたっては事前に選定し、個人情報保護の水準を満たしていることを確認しています。必要に応じて委託先会社とは個人情報保護に関する契約書を交わします。<br />
              <br />
              ５．任意性<br />
                当該個人情報をご提出いただくかはご本人様の任意ですが、この通知文によりご不明な点が解消されず、当該個人情報をご提出いただけない場合、お問い合わせの対応を行えない状況等、ご本人にとって不具合が発生しますことをご承知ください。    <br />
              <br />
              ６．個人情報の開示、訂正、追加又は削除、ならびに利用目的の通知、利用停止、消去、第三者への提供の停止をご請求される場合は、上記１の管理者にお申し出下さい。尚、そのときは本人確認をさ
              せていただくことがございますので、身分証明書のご提示をお願いする場合があります。<br />
              <br />
              <br />
              以上
            </p>
        </div>

        <div id = "policy_doui">
          <p>※送信された時点で同意したものとみなされます。</p>
        </div>
        <div class="clear"></div>

     </div>
     <!---->

    <div class="last_margin_or"></div>
</div>

{include "common/footer.tpl"}