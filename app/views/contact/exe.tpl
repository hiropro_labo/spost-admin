{include "common/header_error.tpl"}

<link href="/assets/img/common/favicon/ipost.png" rel="shortcut icon" type="image/png" />

<link rel="stylesheet" type="text/css" href="/assets/css/bass.css">
<link rel="stylesheet" type="text/css" href="/assets/css/original.css">
<link rel="stylesheet" type="text/css" href="/assets/css/path/sub.css">
<link rel="stylesheet" type="text/css" href="/assets/css/tooltipster.css">
<link type="text/css" rel="stylesheet" href="http://admin.hirolabo.net/assets/css/path/menu.css?1384054704" />

  <div id="pan">
    <ul id="topicpath">
     <li><a href="/">ホーム</a></li>
     <li>サポート</li>
     <li>お問い合わせ</li>
     <li>送信完了画面</li>
    </ul>
  </div>

<div id="contents" class="no_preview">

   <div id="title_wrap">
      <div id="title_bar" class="info_color"></div>
        <div id="title_box">
          <h2>お問い合わせ</h2>
          <p id="title_sub">Inquiry</p>
        </div>
   </div>

    <div class="contents_box original">
       <div class="contents_box_head">お問い合わせ送信完了画面</div>
        <form action="/coupon/update" method="POST">
        <div class="contents_form">
          <ul>
            <li>
              <span class="ml_30">お問い合わせの送信が完了しました。</span>
            </li>
          </ul>
          <hr />
          <p><a href="/" class="back_btn ml_30">戻る</a></p>
        </div>
        </form>
    </div>
    <div class="last_margin"></div>

</div>

{include "common/footer.tpl"}
