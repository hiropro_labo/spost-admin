{include "common/header_error.tpl"}

<link href="/assets/img/common/favicon/ipost.png" rel="shortcut icon" type="image/png" />

<link rel="stylesheet" type="text/css" href="/assets/css/bass.css">
<link rel="stylesheet" type="text/css" href="/assets/css/original.css">
<link rel="stylesheet" type="text/css" href="/assets/css/path/sub.css">
<link rel="stylesheet" type="text/css" href="/assets/css/tooltipster.css">
<link type="text/css" rel="stylesheet" href="http://admin.hirolabo.net/assets/css/path/menu.css?1384054704" />

<style type="text/css">

p.example1 { word-wrap: break-word; }

</style>

  <div id="pan">
    <ul id="topicpath">
     <li><a href="/">ホーム</a></li>
     <li>サポート</li>
     <li>お問い合わせ</li>
     <li>確認画面</li>
    </ul>
  </div>

<div id="contents" class="no_preview">

 <div id="title_wrap">
   <div id="title_bar" class="info_color"></div>
    <div id="title_box">
      <h2>お問い合わせ</h2>
      <p id="title_sub">Inquiry</p>
    </div>
    <div id="help_pre_box"></div>
</div>

  <!---->
  <div class="contents_box original">
  <div class="contents_box_head">メール送信確認画面</div>
  <div>

    {* ------- Form : HTML [ Start ] ----------- *}
    <form method="POST" accept-charset="utf-8" action="/contact/mail/exe">
    <div class="contents_form_or">

       <div class="border">
         <p>件名:<span>{$fieldset->value('title')}</span></p>
         <p>{$fieldset->description('title')} {$fieldset->error_msg('title')}</p>
       </div>

       <div class="border_bo">
         <p>本文:<span class="example1">{$fieldset->value('body')|nl2br}</span></p>
         <p>{$fieldset->description('body')} {$fieldset->error_msg('body')}</p>
       </div>
      　
        <hr />
      　<p class="mb_20 ta_c">こちらの内容で送信してよろしいですか？</p>
        <p class="ml_350mb_20">
          <input class="save_btn" type="submit" name="submit" value="送信する" />
          <input class="back_btn contact_back_btn mr_5" type="button" value="戻る" onclick="javascript:history.back();" />
        </p>

      <input type="hidden" name="{$token_key}" value="{$token}" />
      {* ------- Hidden Data [ Start ] -------- *}
      {foreach from=$fieldset->field() item=field}
      {$field->build()}
      {/foreach}
      {* ------- Hidden Data [ End ] ---------- *}
        </div>
    </form>
    {* ------- Form : HTML [ End ] ------------- *}

 </div>

 </div>
<div class="last_margin_or"></div>
</div>
</div>
{include "common/footer.tpl"}
