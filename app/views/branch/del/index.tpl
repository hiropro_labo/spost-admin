{include "common/header.tpl"}


{include "common/header_meta/branch.tpl"}


<!-- 支店の削除 -->
<div class="contents_box">
  <div class="contents_box_head">支店の削除
    <a href="/support/manual/menu#menu_2" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="登録されている支店データを削除できます。"></a>
  </div>

  <form action="/branch/del/exe/{$branch->id}" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>支店画像</label>
      <img src="{$branch->image_path()}?{time()}" width="160" height="88" alt="支店画像" class="form_img con_img" />
    </li>
  </ul>


  <ul>
    <li>
      <label for="name">お店の名前</label>
      <span>{$fieldset->value('shop_name')}</span>
    </li>

    <li>
      <label for="zip1">郵便番号</label>
      <span>{$fieldset->value('zip_code1')}&nbsp;-&nbsp;{$fieldset->value('zip_code2')}</span>
    </li>

    <li>
      <label for="pref">都道府県</label>
      <span>{$selected_pref}</span>
    </li>

    <li>
      <label for="pref">市区町村</label>
      <span>{$fieldset->value('city')}</span>
    </li>

    <li>
      <label for="pref">住所（番地）</label>
      <span>{$fieldset->value('address_opt1')}</span>
    </li>

    <li>
      <label for="pref">住所（建物）</label>
      <span>{$fieldset->value('address_opt2')}</span>
    </li>

    <li>
      <label for="pref">電話番号</label><span>{$fieldset->value('tel1')}&nbsp;-&nbsp;{$fieldset->value('tel2')}&nbsp;-&nbsp;{$fieldset->value('tel3')}</span>
    </li>

    <li>
      <label for="pref">FAX</label><span>{$fieldset->value('fax1')}&nbsp;-&nbsp;{$fieldset->value('fax2')}&nbsp;-&nbsp;{$fieldset->value('fax3')}</span>
    </li>

    <li>
      <label for="pref">ホームページアドレス</label>
      <span>{$fieldset->value('url')}</span>
    </li>

    <li>
      <label for="pref">オンラインショップ</label>
      <span>{$fieldset->value('online_shop')}</span>
    </li>

    <li>
      <label for="pref">メールアドレス</label>
      <span>{$fieldset->value('email')}</span>
    </li>

    <li>
      <label for="pref">営業時間</label>
      <span>{$fieldset->value('open_hours')}</span>
    </li>

    <li>
      <label for="pref">定休日</label>
      <span>{$fieldset->value('holiday')}</span>
    </li>
  </ul>

  <hr />

   <input type="submit" name="button" value="削除" class="save_btn" />
 <a href="/branch" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


{include "common/footer.tpl"}
