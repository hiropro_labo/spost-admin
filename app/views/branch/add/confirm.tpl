{include "common/header.tpl"}


{include "common/header_meta/menu.tpl"}


<!-- 支店の新規作成 -->
<div class="contents_box">
  <div class="contents_box_head">支店の新規作成
  </div>
  <h4>支店の新規作成</h4>

  <form action="/branch/add/exe" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>支店画像</label>
      <img src="{$branch->tmp_image_path_by_id($SPONSOR->id(), 'branch')}?{time()}" width="160" height="88" alt="支店画像" class="form_img con_img" />
    </li>
  </ul>

  <ul>
    <li>
      <label for="name">お店の名前</label>
      <span>{$fieldset->value('shop_name')}</span>
    </li>

    <li>
      <label for="zip1">郵便番号</label>
      <span>{$fieldset->value('zip_code1')}&nbsp;-&nbsp;{$fieldset->value('zip_code2')}</span>
    </li>

    <li>
      <label for="pref">都道府県</label>
      <span>{$selected_pref}</span>
    </li>

    <li>
      <label for="pref">市区町村</label>
      <span>{$fieldset->value('city')}</span>
    </li>

    <li>
      <label for="pref">住所（番地）</label>
      <span>{$fieldset->value('address_opt1')}</span>
    </li>

    <li>
      <label for="pref">住所（建物）</label>
      <span>{$fieldset->value('address_opt2')}</span>
    </li>

    <li>
      <label for="pref">電話番号</label><span>{$fieldset->value('tel1')}&nbsp;-&nbsp;{$fieldset->value('tel2')}&nbsp;-&nbsp;{$fieldset->value('tel3')}</span>
    </li>

    <li>
      <label for="pref">FAX</label><span>{$fieldset->value('fax1')}&nbsp;-&nbsp;{$fieldset->value('fax2')}&nbsp;-&nbsp;{$fieldset->value('fax3')}</span>
    </li>

    <li>
      <label for="pref">ホームページ</label>
      <span>{$fieldset->value('url')}</span>
    </li>

    <li>
      <label for="pref">オンラインショップ</label>
      <span>{$fieldset->value('online_shop')}</span>
    </li>

    <li>
      <label for="pref">メールアドレス</label>
      <span>{$fieldset->value('email')}</span>
    </li>

    <li>
      <label for="pref">営業時間</label>
      <span>{$fieldset->value('open_hours')}</span>
    </li>

    <li>
      <label for="pref">定休日</label>
      <span>{$fieldset->value('holiday')}</span>
    </li>

    <li>
      <label>表示設定</label>
      <span>{$fieldset->value('enable')|replace:'0':'非表示'|replace:'1':'表示'}</span>
    </li>

  </ul>

  <hr />

  {* ------- Hidden Data [ Start ] -------- *}
  {foreach from=$fieldset->field() item=field}
  {$field}
  {/foreach}
  {* ------- Hidden Data [ End ] ---------- *}
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
