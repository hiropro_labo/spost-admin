{include "common/header.tpl"}


{include "common/header_meta/branch.tpl"}

<!-- 支店の編集 -->
<div class="contents_box">
  <div class="contents_box_head">支店の編集
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="支店データの登録や編集ができます。"></a>
  </div>
  <h4>支店の編集</h4>
  <p class="mb_20"><span class="hisu">※簡易申請時の必須項目ではありません。</span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/branch/add" class="edit_btn mt_m50">支店の新規登録</a>
{/if}
</div>


{* ----- 支店一覧 [Start] ----- *}
<div class="contents_box">
  <div class="contents_box_head">支店一覧
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="支店データを登録できます。"></a>
  </div>

{if ! is_null($list) && count($list)>0}
{* ----- 支店一覧 [Start] ----- *}
{foreach from=$list item=branch}
<div class="row-fluid mb_20 mt_20 br_4 row-fluid_wrap"{if $branch->enable == '0'} style="background-color:#cccccc;"{/if}>
  <dl class="menu_ac">
    <dt>
      <div class="menu_cate_box">
{if ! $SPONSOR->app()->is_lock()}
        <a href="/branch/update/{$branch->id}" title="変更"><img src="{$branch->image_path()}?{time()}" width="160" height="88" alt="支店画像" class="mb_20 con_img"></a>
{else}
        <img src="{$branch->image_path()}?{time()}" width="160" height="88" alt="支店画像" class="mb_20 con_img">
{/if}

        <p class="menu_table_text">
{if ! $SPONSOR->app()->is_lock()}
          <a href="/branch/update/{$branch->id}">{$branch->shop_name}</a><br />
{else}
          {$branch->shop_name}<br />

{/if}
        </p>

{if ! $SPONSOR->app()->is_lock()}
        <a href="/branch/update/{$branch->id}" class="gray_btn mr_5 ml_20">支店の編集</a>

        <a href="/branch/del/{$branch->id}" class="gray_btn mr_5"><img src="/assets/img/common/icon/i11.png" width="18" id="i11" style="margin-left:0;"></a>
{/if}

      </div>

{if ! $SPONSOR->app()->is_lock()}
      <div class="up_down">
        {if !is_null($branch->file_name) && count($list) > 1}
          {if $branch->position != 1}
          <a href="/branch/order/up/{$branch->position}" class="gray_btn ml_20 mb_10"><img src="/assets/img/common/icon/i16.png" width="14" style="margin-left: 0;"/ ></a>
          {/if}

          {if $branch->position < count($list)}
          <a href="/branch/order/down/{$branch->position}" class="gray_btn ml_20"><img src="/assets/img/common/icon/i17.png" width="14" style="margin-left:0;"/ ></a>
          {/if}
        {/if}
      </div>
{/if}
      <br class="clear">
    </dt>
  </dl>
</div>

{/foreach}
{* ----- 支店一覧 [End]   ----- *}
{else}
  <div class="menu_list row-fluid mb_20 mt_20 br_4 row-fluid_wrap">
    <p>現在、支店のデータはありません</p>
  </div>
{/if}

</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
