{include "common/header.tpl"}


{include "common/header_meta/branch.tpl"}


<!-- 支店の変更 -->
<div class="contents_box">
  <div class="contents_box_head">支店の変更
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="支店データの編集ができます。"></a>
  </div>

  <form action="/branch/update/{$b_id}" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label>画像のアップロード</label>
      <img src="{$branch->image_path()}?{time()}" width="160" height="88" alt="支店画像" class="form_img con_img" />
    </li>
    <li>
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br>640px&nbsp;×&nbsp;350px&nbsp;以上の大きさを推奨</p>
    </li>
  </ul>

  <ul>
    <li>
      <label for="name" class="hisu">お店の名前</label>
      {$fieldset->field('shop_name')->build()}{$fieldset->error_msg('shop_name')}
    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="zip1">郵便番号</label>
    {$fieldset->field('zip_code1')->build()}&nbsp;{$fieldset->field('zip_code2')->build()}
    </li>

    <li>
      <label for="pref">都道府県</label>
      {$fieldset->field('pref')->build()}
    </li>

    <li>
      <label for="pref">市区町村</label>
      {$fieldset->field('city')->build()} {$fieldset->error_msg('city')}
    </li>

    <li>
      <label for="pref">住所（番地）</label>
      {$fieldset->field('address_opt1')->build()} {$fieldset->error_msg('address_opt1')}
    </li>

    <li>
      <label for="pref">住所（建物）</label>
      {$fieldset->field('address_opt2')->build()} {$fieldset->error_msg('address_opt2')}
    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">電話番号</label>
      {$fieldset->field('tel1')->build()}&nbsp;{$fieldset->field('tel2')->build()}&nbsp;{$fieldset->field('tel3')->build()} {$fieldset->description('tel1')}
    </li>

    <li>
      <label for="pref">FAX</label>
      {$fieldset->field('fax1')->build()}&nbsp;{$fieldset->field('fax2')->build()}&nbsp;{$fieldset->field('fax3')->build()} {$fieldset->description('fax1')}
    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">ホームページ</label>
      {$fieldset->field('url')->build()} {$fieldset->description('url')} {$fieldset->error_msg('url')}
    </li>
  </ul>

  <ul>
    <li>
      <label for="pref">オンラインショップ</label>
      {$fieldset->field('online_shop')->build()} {$fieldset->description('online_shop')} {$fieldset->error_msg('online_shop')}
    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">メールアドレス</label>
      {$fieldset->field('email')->build()} {$fieldset->description('email')} {$fieldset->error_msg('email')}
    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">営業時間</label>
      {$fieldset->field('open_hours')->build()} {$fieldset->description('open_hours')} {$fieldset->error_msg('open_hours')}
    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">定休日</label>
      {$fieldset->field('holiday')->build()} {$fieldset->description('holiday')} {$fieldset->error_msg('holiday')}
    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label>&nbsp;</label>
      <label for="form_enable_1"><input type="radio" required="required" value="1" id="form_enable_1" name="enable" checked="checked" />表示</label>

      <label for="form_enable_0"><input type="radio" required="required" value="0" id="form_enable_0" name="enable" />非表示</label>
      <p class="error">{$fieldset->error_msg('enable')}</p>
    </li>
  </ul>
      <p class="desc">お客様に見せるかどうかを選ぶことができます。</p>
  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/branch" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
