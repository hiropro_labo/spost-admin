<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>特定商取引表記</title>

<!-- Favicon Icon -->
<link href="/assets/img/common/favicon/ipost.png" rel="shortcut icon" type="image/png" />

<style type="text/css">
body {
font-size: 13px;
line-height: 1.5;
}
#main_box {
  max-width: 640px;
  margin: 0px auto 30px;
  width: 100%;
}
.box {
  margin: 20px 0 0;
  border: 1px;
}
.box p {
  font-size: 12px;
  color: #666;
  margin: 5px;
  line-height: 1.5;
  margin-top: 20px;
}
.box h3 {
  margin-bottom: 23px;
  margin-top: -7px;
  border-bottom: solid 2px #0D8DBB;
  padding-bottom: 7px;
  color: #4b4b4b;
  font-size: 13px;
}
.privacy table {
  width: 598px;
  margin: 20px;
}
.privacy table .table_l {
  width: 150px;
}
.privacy table th, .privacy table td {
  width: auto;
  height: auto;
  padding: 5px;
  color: rgb(102, 102, 102);
  vertical-align: baseline;
  text-align: left;
  border: solid 1px rgb(102, 102, 102);
}
.privacy table th {
  color: rgb(102, 102, 102);
  background-color: rgb(222, 222, 222);
}
table {
border-collapse: collapse;
border-spacing: 0;
}
.win input[type="text"], .win input[type="password"], .win textarea, .win select {
  font-size: 14px;
  color: #333;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  border-radius: 4px;
  background-color: #f3f1f1;
  box-shadow: inset 1px 1px 1px hsla(0,0%,0%,0.35), 0 1px 0 hsla(0,0%,100%,0.55);
  border-radius: 4px;
  border: 1px solid #ccc;
}
.mac input[type="text"], .mac input[type="password"], .mac textarea, .mac select {
  font-size: 14px;
  color: #333;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  border-radius: 4px;
  background-color: #f3f1f1;
  box-shadow: inset 2px 2px 1px hsla(0,0%,0%,0.35), 0 1px 0 hsla(0,0%,100%,0.55);
  border-radius: 4px;
  border: 1px solid #ccc;
}
.help_view_wrap, .form_wrap {
  clear: both;
  float: none;
  margin: 0 auto;
  margin-top: 15px;
  width: 93.5%;
  display: block;
  border: 1px solid #EBEBEB;
  background: #FFF;
  box-shadow: 0 1px 1px 0 rgba(0,0,0,0.1), inset 0px 1px 1px 0px #ffffff;
  -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,0.1), inset 0px 1px 1px 0px #ffffff;
  -moz-box-shadow: 0 1px 1px 0 rgba(0,0,0,0.1), inset 0px 1px 1px 0px #ffffff;
  border-radius: 3px;
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  overflow: hidden;
  padding-top: 10px;
  padding-bottom: 17px;
}
.help_view_wrap {
  padding-left: 3%;
  padding-right: 3%;
}
.nav {
  overflow: hidden;
  border: solid 1px #ccc;
  border-radius: 3px;
  box-shadow: 2px 2px 4px rgba(0,0,0,0.1), inset 0px 1px 1px 0px #ffffff;
  margin-top: 14px;
}
.nav li {
  border-top: solid 1px #cacaca;
  margin-top: -1px;
}
.nav li a {
  display: block;
  width: 100%;
  height: 100%;
  padding: 10px;
  text-decoration: none;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  background-color: #fff;
  background: url(../../../../images/user/arrow_basic_s.png), -webkit-gradient(linear, left top, left bottom, from(#fff), to(#ebebeb));
  background-repeat: no-repeat;
  background-position: 94% center;
  padding-left: 20px;
  color: #4b4b4b;
  padding-right: 30px;
}
/* yoshitaka 2013-04-16
Select Style */
.nav li.dis a {
  color: #fccf49;
  background: url(../../../../images/user/arrow_basic_s.png), -webkit-gradient(linear, left top, left bottom, from(#393838), to(#1f1e1e));
  background-repeat: no-repeat;
  background-position: 94% center;
}
/* Select Style END */

.nav li a:hover {
  opacity: 0.8;
  filter: alpha(opacity=80);
  -ms-filter: “alpha( opacity=80 )”;
}
.nav ul {
  padding: 0px;
  margin-top: 0;
  margin-bottom: 0;
}
/*
右カラム
*/
#main_box_r {
  width: 94%;
  margin-top: 0px;
  display: block;
  margin: 0 auto;
  background: #fff;
}
.Qes, .Ans {
  font-size: 13px;
  color: #4b4b4b;
  line-height: 1.5;
}
.Qes {
  font-weight: bold;
  margin-top: 5px;
  margin-bottom: 10px;
}
.Ans {
  padding: 20px 10px;
  background: #f0f0f0;
}
.help_view_wrap hr {
  height: 0;
  border: 0;
  margin: 0 auto;
  border-top: 1px solid #D1D1D1;
  border-bottom: 1px solid #fff;
  width: 100%;
  margin-top: 5px;
  margin-bottom: 5px;
}
/*
コンタクト
*/

#left_contact_box {
  margin-right: 44px;
  margin-bottom: 20px;
}
/*
ヘルプ
*/
#topicpath {
  margin: 10px 0;
  padding-right: 0px;
  padding-left: 3px;
  color: #4b4b4b;
}
#topicpath li {
  display: inline;
  line-height: 110%;
  list-style-type: none;
}
#topicpath li a {
  color: #900;
  padding-right: 10px;
  background: url(../../../images/index/topicpath_icon.gif) no-repeat right center;
  text-decoration: none;
}
#topicpath a:hover {
  text-decoration: underline;
}
#topicpath a:visited {
  text-decoration: none;
}
#main_box_r a {
  color: #900;
  float: right;
  margin-top: 10px;
  text-decoration: none;
}
#main_box_r a:hover {
  text-decoration: underline;
}
#main_box_r a:visited {
  text-decoration: none;
}
.main_box_l {
  margin-bottom: 40px;
}

/*iPhone横*/
@media screen and (min-width: 481px) {
/*
ヘルプ
*/
.main_box_l {
  float: left;
  max-width: 320px;
  width: 32%;
}
#main_box_r {
  width: 66%;
  margin-top: 0px;
  float: right;
  max-width: 640px;
  display: block;
  margin: 0 auto;
  background: #fff;
}
.help_view_wrap {
  padding-left: 20px;
  padding-right: 20px;
}
.box h3 {
  margin-bottom: 3px;
  padding: 3px;
}
}
/*iPad*/
@media screen and (min-width: 768px) {
}
/*PC*/
@media screen and (min-width: 1024px) {
.box h3 {
  margin-bottom: 23px;
  margin-top: -7px;
  border-bottom: solid 2px #0D8DBB;
  padding-bottom: 7px;
  color: #4b4b4b;
}
}
/* yoshitaka 2013-04-29
Privacy Style */
.privacy.box {
  margin: 20px 0 0;
  border: solid 1px #aaa;
  box-shadow: 0 1px 1px 0 rgba(0,0,0,0.1), inset 0px 1px 1px 0px #ffffff;
  -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,0.1), inset 0px 1px 1px 0px #ffffff;
  -moz-box-shadow: 0 1px 1px 0 rgba(0,0,0,0.1), inset 0px 1px 1px 0px #ffffff;
  border-radius: 3px;
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
}
.privacy p {
  margin: 20px;
  background: #FFF;
}
.support_window_r {
  float: right;
  width: 16em;
}
.support_window {
  clear: both;
  margin: 10px;
}

/*
tos
*/

.box p span{
font-weight:bold;
}
.box p a {
color: #008cb4;
text-decoration: none;
}
.box p a:hover {
color: #005580;
text-decoration: underline;
}
.ta_r{
text-align: right;
}

/*
law
*/
table{
border: none;
color: #666;
}
tr{
border-top: 1px dotted #ccc;
}
tr:last-child{
border-bottom: solid 1px #e5e5e5;
}

td{
border: none;
padding: 10px 20px 10px 20px;
}
td:nth-child(2n){
border-left: solid 1px #e5e5e5;
border-right: solid 1px #e5e5e5;
}
td:nth-child(2n+1){
width:180px;
background: #F1F1F1;
border-left: solid 1px #e5e5e5;
}
a{
color: #008cb4;
text-decoration: none;
}
a:hover {
color: #005580;
text-decoration: underline;
}

</style>
</head>


<body>
<!-- コンテンツ -->
<div id="main_box">
  <div class="box"><!-- コメント左側に挿絵を入れるためのdiv -->
    <h3>特定商取引表記</h3>
  </div>



<TABLE>
  <TBODY>
  <TR>
    <TD>
      <DIV align=left><STRONG>運営団体名</STRONG></DIV></TD>
    <TD>
      <DIV align=left>株式会社ヒロ企画</DIV></TD></TR>
  <TR>
    <TD>
      <DIV align=left><STRONG>事業内容</STRONG></DIV></TD>
    <TD>
      <DIV align=left>IT開発 / 運営 / コンサル業務<br>iPhone＆Androidアプリケーション開発 / 家庭用ゲーム開発 / WEB制作</DIV></TD></TR>
  <TR>
    <TD>
      <DIV align=left><STRONG>代表者</STRONG></DIV></TD>
    <TD>
      <DIV align=left>代表取締役社長　成広 通義</DIV></TD></TR>
  <TR>
    <TD>
      <DIV align=left><STRONG>本社</STRONG></DIV></TD>
    <TD>
      <DIV align=left>
      <P>所在地　〒700-0913　岡山市北区大供2-2-3　プランドール大供<BR>電話番号　0120-043-019　086-212-0077<BR>FAX　086-212-0066<BR></P></DIV></TD></TR>
  <TR>
    <TD>
      <DIV align=left><STRONG>問い合わせ先メールアドレス</STRONG></DIV></TD>
    <TD>
      <DIV align=left><a href="mailto:support@hiropro.co.jp">support@hiropro.co.jp</a></DIV></TD></TR>
  <TR>
    <TD>
      <DIV align=left><STRONG>販売価格</STRONG></DIV></TD>
    <TD>
      <DIV align=left>各商品ごとに表示、表示価格は税別</DIV></TD></TR>
  <TR>
    <TD>
      <DIV align=left><STRONG>商品代金以外の必要料金</STRONG></DIV></TD>
    <TD>
      <DIV align=left>消費税</DIV></TD></TR>
  <TR>
    <TD>
      <DIV align=left><STRONG>営業時間</STRONG></DIV></TD>
    <TD>
      <DIV align=left>10：00 ～18：00<br>フォームからのご注文は営業時間外でも受け付けております。（対応は翌営業時間より）</DIV></TD></TR>
  <TR>
    <TD>
      <DIV align=left><STRONG>定休日</STRONG></DIV></TD>
    <TD>
      <DIV align=left>土曜日、日曜日、祝日</DIV></TD></TR>
  <TR>
    <TD>
      <DIV align=left><STRONG>御注文方法</STRONG></DIV></TD>
    <TD>
      <DIV align=left>WEBフォーム</DIV></TD></TR>
  <TR>
    <TD>
      <DIV align=left><STRONG>商品代金のお支払方法</STRONG></DIV></TD>
    <TD>
      <DIV align=left>
      <P>・クレジットカード<BR>・銀行振込<BR><BR>クレジットカードでのお支払いは、J-Paymentの決済代行サービスを使用しています。決済情報はSSLで暗号化され、安全性を確保しております。またJ-Payment社のSSL証明書はベリサインにて発行されております。
     </DIV></TD></TR>
  <TR>
    <TD>
      <DIV align=left><STRONG>商品のお渡し時期</STRONG></DIV></TD>
    <TD>
      <DIV align=left>通常ご入金確認でき次第指定のお時間にお渡し致します。</DIV></TD></TR>
  <TR>
    <TD>
      <DIV align=left><STRONG>返品について</STRONG></DIV></TD>
    <TD>
      <DIV
align=left>劣化、欠陥のない商品の特性上お受けできません。</DIV></TD></TR></TBODY></TABLE></BODY></HTML>

</body>
</html>
