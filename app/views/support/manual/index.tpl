{include "common/header_manual.tpl"}


<!--<div class="phone">-->
  <div class="main_width km">
  <div class="top menu_box km">
  <img src="/assets/img/common/manual/top_title.png" class="menu_title">
  <hr class="menu_line">
  <ul class="menu">
  <li><a href="/support/manual/top">アプリのトップ画像を登録(変更)する</a></li>
  <li><a href="/support/manual/top#top_1">アプリのトップ画像を削除する</a></li>
  <li><a href="/support/manual/top#top_2">トピックメッセージを変更する</a></li>
  <li><a href="/support/manual/top#top_3">お店の画像を登録(変更)する</a></li>
  <li><a href="/support/manual/top#top_4">お店の情報を登録(変更)する</a></li>
  </ul>
  </div><!-- (/top) -->

  <div class="coupon menu_box km">
  <img src="/assets/img/common/manual/coupon_title.png" class="menu_title">
  <hr class="menu_line">
  <ul class="menu">
  <li><a href="/support/manual/coupon">クーポンを登録(変更)する</a></li>
  <li><a href="/support/manual/coupon#coupon_1">クーポンを削除する</a></li>
  </ul>
  </div><!-- (/coupon) -->

  <div class="news menu_box km">
  <img src="/assets/img/common/manual/news_title.png" class="menu_title">
  <hr class="menu_line">
  <ul class="menu">
  <li><a href="/support/manual/news">ニュースを登録する</a></li>
  <li><a href="/support/manual/news#news_1">ニュースを編集する</a></li>
  <li><a href="/support/manual/news#news_2">ニュースを削除する</a></li>
  <li><a href="/support/manual/news#news_3">ニュースを配信する</a></li>
  </ul>
  </div><!-- (/news) -->

  <div class="menu menu_box km">
  <img src="/assets/img/common/manual/menu_title.png" class="menu_title">
  <hr class="menu_line">
  <ul class="menu">
  <li><a href="/support/manual/menu">メニューのトップ画像を登録(変更)する</a></li>
  <li><a href="/support/manual/menu#menu_1">メニューのカテゴリーを登録(変更)する</a></li>
  <li><a href="/support/manual/menu#menu_2">メニューのカテゴリーを削除する</a></li>
  <li><a href="/support/manual/menu#menu_3">メニューの商品画像を登録(変更)する</a></li>
  <li><a href="/support/manual/menu#menu_4">メニューの商品画像を削除する</a></li>
  </ul>
  </div><!-- (/menu) -->

  <div class="info menu_box km">
  <img src="/assets/img/common/manual/info_title.png" class="menu_title">
  <hr class="menu_line">
  <ul class="menu">
  <li><a href="/support/manual/store">アプリのアイコンを登録する</a></li>
  <li><a href="/support/manual/store#store_1">アプリの名前を登録する</a></li>
  <li><a href="/support/manual/store#store_2">アプリのスクリーンショットを登録する</a></li>
  <li><a href="/support/manual/store#store_3">アプリのスクリーンショットを削除する</a></li>
  <li><a href="/support/manual/store#store_4">アプリ起動時の画像を登録する</a></li>
  <li><a href="/support/manual/store#store_5">アプリのカテゴリーを登録する</a></li>
  </ul>
  </div><!-- (/info) -->

  <div class="pdf">
  <p><img src="/assets/img/common/manual/pdf_title.png" class="pdf_title"></p>
  <hr>
  <ul>
  <li><a href="/assets/pdf/manual/ipost_manual.pdf" target="_blank">iPostアプリ制作マニュアル</a></li>
  </ul>
  </div><!-- (/pdf") -->

<div class="clear"></div>

</div><!-- (/main_width) -->


{include "common/footer.tpl"}
