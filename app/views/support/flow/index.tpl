<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>iPostご利用までの流れ｜iPost管理画面</title>
<link href="/assets/css/path/flow.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="flow_container">
  <img src="/assets/img/common/flow/flow_title.png" id="flow_title">
  <ul>
    <li>
      <div class="fl"><img src="/assets/img/common/flow/step_1.png"></div>
      <div class="fl"><img src="/assets/img/common/flow/sub_title_1.png" class="ml_20">
      <p class="flow_text">専用フォームより、iPost申し込みのための必要事項をご入力いただきます。</p></div>
      <div class="clear"></div>
      <p class="flow_text_small">※お手続きについては、iPost販売代理人にお申し付けください。</p>
    </li>
    <li>
      <div class="fl"><img src="/assets/img/common/flow/step_2.png"></div>
      <div class="fl"><img src="/assets/img/common/flow/sub_title_2.png" class="ml_20">
      <p class="flow_text">ご登録いただいたメールアドレス宛にIDとパスワードが発行され、<br>制作サイトにログインできるようになります。</p></div>
      <img src="/assets/img/common/flow/flow_img_1.png" class="flow_img">
      <p class="flow_balloon">制作サイトへは、<br>admin.i-post.jp/<br>からログインできます。</p>
    </li>
    <li>
      <div class="fl"><img src="/assets/img/common/flow/step_3.png"></div>
      <div class="fl"><img src="/assets/img/common/flow/sub_title_3.png" class="ml_20">
      <p class="flow_text">制作サイトからアプリが作れます。<br>画像や文章を入れて、あなただけのオリジナルアプリを作りましょう。</p></div>
      <img src="/assets/img/common/flow/flow_img_2.png" class="flow_img">
      <div class="clear"></div>
      <p class="flow_text_small">※詳しいアプリの作り方については、<a href="http://admin.i-post.jp/support/manual" target="_blank">こちら</a>をご覧ください。</p>
    </li>
    <li>
      <div class="fl"><img src="/assets/img/common/flow/step_4.png"></div>
      <div class="fl"><img src="/assets/img/common/flow/sub_title_4.png" class="ml_20">
      <p class="flow_text">アプリの制作が完了したら<a href="http://admin.i-post.jp/inspect" target="_blank">「アプリ審査申請」</a>ボタンを押してください。<br>お客様に制作していただいたアプリを弊社が簡易審査いたします。</p></div>
      <img src="/assets/img/common/flow/flow_img_3.png" class="flow_img">
      <p class="flow_balloon">「アプリ申請」ボタンは<br><a href="http://admin.i-post.jp/inspect" target="_blank">「審査申請」ページ</a>に<br>あります。</p>
      <div class="clear"></div>
      <p class="flow_text_small">※簡易審査を通過するまでに通常数週間程度かかります。<br>　混雑時にはお時間を頂くことがありますので、ご了承ください。</p>
    </li>
    <li>
      <div class="fl"><img src="/assets/img/common/flow/step_5.png"></div>
      <div class="fl"><img src="/assets/img/common/flow/sub_title_5.png" class="ml_20">
      <p class="flow_text">初月のiPost利用料金をお支払いいただきます。<br><a href="http://admin.i-post.jp/payment/apply" target="_blank">「支払い」ページ</a>より、決済をお願いいたします。</p></div>
      <img src="/assets/img/common/flow/flow_img_4.png" class="flow_img">
      <p class="flow_balloon f_b_4"><a href="http://admin.i-post.jp/payment/apply" target="_blank">「支払い」ページ</a>より、<br>クレジットか銀行振込み<br>かを選択できます。</p>
      <div class="clear"></div>
    </li>
    <li>
      <div class="fl"><img src="/assets/img/common/flow/step_6.png"></div>
      <div class="fl"><img src="/assets/img/common/flow/sub_title_6.png" class="ml_20">
      <p class="flow_text">お支払い確認後、弊社がAppStoreとGooglePlayに<br>アプリを代行申請いたします。</p></div>
      <div class="clear"></div>
      <p class="flow_text_small">※アプリ申請が通過するまでに数週間ほどかかります。<br>　AppStoreやGooglePlayの混雑状況によっては長くかかる場合もございます。</p>
      <div class="clear"></div>
    </li>
    <li>
      <div class="fl"><img src="/assets/img/common/flow/step_7.png"></div>
      <div class="fl"><img src="/assets/img/common/flow/sub_title_7.png" class="ml_20">
      <p class="flow_text">アプリの正式公開となります。<br>たくさんのお客様にダウンロードしてもらいましょう！</p></div>
      <img src="/assets/img/common/flow/flow_img_5.png" class="flow_img">
      <p class="flow_balloon f_b_5">AppStoreと<br>GooglePlayから<br>アプリをダウンロード<br>いただけます。</p>
    </li>
  </ul>
</div>
</body>
</html>
