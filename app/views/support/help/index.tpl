{include "common/header.tpl"}


<div class="gridContainer clearfix">
  <div id="mypage_header"></div>

  <!-- ヘルプナビゲーション -->
  <div class="main_box_l">
    <div class="nav">
      <ul>
      {if empty($current) or ! is_numeric($current)}
        <li><a href="/support/help/" id="carrent">よくある質問</a></li>
      {else}
        <li><a href="/support/help/">よくある質問</a></li>
      {/if}

{foreach from=$sub_menu item=item}
  {if $item->id!=1 && $item->id!=2}
    {if $current==$item->id}
          <li><a href="/support/help/{$item->id}" id="carrent">{$item->name}</a></li>
    {else}
        <li><a href="/support/help/{$item->id}">{$item->name}</a></li>
    {/if}
  {/if}
{/foreach}
      </ul>
    </div>
  </div>
  <!--  -->


  <div id="main_box_r">
    <div class="box">
{if empty($current) or ! is_numeric($current)}
      <h3>よくある質問</h3>
{else}
      <h3>{$sub_menu[$current-1]->name}</h3>
{/if}
    </div>

{foreach from=$ipost_list item=item}
    <!--  -->
    <ul class="help_view_wrap">
      <li class="Qes">Q：{$item->question|nl2br}</li>
      <hr>
      <li  class="Ans">{$item->answer|nl2br}</li>
    </ul>
    <!--  -->
{/foreach}

{foreach from=$list item=item}
    <!--  -->
    <ul class="help_view_wrap">
      <li class="Qes">Q：{$item->question|nl2br}</li>
      <hr>
      <li  class="Ans">{$item->answer|nl2br}</li>
    </ul>
    <!--  -->
{/foreach}

    <a href="#mypage_header" id="to_top">▲ページトップに戻る</a>
  </div>

  <div class="clear"></div>
</div>


{literal}
<script type="text/javascript">
//イージング
$(function(){
  $('a[href^=#]').click(function(){
    var speed = 500;
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    var body = 'body';
    if (navigator.userAgent.match(/MSIE/)){
      body = 'html';
    }
    $(body).animate({scrollTop:position}, speed, 'swing');
    return false;
  });
});
</script>
{/literal}


{include "common/footer.tpl"}
