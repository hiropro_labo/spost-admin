{include "common/header_error.tpl"}


{include "common/header_meta/info.tpl"}


<div class="reject">
{if is_null($list)}
  <tr>
      <td colspan="5">現在はお知らせがありません。</td>
  </tr>
{else}
{if $id == 0 || $id == ""}
{foreach from=$list item=item}
  <p class="reject_title">{$item->title}</p>

  <p class="reject_date">{$item->created_at|date_format:"%Y年%m月%d日"}</p>

  <div class="reason">
    <p>{$item->message|nl2br}</p>
  </div>
{/foreach}
{else}
  <p class="reject_title">{$list->title}</p>

  <p class="reject_date">{$list->created_at|date_format:"%Y年%m月%d日"}</p>

  <div class="reason">
    <p>{$list->message|nl2br}</p>
  </div>
{/if}
{/if}
</div>


{include "common/footer_root.tpl"}
