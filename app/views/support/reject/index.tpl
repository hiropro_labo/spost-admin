{include "common/header_error.tpl"}


{include "common/header_meta/reject.tpl"}


<div class="reject">
{if is_null($list)}
  <p class="reject_title">&nbsp;</p>

  <p class="reject_date">&nbsp;</p>

  <div class="reason">
    <p>現在はリジェクト情報がありません。</p>
  </div>
{else}
{foreach from=$list item=item}
  <p class="reject_title">{$item['title']}</p>

  <p class="reject_date">{$item['created_at']|date_format:"%Y年%m月%d日"}</p>

  <div class="reason">
    <p>{$item['comment']|nl2br}</p>
  </div>
{/foreach}
{/if}
</div>


{include "common/footer_root.tpl"}
