  </div>
  <!---->

</div>
<!--▲コンテナ-->
{* -------- Content : Right [End]   -------- *}


{if Agent::browser() != IE}
{* ----- Tutorial Modarl View START ----- *}
{if isset($tutorial_js)}
<!-- Modal -->
<div id="tutorialModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body tutorial">
    <img src="/assets/img/common/tutorial/1.png" width="500" height="375" class="tutorial-step" alt="tutorial_step_image" />

    <a href="/assets/img/common/tutorial/sample/top.jpg" class="tutorial-sample-open" target="_blank">
      <img src="/assets/img/common/tutorial/sample_btn.png" width="122" height="36" class="tutorial-download" alt="SAMPLEs SAVE" />
    </a>

    <a href="javascript:void(0);" class="tutorial-ok" onclick="javascript:tutorial_close();"><img src="/assets/img/common/tutorial/ok.png" width="204" height="66" class="tutorial-ok" alt="OK" /></a>
  </div>
</div>
<!--  -->
{$tutorial_js}
{/if}
{* ----- Tutorial Modarl View END ----- *}
{/if}


{* -------- Content : Footer-Meta [Start] -------- *}
<div id="foot">
<hr>
{include "common/content_footer.tpl"}
</div>
{* -------- Content : Footer-Meta [End]   -------- *}


{literal}
<script type="text/javascript">
// ログインドロップダウン
$(function(){
  $("#login_name").click(function(){
    $("#logout").toggle();
  });
});
// メニュードロップダウン
$(document).ready(function() {
  $("#nav").dropdown();
});
// ホバー
$('.d1').contenthover({
  overlay_opacity:1
});
// ツールチップ
$(document).ready(function() {
  $('.tooltip').tooltipster();
});

$('.tooltip').tooltipster({
  theme: '.my-custom-theme'
});
</script>
{/literal}


</body>
</html>
