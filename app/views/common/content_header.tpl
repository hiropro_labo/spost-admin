
{* -------- Content : Header [Start] -------- *}
<div id="line_1"></div>

<div id="head">
  <h1><a href="/"><img src="/assets/img/common/logo/logo.png" id="logo" width="200px"></a></h1>

{if $SPONSOR->contents()->theme()->is_exsits_category()}
{include "common/navigation.tpl"}
{/if}

  <div class="fr">

    <p id="login_name">{$SPONSOR->username()}{__('login.san')} <span>{__('login.login_now')}</span></p>
    <div id="logout">{__('login.client_id')}&nbsp;|&nbsp;{$SPONSOR->id()}<hr class="mt_10 mb_10" /><a href="/auth/logout" style="color:#008cb4;"><img src="/assets/img/common/nav/nav_logout.png">{__('login.logout')}</a></div>

  </div>
</div>
<div id="line_2"></div>
{* -------- Content : Header [End]   -------- *}
