<!-- コンテンツヘッダ -->
<div id="title_wrap">
  <div id="title_bar" class="menu_color"></div>

  <div id="title_box">
    <h2>{__('header.title')}</h2>
    <p id="title_sub">{__('header.sub')}</p>
  </div>

  <div id="help_pre_box">
    <p><a href="/assets/pdf/manual/ipost_manual.pdf" id="pre_btn" target="_blank" class="tooltip" title="使い方ガイドの PDF ファイルが閲覧できます。<br />PDF ファイルですので容量にご注意下さい！">{__('page.guide')}</a></p>
  </div>
</div>
<!---->
