<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<title>{__('site.title')}</title>

<!-- Favicon Icon -->
<link href="/assets/img/common/favicon/ipost.png" rel="shortcut icon" type="image/png" />

<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.tutorial.css">
<link rel="stylesheet" type="text/css" href="/assets/css/bass.css">
<link rel="stylesheet" type="text/css" href="/assets/css/path/sub.css">
<link rel="stylesheet" type="text/css" href="/assets/css/tooltipster.css">

{* ------------- Load : CSS [Start] ------------- *}
{include "common/load_css.tpl"}
{* ------------- Load : CSS [End]   ------------- *}

<script src="/assets/js/jquery-1.8.0.min.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery-ui.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/bootstrap.min.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.contenthover.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.dropdown.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.tooltipster.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.cookie.js" type="text/javascript" language="javascript"></script>

{* ------------- Load : Javascript [Start] ------------- *}
{include "common/load_js.tpl"}
{* ------------- Load : Javascript [End]   ------------- *}
</head>


<body>

{include "common/content_header.tpl"}

{include "common/pan/index.tpl"}

<!--▼コンテナ-->
<div id="container">

{if ! (in_array($current_page, $not_preview))}
  <!-- モーダルウィンドウ本体 -->
  <div id="dialog-preview" title="アプリプレビュー">
    <iframe src="/preview/{$current_page}" id="preview" name="preview" width="300" height="630">
     この部分はインラインフレームを使用しています。
    }
    </iframe>
{if $current_page=='store' || $current_page=='play'}
    <div class="preview_radio_group">
      <input id="previewStore" type="radio" name="check[]" value="0" onclick="previewChange(0);" checked="checked" />
      <label id="label_play" for="previewStore">App Store</label>

      <input id="previewPlay" type="radio" name="check[]" value="1" onclick="previewChange(1);" />
      <label id="label_store" for="previewPlay">Google Play</label>
    </div>
{/if}
  </div>
  <!---->

  <!--コンテンツ-->
  <div id="contents">
{else}
  <!--コンテンツ-->
  <div id="contents" class="no_preview">
{/if}



