      <div class="item_1">
        <h4><i class="fa fa-wrench mr_5"></i>アプリ管理</h4>
        <ul class="clearfix">
          <li class="top"><a href="/top">トップ</a></li>
          <li class="news"><a href="/news">ニュース</a></li>
          <li class="company"><a href="/company">企業概要</a></li>
          <li class="segments"><a href="/segments">事業内容</a></li>
          <li class="url"><a href="/url">URL</a></li>
          {* ----- Options ----- *}
          {if $SPONSOR->contents()->switcher()->is_active("recruit")}
          <li class="news"><a href="/topics">トピックス</a></li>
          {/if}
          {if $SPONSOR->contents()->switcher()->is_active("recruit")}
          <li class="recruit"><a href="/recruit">採用情報</a></li>
          {/if}
          <li><a href="/switcher">オプション設定</a></li>
        </ul>
      </div>
