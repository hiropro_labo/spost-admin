      <div class="item_1">
        <h4><i class="fa fa-wrench mr_5"></i>アプリ管理</h4>
        <ul class="clearfix">
          <li class="top"><a href="/top">トップ</a></li>
          <li class="news"><a href="/news">ニュース</a></li>
          <li class="menu"><a href="/menu">メニュー</a></li>
          <li class="store"><a href="/shop">店舗情報</a></li>
          <li class="url"><a href="/url">URL</a></li>
          {* ----- Options ----- *}
          {if $SPONSOR->contents()->switcher()->is_active("coupon")}
          <li class="coupon"><a href="/coupon">クーポン</a></li>
          {/if}
          {if $SPONSOR->contents()->switcher()->is_active("branch")}
          <li class="group"><a href="/branch">多店舗</a></li>
          {/if}
          {if $SPONSOR->contents()->switcher()->is_active("recruit")}
          <li class="recruit"><a href="/recruit">採用情報</a></li>
          {/if}
          <li><a href="/switcher">オプション設定</a></li>
        </ul>
      </div>
