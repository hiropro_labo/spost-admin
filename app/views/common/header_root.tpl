<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<title>{__('site.title')}</title>

<!-- Favicon Icon -->
<link href="/assets/img/common/favicon/ipost.png" rel="shortcut icon" type="image/png" />

<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.tutorial.css">
<link rel="stylesheet" type="text/css" href="/assets/css/path/root.css" media="all" />
<link rel="stylesheet" type="text/css" href="/assets/css/bass.css">
<link rel="stylesheet" type="text/css" href="/assets/css/path/sub.css">
<link rel="stylesheet" type="text/css" href="/assets/css/tooltipster.css">

{* ------------- Load : CSS [Start] ------------- *}
{include "common/load_css.tpl"}
{* ------------- Load : CSS [End]   ------------- *}

<script src="http://code.jquery.com/jquery-1.8.0.min.js" type="text/javascript" language="javascript"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/bootstrap.min.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.contenthover.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.dropdown.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.tooltipster.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.cookie.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.carouFredSel-6.2.1-packed.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/analytics.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/modal/popup.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/modal/tutorial.js" type="text/javascript" language="javascript"></script>

{* ------------- Load : Javascript [Start] ------------- *}
{include "common/load_js.tpl"}
{* ------------- Load : Javascript [End]   ------------- *}

</head>
<body>


{include "common/content_header.tpl"}

{include "common/pan/index.tpl"}
