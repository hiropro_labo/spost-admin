<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<title>{__('site.title')}</title>

<!-- Favicon Icon -->
<link href="/assets/img/common/favicon/ipost.png" rel="shortcut icon" type="image/png" />

<link rel="stylesheet" type="text/css" href="/assets/css/path/manual.css">

{* ------------- Load : CSS [Start] ------------- *}
{include "common/load_css.tpl"}
{* ------------- Load : CSS [End]   ------------- *}

<script src="http://code.jquery.com/jquery-1.8.0.min.js" type="text/javascript" language="javascript"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/manual/manual.js" type="text/javascript" language="javascript"></script>

{* ------------- Load : Javascript [Start] ------------- *}
{include "common/load_js.tpl"}
{* ------------- Load : Javascript [End]   ------------- *}

</head>
<body>

<div id="manual_head">
<p id="page-top"><a href="#top"></a></p>
<p>
<a href="/support/manual"><img src="/assets/img/common/manual/manual_title.png" class="manual_title" /></a>
<a href="#" onClick="window.close(); return false;"><img src="/assets/img/common/manual/close.png" alt="とじる" class="close" /></a>
</p>
<div class="clear"></div>
</div><!-- (/manual_head) -->
