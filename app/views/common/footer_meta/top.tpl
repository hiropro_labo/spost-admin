{literal}
<script type="text/javascript">
// ▼正式版
// 並び替え
$(function(){
  $('#sortable-li').sortable({
    update: function(ev, li){
      var update = $('#sortable-li').sortable("toArray");
      // alert(update);
      $.post("/top/order/set",
          {
            "positions": update,
          }, function(res)
          {
        	location.reload();
          }, 'json');
    },
    cursor: "move",
    opacity: "0.5"
  });
});
// ▲正式版
</script>
{/literal}