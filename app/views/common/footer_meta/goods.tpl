{literal}
<script type="text/javascript">
// カテゴリーのアコーディオン
$(function(){
  $(".menu_ac dt .blue_btn").hover(function(){
    $(this).css("cursor","pointer");
  },function(){
    $(this).css("cursor","default");
  });
  $(".menu_ac dd .menu_inline").css("display","none");
  $(".menu_ac dt .blue_btn:not(:animated)").click(function(){
    $(this).parent().parent().next().children().slideToggle("fast");
  });
});

function pickup(id){
    var data = {c_id: id};

    $.ajax({
        type: "post",
        url: '/goods/item/pickup/' + id,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(data),
        success: function(res){
            if (!res[0]){
                alert("おすすめの更新処理に失敗しました");
            }else{
                $('#pickup_' + id).html(res[1]);
                alert('指定された商品のおすすめ設定を更新しました');
            }
        },
        error: function(){
            alert('おすすめの更新処理に失敗しました');
        }
    });
}
</script>
{/literal}