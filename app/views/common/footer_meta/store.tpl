{literal}
<script type="text/javascript">
// 並び替え
$(function(){

  // スクリーンショット１
  $('#sortable-li').sortable({
    update: function(ev, li){
      var update = $('#sortable-li').sortable("toArray");
      $.post("/store/sshot/order",
          {
            "positions": update,
          }, function(res)
          {
        	  location.reload();
          }, 'json');
    },
    cursor: "move",
    opacity: "0.5"
  });

  // スクリーンショット2
  $('#sortable-li2').sortable({
    update: function(ev, li){
      var update = $('#sortable-li2').sortable("toArray");
      // alert(update);
      $.post("/store/sshot2/order",
          {
            "positions": update,
          }, function(res)
          {
        	  location.reload();
          }, 'json');
    },
    cursor: "move",
    opacity: "0.5"
  });

});
</script>
{/literal}