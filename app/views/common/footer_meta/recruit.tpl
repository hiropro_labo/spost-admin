{literal}
<script type="text/javascript">
function update(idx, obj){
    var button = $(obj);
    button.attr("disabled", true);

    var data = {
        title: $("#title" + idx).val(),
        body: $("#body" + idx).val()
    };

    $.ajax({
    	type: "post",
    	url: "/recruit/info/update/" + idx,
    	contentType: "application/json",
    	dataType: 'json',
    	data: JSON.stringify(data),
    	success: function(res){
    		if (!res[0]){
    			$("#error_msg" + idx).empty().append("更新処理に失敗しました<br />" + res[1]);
    		}else{
    		    location.reload();
    		}
    	},
        error: function(){
        	$("#error_msg" + idx).empty().append("更新処理に失敗しました[通信エラー発生]");
        },
        complete: function(){
        	button.attr("disabled", false);
        }
    });
}

function add(id, obj){
	var button = $(obj);
	button.attr("disabled", true);

	var data = {
		p_id: id,
		title: $("#add_title").val(),
	    body: $("#add_body").val()
	};

	$.ajax({
		type: "post",
		url: "/recruit/info/add",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(data),
		success: function(res){
			if (!res[0]){
				$("#error_msg_add").empty().append("更新処理に失敗しました<br />" + res[1]);
			}else{
				location.reload();
			}
		},
		error: function(){
			$("#error_msg_add").empty().append("更新処理に失敗しました[通信エラー発生]");
		},
		complete: function(){
			button.attr("disabled", false);
		}
	});
}

function del(idx, obj){
    if (!confirm("指定された項目を削除します\nよろしいですか？")) return false;

    var button = $(obj);
	button.attr("disabled", true);

    $.ajax({
    	type: "post",
    	url: "/recruit/info/del/" + idx,
    	success: function(res){
    		if (!res[0]){
    			$("#error_msg" + idx).empty().append("削除処理に失敗しました<br />" + res[1]);
    		}else{
    			location.reload();
    		}
    	},
        error: function(){
        	$("#error_msg" + idx).empty().append("更新処理に失敗しました[通信エラー発生]");
        },
        complete: function(){
        	button.attr("disabled", false);
        }
    });
}
</script>
{/literal}