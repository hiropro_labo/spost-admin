<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<title>{__('site.title')}</title>

<!-- Favicon Icon -->
<link href="/assets/img/common/favicon/ipost.png" rel="shortcut icon" type="image/png" />

<link rel="stylesheet" type="text/css" href="/assets/css/bass.css">
<link rel="stylesheet" type="text/css" href="/assets/css/path/error.css">

{* ------------- Load : CSS [Start] ------------- *}
{include "common/load_css.tpl"}
{* ------------- Load : CSS [End]   ------------- *}

<script src="http://code.jquery.com/jquery-1.8.0.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="/assets/js/analytics.js" type="text/javascript" language="javascript"></script>

{* ------------- Load : Javascript [Start] ------------- *}
{include "common/load_js.tpl"}
{* ------------- Load : Javascript [End]   ------------- *}
</head>


<body>

<div id="line_1"></div>

<div id="head">
  <h1><a href="/"><img src="/assets/img/common/logo/logo.png" id="logo" width="200px"></a></h1>

  </div>
</div>
<div id="line_2"></div>

{if $current_page=='info' || $current_page=='reject'}
{include "common/pan/index.tpl"}
{/if}


<!--▼コンテナ-->
<div id="container">

  <!--コンテンツ-->
  <div id="contents_error">

