<!-- パンくず -->
<div id="pan">
  <ul id="topicpath">
    <li><a href="/">{__('breadlist.bread_home')}</a></li>

{* -------- Breadcrumb list : Item1 [Start]   -------- *}
{if $current_page=='top' || $current_page=='coupon' || $current_page=='news' || $current_page=='menu'}
    <li>{__('breadlist.bread_edit_app')}</li>
{else if $current_page=='store' || $current_page=='inspect' || $current_page=='payment'}
    <li>{__('breadlist.bread_app_info')}</li>
{else if $current_page=='info' || $current_page=='reject' || $current_page=='help'}
    <li>{__('breadlist.bread_support')}</li>
{/if}
{* -------- Breadcrumb list : Item1 [End]   -------- *}


{* -------- Breadcrumb list : Item2 [Start]   -------- *}
{if $current_page=='top'}
    <li><a href="/top">{__('breadlist.bread_top')}</a></li>
{else if $current_page=='coupon'}
    <li><a href="/coupon">{__('breadlist.bread_coupon')}</a></li>
{else if $current_page=='news'}
    <li><a href="/news">{__('breadlist.bread_news')}</a></li>
{else if $current_page=='menu'}
    <li><a href="/menu">{__('breadlist.bread_menu')}</a></li>
{else if $current_page=='store'}
    <li><a href="/store">{__('breadlist.bread_edit')}</a></li>
{else if $current_page=='inspect'}
    <li><a href="/inspect">{__('breadlist.bread_inspect')}</a></li>
{else if $current_page=='payment'}
    <li><a href="/payment/apply">{__('breadlist.bread_payment')}</a></li>
{else if $current_page=='pop'}
    <li><a href="/pop">{__('breadlist.bread_pop')}</a></li>
{else if $current_page=='info'}
    <li><a href="/support/info">{__('breadlist.bread_info')}</a></li>
{else if $current_page=='reject'}
    <li><a href="/support/reject">{__('breadlist.bread_reject')}</a></li>
{else if $current_page=='help'}
    <li><a href="/support/help">{__('breadlist.bread_help')}</a></li>
{/if}
{* -------- Breadcrumb list : Item2 [End]   -------- *}
  </ul>
</div>
<!---->
