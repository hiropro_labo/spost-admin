<!-- ナビゲーション -->
<ul id="nav">
  <li class="w_100"><a href="/"><img src="/assets/img/common/nav/nav_home.png">{__('nav_home')}</a></li>
  <li class="w_120">
    <a href="/top"{if $current_page=='top' || $current_page=='coupon' || $current_page=='news'|| $current_page=='menu'} class="selected"{/if}><img src="/assets/img/common/nav/nav_admin.png">{__('app_edit.nav_edit')}</a>
    <ul>
      {if $SPONSOR->contents()->theme()->model()->category == 1}
      <li><a href="/top"{if $current_page=='top'} class="selected"{/if}><img src="/assets/img/common/nav/nav_top.png">{__('app_edit.nav_top')}</a></li>
      <li><a href="/news"{if $current_page=='news'} class="selected"{/if}><img src="/assets/img/common/nav/nav_news.png">{__('app_edit.nav_news')}</a></li>
      <li><a href="/menu"{if $current_page=='menu'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_menu')}</a></li>
      <li><a href="/shop"{if $current_page=='shop'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_shop')}</a></li>
      <li><a href="/url"{if $current_page=='url'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_url')}</a></li>
      {* ----- Options ----- *}
      {if $SPONSOR->contents()->switcher()->is_active("coupon")}
      <li><a href="/coupon"{if $current_page=='coupon'} class="selected"{/if}><img src="/assets/img/common/nav/nav_coupon.png">{__('app_edit.nav_coupon')}</a></li>
      {/if}
      {if $SPONSOR->contents()->switcher()->is_active("branch")}
      <li><a href="/branch"{if $current_page=='branch'} class="selected"{/if}><img src="/assets/img/common/nav/nav_coupon.png">{__('app_edit.nav_branch')}</a></li>
      {/if}
      {if $SPONSOR->contents()->switcher()->is_active("recruit")}
      <li><a href="/recruit"{if $current_page=='recruit'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_recruit')}</a></li>
      {/if}
      {/if}
      {if $SPONSOR->contents()->theme()->model()->category == 2}
      <li><a href="/top"{if $current_page=='top'} class="selected"{/if}><img src="/assets/img/common/nav/nav_top.png">{__('app_edit.nav_top')}</a></li>
      <li><a href="/news"{if $current_page=='news'} class="selected"{/if}><img src="/assets/img/common/nav/nav_news.png">{__('app_edit.nav_news')}</a></li>
      <li><a href="/goods"{if $current_page=='goods'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_goods')}</a></li>
      <li><a href="/shop"{if $current_page=='shop'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_shop')}</a></li>
      <li><a href="/url"{if $current_page=='url'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_url')}</a></li>
      {* ----- Options ----- *}
      {if $SPONSOR->contents()->switcher()->is_active("coupon")}
      <li><a href="/coupon"{if $current_page=='coupon'} class="selected"{/if}><img src="/assets/img/common/nav/nav_coupon.png">{__('app_edit.nav_coupon')}</a></li>
      {/if}
      {if $SPONSOR->contents()->switcher()->is_active("branch")}
      <li><a href="/branch"{if $current_page=='branch'} class="selected"{/if}><img src="/assets/img/common/nav/nav_coupon.png">{__('app_edit.nav_branch')}</a></li>
      {/if}
      {if $SPONSOR->contents()->switcher()->is_active("recruit")}
      <li><a href="/recruit"{if $current_page=='recruit'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_recruit')}</a></li>
      {/if}
      {/if}
      {if $SPONSOR->contents()->theme()->model()->category == 3}
      <li><a href="/top"{if $current_page=='top'} class="selected"{/if}><img src="/assets/img/common/nav/nav_top.png">{__('app_edit.nav_top')}</a></li>
      <li><a href="/news"{if $current_page=='news'} class="selected"{/if}><img src="/assets/img/common/nav/nav_news.png">{__('app_edit.nav_news')}</a></li>
      <li><a href="/company"{if $current_page=='company'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_company')}</a></li>
      <li><a href="/segments"{if $current_page=='segments'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_segments')}</a></li>
      <li><a href="/url"{if $current_page=='url'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_url')}</a></li>
      {* ----- Options ----- *}
      {if $SPONSOR->contents()->switcher()->is_active("recruit")}
      <li><a href="/topics"{if $current_page=='topics'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_topics')}</a></li>
      {/if}
      {if $SPONSOR->contents()->switcher()->is_active("recruit")}
      <li><a href="/recruit"{if $current_page=='recruit'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_recruit')}</a></li>
      {/if}
      {/if}
      {if $SPONSOR->contents()->theme()->model()->category == 4}
      <li><a href="/news"{if $current_page=='news'} class="selected"{/if}><img src="/assets/img/common/nav/nav_news.png">{__('app_edit.nav_news')}</a></li>
      <li><a href="/profile"{if $current_page=='profile'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_profile')}</a></li>
      <li><a href="/url"{if $current_page=='url'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_url')}</a></li>
      {* ----- Options ----- *}
      {if $SPONSOR->contents()->switcher()->is_active("recruit")}
      <li><a href="/topics"{if $current_page=='topics'} class="selected"{/if}><img src="/assets/img/common/nav/nav_menu.png">{__('app_edit.nav_topics')}</a></li>
      {/if}
      {/if}
    </ul>
    <div class="clear"></div>
  </li>
  <li class="w_120"><a href="/store"{if $current_page=='payment'} class="selected"{/if}><img src="/assets/img/common/nav/nav_info.png">{__('app_info.nav_info')}</a>
  <ul>
    <li><a href="/store"{if $current_page=='store'} class="selected"{/if}><img src="/assets/img/common/nav/nav_edit.png">{__('app_info.nav_edit')}</a></li>
    <li><a href="/inspect"{if $current_page=='inspect'} class="selected"{/if}><img src="/assets/img/common/nav/nav_app.png">{__('app_info.nav_inspect')}</a></li>
    <li><a href="/"{if $current_page=='expire'} class="selected"{/if}><img src="/assets/img/common/nav/nav_app.png">{__('app_info.nav_expire')}</a></li>
  </ul>
    <div class="clear"></div>
  </li>
</ul>
<!--  -->


