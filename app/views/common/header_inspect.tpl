<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<title>{__('site.title')}</title>

<!-- Favicon Icon -->
<link href="/assets/img/common/favicon/ipost.png" rel="shortcut icon" type="image/png" />

<link rel="stylesheet" type="text/css" href="/assets/css/top/bootstrap.tutorial.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/root.css" media="all" />
<link rel="stylesheet" type="text/css" href="/assets/css/top/bass.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/sub.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/tooltipster.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/spost.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/spost_a.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/blue.css">

{* ------------- Load : CSS [Start] ------------- *}
{include "common/load_css.tpl"}
{* ------------- Load : CSS [End]   ------------- *}

<script src="/assets/js/jquery-1.8.0.min.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery-ui.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/bootstrap.min.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.contenthover.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.dropdown.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.tooltipster.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.cookie.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.carouFredSel-6.2.1-packed.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/modal/popup.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/modal/tutorial.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/icheck.js" type="text/javascript" language="javascript"></script>
{literal}
<script>
    $(function(){
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '10%', // optional
            cursor: true // optional
        });
    });
</script>
{/literal}

{* ------------- Load : Javascript [Start] ------------- *}
{include "common/load_js.tpl"}
{* ------------- Load : Javascript [End]   ------------- *}

</head>
<body>


{include "common/content_header.tpl"}

{include "common/pan/index.tpl"}
