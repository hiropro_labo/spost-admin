{* Popup News *}
{if isset($popup_js)}
<!-- Modal -->
<div id="popupModal" class="modal hide fade popup-cp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body popup">
    <button class="close popup-close" onclick="javascript:popup_close();">×</button>

    <img src="/assets/img/common/popup/cp1.png" width="500" height="375" alt="POPUP NEWS IMAGE" />

    <a href="javascript:void(0);" class="popup-ok" onclick="javascript:popup_ok();" target="_blank"><img src="/assets/img/common/popup/to_detail.png" width="204" height="66" class="popup-ok" alt="OK" /></a>

  </div>
</div>
<!--  -->
{$popup_js}
{/if}

{* Tutorial Menu Click *}
{if isset($tutorial_js_root)}
{$tutorial_js_root}
{/if}

{if Agent::browser() != IE}
{* ----- Tutorial Modarl View START ----- *}
{if isset($tutorial_js)}
<!-- Modal -->
<div id="tutorialModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body tutorial">
    <img src="/assets/img/common/tutorial/1.png" width="500" height="375" class="tutorial-step" alt="tutorial_step_image" />

    <a href="javascript:void(0);" class="tutorial-ok" onclick="javascript:tutorial_close();"><img src="/assets/img/common/tutorial/ok.png" width="204" height="66" class="tutorial-ok" alt="OK" /></a>

  </div>
</div>
<!--  -->
{$tutorial_js}
{/if}
{* ----- Tutorial Modarl View END ----- *}
{/if}


<div id="foot">
<hr>
{include "common/content_footer.tpl"}
</div>


{literal}
<script type="text/javascript" language="javascript">
$(function() {
  //  Scrolled by user interaction
  $('#foo2').carouFredSel({
    circular: false,
    infinite: false,
    auto    : false,
    prev: '#prev2',
    next: '#next2',
    pagination: "#pager2",
    mousewheel: true,
    swipe: {
      onMouse: true,
      onTouch: true
    }
  });
});
// ログインドロップダウン
$(function(){
  $("#login_name").click(function(){
    $("#logout").toggle();
  });
});
// メニュードロップダウン
$(document).ready(function() {
  $("#nav").dropdown();
});
// ホバー
$('.d1').contenthover({
  overlay_opacity:1
});
// ツールチップ
$(document).ready(function() {
  $('.tooltip').tooltipster();
});

$('.tooltip').tooltipster({
  theme: '.my-custom-theme'
});
</script>
{/literal}


</body>
</html>