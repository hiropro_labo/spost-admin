{include "common/header.tpl"}


{include "common/header_meta/topics.tpl"}


<!-- トピックスの新規作成 -->
<div class="contents_box">
  <div class="contents_box_head">トピックスの新規作成
    <a href="/support/manual/news" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="トピックスの配信内容を設定することが出来ます。<br>「ファイルを選択」ボタンを押し、お好きな画像をアップロード・<br>
    テキストの記入・PUSH通知選択後、「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>トピックスの新規作成</h4>

  <form action="/topics/add" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label>画像のアップロード</label>
      <img src="{$topics->blank_image()}?{time()}" width="160" height="107" alt="トピックス画像" class="form_img con_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />600px&nbsp;×&nbsp;380px&nbsp;以上の大きさを推奨</p>
    </li>
    <li>
      <label class="hisu">タイトル</label>
      {$fieldset->field('title')->build()}
      <p class="desc">文字数：全角20文字以内を推奨</p>
      <p class="error">{$fieldset->error_msg('title')}</p>
    </li>
    <li>
      <label class="hisu">本文</label>
      {$fieldset->field('body')->build()}
      <p class="error">{$fieldset->error_msg('body')}</p>
    </li>
  </ul>
  <hr />

  <input type="submit" name="button" value="変更の確認" id="save_btn" class="save_btn" onclick="DisableButton(this);" />
  <a href="/topics" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
