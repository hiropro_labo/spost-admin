{include "common/header.tpl"}


{include "common/header_meta/topics.tpl"}


<!-- トピックスの新規作成 -->
<div class="contents_box">
  <div class="contents_box_head">トピックスの新規作成

  </div>
  <h4>トピックスの新規作成</h4>

  <form action="/topics/add/exe" method="POST">

<div class="contents_form">
  <ul class="cel">
    <li>
      <label>トピックス画像</label>
      <img src="{$topics->tmp_image_path_by_id($SPONSOR->id(), 'topics')}?{time()}" width="160" height="107" alt="トピックス画像" class="form_img con_img" />
    </li>

    <li>
      <label>タイトル</label>
      <span>{$fieldset->value('title')}</span>
    </li>

    <li>
      <label>本文</label>
      <span>{$fieldset->value('body')|nl2br}</span>
    </li>

  </ul>

  <hr />

  {* ------- Hidden Data [ Start ] -------- *}
  {foreach from=$fieldset->field() item=field}
  {$field}
  {/foreach}
  {* ------- Hidden Data [ End ] ---------- *}
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
