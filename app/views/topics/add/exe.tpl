{include "common/header.tpl"}


{include "common/header_meta/topics.tpl"}


<!-- トピックスの新規作成 -->
<div class="contents_box">
  <div class="contents_box_head">トピックスの新規作成
  </div>
  <h4>トピックスの新規作成</h4>

  <form action="/news/add" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <span class="ml_30">トピックスの新規作成が完了しました</span>
    </li>
  </ul>

  <hr />

  <a href="/topics" id="save_btn" class="back_btn ml_30">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
