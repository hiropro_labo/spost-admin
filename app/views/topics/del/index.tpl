{include "common/header.tpl"}


{include "common/header_meta/topics.tpl"}


<!-- トピックスの削除 -->
<div class="contents_box">
  <div class="contents_box_head">トピックスの削除
    <a href="/support/manual/news#news_2" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="トピックスの削除をすることが出来ます。<br>内容を確認後、「削除」ボタンを押して下さい。"></a>
  </div>
  <h4>トピックスの削除</h4>

  <form action="/topics/del/exe/{$topics->id}" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>トピックス画像</label>
      <img src="{$topics->image_path()}?{time()}" width="160" height="105" alt="トピックス画像" class="form_img con_img" />
    </li>

    <li>
      <label>タイトル</label>
      <span>{$fieldset->value('title')|default:"-----"}</span>
    </li>
    <li>
      <label>本文</label>
      <span>{$fieldset->value('body')|default:"-----"}</span>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="削除" class="save_btn" />
  <a href="/topics" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


{include "common/footer.tpl"}
