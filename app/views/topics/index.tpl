{include "common/header.tpl"}


{include "common/header_meta/topics.tpl"}


<!-- トピックスの編集 -->
<div class="contents_box">
  <div class="contents_box_head">トピックスの編集
    <a href="support/manual/news" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="トピックスの新規作成ができます。<br>あなたのお店の最新情報を配信していきましょう！"></a>
  </div>
  <h4>トピックスの編集</h4>
  <p class="mb_10"><span class="hisu">※簡易申請時に必須の項目です。</span><br />最低、１つのトピックスは配信準備が必要になっております。<br />
  PUSH通知機能は、アプリが公開状態になりましたらご利用できます。</p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/topics/add" class="edit_btn mt_m40">トピックスの新規作成</a>
{/if}
</div>

<!-- トピックス一覧 -->
<div class="contents_box">
  <div class="contents_box_head">トピックス一覧
    <a href="support/manual/news#news_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="トピックスの編集・削除ができます。"></a>
  </div>
  <h4>トピックス一覧</h4>

  <div class="row-fluid mb_20">

    {$pager}

    <ul id="news_table">

{if ! is_null($list)}
{foreach from=$list item=topics}
      <li class="news_table">
{if $SPONSOR->app()->is_lock()}
        <img src="{$topics->image_path()}?{time()}" width="160" height="107" class="mb_20 con_img" alt="トピックス画像" />
{else}
        <a href="/topics/update/{$topics->id}" title="編集"><img src="{$topics->image_path()}?{time()}" width="160" height="107" class="mb_20 con_img" alt="トピックス画像" /></a>
{/if}

        <div class="news_text_box">
          <p id="table_text"><b>{$topics->title}</b><br />{$topics->body}</p>

{if ! $SPONSOR->app()->is_lock()}
          <a href="/topics/update/{$topics->id}" class="mr_3 ml_30 news_edit_btn">編集</a>
          <a href="/topics/del/{$topics->id}" class="mr_3 news_edit_btn"><img src="/assets/img/common/icon/i11.png" width="18" id="i11"/></a>
{/if}
        </div>
        <div class="clear"></div>
      </li>

      <hr>
{/foreach}
{else}
      <li class="news_table">
      <p>現在、配信準備、または配信中のトピックスが登録されていません。</p>
      </li>
{/if}

    </ul>

{$pager}

  </div>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}