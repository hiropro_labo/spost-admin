{include "common/header.tpl"}

{include "common/header_meta/banner.tpl"}

<div class="contents_box">
  <div class="contents_box_head">バナーの削除
    <a href="/support/manual/menu#menu_2" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="登録したバナーを削除できます。<br>「削除」ボタンを押して、削除して下さい。"></a>
  </div>
  <h4>バナーの削除</h4>

  <form action="/banner/del/exe/{$model->id}" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>アップロード画像</label>
      <img src="{$model->image_path()}?{time()}" width="320" height="71" alt="バナー画像" class="form_img con_img" />
    </li>

    <li>
      <label>URL</label>
      <span>{$fieldset->value('url')|default:"-----"}</span>
    </li>

    <li>
      <label>表示設定</label>
      <span>{$fieldset->value('enable')|replace:'0':'非表示'|replace:'1':'表示'}</span>
    </li>
  </ul>

  <hr />

   <input type="submit" name="button" value="削除" class="save_btn" />
 <a href="/profile" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


{include "common/footer.tpl"}
