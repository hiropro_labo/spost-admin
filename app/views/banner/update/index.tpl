{include "common/header.tpl"}

{include "common/header_meta/banner.tpl"}

<div class="contents_box">
  <div class="contents_box_head">バナーの変更
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="新規追加したカテゴリーの内容が変更できます。<br>変更したい内容を変更後、「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>バナーの変更</h4>

  <form action="/banner/update/{$c_id}" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label>画像のアップロード</label>
      <img src="{$model->image_path()}?{time()}" width="320" height="71" alt="チームメンバー画像" class="form_img con_img" />
    </li>
    <li>
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br>640px&nbsp;×&nbsp;142px&nbsp;以上の大きさを推奨</p>
    </li>

    <li>
      <label class="hisu">URL</label>
      {$fieldset->field('url')->build()}
      <p class="error">{$fieldset->error_msg('url')}</p>
    </li>

    <li>
      <label>&nbsp;</label>
      <label for="form_enable_1"><input type="radio" required="required" value="1" id="form_enable_1" name="enable" {if $fieldset->value('enable') == '1'}checked="checked"{/if} />表示</label>
      <label for="form_enable_0"><input type="radio" required="required" value="0" id="form_enable_0" name="enable" {if $fieldset->value('enable') == '0'}checked="checked"{/if} />非表示</label>
      <p class="error">{$fieldset->error_msg('enable')}</p>
    </li>
  </ul>
      <p class="desc">お客様に見せるかどうかを選ぶことができます。</p>
  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/profile" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
