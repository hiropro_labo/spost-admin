{include "common/header.tpl"}

{include "common/header_meta/banner.tpl"}

<div class="contents_box">
  <div class="contents_box_head">バナーの変更
  </div>
  <h4>バナーの変更</h4>

  <form action="/banner/update/exe/{$c_id}" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>&nbsp;</label>
      {if $img_upload_flg}
      <img src="{$model->tmp_image_path()}?{time()}" width="320" height="71" alt="バナー画像" class="form_img con_img" />
      {else}
      <img src="{$model->image_path()}?{time()}" width="320" height="71" alt="バナー画像" class="form_img con_img" />
      {/if}
    </li>

    <li>
      <label>URL</label>
      <span>{$fieldset->value('url')|default:"-----"}</span>
    </li>

    <li>
      <label>表示設定</label>
      <span>{$fieldset->value('enable')|replace:'0':'非表示'|replace:'1':'表示'}</span>
    </li>
  </ul>

  <hr />

  {* ------- Hidden Data [ Start ] -------- *}
  {foreach from=$fieldset->field() item=field}
  {$field}
  {/foreach}
  {* ------- Hidden Data [ End ] ---------- *}
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
