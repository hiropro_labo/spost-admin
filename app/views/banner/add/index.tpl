{include "common/header.tpl"}


{include "common/header_meta/banner.tpl"}


<!-- バナーの新規作成 -->
<div class="contents_box">
  <div class="contents_box_head">バナーの新規登録
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="チームメンバーの新規登録ができます。<br>「ファイルを選択」ボタンを押して、画像をアップロード<br>テキストの記入、表示・非表示選択後<br>「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>バナーの新規登録</h4>

  <form action="/banner/add" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

  <div class="contents_form">
    <ul>
      <li>
        <label class="hisu">画像のアップロード</label>
        <img src="{$model->blank_image()}?{time()}" width="320" height="71" alt="バナー画像" class="form_img con_img" />
        <input type="file" name="upload" id="upload">
        <p class="desc">ファイルサイズ：3MBまで<br />640px&nbsp;×&nbsp;142px&nbsp;以上の大きさを推奨</p>
      </li>

      <li>
        <label class="hisu">URL</label>
        {$fieldset->field('url')->build()}
        <p class="error">{$fieldset->error_msg('url')}</p>
      </li>

      <li>
        <label>&nbsp;</label>
        <label for="form_enable_1"><input type="radio" required="required" value="1" id="form_enable_1" name="enable" checked="checked" />表示</label>
        <label for="form_enable_0"><input type="radio" required="required" value="0" id="form_enable_0" name="enable" />非表示</label>
        <p class="error">{$fieldset->error_msg('enable')}</p>
      </li>
    </ul>

    <p class="desc">お客様に見せるかどうかを選ぶことができます。</p>

    <hr />

    <input type="submit" name="button" value="変更の確認" class="save_btn" />
    <a href="/profile" id="save_btn" class="back_btn">戻る</a>
  </div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
