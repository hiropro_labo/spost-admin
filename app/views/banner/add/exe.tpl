{include "common/header.tpl"}

{include "common/header_meta/banner.tpl"}

<div class="contents_box">
  <div class="contents_box_head">バナーの新規登録
  </div>
  <h4>バナーの新規登録</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">バナーの新規登録が完了しました</span>
      </li>
    </ul>
    <hr />
    <a href="/profile" id="save_btn" class="back_btn ml_30">戻る</a>
  </div>
</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}
