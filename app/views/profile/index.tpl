{include "common/header.tpl"}

{include "common/header_meta/profile.tpl"}

{* ----- プロフィール：基本情報 [Start] ----- *}
<div class="contents_box">
  <div class="contents_box_head">
    プロフィール
    <a href="/support/manual/top#top_3"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="電話番号や営業時間などの、お店の情報を変更する事ができます。<br>「お店の画像」には、あなたのお店の魅力が伝わる写真をアップロードしましょう。。"></a>
  </div>

  <h4>写真の変更</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span></p>
{if ! $SPONSOR->app()->is_lock()}
  <a href="/profile/img/update" class="edit_btn">編集する</a>
  <a href="/profile/img/update"><img src="{$SPONSOR->contents()->image_profile()->image_path()}?{time()}" width="240" alt="プロフィール画像" class="mb_20 con_img" /></a>
{else}
  <img src="{$SPONSOR->contents()->image_profile()->image_path()}?{time()}" width="240" alt="プロフィール画像" class="mb_20 con_img" />
{/if}

  <hr />

  <h4>プロフィール情報</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span></p>
{if ! $SPONSOR->app()->is_lock()}
  <a href="/profile/info/update" class="edit_btn">編集する</a>
{/if}
  <ul class="cel">
    <li><label>お名前</label><span>{$SPONSOR->contents()->profile()->name()|default:"---"}</span></li>
    <li><label>肩書き</label><span>{$SPONSOR->contents()->profile()->position()|default:"---"}</span></li>
    <li><label>生年月日</label><span>{$SPONSOR->contents()->profile()->birth_day()|default:"---"}</span></li>
    <li><label>血液型</label><span>{$SPONSOR->contents()->profile()->blood()|default:"---"}</span></li>
    <li><label>趣味</label><span>{$SPONSOR->contents()->profile()->hobby()|default:"---"}</span></li>
  </ul>
</div>
{* ----- プロフィール：基本情報 [End] ----- *}


{* ----- 略歴一覧 [Start] ----- *}
<div class="contents_box">
  <div class="contents_box_head">略　歴
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="表示項目の詳細が作成できます。"></a>
  </div>

  <h4>略歴の登録・編集・削除ができます</h4>
  <p><span class="hisu"></span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/profile/history" class="edit_btn">編集する</a>
{/if}

  {if $profile_history}
  <ul class="cel">
  {foreach from=$profile_history item=item}
    <li><label>{$item->title}</label><span>{$item->body}</span></li>
  {/foreach}
  </ul>
  {else}
  <div class="menu_list row-fluid mb_20 mt_20 br_4 row-fluid_wrap">
    <p>現在、略歴が登録されていません。</p>
  </div>
  {/if}
</div>
{* ----- 略歴一覧 [End] ----- *}


{* ----- バナー一覧 [Start] ----- *}
<div class="contents_box">
  <div class="contents_box_head">バナー一覧
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="バナー関連の編集ができます。"></a>
  </div>

  <h4>バナーの登録・編集・削除ができます</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/banner/add" class="edit_btn">バナーの新規登録</a>
{/if}

{if ! is_null($list_banner) && count($list_banner)>0}
{* ----- バナー一覧 [Start] ----- *}
{foreach from=$list_banner item=item}
  <div class="row-fluid mb_20 mt_20 br_4 row-fluid_wrap" {if $item>enable == 0} style="background-color:#cccccc;max-height:100px;"{else}style="max-height:100px;"{/if}>
      <dl class="menu_ac">
        <dt>
          <div class="menu_cate_box">
{if ! $SPONSOR->app()->is_lock()}
            <a href="/banner/update/{$item->id}" title="編集"><img src="{$item->image_path()}?{time()}" width="320" height="71" alt="バナー画像" class="mb_20 con_img"></a>
{else}
            <img src="{$item->image_path()}?{time()}" width="320" height="71" alt="バナー画像" class="mb_20 con_img">
{/if}

{if ! $SPONSOR->app()->is_lock()}
            {* 編集ボタン *}
            <a href="/banner/update/{$item->id}" class="gray_btn mr_5 ml_20">編集</a>
            {* 削除ボタン *}
            <a href="/banner/del/{$item->id}" class="gray_btn mr_5"><img src="/assets/img/common/icon/i11.png" width="18" id="i11" style="margin-left:0;"></a>
{/if}

      </div>

{if ! $SPONSOR->app()->is_lock()}
      <div class="up_down">
        {if ! is_null($item->file_name) && count($list_banner) > 1}
          {* 表示順: アップ *}
          {if $item->position != 1}
          <a href="/banner/order/up/{$item->position}" class="gray_btn ml_20 mb_10"><img src="/assets/img/common/icon/i16.png" width="14" style="margin-left: 0;"/ ></a>
          {/if}

          {* 表示順: ダウン *}
          {if $item->position < count($list_banner)}
          <a href="/banner/order/down/{$item->position}" class="gray_btn ml_20"><img src="/assets/img/common/icon/i17.png" width="14" style="margin-left:0;"/ ></a>
          {/if}
        {/if}
      </div>
{/if}
      <div class="clear"></div>
      </dt>
    </dl>
  </div>

{/foreach}
{* ----- バナー一覧 [End]   ----- *}
{else}
  <div class="menu_list row-fluid mb_20 mt_20 br_4 row-fluid_wrap">
    <p>現在、バナーが登録されていません。</p>
  </div>
{/if}

</div>

<div class="last_margin"></div>

{include "common/footer_meta/recruit.tpl"}
{include "common/footer.tpl"}
