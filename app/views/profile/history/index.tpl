{include "common/header.tpl"}

{include "common/header_meta/profile.tpl"}

{* ----- 略歴一覧 [Start] ----- *}
<div class="contents_box">
  <div class="contents_box_head">略　歴
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="略歴が作成できます。"></a>
  </div>

  <h4 class="mb_20">略歴の登録・編集・削除ができます</h4>

  {if $list}
  {foreach from=$list item=item}
  <ul class="cel cel_recruit">
    <li class="clearfix">
      <div class="cel_recruit_wrap">
        <label><input type="text" id="title{$item->id}" value="{$item->title}" class="w_120"></label>
        <p class="fl"><textarea id="body{$item->id}">{$item->body}</textarea></p>
        <p class="fr"><input type="button" id="upd_{$item->id}" class="mr_3 ml_30 news_edit_btn" value="更新" onclick="update('{$item->id}', this)">&emsp;<input type="button" id="del_{$item->id}" class="mr_3 news_edit_btn" value="削除" onclick="del('{$item->id}', this)"></p>
        <p id="error_msg{$item->id}"></p>
       </div>
    </li>
  </ul>
  {/foreach}
  {/if}

  <ul class="cel cel_recruit">
      <li class="clearfix">
        <div class="cel_recruit_wrap">
          <label><input type="text" id="add_title" value="平成15年" class="w_120"></label>
          <p class="fl"><textarea id="add_body">第44回衆議院選挙に於いて2期目当選</textarea></p>
          <p class="di fr mr_13"><input type="button" value="追加" onclick="add(this)" class="news_edit_btn"></p>
          <p id="error_msg_add"></p>
        </div>
    </li>
  </ul>
</div>
{* ----- 略歴一覧 [End] ----- *}

<div class="last_margin"></div>

{include "common/footer_meta/profile.tpl"}
{include "common/footer.tpl"}
