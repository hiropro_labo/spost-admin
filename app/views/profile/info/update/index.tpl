{include "common/header.tpl"}

{include "common/header_meta/profile.tpl"}

<div class="contents_box">
  <div class="contents_box_head">プロフィールの変更
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="新規追加したカテゴリーの内容が変更できます。<br>変更したい内容を変更後、「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>プロフィールの変更</h4>

  <form action="/profile/info/update/" method="POST" name="form1" id="form1" class="form1">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">お名前</label>
      {$fieldset->field('name')->build()}
      <p class="error">{$fieldset->error_msg('name')}</p>
    </li>
    <li>
      <label class="hisu">肩書き</label>
      {$fieldset->field('position')->build()}
      <p class="error">{$fieldset->error_msg('position')}</p>
    </li>
    <li>
      <label >生年月日</label>
      昭和{$fieldset->field('birth_year')->build()}&nbsp;年&nbsp;{$fieldset->field('birth_month')->build()}&nbsp;月&nbsp;{$fieldset->field('birth_date')->build()}
      <p class="error">{$fieldset->error_msg('birth_year')}</p>
      <p class="error">{$fieldset->error_msg('birth_month')}</p>
      <p class="error">{$fieldset->error_msg('birth_date')}</p>
    </li>
    <li>
      <label >血液型</label>
      {$fieldset->field('blood')->build()}
      <p class="error">{$fieldset->error_msg('blood')}</p>
    </li>
    <li>
      <label >趣味</label>
      {$fieldset->field('hobby')->build()}
      <p class="error">{$fieldset->error_msg('hobby')}</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/profile" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
