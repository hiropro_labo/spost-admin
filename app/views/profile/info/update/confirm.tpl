{include "common/header.tpl"}

{include "common/header_meta/profile.tpl"}

<div class="contents_box">
  <div class="contents_box_head">プロフィールの変更
  </div>
  <h4>プロフィールの変更</h4>

  <form action="/profile/info/update/exe" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>お名前</label>
      <span>{$fieldset->value('name')|default:"-----"}</span>
    </li>

    <li>
      <label>肩書き</label>
      <span>{$fieldset->value('position')|default:"-----"}</span>
    </li>

    <li>
      <label>生年月日</label>
      <span>昭和{$fieldset->value('birth_year')}年{$fieldset->value('birth_month')}月{$fieldset->value('birth_date')}日</span>
    </li>

    <li>
      <label>血液型</label>
      <span>{$fieldset->value('blood')|default:"-----"}</span>
    </li>

    <li>
      <label>趣味</label>
      <span>{$fieldset->value('hobby')|default:"-----"}</span>
    </li>

  </ul>

  <hr />

  {* ------- Hidden Data [ Start ] -------- *}
  {foreach from=$fieldset->field() item=field}
  {$field}
  {/foreach}
  {* ------- Hidden Data [ End ] ---------- *}
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
