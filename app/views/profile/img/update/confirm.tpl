{include "common/header.tpl"}

{include "common/header_meta/profile.tpl"}

<!-- プロフィール画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">プロフィール画像の変更
  </div>
  <h4>プロフィール画像の変更</h4>

	<form action="/profile/img/update/exe" method="POST">

    <div class="contents_form">
      <ul>
        <li>
          <label>プロフィール画像</label>
    	    <img src="{$image->tmp_image_path()}?{time()}" width="240" height="240" alt="プロフィール画像" class="form_img con_img" />
        </li>
      </ul>

      <hr />

      <input type="submit" name="button" value="変更の保存" class="save_btn" />
      <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
    </div>

  </form>
</div>


{include "common/footer.tpl"}
