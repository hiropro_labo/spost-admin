{include "common/header.tpl"}

{include "common/header_meta/profile.tpl"}

<div class="contents_box">
  <div class="contents_box_head">プロフィール画像の変更
  </div>
  <h4>プロフィール画像の変更</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">プロフィール画像の変更が完了しました</span>
      </li>
    </ul>
    <hr />
    <a href="/profile" id="save_btn" class="back_btn ml_30">戻る</a>
  </div>
</div>

{include "common/footer.tpl"}
