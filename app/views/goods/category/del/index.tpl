{include "common/header.tpl"}


{include "common/header_meta/goods.tpl"}


<!-- カテゴリーの削除 -->
<div class="contents_box">
  <div class="contents_box_head">カテゴリーの削除
    <a href="/support/manual/menu#menu_2" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="登録したカテゴリーを削除できます。<br>「削除」ボタンを押して、削除して下さい。"></a>
  </div>
  <h4>カテゴリーの削除</h4>

  <form action="/goods/category/del/exe/{$category->id}" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>カテゴリー画像</label>
      <img src="{$category->image_path()}?{time()}" width="160" height="88" alt="商品カテゴリー画像" class="form_img con_img" />
    </li>

    <li>
      <label>カテゴリー名</label>
      <span>{$fieldset->value('title')|default:"-----"}</span>
    </li>

    <li>
      <label>サブタイトル名</label>
      <span>{$fieldset->value('sub_title')|default:"-----"}</span>
    </li>

    <li>
      <label>&nbsp;</label>
      <span>{$fieldset->value('enable')|replace:'0':'非表示'|replace:'1':'表示'}</span>
    </li>
  </ul>

  <hr />

   <input type="submit" name="button" value="削除" class="save_btn" />
 <a href="/goods" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


{include "common/footer.tpl"}
