{include "common/header.tpl"}


{include "common/header_meta/goods.tpl"}


<!-- カテゴリーの削除 -->
<div class="contents_box">
  <div class="contents_box_head">カテゴリーの削除
  </div>
  <h4>カテゴリーの削除</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">カテゴリーの削除が完了しました</span>
      </li>
    </ul>

    <hr />

    <a href="/goods" id="save_btn" class="back_btn ml_30">戻る</a>
  </div>

</div>
<!---->


{include "common/footer.tpl"}
