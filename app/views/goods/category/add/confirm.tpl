{include "common/header.tpl"}


{include "common/header_meta/goods.tpl"}


<!-- カテゴリーの新規作成 -->
<div class="contents_box">
  <div class="contents_box_head">カテゴリーの新規作成
  </div>
  <h4>カテゴリーの新規作成</h4>

  <form action="/goods/category/add/exe" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>カテゴリー画像</label>
      <img src="{$category->tmp_image_path_by_id($SPONSOR->id(), 'goods_category')}?{time()}" width="160" height="88" alt="商品カテゴリー画像" class="form_img con_img" />
    </li>

    <li>
      <label>カテゴリー名</label>
      <span>{$fieldset->value('title')|default:"-----"}</span>
    </li>

    <li>
      <label>サブタイトル名</label>
      <span>{$fieldset->value('sub_title')|default:"-----"}</span>
    </li>

    <li>
      <label>&nbsp;</label>
      <span>{$fieldset->value('enable')|replace:'0':'非表示'|replace:'1':'表示'}</span>
    </li>
  </ul>

  <hr />

  {* ------- Hidden Data [ Start ] -------- *}
  {foreach from=$fieldset->field() item=field}
  {$field}
  {/foreach}
  {* ------- Hidden Data [ End ] ---------- *}
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
