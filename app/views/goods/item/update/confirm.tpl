{include "common/header.tpl"}


{include "common/header_meta/goods.tpl"}


<!-- 商品の変更 -->
<div class="contents_box">
  <div class="contents_box_head">商品の変更
  </div>
  <h4>商品の変更</h4>

  <form action="/goods/item/update/exe/{$c_id}" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label>画像のアップロード</label>
      {if $img_upload_flg}
      <img src="{$item->tmp_image_path()}?{time()}" width="160" height="88" alt="商品画像" class="mb_20 con_img" />
      {else}
      <img src="{$item->image_path()}?{time()}" width="160" height="88" alt="商品画像" class="mb_20 con_img" />
      {/if}
    </li>

    <li>
      <label>カテゴリー</label>
      <span>{$category->title}</span>
    </li>

    <li>
      <label>商品名</label>
      <span>{$fieldset->value('title')}</span>
    </li>

    <li>
      <label>サブタイトル名</label>
      <span>{$fieldset->value('sub_title')}</span>
    </li>

    <li>
      <label>商品説明</label>
      <span>{$fieldset->value('description')}</span>
    </li>

    <li>
      <label>価格</label>
      <span>{$fieldset->value('price')|number_format}円</span>
    </li>

    <li>
      <label>URL</label>
      <span>{$fieldset->value('url')}</span>
    </li>

    <li>
      <label>&nbsp;</label>
      <span>{$fieldset->value('enable')|replace:'0':'非表示'|replace:'1':'表示'}</span>
     </li>
  </ul>

  <hr />

  {* ------- Hidden Data [ Start ] -------- *}
  {foreach from=$fieldset->field() item=field}
  {$field}
  {/foreach}
  {* ------- Hidden Data [ End ] ---------- *}
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
