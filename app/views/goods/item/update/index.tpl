{include "common/header.tpl"}


{include "common/header_meta/goods.tpl"}


<!-- 商品の変更 -->
<div class="contents_box">
  <div class="contents_box_head">商品の変更
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="商品の内容を変更できます。<br>変更したい内容を変更後、「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>商品の変更</h4>

  <form action="/goods/item/update/{$c_id}" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">画像のアップロード</label>
      <img src="{$item->image_path()}?{time()}" width="160" height="88" alt="商品トップ画像" class="form_img con_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />560px&nbsp;×&nbsp;320px&nbsp;以上の大きさを推奨</p>
    </li>

    <li>
      <label class="hisu">カテゴリー</label>
      {$fieldset->field('parent_id')->build()}
      <p class="error">{$fieldset->error_msg('parent_id')}</p>
    </li>
    <li>
      <label class="hisu">商品名</label>
      {$fieldset->field('title')->build()}
      <p class="desc">全角20文字まで</p>
      <p class="error">{$fieldset->error_msg('title')}</p>
    </li>
    <li>
      <label class="hisu">サブタイトル名</label>
      {$fieldset->field('sub_title')->build()}
      <p class="desc">全角28文字まで</p>
      <p class="error">{$fieldset->error_msg('sub_title')}</p>
    </li>
    <li>
      <label class="hisu">商品説明</label>
      {$fieldset->field('description')->build()}
      <p class="error">{$fieldset->error_msg('description')}</p>
    </li>
    <li>
      <label class="hisu">価格</label>
      {$fieldset->field('price')->build()}<span class="ml_5">円</span>
      <p class="desc mt_m15">半角英数字で入力して下さい。<br />
      価格の非表示は、「0」を設定して下さい。</p>
      <p class="error">{$fieldset->error_msg('price')}</p>
    </li>
    <li>
      <label>URL</label>
      {$fieldset->field('url')->build()}
      <p class="desc">オンラインショプなどへのリンクを貼ることができます。</p>
      <p class="error">{$fieldset->error_msg('url')}</p>
    </li>
    <li>
      <label>&nbsp;</label>
      <label for="form_enable_1"><input type="radio" required="required" value="1" id="form_enable_1" name="enable" {if $fieldset->value('enable') == '1'}checked="checked"{/if} />表示</label>

      <label for="form_enable_0"><input type="radio" required="required" value="0" id="form_enable_0" name="enable" {if $fieldset->value('enable') == '0'}checked="checked"{/if} />非表示</label>
      <p class="error">{$fieldset->error_msg('enable')}</p>
    </li>
    <li>
      <p class="desc">お客様に見せるかどうかを選ぶことができます。</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/goods" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
