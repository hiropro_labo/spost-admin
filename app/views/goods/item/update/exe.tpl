{include "common/header.tpl"}


{include "common/header_meta/goods.tpl"}


<!-- 商品の変更 -->
<div class="contents_box">
  <div class="contents_box_head">商品の変更
  </div>
  <h4>商品の変更</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">商品の変更が完了しました</span>
      </li>
    </ul>

    <hr />

    <a href="/goods" id="save_btn" class="back_btn ml_30">戻る</a>
  </div>

</div>
<!---->


{include "common/footer.tpl"}
