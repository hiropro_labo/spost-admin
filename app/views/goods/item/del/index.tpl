{include "common/header.tpl"}


{include "common/header_meta/goods.tpl"}


<!-- 商品トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">商品の削除
    <a href="/support/manual/menu#menu_4" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="カテゴリー商品の削除ができます。<br>「削除」ボタンを押して、削除して下さい。"></a>
  </div>
  <h4>商品の削除</h4>

  <form action="/goods/item/del/exe/{$item->id}" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label>商品トップ画像</label>
      <img src="{$item->image_path()}?{time()}" width="160" height="88" alt="商品トップ画像" class="mb_20 con_img" />
    </li>

    <li>
      <label>カテゴリー</label>
      <span>{$category->title}</span>
    </li>

    <li>
      <label>商品名</label>
      <span>{$fieldset->value('title')}</span>
    </li>

    <li>
      <label>サブタイトル名</label>
      <span>{$fieldset->value('sub_title')}</span>
    </li>

    <li>
      <label>商品説明</label>
      <span>{$fieldset->value('description')}</span>
    </li>

    <li>
      <label>価格</label>
      <span>{$fieldset->value('price')}</span>
    </li>

    <li>
      <label>URL</label>
      <span>{$fieldset->value('url')}</span>
    </li>

    <li>
      <label>&nbsp;</label>
      <span>{$fieldset->value('enable')|replace:'0':'非表示'|replace:'1':'表示'}</span>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="削除" class="save_btn" />
  <a href="/goods" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
