{include "common/header_root_new.tpl"}

<!--コンテンツ-->
<div id="contents" class="no_preview option">

  <!-- コンテンツヘッダ -->
  <div id="title_wrap">
    <div id="title_bar" class="top_color"></div>
    <div id="title_box">
      <h2>オプション設定</h2>
      <p id="title_sub">アプリのカテゴリーやオプションページを選択することができます。</p>
    </div>
  </div>
  <br class="clear" />
  <!--/コンテンツヘッダー-->

  <!--カテゴリー選択-->
  <div class="category_choose mt_10">
    <h3>選択カテゴリー</h3>
    <p class="text_sub">選択したカテゴリーが表示されます。</p>
    {if $category == 1}
      <div class="box clearfix">
        <div class="restaurant_on fl mr_20"></div>
        <div class="box_fr">
          <h4>飲食</h4>
          <p class="description">店舗情報、メニュー</p>
        </div>
      </div>
    {/if}
    {if $category == 2}
      <div class="box clearfix">
        <div class="goods_on fl mr_20"></div>
        <div class="box_fr">
          <h4>物販</h4>
          <p class="description">店舗情報、商品一覧</p>
        </div>
      </div>
    {/if}
    {if $category == 3}
      <div class="box clearfix">
        <div class="company_on fl mr_20"></div>
        <div class="box_fr">
          <h4>企業</h4>
          <p class="description">会社概要、事業内容</p>
        </div>
      </div>
    {/if}
  </div>
  <br class="clear" />
  <hr />
  <!--/カテゴリー選択-->

  <!--オプション選択-->
  {include "switcher/options/category_{$category}.tpl"}
  <!--/オプション選択-->

</div>

{literal}
<script type="text/javascript">
$(function(){
	$('input').on('ifChanged', function(event){
		var ctl = $('#' + event.currentTarget.id).val();
		var url = '/switcher/update/' + ctl;
		$.get(url, null, function(data, status){
			alert('オプション設定を更新しました');
		});
	});
});
</script>
{/literal}



{include "common/footer.tpl"}
