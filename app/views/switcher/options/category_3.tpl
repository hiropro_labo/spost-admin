  <!--オプション選択-->
  <div class="option_choose mt_40 clearfix">
    <h3>オプション選択</h3>
    <p class="text_sub">必要に応じて、アプリ内にお好きなページを追加することができます。</p>


    <div class="box">
      <label><input type="checkbox" id="check1" value="topics" {if $SPONSOR->contents()->switcher()->is_active("topics")}checked{/if}>
      <img src="/assets/img/app/spost/option/o_list4.png"></label>
      <div class="box_right">
        <h4>TOPICS</h4>
        <p>特集記事をアプリに表示することができます。</p>
      </div>
    </div>

    <div class="box">
      <label><input type="checkbox" id="check2" value="recruit" {if $SPONSOR->contents()->switcher()->is_active("recruit")}checked{/if}>
      <img src="/assets/img/app/spost/option/o_list4.png"></label>
      <div class="box_right">
        <h4>採用情報</h4>
        <p>採用情報のページをアプリに追加します。</p>
      </div>
    </div>

  </div>
  <!--/オプション選択-->
