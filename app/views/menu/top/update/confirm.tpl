{include "common/header.tpl"}


{include "common/header_meta/menu.tpl"}


<!-- メニュートップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">メニュートップ画像の変更
  </div>
  <h4>メニュートップ画像の変更</h4>

	<form action="/menu/top/update/exe" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>メニュートップ画像</label>
	    <img src="{$image->tmp_image_path()}?{time()}" width="160" height="88" alt="メニュートップ画像" class="form_img con_img" />
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


{include "common/footer.tpl"}
