{include "common/header.tpl"}


{include "common/header_meta/menu.tpl"}


<!-- カテゴリーの新規作成 -->
<div class="contents_box">
  <div class="contents_box_head">カテゴリーの新規作成
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="カテゴリーの新規作成ができます。<br>「ファイルを選択」ボタンを押して、画像をアップロード<br>テキストの記入、表示・非表示選択後<br>「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>カテゴリーの新規作成</h4>

  <form action="/menu/category/add" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

  <div class="contents_form">
    <ul>
      <li>
        <label class="hisu">画像のアップロード</label>
        <img src="{$category->blank_image()}?{time()}" width="160" height="88" alt="メニュートップ画像" class="form_img con_img" />
        <input type="file" name="upload" id="upload">
        <p class="desc">ファイルサイズ：3MBまで<br />640px&nbsp;×&nbsp;350px&nbsp;以上の大きさを推奨</p>
      </li>

      <li>
        <label class="hisu">カテゴリー名</label>
        {$fieldset->field('title')->build()}
        <p class="error">{$fieldset->error_msg('title')}</p>
      </li>

      <li>
        <label class="hisu">サブタイトル名</label>
        {$fieldset->field('sub_title')->build()}
        <p class="error">{$fieldset->error_msg('sub_title')}</p>
      </li>

      <li>
        <label>&nbsp;</label>
        <label for="form_enable_1"><input type="radio" required="required" value="1" id="form_enable_1" name="enable" checked="checked" />表示</label>

        <label for="form_enable_0"><input type="radio" required="required" value="0" id="form_enable_0" name="enable" />非表示</label>
        <p class="error">{$fieldset->error_msg('enable')}</p>
      </li>
    </ul>

    <p class="desc">お客様に見せるかどうかを選ぶことができます。</p>

    <hr />

    <input type="submit" name="button" value="変更の確認" class="save_btn" />
    <a href="/menu" id="save_btn" class="back_btn">戻る</a>
  </div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
