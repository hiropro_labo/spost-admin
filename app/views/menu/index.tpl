{include "common/header.tpl"}


{include "common/header_meta/menu.tpl"}


<!-- メニュートップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">メニュートップ画像の変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="メニュートップ画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>メニュートップ画像の変更</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/menu/top/update" class="edit_btn">変更する</a>
  <a href="/menu/top/update" title="変更"><img src="{$image->image_path()}?{time()}" width="160" height="88" alt="メニュートップ画像" class="mb_20 con_img" /></a>
{else}
  <img src="{$image->image_path()}" width="160" height="88" alt="メニュートップ画像" class="mb_20 con_img" />
{/if}
</div>
<!---->


<!-- カテゴリーの編集 -->
<div class="contents_box">
  <div class="contents_box_head">カテゴリーの編集
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="カテゴリーが追加できます。<br>「カテゴリーの新規登録」ボタンを押して、カテゴリーを追加して下さい。"></a>
  </div>
  <h4>カテゴリーの編集</h4>
  <p class="mb_20"><span class="hisu">※簡易申請時に必須の項目です。</span><br />最低、１個のカテゴリーと、１個の商品は必要になっております。<br />
  <span class="hisu">商品の登録はカテゴリーの登録後、「メニュー作成」を押して下さい。</span>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/menu/category/add" class="edit_btn mt_m12">カテゴリーの新規登録</a>
{/if}
  </p>
</div>


{* ----- メニュー一覧 [Start] ----- *}
<div class="contents_box">
  <div class="contents_box_head">メニュー一覧
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="カテゴリー商品の詳細が作成できます。<br>商品の魅力が伝わる画像と内容をご紹介しましょう。"></a>
  </div>
  <h4>メニュー一覧</h4>

{if ! is_null($list) && count($list)>0}
{* ----- カテゴリー一覧 [Start] ----- *}
{foreach from=$list item=entry}
<div class="row-fluid mb_20 mt_20 br_4 row-fluid_wrap"{if $entry['category']->enable == 0} style="background-color:#cccccc;"{/if}>
  <dl class="menu_ac">
    <dt>
      <div class="menu_cate_box">
{if ! $SPONSOR->app()->is_lock()}
        <a href="/menu/category/update/{$entry['category']->id}" title="変更"><img src="{$entry['category']->image_path()}?{time()}" width="160" height="88" alt="メニューカテゴリー画像" class="mb_20 con_img"></a>
{else}
        <img src="{$entry['category']->image_path()}?{time()}" width="160" height="88" alt="メニューカテゴリー画像" class="mb_20 con_img">
{/if}

        <p class="menu_table_text">
{if ! $SPONSOR->app()->is_lock()}
          <a href="/menu/category/update/{$entry['category']->id}">{$entry['category']->title}</a><br />
{else}
          {$entry['category']->title}<br />

{/if}
          {$entry['category']->sub_title}
        </p>

{if ! $SPONSOR->app()->is_lock()}
        <a href="/menu/category/update/{$entry['category']->id}" class="gray_btn mr_5 ml_20">カテゴリの編集</a>

        <a href="/menu/category/del/{$entry['category']->id}" class="gray_btn mr_5"><img src="/assets/img/common/icon/i11.png" width="18" id="i11" style="margin-left:0;"></a>
{/if}

        <a href="javascript:void(0);" class="blue_btn">メニュー作成</a>
      </div>

{if ! $SPONSOR->app()->is_lock()}
      <div class="up_down">
        {if ! is_null($entry['category']->file_name) && count($list) > 1}
          {if $entry['category']->position != 1}
          <a href="/menu/category/order/up/{$entry['category']->position}" class="gray_btn ml_20 mb_10"><img src="/assets/img/common/icon/i16.png" width="14" style="margin-left: 0;"/ ></a>
          {/if}

          {if $entry['category']->position < count($list)}
          <a href="/menu/category/order/down/{$entry['category']->position}" class="gray_btn ml_20"><img src="/assets/img/common/icon/i17.png" width="14" style="margin-left:0;"/ ></a>
          {/if}
        {/if}
      </div>
{/if}
      <br class="clear">
    </dt>

    {* ----- 商品一覧 [Start] ----- *}
    <dd>
      {foreach from=$entry['menu'] item=menu}
      <div class="menu_inline">
{if ! $SPONSOR->app()->is_lock()}
        <a href="/menu/item/update/{$menu->id}"><img src="{$menu->image_path()}?{time()}" width="160" height="88" alt="メニュー商品画像" class="mb_20 con_img" /></a>
{else}
        <img src="{$menu->image_path()}?{time()}" width="160" height="88" alt="メニュー商品画像" class="mb_20 con_img" />
{/if}

        <p class="menu_table_text">
{if ! $SPONSOR->app()->is_lock()}
          <a href="/menu/item/update/{$menu->id}">{$menu->title}</a><br />
{else}
          {$menu->title}<br />
{/if}
          {$menu->sub_title}
        </p>

{if ! $SPONSOR->app()->is_lock()}
        <a href="/menu/item/update/{$menu->id}" class="gray_btn mr_5 ml_20">商品の編集</a>

        <a href="/menu/item/del/{$menu->id}" class="gray_btn mr_5"><img src="/assets/img/common/icon/i11.png" width="18" style="margin-left:0px;"/></a>
        <a href="javascript:pickup('{$menu->id}');" id="pickup_{$menu->id}" class="gray_btn">{if $menu->pickup == '0'}☆{else}★{/if}</a>
{/if}

{if ! $SPONSOR->app()->is_lock()}
        <div class="up_down" id="up_down_inside">
        {if ! is_null($menu->file_name) && $entry['category']->menu_max_cnt > 1}
          {if $menu->position != 1}
          <a href="/menu/item/order/up/{$menu->position}/{$menu->parent_id}" class="gray_btn ml_20 mb_10"><img src="/assets/img/common/icon/i16.png" width="14" style="margin-left:0;"></a>
          {/if}
          {if $menu->position < $entry['category']->menu_max_cnt}
          <a href="/menu/item/order/down/{$menu->position}/{$menu->parent_id}" class="gray_btn ml_20"><img src="/assets/img/common/icon/i17.png" width="14" style="margin-left:0;"></a>
          {/if}
        {/if}
        </div>
{/if}
      </div>
      {/foreach}
      <div class="clear"></div>

{if ! $SPONSOR->app()->is_lock()}
      <div class="menu_inline add_box">
        <a href="/menu/item/add/{$entry['category']->id}" class="blue_btn add_btn" id="new_edit">商品の追加</a>
      </div>
{/if}
    </dd>
  </dl>

  <div class="clear"></div>
</div>
{* ----- 商品一覧 [End]   ----- *}

{/foreach}
{* ----- カテゴリー一覧 [End]   ----- *}
{else}
  <div class="menu_list row-fluid mb_20 mt_20 br_4 row-fluid_wrap">
    <p>現在、カテゴリー、商品の登録がされていません。</p>
  </div>
{/if}

</div>
<!---->


<div class="last_margin"></div>


{include "common/footer_meta/menu.tpl"}
{include "common/footer.tpl"}
