{include "common/header_root_new.tpl"}

<div id="home" class="clearfix">

  <!--left-->
  <div class="left">

    <!--アプリ状況-->
    <div class="app">
      <h3><i class="fa fa-tablet mr_5"></i>Your App</h3>
      <div id="app_status">
        <img src="{$SPONSOR->contents()->icon()->image_path()}" />
        <h2>{$SPONSOR->app()->app_name()|default:"オリジナルアプリ"}</h2>
        {if $SPONSOR->app()->status() == 500}
        <p class="app_lv"><span class="open"><i class="fa fa-check sign"></i></span>アプリ公開中</p>
        {elseif $SPONSOR->app()->status() < 500}
        <p class="app_lv"><span class="stop"><i class="fa fa-minus sign"></i></span>アプリ準備中</p>
        {else}
        <p class="app_lv"><span class="time"><i class="fa fa-times sign"></i></span>アプリ公開停止</p>
        {/if}
      </div>
      <p id="app_text">
      {if $SPONSOR->app()->status() == 500}
      アプリ公開中です。
      {elseif $SPONSOR->app()->status() < 500}
      アプリ準備中です。
      {else}
      アプリ公開停止中です。
      {/if}
      <br />現在までのダウンロード数 : {$SPONSOR->app()->app_count()|replace:"0":""|default:"-----"} 個</p>
    </div>
    <!--アプリ状況-->

    <!--インフォメーション-->
    <div class="info">
      <h3><i class="fa fa-bell mr_5"></i>Infomation</h3>
      <dl>
        <dt><span><i class="fa fa-bullhorn"></i></span><a href="#">【重要】銀行振替開始のお知らせ</a></dt><dd>iPost利用料の決済方法に銀行振替が追加されましたのでお知らせいたします。<p>2014-05-30<span><!--▶新着だった場合--><i class="fa fa-circle-o mr_5"></i>new</span><!--◀ここまで--></p></dd>
        <dt><span><i class="fa fa-bullhorn"></i></span><a href="#">【重要】次回決済のお知らせ</a></dt><dd>お客様がご利用されているアプリの次回決済期日が5月20日（火）となっております。<p>2014-05-15<span></p></dd>
        <dt><span><i class="fa fa-bullhorn"></i></span><a href="#">アップグレードについて</a></dt><dd>アップグレードにより変更された点についてご報告いたします。<p>2014-05-07</p></dd>
        <dt><span><i class="fa fa-bullhorn"></i></span><a href="#">【重要】本社移転のご案内</a></dt><dd>このたび弊社は事務所を下記に移転いたしましたのでご案内申し上げます。<p>2014-01-27</p></dd>
      </dl>
    </div>
    <!--/インフォメーション-->

  </div>
  <!--/left-->

  <!--right-->
  <div class="right">

    <!--コンテンツメニュー-->
    <div class="edit">
      <!--アプリ管理-->
      {include "common/menu/category_{$category}.tpl"}
      <!--/アプリ管理-->
      <!--アプリ情報-->
      <div class="item_2">
        <h4><i class="fa fa-bar-chart-o mr_5"></i>アプリ情報</h4>
        <ul class="clearfix">
          <li class="info_edit"><a href="/store"><p>情報編集</p><p class="f_gra">アプリ公開用の情報を入力して下さい</p></a></li>
          <li class="apply"><a href="/inspect"><p>審査申請</p><p class="f_gra">アプリ制作完了後、申請を行って下さい</p></a></li>
          <li class="limit"><a href="/"><p>有効期限</p><p class="f_gra">アプリの利用有効期限の確認ができます</p></a></li>
        </ul>
      </div>
      <!--/アプリ情報-->
    </div>
    <!--/コンテンツメニュー-->

    <!--ヘルプ-->
    <div class="help clearfix">
      <h3><i class="fa fa-graduation-cap mr_5"></i>Help</h3>
        <a href="#" class="hl1">
          <img src="/assets/img/app/spost/top/hl1.png">
          <p class="f_or">アプリ制作マニュアル</p><p class="f_gra">詳しいアプリの作り方</p>
        </a>
        <a href="#" class="hl2">
          <img src="/assets/img/app/spost/top/hl2.png">
          <p class="f_pu">アプリ公開までの流れ</p><p class="f_gra">アプリ制作から公開までの手順</p>
        </a>
        <a href="#" class="hl3">
          <img src="/assets/img/app/spost/top/hl3.png">
          <p class="f_gre">販促物</p><p class="f_gra">見本の閲覧とダウンロード</p>
        </a>
        <a href="#" class="hl4">
          <img src="/assets/img/app/spost/top/hl4.png">
          <p class="f_re">ヘルプ</p><p class="f_gra">お困りのときはこちら</p>
        </a>
    </div>
    <!--/ヘルプ-->

  </div>
  <!--/right-->

</div>

<br class="clear">








<div id="foot">
<hr>
{include "common/content_footer.tpl"}
</div>


{literal}
<script type="text/javascript" language="javascript">
$(function() {
  //  Scrolled by user interaction
  $('#foo2').carouFredSel({
    circular: false,
    infinite: false,
    auto    : false,
    prev: '#prev2',
    next: '#next2',
    pagination: "#pager2",
    mousewheel: true,
    swipe: {
      onMouse: true,
      onTouch: true
    }
  });
});
// ログインドロップダウン
$(function(){
  $("#login_name").click(function(){
    $("#logout").toggle();
  });
});
// メニュードロップダウン
$(document).ready(function() {
  $("#nav").dropdown();
});
// ホバー
$('.d1').contenthover({
  overlay_opacity:1
});
// ツールチップ
$(document).ready(function() {
  $('.tooltip').tooltipster();
});

$('.tooltip').tooltipster({
  theme: '.my-custom-theme'
});
</script>
{/literal}

</body>
</html>
