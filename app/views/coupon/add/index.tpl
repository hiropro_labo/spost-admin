{include "common/header.tpl"}


{include "common/header_meta/coupon.tpl"}


<!-- クーポンの新規登録 -->
<div class="contents_box">
  <div class="contents_box_head">クーポンの新規登録
　<a href="/support/manual/coupon"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="お客様に特別なクーポンを提供することが出来ます。<br>「ファイルを選択」ボタンを押して、画像をアップロード<br>利用条件、期間設定後「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>クーポンの新規登録</h4>

  <form action="/coupon/add" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label>タイトル</label>
      {$fieldset->field('title')->build()}
      {$fieldset->error_msg('title')}
    </li>

    <li>
      <label>クーポン内容</label>
      {$fieldset->field('body')->build()}
      {$fieldset->error_msg('body')}
    </li>

    <li>
      <label>利用条件</label>
      {$fieldset->field('policy')->build()}
      {$fieldset->error_msg('policy')}
    <p class="desc">実際のスマホアプリでは、クーポン画像をタップすると表示されます。<br />※左の簡易プレビューには表示されません。</p>
    </li>

    <li class="mb_20">
      <label>期間指定</label>
      <!-- {$fieldset->field('term_flg')->build()} -->
      <label for="form_term_flg_0"><input type="radio" required="required" id="form_term_flg_0" name="term_flg" value="0" checked="checked" />指定しない</label>
      <label for="form_term_flg_1"><input type="radio" required="required" id="form_term_flg_1" name="term_flg" value="1" />指定する</label>
      <p class="error">{$fieldset->error_msg('term_flg')}</p>
    </li>

    <li>
      <label>表示開始日</label>
      {$fieldset->field('start_year')->build()}年{$fieldset->field('start_month')->build()}月{$fieldset->field('start_day')->build()}日
      <p class="error">{$fieldset->error_msg('start_year')}</p>
    </li>

    <li>
      <label>表示終了日</label>
      {$fieldset->field('end_year')->build()}年{$fieldset->field('end_month')->build()}月{$fieldset->field('end_day')->build()}日
      <p class="error">{$fieldset->error_msg('end_year')}</p>
    </li>
  </ul>


  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/coupon" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{literal}
<script type="text/javascript">
$(function(){
	  if($('#form_body').val() != "")
	  {
	    $('#form_body').css({"height":"300px"});
	  }
	  $('#form_body').focus(function(){
	    $(this).animate({"height":"300px"}, "swing");
	  });
	  if($('#form_policy').val() != "")
	  {
	    $('#form_policy').css({"height":"300px"});
	  }
	  $('#form_policy').focus(function(){
	    $(this).animate({"height":"300px"}, "swing");
	  });
});
</script>
{/literal}


{include "common/footer.tpl"}
