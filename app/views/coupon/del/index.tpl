{include "common/header.tpl"}


{include "common/header_meta/coupon.tpl"}


<!-- クーポンの削除 -->
<div class="contents_box">
  <div class="contents_box_head">クーポンの削除
 <a href="/support/manual/coupon#coupon_1"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="クーポンの削除ができます。<br>「このクーポンを削除」ボタンを押して、削除して下さい。"></a>
  </div>
  <h4>クーポンの削除</h4>

  <form action="/coupon/del/exe/{$coupon->id}" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>タイトル</label>
      {$fieldset->value('title')|default:"-----"}
    </li>

    <li>
      <label>クーポン内容</label>
      {$fieldset->value('body')|default:"-----"}
    </li>

    <li>
      <label>利用条件</label>
      {$fieldset->value('policy')|default:"-----"}
    </li>

    <li>
      <label>期間指定</label>
      {$fieldset->value('term_flg')|replace:'0':'指定なし'|replace:'1':'指定あり'}
    </li>

    {if $fieldset->value('term_flg') == '1'}
    <li>
      <label>表示期間</label>
      {$fieldset->value('start_year')}年{$fieldset->value('start_month')}月{$fieldset->value('start_day')}日
      〜{$fieldset->value('end_year')}年{$fieldset->value('end_month')}月{$fieldset->value('end_day')}日
    </li>
    {/if}
  </ul>

  <hr />

  <input type="submit" name="button" value="このクーポンを削除" class="save_btn" />
  <a href="/coupon" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
