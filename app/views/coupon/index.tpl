{include "common/header.tpl"}


{include "common/header_meta/coupon.tpl"}


<!-- クーポンの変更 -->
<div class="contents_box">
  <div class="contents_box_head">クーポンの変更
    <a href="/support/manual/coupon" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="お客様に特別なクーポンを提供する事ができます。<br>「編集」ボタンから画像や内容を変更することができます。<br>クーポンは、全部で５枚まで登録可能です。"></a></div>

  <h4>クーポンの登録・変更</h4>
  <p class="mb_20"><span class="hisu">※簡易申請時に必須の項目です。</span><br />最低、１枚は必要になっております。

{if ! $SPONSOR->app()->is_lock() && !$SPONSOR->contents()->coupon()->is_entry_max()}
  <a href="/coupon/add" class="edit_btn">クーポンの新規登録</a>
{/if}
  </p>

{if ! is_null($list) && count($list)>0}
{* ----- クーポン一覧 [Start] ----- *}
{foreach from=$list item=item}
  <div class="row-fluid br_4 row-fluid_wrap"style="max-height:100px;">
      <dl class="menu_ac_recruit">
        <dt>
          <div class="menu_cate_box_recruit">
            <p class="menu_table_text_recruit">
{if ! $SPONSOR->app()->is_lock()}
            <a href="/coupon/update/{$item->id}">{$item->title}</a><br />
{else}
            {$item->title}<br />
{/if}
            </p>

  <div class="fr">
    {if ! $SPONSOR->app()->is_lock()}
                {* 編集ボタン *}
                <a href="/coupon/update/{$item->id}" class="gray_btn mr_5 ml_20">編集</a>
                {* 削除ボタン *}
                <a href="/coupon/del/{$item->id}" class="gray_btn mr_5"><img src="/assets/img/common/icon/i11.png" width="18" id="i11" style="margin-left:0;"></a>
    {/if}
    
          
    
    {if ! $SPONSOR->app()->is_lock()}
          
            {if ! is_null($item->file_name) && count($list) > 1}
              {* 表示順: アップ *}
              {if $item->position != 1}
              <a href="/coupon/order/up/{$item->position}" class="gray_btn ml_20 mb_10"><img src="/assets/img/common/icon/i16.png" width="14" style="margin-left: 0;"/ ></a>
              {/if}
    
              {* 表示順: ダウン *}
              {if $item->position < count($list)}
              <a href="/coupon/order/down/{$item->position}" class="gray_btn ml_20"><img src="/assets/img/common/icon/i17.png" width="14" style="margin-left:0;"/ ></a>
              {/if}
            {/if}
          
    {/if}
  </div>
  </div>
      
      </dt>
    </dl>
  </div>

{/foreach}
{* ----- クーポン一覧 [End]   ----- *}
{else}
  <div class="menu_list row-fluid mb_20 mt_20 br_4 row-fluid_wrap">
    <p>現在、クーポンが登録されていません。</p>
  </div>
{/if}

</div>







</div>
<!---->


<div class="last_margin"></div>


{if ! $SPONSOR->app()->is_lock()}
{include "common/footer_meta/coupon.tpl"}
{/if}
{include "common/footer.tpl"}