{include "common/header.tpl"}


{include "common/header_meta/coupon.tpl"}


<!-- クーポン情報の変更 -->
<div class="contents_box">
  <div class="contents_box_head">クーポンの編集 </div>
  <h4>クーポン情報の変更</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">クーポン情報の更新が完了しました</span>
      </li>
    </ul>
    <hr />
    <a href="/coupon" id="save_btn" class="back_btn ml_30">戻る</a>
  </div>

</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
