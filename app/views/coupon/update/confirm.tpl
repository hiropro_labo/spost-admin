{include "common/header.tpl"}


{include "common/header_meta/coupon.tpl"}


<!-- クーポンの編集 -->
<div class="contents_box">
  <div class="contents_box_head">クーポンの編集 </div>
  <h4>クーポンの編集</h4>

  <form action="/coupon/update/exe/{$coupon->id}" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>タイトル</label>
      <span>{$fieldset->value('title')|default:"-----"|nl2br}</span>
    </li>

    <li>
      <label>クーポン内容</label>
      <span>{$fieldset->value('body')|default:"-----"|nl2br}</span>
    </li>

    <li>
      <label>利用条件</label>
      <span>{$fieldset->value('policy')|default:"-----"|nl2br}</span>
    </li>

    <li>
      <label>利用期間</label>
      <span>{$fieldset->value('term_flg')|replace:'0':'指定なし'|replace:'1':'指定あり'}</span>
    </li>

    {if $fieldset->value('term_flg') == '1'}
    <li>
      <label>表示期間</label>
      <span>{$fieldset->value('start_year')}年{$fieldset->value('start_month')}月{$fieldset->value('start_day')}日
      〜{$fieldset->value('end_year')}年{$fieldset->value('end_month')}月{$fieldset->value('end_day')}日</span>
    </li>
    {/if}
  </ul>

  <hr />

  {* ------- Hidden Data [ Start ] -------- *}
  {foreach from=$fieldset->field() item=field}
  {$field}
  {/foreach}
  {* ------- Hidden Data [ End ] ---------- *}
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
