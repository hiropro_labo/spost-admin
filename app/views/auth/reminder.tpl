{include "common/header.tpl"}

    <fieldset>
        <legend>iPostマスター管理：パスワードを忘れた場合</legend>

        {* ------- Error-Massage [ Start ] --------- *}
	    {if $message}
	    <fieldset>
		    <legend>System Message</legend>
		    <div>{$message|nl2br}</div>
	    </fieldset>
	    {/if}
	    {* ------- Error-Massage [ End ] ----------- *}

	    {* ------- Form : HTML [ Start ] ----------- *}
        <form method="POST" accept-charset="utf-8" action="/auth/login">
            <table>
                <tr>
                    <td>{$fieldset->label('login_id')}</td>
                    <td>{$fieldset->field('login_id')->build()} {$fieldset->error_msg('login_id')}</td>
                </tr>
                <tr>
                    <td>{$fieldset->label('password')}</td>
                    <td>{$fieldset->field('password')->build()} {$fieldset->error_msg('password')}</td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" name="submit" value="　ログイン　" /></td>
                </tr>
                <tr>
                    <td colspan="2"><a href="/auth/reminder">パスワードを忘れた場合</a></td>
                </tr>
            </table>
        </form>
        {* ------- Form : HTML [ End ] ------------- *}
    </fieldset>

{include "common/footer.tpl"}