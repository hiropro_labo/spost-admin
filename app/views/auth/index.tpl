{include "common/header_login.tpl"}


	<div id="login_title_wrap">
	    <img src="{$img_common_path}logo/login_title.png" width="192" height="97"  />
	</div>

	<form method="POST" accept-charset="utf-8" action="/auth/login">
		<div id="login_box">
			<div id="head_title">Login</div>


			<div id="login_box_wrap">
				<label for="login">{$fieldset->label('login_id')}</label>
				{$fieldset->field('login_id')->build()}
				<span class="error">{$fieldset->error_msg('login_id')}</span>

				<label for="secret">{$fieldset->label('password')}</label>
				{$fieldset->field('password')->build()}
				<span class="error">{$fieldset->error_msg('password')}</span>

				<input type="hidden" name="commit" id="commit" class="commit" value="1" />
				<input type="submit" id="login_btn" class="btn_red" value="Login" />
            {* ------- Error-Massage [ Start ] --------- *}
            {if isset($message)}
            <div class="error_message_wrapper">
                {$message|nl2br}
            </div>
            {/if}
            {* ------- Error-Massage [ End ] ----------- *}
				<div class="clear"></div>
			</div>
		</div>
	</form>

	<p id="copy">Copyright © 2013 HIROPRO, Inc. All Rights Reserved.</p>

{include "common/footer_login.tpl"}