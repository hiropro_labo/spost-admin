{include "common/header.tpl"}

{include "common/header_meta/shop.tpl"}

<!-- お店の写真の変更 -->
<div class="contents_box" id="shop_info">
  <div class="contents_box_head">
    お店の情報・写真の変更
    <a href="/support/manual/top#top_3"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="電話番号や営業時間などの、お店の情報を変更する事ができます。<br>「お店の画像」には、あなたのお店の魅力が伝わる写真をアップロードしましょう。。"></a>
  </div>

  <h4>お店の写真の変更</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span></p>
{if ! $SPONSOR->app()->is_lock()}
  <a href="/top/img/shop/update" class="edit_btn">編集する</a>
  <a href="/top/img/shop/update"><img src="{$SPONSOR->contents()->shop()->image_path()}?{time()}" width="160" alt="トップ画像" class="mb_20 con_img" /></a>
{else}
  <img src="{$SPONSOR->contents()->shop()->image_path()}?{time()}" width="160" alt="トップ画像" class="mb_20 con_img" />
{/if}
  <hr />

  <h4>お店の情報</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span><br />
  お店の名前が必要になります。</p>
{if ! $SPONSOR->app()->is_lock()}
  <a href="/top/shop/update" class="edit_btn">編集する</a>
{/if}
  <ul class="cel">
    <li><label>お店の名前</label><span>{$SPONSOR->contents()->shop_profile()->shop_name()}</span></li>
    <li><label>郵便番号</label><span>{$SPONSOR->contents()->shop_profile()->zip_code1()}&nbsp;-&nbsp;{$SPONSOR->contents()->shop_profile()->zip_code2()}</span></li>
    <li><label>住所</label><span>{$SPONSOR->contents()->shop_profile()->pref_name()} {$SPONSOR->contents()->shop_profile()->city()} {$SPONSOR->contents()->shop_profile()->address_opt1()} <br />{$SPONSOR->contents()->shop_profile()->address_opt2()}</span></li>
    <li><label>電話番号</label><span>{$SPONSOR->contents()->shop_profile()->tel1()}&nbsp;-&nbsp;{$SPONSOR->contents()->shop_profile()->tel2()}&nbsp;-&nbsp;{$SPONSOR->contents()->shop_profile()->tel3()}</span></li>
    <li><label>FAX</label><span>{$SPONSOR->contents()->shop_profile()->fax1()}&nbsp;-&nbsp;{$SPONSOR->contents()->shop_profile()->fax2()}&nbsp;-&nbsp;{$SPONSOR->contents()->shop_profile()->fax3()}</span></li>
    <li><label>メールアドレス</label><span>{$SPONSOR->contents()->shop_profile()->email()}</span></li>
    <li><label>ホームページアドレス</label><a href="{$SPONSOR->contents()->shop_profile()->url()}" target="_blank"><span style="color:#08C;">{$SPONSOR->contents()->shop_profile()->url()}</span></a></li>
    <li><label>オンラインショップ</label><a href="{$SPONSOR->contents()->shop_profile()->online_shop()}" target="_blank"><span style="color:#08C;">{$SPONSOR->contents()->shop_profile()->online_shop()}</span></a></li>
    <li><label>営業時間</label><span>{$SPONSOR->contents()->shop_profile()->open_hours()}</span></li>
    <li><label>定休日</label><span>{$SPONSOR->contents()->shop_profile()->holiday()}</span></li>
  </ul>
</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}
