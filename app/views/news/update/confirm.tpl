{include "common/header.tpl"}


{include "common/header_meta/news.tpl"}


<!-- ニュースの編集 -->
<div class="contents_box">
  <div class="contents_box_head">ニュースの編集
  </div>
  <h4>ニュースの編集</h4>

  <form action="/news/update/exe/{$news->id}" method="POST">

<div class="contents_form">
  <ul class="cel">
    <li>
      <label>ニュース画像</label>
      {if $img_upload_flg}
      <img src="{$news->tmp_image_path($SPONSOR->id())}?{time()}" width="160" height="107" alt="ニュース画像" class="form_img con_img" />
      {else}
      <img src="{$news->image_path($SPONSOR->id())}?{time()}" width="160" height="107" alt="ニュース画像" class="form_img con_img" />
      {/if}
    </li>

    <li>
      <label>タイトル</label>
      <span>{$fieldset->value('title')}</span>
    </li>

    <li>
      <label>本文</label>
      <span>{$fieldset->value('body')|nl2br}</span>
    </li>

    <li>
      <label>配信対象</label>
      <span>{$fieldset->value('target')|replace:'1':'球団アプリユーザーへも配信する'|default:"球団アプリユーザーへは配信しない"}</span>
    </li>

    <li>
      <label>配信予約日時</label>
      <span>{$fieldset->value('reserved_at_year')}年{$fieldset->value('reserved_at_month')}月{$fieldset->value('reserved_at_day')}日&emsp;{$fieldset->value('reserved_at_hour')}：{$fieldset->value('reserved_at_min')}</span>
    </li>
  </ul>

  <hr />

  {* ------- Hidden Data [ Start ] -------- *}
  {foreach from=$fieldset->field() item=field}
  {$field}
  {/foreach}
  {* ------- Hidden Data [ End ] ---------- *}
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
