{include "common/header.tpl"}

{include "common/header_meta/news.tpl"}

<div class="contents_box">
  <div class="contents_box_head">ニュースの新規作成
  </div>
  <h4>ニュースの新規作成</h4>
  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">ニュースの新規作成が完了しました</span>
      </li>
    </ul>

    <hr />

    <a href="/news" id="save_btn" class="back_btn ml_30">戻る</a>
  </div>
</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}
