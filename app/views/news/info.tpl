{include "common/header.tpl"}


{include "common/header_meta/news.tpl"}


<!-- ニュースの新規作成 -->
<div class="contents_box">
  <div class="contents_box_head">ニュースの確認
  </div>
  <h4>ニュースの確認</h4>


<div class="contents_form">
  <ul class="cel">
    <li>
      <label>ニュース画像</label>
      <img src="{$news->image_path()}?{time()}" width="160" height="105" alt="ニュース画像" class="form_img con_img" />
    </li>

    <li>
      <label>タイトル</label>
      <span>{$news->title}</span>
    </li>

    <li>
      <label>本文</label>
      <span>{$news->body|nl2br}</span>
    </li>
  </ul>

  <hr />

  <a href="/news" id="save_btn" class="back_btn ml_206">戻る</a>
</div>

</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}

