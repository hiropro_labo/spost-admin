{include "common/header.tpl"}


{include "common/header_meta/news.tpl"}


<!-- ニュースの削除 -->
<div class="contents_box">
  <div class="contents_box_head">ニュースの削除
    <a href="/support/manual/news#news_2" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="ニュースの削除をすることが出来ます。<br>内容を確認後、「削除」ボタンを押して下さい。"></a>
  </div>
  <h4>ニュースの削除</h4>

  <form action="/news/del/exe/{$news->id}" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>ニュース画像</label>
      <img src="{$news->image_path()}?{time()}" width="160" height="105" alt="ニュース画像" class="form_img con_img" />
    </li>

    <li>
      <label>タイトル</label>
      <span>{$fieldset->value('title')|default:"-----"}</span>
    </li>
    <li>
      <label>本文</label>
      <span>{$fieldset->value('body')|default:"-----"}</span>
    </li>
    <li>
      <label>配信対象</label>
      <span>{$fieldset->value('target')|replace:'1':'球団アプリユーザーへも配信する'|default:"球団アプリユーザーへは配信しない"}</span>
    </li>
    <li>
      <label>配信予約日時</label>
      <span>{$fieldset->value('reserved_at_year')}年{$fieldset->value('reserved_at_month')}月{$fieldset->value('reserved_at_day')}日&emsp;{$fieldset->value('reserved_at_hour')}：{$fieldset->value('reserved_at_min')}</span>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="削除" class="save_btn" />
  <a href="/news" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


{include "common/footer.tpl"}
