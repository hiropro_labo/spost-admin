{include "common/header.tpl"}

{include "common/header_meta/news.tpl"}

<div class="contents_box">
  <div class="contents_box_head">ニュースの削除
  </div>
  <h4>ニュースの削除</h4>
  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">ニュースの削除が完了しました</span>
      </li>
    </ul>
    <hr />
    <a href="/news" id="save_btn" class="back_btn ml_30">戻る</a>
  </div>
</div>

{include "common/footer.tpl"}
