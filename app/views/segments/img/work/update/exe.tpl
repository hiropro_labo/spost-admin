{include "common/header.tpl"}

{include "common/header_meta/segments.tpl"}

<div class="contents_box">
  <div class="contents_box_head">Work画像の変更</div>
  <h4>Work画像の更新完了</h4>

  <form action="/segments/img/work/update" method="POST">

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">Work画像の変更が完了しました</span>
      </li>
    </ul>
    <hr />
    <a href="/segments" id="save_btn" class="back_btn">戻る</a>
  </div>

  </form>
</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}
