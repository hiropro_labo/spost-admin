{include "common/header.tpl"}

{include "common/header_meta/segments.tpl"}

<div class="contents_box">
  <div class="contents_box_head">Work 画像の変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="Work画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>Workの画像を変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/segments/img/work/update" class="edit_btn">変更する</a>
  <a href="/segments/img/work/update" title="変更"><img src="{$image_work->image_path()}?{time()}" width="160" height="70" alt="事業内容Work画像" class="mb_20 con_img" /></a>
{else}
  <img src="{$image_work->image_path()}" width="160" height="70" alt="事業内容Work画像" class="mb_20 con_img" />
{/if}
</div>


<div class="contents_box">
  <div class="contents_box_head">Work テキストの変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="Work画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>Workのテキストを変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/segments/text/work/update" class="edit_btn">変更する</a>
{/if}

  <div class="contents_form">
    <ul>
      <li>
        <label for="name">タイトル</label>
        <span>{$segments->title_work|default:"---"}</span>
      </li>
      <li>
        <label for="name">本文</label>
        <span>{$segments->body_work|nl2br|default:"---"}</span>
      </li>
    </ul>
  </div>
</div>


<div class="contents_box">
  <div class="contents_box_head">Policy 画像の変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="Policy画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>Policyの画像を変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/segments/img/policy/update" class="edit_btn">変更する</a>
  <a href="/segments/img/policy/update" title="変更"><img src="{$image_policy->image_path()}?{time()}" width="160" height="70" alt="事業内容Policy画像" class="mb_20 con_img" /></a>
{else}
  <img src="{$image_policy->image_path()}" width="160" height="70" alt="事業内容Policy画像" class="mb_20 con_img" />
{/if}
</div>


<div class="contents_box">
  <div class="contents_box_head">Policy テキストの変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="Policy画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>Policyのテキストを変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/segments/text/policy/update" class="edit_btn">変更する</a>
{/if}

  <div class="contents_form">
    <ul>
      <li>
        <label for="name">タイトル</label>
        <span>{$segments->title_policy|default:"---"}</span>
      </li>
      <li>
        <label for="name">本文</label>
        <span>{$segments->body_policy|nl2br|default:"---"}</span>
      </li>
    </ul>
  </div>
</div>


<div class="contents_box">
  <div class="contents_box_head">Message 画像の変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="Message画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>Messageの画像を変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/segments/img/message/update" class="edit_btn">変更する</a>
  <a href="/segments/img/message/update" title="変更"><img src="{$image_message->image_path()}?{time()}" width="160" height="70" alt="事業内容Message画像" class="mb_20 con_img" /></a>
{else}
  <img src="{$image_message->image_path()}" width="160" height="70" alt="事業内容Message画像" class="mb_20 con_img" />
{/if}
</div>


<div class="contents_box">
  <div class="contents_box_head">Message テキストの変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="Message画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>Messageのテキストを変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/segments/text/message/update" class="edit_btn">変更する</a>
{/if}

  <div class="contents_form">
    <ul>
      <li>
        <label for="name">タイトル</label>
        <span>{$segments->title_message|default:"---"}</span>
      </li>
      <li>
        <label for="name">本文</label>
        <span>{$segments->body_message|nl2br|default:"---"}</span>
      </li>
    </ul>
  </div>
</div>


<div class="last_margin"></div>

{include "common/footer_meta/company.tpl"}
{include "common/footer.tpl"}
