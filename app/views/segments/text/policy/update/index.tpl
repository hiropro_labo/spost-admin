{include "common/header.tpl"}

{include "common/header_meta/segments.tpl"}

<div class="contents_box">
  <div class="contents_box_head">Policyのテキスト編集
　<a href="/support/manual/coupon"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title=""></a>
  </div>
  <h4>Policyのテキスト編集</h4>

  <form action="/segments/text/policy/update" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

  <div class="contents_form">
    <ul>
      <li>
        <label>タイトル</label>
        {$fieldset->field('title_policy')->build()}
        {$fieldset->error_msg('title_policy')}
      </li>
      <li>
        <label>本文</label>
        {$fieldset->field('body_policy')->build()}
        {$fieldset->error_msg('body_policy')}
      </li>
    </ul>
    <hr />
    <input type="submit" name="button" value="変更の確認" class="save_btn" />
    <a href="/segments" id="save_btn" class="back_btn">戻る</a>
  </div>

  </form>
</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}
