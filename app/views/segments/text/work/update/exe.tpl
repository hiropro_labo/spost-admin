{include "common/header.tpl"}

{include "common/header_meta/segments.tpl"}

<div class="contents_box">
  <div class="contents_box_head">Workのテキスト編集 </div>
  <h4>Workのテキスト編集</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">Workのテキストの更新が完了しました</span>
      </li>
    </ul>
    <hr />
    <a href="/segments" id="save_btn" class="back_btn ml_30">戻る</a>
  </div>

</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}
