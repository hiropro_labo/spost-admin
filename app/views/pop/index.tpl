{include "common/header.tpl"}


{include "common/header_meta/pop.tpl"}


<!-- ポスター・ポップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">ポスター&nbsp;A3サイズ</div>
  <div>
    <img src="/assets/img/common/pop/poster_a3.jpg" width="284" height="100%" alt="ポスター A3サイズ" class="ml_30"/>
  </div>
  <div>
    <ul>
      <li><label>用紙規格</label><span>A3サイズ</span></li>
      <li><label>用紙サイズ</label><span>297mm x 420mm</span></li>
    </ul>
    <a href="/assets/pdf/pop/pop_order.pdf" class="download_btn" title="ポスター A3サイズ" target="_brank">注文はこちら</a>
    <span class="tyui">※&nbsp;pdfファイルになります。</span>
    <span class="tyui">ポスター・ポップは、アート印刷株式会社が販売しています。</span>
  </div>
  <div class="clear"></div>
</div>


<div class="contents_box">
  <div class="contents_box_head">ポスター&nbsp;A4サイズ</div>
  <div>
    <img src="/assets/img/common/pop/poster_a4.jpg" width="284" height="100%" alt="ポスター A4サイズ" class="ml_30"/>
  </div>
  <div>
    <ul>
      <li><label>用紙規格</label><span>A4サイズ</span></li>
      <li><label>用紙サイズ</label><span>210mm x 297mm</span></li>
    </ul>
    <a href="/assets/pdf/pop/pop_order.pdf" class="download_btn" title="ポスター A4サイズ" target="_brank">注文はこちら</a>
    <span class="tyui">※&nbsp;pdfファイルになります。</span>
    <span class="tyui">ポスター・ポップは、アート印刷株式会社が販売しています。</span>
    
  </div>
  <div class="clear"></div>
</div>



<div class="contents_box">
  <div class="contents_box_head">三角POP</div>
  <div>
    <img src="/assets/img/common/pop/triangle_pop.jpg" width="284" height="100%" alt="三角POP" class="ml_30"/>
</div>
  <div>
    <ul>
      <li><label>用紙規格</label><span>A3サイズ</span></li>
      <li><label>用紙サイズ</label><span>&nbsp;</span></li>
      <li><label>組み立て前</label><span>100mm x 160mm</span></li>
      <li><label>組み立て後</label><span>306mm x 160mm</span></li>
    </ul>
    <a href="/assets/pdf/pop/pop_order.pdf" class="download_btn" title="三角POP" target="_brank">注文はこちら</a>
    <span class="tyui">※&nbsp;pdfファイルになります。</span>
    <span class="tyui">ポスター・ポップは、アート印刷株式会社が販売しています。</span>
  </div>
<div class="clear"></div>
</div>


<div class="last_margin"></div>


{include "common/footer.tpl"}
