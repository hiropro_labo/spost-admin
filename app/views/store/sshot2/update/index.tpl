{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<!-- スクリーンショット画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">スクリーンショット２の変更<a href="/support/manual/store#store_2"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="スクリーンショット画像を変更する事ができます。<br>「ファイルを選択」ボタンを押し、お好きな画像をアップロード後、<br>「変更を保存」ボタンを押してください。"></a></div>
  <h4>iPhone 4S以前の機種用：サイズ 640x960px（※最低１枚は必ず登録してください。最大５枚まで）</h4>

  <form action="/store/sshot2/update/{$position}" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">画像のアップロード</label>
      <img src="{$image->image_path()}?{time()}" width="160" height="240" alt="トップ画像" class="con_img form_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">640x960px （※3MBまで）</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/store" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}