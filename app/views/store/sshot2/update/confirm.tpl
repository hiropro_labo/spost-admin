{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<!-- スクリーンショット画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">スクリーンショット２の変更</div>
  <h4>iPhone 4S以前の機種用：サイズ 640x960px（※最低１枚は必ず登録してください。最大５枚まで）</h4>

  <form action="/store/sshot2/update/exe/{$position}" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label>画像のアップロード</label>
      {if $img_upload_flg}
      <img src="{$image->tmp_image_path()}?{time()}" width="160" height="240" alt="スクリーンショット画像" class="con_img form_img" />
      {else}
      <img src="{$image->image_path()}?{time()}" width="160" height="240" alt="スクリーンショット画像" class="con_img form_img" />
      {/if}
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
