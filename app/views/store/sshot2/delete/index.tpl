{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<!-- トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">スクリーンショット２の削除
   <a href="/support/manual/store#store_3"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="スクリーンショット１の画像を削除できます。<br>「この画像を削除」ボタンを押して、削除して下さい。"></a>
  </div>
  <h4>スクリーンショット２の画像を削除します</h4>

  <form action="/store/sshot2/delete/exe/{$position}" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <img src="{$image->image_path()}?{time()}" width="160" height="240" alt="スクリーンショット画像" class="mb_20 con_img" />
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="この画像を削除" class="save_btn" />
  <a href="/store" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
