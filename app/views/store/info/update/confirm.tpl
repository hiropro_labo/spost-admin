{include "common/header.tpl"}

{include "common/header_meta/store.tpl"}

<div class="contents_box">
  <div class="contents_box_head">TEL/MAIL</div>
  <h4>アプリ内のスライドメニュー内のTEL/MAILボタンに埋め込む情報を編集できます</h4>

  <form action="/store/info/update/exe" method="POST">
    <div class="contents_form">
      <ul>
        <li>
          <label for="pref">電話番号</label>
          <span>{$fieldset->value('tel1')}&nbsp;-&nbsp;{$fieldset->value('tel2')}&nbsp;-&nbsp;{$fieldset->value('tel3')}</span>
        </li>
        <li>
          <label for="pref">メールアドレス</label>
          <span>{$fieldset->value('email')}</span>
        </li>
      </ul>
      <hr />
      {foreach from=$fieldset->field() item=field}
      {$field}
      {/foreach}
      <input type="submit" name="button" value="変更の保存" class="save_btn" />
      <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
    </div>
  </form>
</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}
