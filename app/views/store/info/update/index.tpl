{include "common/header.tpl"}

{include "common/header_meta/store.tpl"}

<div class="contents_box">
  <div class="contents_box_head">TEL/MAIL</div>
  <h4>アプリ内のスライドメニュー内のTEL/MAILボタンに埋め込む情報を編集できます</h4>

  <form action="/store/info/update" method="POST">
    <div class="contents_form">
      <ul>
        <li>
          <label for="pref">電話番号</label>
          {$fieldset->field('tel1')->build()}&nbsp;{$fieldset->field('tel2')->build()}&nbsp;{$fieldset->field('tel3')->build()} {$fieldset->description('tel1')}
        </li>
        <li>
          <label for="pref">メールアドレス</label>
          {$fieldset->field('email')->build()} {$fieldset->description('email')} {$fieldset->error_msg('email')}
        </li>
      </ul>
      <hr />
      <input type="submit" name="button" value="変更の確認" class="save_btn" />
      <a href="/store" id="save_btn" class="back_btn">戻る</a>
    </div>
  </form>
</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}