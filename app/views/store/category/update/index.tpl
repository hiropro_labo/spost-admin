{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<div class="contents_box">
  <div class="contents_box_head">アプリのカテゴリーの変更
    <a href="/support/manual/store#store_5"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="アプリストア(App Store/Google Play)表示項目を編集することができます。<br>フォームに入力した後、「確認する」ボタンを押してください。"></a></div>
  <h4>アプリのカテゴリーを選択してください</h4>

  <form action="/store/category/update" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label for="zip1" class="hisu">iPhone１</label>
      {$fieldset->field('category_iphone1')->build()}
      <p class="error">{$fieldset->error_msg('category_iphone1')}</p>
    </li>

    <li>
      <label for="zip1" class="hisu">iPhone２</label>
      {$fieldset->field('category_iphone2')->build()}
      <p class="error">{$fieldset->error_msg('category_iphone2')}</p>
    </li>

    <li>
      <label for="zip1" class="hisu">Android</label>
      {$fieldset->field('category_android')->build()}
      <p class="error">{$fieldset->error_msg('category_android')}</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/store" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>


<div class="last_margin"></div>


{include "common/footer.tpl"}
