{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<div class="contents_box">
  <div class="contents_box_head">アプリのカテゴリーの変更
  </div>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">アプリのカテゴリーの変更が完了しました</span>
      </li>
    </ul>

    <hr />

    <a href="/store" id="save_btn" class="back_btn ml_30">戻る</a>
  </div>

</div>


<div class="last_margin"></div>


{include "common/footer.tpl"}
