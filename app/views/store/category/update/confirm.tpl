{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<div class="contents_box">
  <div class="contents_box_head">アプリのカテゴリーの変更
  </div>
  <h4>アプリのカテゴリーを選択してください</h4>

  <form action="/store/category/update/exe" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label for="name">iPhone１</label>
      <span>{$fieldset->value('category_iphone1')}</span>
    </li>
    <li>
      <label for="name">iPhone2</label>
      <span>{$fieldset->value('category_iphone2')}</span>
    </li>
    <li>
      <label for="name">Android</label>
      <span>{$fieldset->value('category_android')}</span>
    </li>
  </ul>

  <hr />

  {* ------- Hidden Data [ Start ] -------- *}
  {foreach from=$fieldset->field() item=field}
  {$field}
  {/foreach}
  {* ------- Hidden Data [ End ] ---------- *}
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>


<div class="last_margin"></div>


{include "common/footer.tpl"}
