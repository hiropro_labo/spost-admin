{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<div class="contents_box">
  <div class="contents_box_head">スクリーンショット１の削除</div>

  <div class="contents_form">
    <ul>
      <li>
        <label>&nbsp;</label>
        <span>スクリーンショット１の画像の削除が完了しました</span>
      </li>
    </ul>

    <hr />

    <a href="/store" id="save_btn" class="back_btn">戻る</a>
  </div>

</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}
