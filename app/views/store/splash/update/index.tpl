{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<!-- トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">起動画面用の画像<a href="/support/manual/store#store_4"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="起動画面用の画像が登録できます。<br>「ファイルを選択」ボタンを押して、画像をアップロードして下さい。"></a></div>
  <h4>アプリ起動時の画像：サイズ 640x1136px（※登録がない場合は規定の画像が表示されます。）</h4>

  <form action="/store/splash/update/" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">画像のアップロード</label>
      <img src="{$image->image_path()}" width="160" height="284" alt="起動画面用の画像" class="con_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/store" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
