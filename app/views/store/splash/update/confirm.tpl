{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<!-- 起動画面用の画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">起動画面用の画像</div>
  <h4>アプリ起動時の画像：サイズ 640x1136px （※登録がない場合は規定の画像が表示されます。）</h4>

  <form action="/store/splash/update/exe" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>画像のアップロード</label>
      <img src="{$image->tmp_image_path()}?{time()}" width="160" height="284" alt="起動画面用の画像" class="mb_20 con_img" />
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
