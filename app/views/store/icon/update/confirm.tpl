{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<!-- アプリアイコン画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">アプリ情報</div>
  <h4>アプリアイコン画像の変更</h4>

  <form action="/store/icon/update/exe" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>画像のアップロード</label>
      <img src="{$image_icon->tmp_image_path()}?{time()}" width="160" height="160" alt="アプリアイコン画像" class="mb_20 con_img" />
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
