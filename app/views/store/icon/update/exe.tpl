{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<!-- アプリアイコン画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">アプリ情報</div>
  <h4>アプリアイコン画像の変更</h4>

  <form action="/store/icon/update" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <span class="ml_30">アプリアイコン画像の変更が完了しました</span>
    </li>
  </ul>

  <hr />

  <a href="/store" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
