{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<!-- お店の写真の変更 -->
<div class="contents_box">
  <div class="contents_box_head lock">
    アイコン画像
    <a href="/support/manual/store"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playに表示される<br>アイコン画像を登録する事ができます。どのアプリよりも目を引く<br>アイコンを表示させましょう。"></a>
  </div>
  <h4>アイコン画像の変更</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span><br />
  <span class="hisu">※簡易申請後、変更することが出来ません。</span><br />
  推奨サイズ : 1024px x 1024px</p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/store/icon/update" class="edit_btn">編集する</a>
{/if}

{if ! $SPONSOR->app()->is_lock()}
  <a href="/store/icon/update"><img src="{$image_icon->image_path()}" width="160" alt="アイコン画像" class="mb_20 con_img" /></a>
{else}
  <img src="{$image_icon->image_path()}" width="160" alt="アイコン画像" class="mb_20 con_img" />
{/if}
</div>

<!-- アプリ内ステータスバー表示用画像 -->
<div class="contents_box">
  <div class="contents_box_head lock">
    アプリ内ステータスバー表示用画像
    <a href="/support/manual/store#store_1"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playに表示される<br>アプリ名とアプリの説明文が作成できます。<br>表示名はインストールしたアプリの名前です。<br>「編集する」ボタンを押して<br>表示したい内容を記入して下さい。"></a>
  </div>
  <h4>アプリ内ステータスバー表示画像の変更</h4>
  <p><span class="hisu">※未指定の場合はアプリ内のステータスバーには アプリストア->表示名 にて指定されたテキストが表示されます</span><br />
  <span class="hisu">※画像サイズは高さ32pxを基準に縦横比を保ったままリサイズされます</span><br />
  推奨サイズ : 32px x 100px</p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/store/appicon/update" class="edit_btn">編集する</a>
{/if}

{if ! $SPONSOR->app()->is_lock()}
  <a href="/store/appicon/update"><img src="{$image_appicon->image_path()}" width="50" alt="アプリ内ステータスバー表示用画像" class="mt_20 mb_20 con_img" style="background-color: {if $SPONSOR->contents()->theme()->model()->category == 1}#d82a1a{elseif $SPONSOR->contents()->theme()->model()->category == 2}#f7e9c8{elseif $SPONSOR->contents()->theme()->model()->category == 3}#FFFFFF{elseif $SPONSOR->contents()->theme()->model()->category == 4}#003077{/if}" /></a>
{else}
  <img src="{$image_appicon->image_path()}" width="50" alt="アプリ内ステータスバー表示用画像" class="mb_20 con_img" style="background-color: {if $SPONSOR->contents()->theme()->model()->category == 1}#d82a1a{elseif $SPONSOR->contents()->theme()->model()->category == 2}#f7e9c8{elseif $SPONSOR->contents()->theme()->model()->category == 3}#FFFFFF{elseif $SPONSOR->contents()->theme()->model()->category == 4}#003077{/if}" />
{/if}
</div>

<!-- アプリ名 -->
<div class="contents_box">
  <div class="contents_box_head lock">
    アプリストア(App Store/Google Play)表示項目
    <a href="/support/manual/store#store_1"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playに表示される<br>アプリ名とアプリの説明文が作成できます。<br>表示名はインストールしたアプリの名前です。<br>「編集する」ボタンを押して<br>表示したい内容を記入して下さい。"></a>
  </div>
  <h4></h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span><br />
  <span class="hisu">※簡易申請後、変更することが出来ません。</span><br />
  アプリストア(App Store/Google Play)での表示内容を設定してください（※全て登録必須の項目です）</p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/store/text/update" class="edit_btn">編集する</a>
{/if}
  <ul class="cel">
    <li><label>アプリ</label><span>{$SPONSOR->contents()->store()->app_name()|default:"---"}</span></li>
    <li><label>表示名</label><span>{$SPONSOR->contents()->store()->app_disp_name()|default:"---"}</span></li>
    <li><label>説明文</label><span>{$SPONSOR->contents()->store()->description()|default:"---"|nl2br}</span></li>
  </ul>
</div>

<!-- スクリーンショット1 -->
<div class="contents_box">
  <div class="contents_box_head">
    スクリーンショット１
    <a href="/support/manual/store#store_2"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playに表示される<br>アプリのイメージ画像が登録できます。<br>ダウンロードしたいと気を引く画像を登録しましょう。"></a>
  </div>
  <h4>iPhone5/Android用（５枚まで）</h4>
  <!-- <p><span class="hisu">※簡易申請時に必須の項目です。</span><br /> -->
  <p><span class="hisu">※簡易申請後、変更することが出来ません。</span><br />
  最低、２枚は必要になっております。<br />
  推奨サイズ 640px x 1136px<br />
  登録されていない場合は、こちらで、<br />
  トップ、クーポン/イベント、メニューのスクリーンショットを、<br />
  1枚ずつご用意します。</p>

  <ul id="sortable-li">
{foreach from=$list_sshot1 item=image }
    <li id="{$image->position}" class="sort_box"><img src="{$image->image_path()}" width="160" height="284" alt="スクリーンショット２" class="d1"/>
{if ! $SPONSOR->app()->is_lock()}
      <div class="contenthover">
        <a href="/store/sshot/delete/{$image->seq}"><img src="/assets/img/common/del_icon.png" class="del_icon"></a>
        <a href="/store/sshot/update/{$image->seq}" class="edit_btn">変更</a>
      </div>
{/if}
    </li>
{/foreach}
  </ul>
</div>

<!-- スクリーンショット2 -->
<div class="contents_box">
  <div class="contents_box_head">
    スクリーンショット２
    <a href="/support/manual/store#store_2"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playに表示される<br>アプリのイメージ画像が登録できます。<br>ダウンロードしたいと気を引く画像を登録しましょう。"></a>
  </div>
  <h4>iPhone 4S以前の機種用（５枚まで）</h4>
  <!-- <p><span class="hisu">※簡易申請時に必須の項目です。</span> -->
  <p><span class="hisu">※簡易申請後、変更することが出来ません。</span><br />
  最低、２枚は必要になっております。<br />
  推奨サイズ 640px x 960px<br />
  登録されていない場合は、こちらで、<br />
  トップ、クーポン/イベント、メニューのスクリーンショットを、<br />
  1枚ずつご用意します。</p>

  <ul id="sortable-li2">
{foreach from=$list_sshot2 item=image }
    <li id="{$image->position}" class="sort_box"><img src="{$image->image_path()}" width="160" height="240" alt="スクリーンショット２" class="d1"/>
{if ! $SPONSOR->app()->is_lock()}
      <div class="contenthover">
        <a href="/store/sshot2/delete/{$image->seq}"><img src="/assets/img/common/del_icon.png" class="del_icon"></a>
        <a href="/store/sshot2/update/{$image->seq}" class="edit_btn">変更</a>
      </div>
{/if}
    </li>
{/foreach}
  </ul>

</div>

<!-- 起動画面用の画像 -->
<div class="contents_box">
  <div class="contents_box_head lock">
    起動画面用の画像
    <a href="/support/manual/store#store_4"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="アプリを起動した時に一番最初に表示される画像です。<br>「編集する」ボタンを押して、表示させたい画像を登録して下さい。"></a>
  </div>
  <h4>アプリ起動時の画像</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span><br />
  <span class="hisu">※簡易申請後、変更することが出来ません。</span><br />
  推奨サイズ 640px x 1136px</p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/store/splash/update" class="edit_btn">編集する</a>
{/if}

{if ! $SPONSOR->app()->is_lock()}
  <a href="/store/splash/update"><img src="{$image_splash->image_path()}" width="160" height="284" alt="起動画面用の画像" class="mb_20 con_img" /></a>
{else}
  <img src="{$image_splash->image_path()}" width="160" height="284" alt="起動画面用の画像" class="mb_20 con_img" />
{/if}
</div>

<!-- カテゴリー選択 -->
<div class="contents_box">
  <div class="contents_box_head lock">
    App Store / Google Playカテゴリー
    <a href="/support/manual/store#store_5"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playで<br>どのカテゴリーに区分するか選択します。「編集する」ボタンを押して<br>どのカテゴリーにするか選択して下さい。"></a>
  </div>
  <h4>カテゴリーの選択</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span>
  <span class="hisu">※簡易申請後、変更することが出来ません。</span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/store/category/update" class="edit_btn">編集する</a>
{/if}
  <ul class="cel">
    <li><label>iPhone1</label><span>{$SPONSOR->contents()->store()->category_iphone1()}</span></li>
    <li><label>iPhone2</label><span>{$SPONSOR->contents()->store()->category_iphone2()}</span></li>
    <li><label>Android</label><span>{$SPONSOR->contents()->store()->category_android()}</span></li>
  </ul>
</div>


<!-- Copyright表記 -->
<div class="contents_box">
  <div class="contents_box_head lock">
    Copyright表記
    <a href="/support/manual/store#store_5"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playで<br>どのカテゴリーに区分するか選択します。「編集する」ボタンを押して<br>どのカテゴリーにするか選択して下さい。"></a>
  </div>
  <h4>Copyright表記を編集できます</h4>
  <p><span class="hisu">※未登録の場合は非表示となります</span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/store/copyright/update" class="edit_btn">編集する</a>
{/if}
  <ul class="cel">
    <li><label>表記内容</label><span>{$SPONSOR->contents()->store()->copyright()|default:"未登録"}</span></li>
  </ul>
</div>


<!-- TEL/MAIL -->
<div class="contents_box">
  <div class="contents_box_head lock">
    TEL/MAIL
    <a href="/support/manual/store#store_5"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playで<br>どのカテゴリーに区分するか選択します。「編集する」ボタンを押して<br>どのカテゴリーにするか選択して下さい。"></a>
  </div>
  <h4>アプリ内のスライドメニュー内のTEL/MAILボタンに埋め込む情報を編集できます</h4>
  <p><span class="hisu">※未登録の場合は非表示となります</span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/store/info/update" class="edit_btn">編集する</a>
{/if}
  <ul class="cel">
    <li><label>TEL</label><span>{$SPONSOR->contents()->store()->tel()|default:"未登録"}</span></li>
    <li><label>MAIL</label><span>{$SPONSOR->contents()->store()->email()|default:"未登録"}</span></li>
  </ul>
</div>


<div class="last_margin"></div>


{if ! $SPONSOR->app()->is_lock()}
{include "common/footer_meta/store.tpl"}
{/if}
{include "common/footer.tpl"}