{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<div class="contents_box">
  <div class="contents_box_head">アプリストア(App Store/Google Play)表示項目の変更
  </div>
  <h4>アプリストア(App Store/Google Play)表示項目の変更</h4>

  <form action="/store/text/update/exe" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label for="name">アプリ名</label>
      <span>{$fieldset->value('app_name')}</span>
    </li>
    <li>
      <label for="name">表示名</label>
      <span>{$fieldset->value('app_disp_name')}</span>
    </li>
    <li>
      <label for="name">説明文</label>
      <span>{$fieldset->value('description')|nl2br}</span>
    </li>
  </ul>

  <hr />

  {* ------- Hidden Data [ Start ] -------- *}
  {foreach from=$fieldset->field() item=field}
  {$field}
  {/foreach}
  {* ------- Hidden Data [ End ] ---------- *}
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>


<div class="last_margin"></div>


{include "common/footer.tpl"}
