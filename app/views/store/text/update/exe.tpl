{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<div class="contents_box">
  <div class="contents_box_head">アプリストア(App Store/Google Play)表示項目の変更
  </div>

  <form action="/top/topic/update" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <span class="ml_30">アプリストア(App Store/Google Play)表示項目の更新が完了しました</span>
    </li>
  </ul>

  <hr />

  <a href="/store" id="save_btn" class="back_btn ml_30">戻る</a>
</div>

  </form>
</div>


<div class="last_margin"></div>


{include "common/footer.tpl"}
