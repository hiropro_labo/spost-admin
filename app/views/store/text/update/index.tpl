{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<div class="contents_box">
  <div class="contents_box_head">アプリストア(App Store/Google Play)表示項目の変更
    <a href="/support/manual/store#store_1"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="アプリストア(App Store/Google Play)表示項目を編集することができます。<br>フォームに入力した後、「確認する」ボタンを押してください。"></a></div>
  <h4>アプリストア(App Store/Google Play)表示項目をこちらで編集できます</h4>

  <form action="/store/text/update" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label for="zip1" class="hisu">アプリ名</label>
      {$fieldset->field('app_name')->build()}
      <p class="desc">最大全角112文字／推奨全角9文字以内</p>
      <p class="error">{$fieldset->error_msg('app_name')}</p>
    </li>

    <li>
      <label for="pref" class="hisu">表示名</label>
      {$fieldset->field('app_disp_name')->build()}
      <p class="desc">最大全角６文字</p>
      <p class="error">{$fieldset->error_msg('app_disp_name')}</p>
    </li>

    <li>
      <label for="pref" class="hisu">説明文</label>
      {$fieldset->field('description')->build()}
      <p class="desc">全角100 〜 2,000文字</p>
      <p class="error">{$fieldset->error_msg('description')}</p>
      <p class="desc_margin">審査に通過しやすい説明文<br><br><br>
      例文：<br><br>
      みつの里の公式アプリです。<br>
      このアプリでは、アプリユーザー限定の様々なサービスを受けることができます。<br><br><br>
      例えば…<br>
      ・お店情報や最新の商品情報を入手することができます。<br>
      ・標準アプリのマップを起動させてお店の位置を確認することができます。<br>
      ・お店から最新情報やお得なクーポンなどを受け取ることができます。<br>
      ・オンラインショップや公式ホームページなどもすぐにチェックできます。<br>
      ・オーダーエントリーがお使いのスマートフォンからできるようになります。（導入店に限る）<br><br><br>
      このようにアプリで実際に提供できる内容を記載すると、審査に通過しやすいポイントになります。
      </p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/store" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>


<div class="last_margin"></div>


{include "common/footer.tpl"}
