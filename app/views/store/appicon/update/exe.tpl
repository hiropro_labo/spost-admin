{include "common/header.tpl"}

{include "common/header_meta/store.tpl"}

<div class="contents_box">
  <div class="contents_box_head">アプリ情報</div>
  <h4>アプリ内ステータスバー表示画像の変更</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">アプリ内ステータスバー表示用画像の変更が完了しました</span>
      </li>
    </ul>
    <hr />
    <a href="/store" id="save_btn" class="back_btn">戻る</a>
  </div>
</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}
