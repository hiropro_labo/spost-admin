{include "common/header.tpl"}

{include "common/header_meta/store.tpl"}

<div class="contents_box">
  <div class="contents_box_head">アプリ情報<a href="/support/manual/store"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playに表示される<br>アイコン画像を登録する事ができます。「ファイルを選択」ボタンを押して<br>アイコン画像をアップロードして下さい。"></a></div>
  <h4>アプリ内ステータスバー表示画像の変更</h4>

  <form action="/store/appicon/update/" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">画像のアップロード</label>
      <img src="{$image_icon->image_path()}" width="50" height="50" alt="トップ画像" class="con_img form_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />40px x 65px 以上の大きさを推奨<br /><span></span></p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/store" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
