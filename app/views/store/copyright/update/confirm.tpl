{include "common/header.tpl"}


{include "common/header_meta/store.tpl"}


<div class="contents_box">
  <div class="contents_box_head">Copyright表記の変更
  </div>
  <h4>Copyright表記内容の変更</h4>

  <form action="/store/copyright/update/exe" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label for="name">Copyright表記内容</label>
      <span>{$fieldset->value('copyright')}</span>
    </li>
  </ul>

  <hr />

  {* ------- Hidden Data [ Start ] -------- *}
  {foreach from=$fieldset->field() item=field}
  {$field}
  {/foreach}
  {* ------- Hidden Data [ End ] ---------- *}
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>


<div class="last_margin"></div>


{include "common/footer.tpl"}
