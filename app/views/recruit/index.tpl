{include "common/header.tpl"}


{include "common/header_meta/company.tpl"}


{* ----- 採用情報一覧 [Start] ----- *}
<div class="contents_box">
  <div class="contents_box_head">採用情報一覧
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="採用情報データを登録できます。"></a>
  </div>
  <h4>採用情報を新規に追加できます</h4>
  <p class="mb_20"><span class="hisu">※簡易申請時の必須項目ではありません。</span>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/recruit/title/add" class="edit_btn mt_m12">採用情報の追加</a>
{/if}

</p>

{if ! is_null($list) && count($list)>0}
{* ----- 採用情報一覧 [Start] ----- *}
{foreach from=$list item=item}
<div class="row-fluid br_4 row-fluid_wrap">
  <dl class="menu_ac_recruit">
    <dt>
      <div class="menu_cate_box_recruit">
        <p class="menu_table_text_recruit clearfix">
{if ! $SPONSOR->app()->is_lock()}
          <a href="/recruit/info/{$item->id}">{$item->title}</a><br />
{else}
          {$item->title}<br />
{/if}
        </p>

{if ! $SPONSOR->app()->is_lock()}
       <p class="fr">
        <a href="/recruit/info/{$item->id}" class="gray_btn mr_5 ml_20">編集</a>
        <a href="/recruit/del/{$item->id}" class="gray_btn mr_5 bucket"></a>
       </p>
{/if}
      </div>

    </dt>
  </dl>
</div>

{/foreach}
{* ----- 採用情報一覧 [End]   ----- *}
{else}
  <div class="menu_list row-fluid mb_20 mt_20 br_4 row-fluid_wrap">
    <p>現在、採用情報のデータはありません</p>
  </div>
{/if}

</div>


<div class="last_margin"></div>

{include "common/footer.tpl"}
