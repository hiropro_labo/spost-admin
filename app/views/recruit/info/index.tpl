{include "common/header.tpl"}

{include "common/header_meta/company.tpl"}

<div class="contents_box cel_recruit">
  <div class="contents_box_head">採用情報のタイトル編集
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="チームトップ画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>採用情報のタイトルを変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/recruit/title/update/{$title->id}" class="edit_btn">変更する</a>
{/if}

  <ul class="cel">
    <li><p class="fw_b di mr_20">タイトル</p><p class="di">{$title->title|default:"---"}</p></li>
  </ul>
</div>
<!---->


{* ----- 表示項目一覧 [Start] ----- *}
<div class="contents_box cel_recruit">
  <div class="contents_box_head">表示項目一覧
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="表示項目の詳細が作成できます。"></a>
  </div>

  <h4>表示項目の登録・編集・削除ができます</h4>
  <p class="mb_20"></p>

  {if $list}
  {foreach from=$list item=item}
  <ul class="cel cel_recruit">
    <li class="clearfix">
      <div class="cel_recruit_wrap">
        <label><input type="text" id="title{$item->id}" value="{$item->title}" class="w_120"></label>
        <p class="fl"><textarea id="body{$item->id}">{$item->body}</textarea></p>
        <p class="fr"><input type="button" id="upd_{$item->id}" class="mr_3 ml_30 news_edit_btn" value="更新" onclick="update('{$item->id}', this)">&emsp;<input type="button" id="del_{$item->id}" class="mr_3 news_edit_btn" value="削除" onclick="del('{$item->id}', this)"></p>
        <p id="error_msg{$item->id}"></p>
      </div>
    </li>
  </ul>
  {/foreach}
  {/if}

  <ul class="cel cel_recruit">
      <li class="clearfix">
        <div class="cel_recruit_wrap">
          <label><input type="text" id="add_title" value="タイトル" class="w_120"></label>
          <p class="fl"><textarea id="add_body">本文をココに記載してください</textarea></p>
          <p class="di fr mr_13"><input type="button" value="追加" onclick="add('{$title->id}', this)" class="news_edit_btn"></p>
          <p id="error_msg_add"></p>
        </div>
    </li>
  </ul>

</div>


<div class="last_margin"></div>

{include "common/footer_meta/recruit.tpl"}
{include "common/footer.tpl"}
