{include "common/header.tpl"}

{include "common/header_meta/recruit.tpl"}

<div class="contents_box">
  <div class="contents_box_head">採用情報の新規作成
  </div>
  <h4>採用情報の新規作成</h4>

  <form action="/recruit/title/add/exe" method="POST">

<div class="contents_form">

  <ul>
    <li>
      <label for="name">タイトル</label>
      <span>{$fieldset->value('title')}</span>
    </li>
    <li>
      <label>表示設定</label>
      <span>{$fieldset->value('enable')|replace:'0':'非表示'|replace:'1':'表示'}</span>
    </li>
  </ul>

  <hr />

  {* ------- Hidden Data [ Start ] -------- *}
  {foreach from=$fieldset->field() item=field}
  {$field}
  {/foreach}
  {* ------- Hidden Data [ End ] ---------- *}
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
