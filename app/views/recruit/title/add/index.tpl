{include "common/header.tpl"}

{include "common/header_meta/recruit.tpl"}

<div class="contents_box">
  <div class="contents_box_head">採用情報の新規作成
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="カテゴリーの新規作成ができます。<br>「ファイルを選択」ボタンを押して、画像をアップロード<br>テキストの記入、表示・非表示選択後<br>「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>採用情報の新規作成</h4>

  <form action="/recruit/title/add" method="POST" name="form1" id="form1" class="form1">

  <div class="contents_form">

    <ul>
      <li>
        <label for="name" class="hisu">タイトル</label>
        {$fieldset->field('title')->build()}
        <p class="desc">例）[正][ア][パ]キッチンスタッフ・ホールスタッフ</p>
        <p class="error">{$fieldset->error_msg('title')}</p>
      </li>
    </ul>
    <ul>
      <li>
        <label>表示設定</label>
        <label for="form_enable_1"><input type="radio" required="required" value="1" id="form_enable_1" name="enable" checked="checked" />表示</label>
        <label for="form_enable_0"><input type="radio" required="required" value="0" id="form_enable_0" name="enable" />非表示</label>
        <p class="desc">お客様に見せるかどうかを選ぶことができます。</p>
        <p class="error">{$fieldset->error_msg('enable')}</p>
      </li>
    </ul>

    <hr />

    <input type="submit" name="button" value="変更の確認" class="save_btn" />
    <a href="/recruit" id="save_btn" class="back_btn">戻る</a>
  </div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
