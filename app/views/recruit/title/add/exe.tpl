{include "common/header.tpl"}

{include "common/header_meta/recruit.tpl"}

<div class="contents_box">
  <div class="contents_box_head">採用情報の新規作成
  </div>
  <h4>採用情報の新規作成</h4>

<div class="contents_form">
  <ul>
    <li>
      <span class="ml_30">採用情報の新規作成が完了しました</span>
    </li>
  </ul>

  <hr />
  <a href="/recruit" id="save_btn" class="back_btn ml_30">戻る</a>&emsp;<a href="/recruit/info/update/{$title->id}" id="save_btn" class="back_btn ml_30">続けて詳細情報入力へ</a>
</div>

</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
