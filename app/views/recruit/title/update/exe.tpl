{include "common/header.tpl"}

{include "common/header_meta/segments.tpl"}

<div class="contents_box">
  <div class="contents_box_head">採用情報のタイトル編集 </div>
  <h4>採用情報のタイトル編集</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">採用情報のタイトルの更新が完了しました</span>
      </li>
    </ul>
    <hr />
    <a href="/recruit" id="save_btn" class="back_btn ml_30">戻る</a>
  </div>

</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}
