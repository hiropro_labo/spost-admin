{include "common/header.tpl"}

{include "common/header_meta/segments.tpl"}

<div class="contents_box">
  <div class="contents_box_head">採用情報のタイトル編集 </div>
  <h4>編集内容の確認</h4>

  <form action="/recruit/title/update/exe/{$c_id}" method="POST">

  <div class="contents_form">
    <ul>
      <li>
        <label>タイトル</label>
        <span>{$fieldset->value('title')}</span>
      </li>
    </ul>
    <hr />
    {* ------- Hidden Data [ Start ] -------- *}
    {foreach from=$fieldset->field() item=field}
    {$field}
    {/foreach}
    {* ------- Hidden Data [ End ] ---------- *}
    <input type="submit" name="button" value="変更の保存" class="save_btn" />
    <a href="javascript:history.back();" class="back_btn">戻る</a>
  </div>

  </form>
</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}
