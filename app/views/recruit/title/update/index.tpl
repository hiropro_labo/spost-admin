{include "common/header.tpl"}

{include "common/header_meta/segments.tpl"}

<div class="contents_box">
  <div class="contents_box_head">採用情報のタイトル編集
　<a href="/support/manual/coupon"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title=""></a>
  </div>
  <h4>採用情報のタイトル編集</h4>

  <form action="/recruit/title/update/{$c_id}" method="POST" name="form1" id="form1" class="form1">

  <div class="contents_form">
    <ul>
      <li>
        <label>タイトル</label>
        {$fieldset->field('title')->build()}
        {$fieldset->error_msg('title')}
      </li>
    </ul>
    <hr />
    <input type="submit" name="button" value="変更の確認" class="save_btn" />
    <a href="/recruit" id="save_btn" class="back_btn">戻る</a>
  </div>

  </form>
</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}
