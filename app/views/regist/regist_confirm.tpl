{include "common/header_login.tpl"}

        <fieldset>
            <legend>クライアント新規登録：登録内容確認</legend>
            {* ------- Form : HTML [ Start ] ----------- *}
            <form method="POST" accept-charset="utf-8" action="/regist/confirm">
                <input type="hidden" name="agent_id" value="{$agent_id}" />
                <input type="hidden" name="email" value="{$email}" />
	            <table>
	                <tr>
	                    <td>{$fieldset->label('username')}</td>
	                    <td>{$fieldset->field('username')->build()} {$fieldset->description('username')} {$fieldset->error_msg('username')}</td>
	                </tr>
	                <tr>
	                    <td>{$fieldset->label('pref')}</td>
	                    <td>{$fieldset->field('pref')->build()} {$fieldset->description('pref')} {$fieldset->error_msg('pref')}</td>
	                </tr>
 	                <tr>
	                  <td colspan="2"><input type="button" value="　戻る　" onclick="history.back();" />&nbsp;<input type="submit" name="submit" value="　登録　" /></td>
	                </tr>
	            </table>
            </form>
            {* ------- Form : HTML [ End ] -------------- *}
        </fieldset>

{include "common/footer_login.tpl"}