{include "common/header_login.tpl"}

        <fieldset>
            <legend>クライアント新規登録：メールアドレス内容確認</legend>
            {* ------- Form : HTML [ Start ] ----------- *}
            <form method="POST" accept-charset="utf-8" action="/regist/send">
	            <table>
	                <tr>
	                    <td>{$fieldset->label('email')}</td>
                        <td>{$fieldset->value('email')}</td>
	                </tr>
 	                <tr>
	                  <td colspan="2"><input type="button" value="　戻る　" onclick="history.back();" />&nbsp;<input type="submit" name="submit" value="　登録　" /></td>
	                </tr>
	            </table>
	            {* ------- Hidden Data [ Start ] -------- *}
	            {foreach from=$fieldset->field() item=field}
	            {$field->build()}
	            {/foreach}
	            {* ------- Hidden Data [ End ] ---------- *}
            </form>
            {* ------- Form : HTML [ End ] -------------- *}
        </fieldset>

{include "common/footer_login.tpl"}