{include "common/header_login.tpl"}

    <fieldset>
        <legend>クライアント新規登録：メールアドレス入力画面</legend>
        {* ------- Error Messages [ Start ] -------- *}
        {$fieldset->validation()->show_errors()}
        {* ------- Error Messages [ End ]----------- *}

        {* ------- Form : HTML [ Start ] ----------- *}
        <form method="POST" accept-charset="utf-8" action="/regist/{$agent->id}">
            <input type="hidden" name="agent_id" value="{$agent->id}" />
            <table>
                <tr>
                    <td>{$fieldset->label('email')}</td>
                    <td>{$fieldset->field('email')->build()} {$fieldset->description('email')} {$fieldset->error_msg('email')}</td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" name="submit" value="　内容確認　" /></td>
                </tr>
            </table>
        </form>
        {* ------- Form : HTML [ End ] ------------- *}
    </fieldset>

{include "common/footer_login.tpl"}