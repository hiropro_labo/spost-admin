{include "common/header_login.tpl"}

	<fieldset>
	    <legend>仮登録のご案内メール送信完了</legend>
	    <p>
	       以下のメールアドレス宛てに仮登録のご案内メールをお送りしました。<br />
	       メール本文に記載のURLへアクセスして登録を完了させてください。
	    </p>
	    <p>{$email}</p>
	</fieldset>

{include "common/footer_login.tpl"}