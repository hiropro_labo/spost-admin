{include "common/header_error.tpl"}


       <div id="title_wrap_error">
        <div id="title_bar_error"></div>
        <div id="title_box">
          <h2>Maintenance</h2>
          <p id="title_sub">システムメンテナンスのため、サービスを停止しています。</p>
        </div>
      </div>

      <div class="content_box_error">
        <!--<div class="contents_box_head"></div>-->
        <h4>ただいまメンテナンス中です。</h4>
        <p id="error_text">
          ご迷惑をお掛けしておりますが、ご理解のほどよろしくお願いします。
          {if isset($message)}
          <br />
          {$message}
          {/if}
        </p>
        <a href="javascript:history.back();" class="goback_btn">一つ前へ戻る</a>
      </div>


{include "common/footer_error.tpl"}
