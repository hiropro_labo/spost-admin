{include "preview/common/header.tpl"}


<!-- ▼コンテンツ -->
<div id="p_content_wrap" class="p_setting">

<div class="p_setting_list">
  <ul>
    <li class="p_section_title p_no_allow">通知設定</label></li>
    <li class="p_no_allow">
      <label class="p_switch">iPost からのお知らせ</label>
      <span class="p_switch"><input type="checkbox" name="toggle" id="p_toggle" class="ios-switch" checked="checked" /></span>
    </li>

    <li class="p_section_title">&nbsp;</label></li>
    <li>
      <a href="javascript:void(0);">アプリの使い方</a>
    </li>

    <li>
      <a href="javascript:void(0);">よくある質問</a>
    </li>

    <li>
      <a href="javascript:void(0);">お問い合わせ</a>
    </li>

    <li>
      <a href="javascript:void(0);">
        <label>制作会社</label>
        <span>株式会社ヒロ企画</span>
      </a>
    </li>

    <li class="p_no_allow">
      <label>バージョン</label>
      <span>1.0</span>
    </li>
  </ul>
</div>

</div>
<!-- ▲ -->


{literal}
<script type="text/javascript">
// JS is only used to add the <div>s
var switches = document.querySelectorAll('input[type="checkbox"].ios-switch');

for (var i=0, sw; sw = switches[i++]; ) {
  var div = document.createElement('div');
  div.className = 'switch';
  sw.parentNode.insertBefore(div, sw.nextSibling);
}
</script>
{/literal}


{include "preview/common/footer.tpl"}
