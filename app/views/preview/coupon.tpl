{include "preview/common/header.tpl"}


<!-- ▼コンテンツ -->
<div id="p_content_wrap" class="p_coupon">

<!-- クーポン -->
<div class="p_image_carousel">
  <div id="p_slides">
{foreach from=$list item=entry }
    <img src="{$entry->image_path()}?{time()}" id="{$entry->id}" class="coupon_img" width="210" height="225" alt="クーポン画像" />
{/foreach}
  </div>
  <div id="p_paging" class="p_pagination"></div>
</div>
<!--  -->

</div>
<!-- ▲ -->


{literal}
<script type="text/javascript">
$("#p_slides").carouFredSel({
    circular: false,
    infinite: false,
    auto    : false,
    pagination  : "#p_paging"
});
$('.coupon_img').click(function(e) {
});
</script>
{/literal}


{include "preview/common/footer.tpl"}
