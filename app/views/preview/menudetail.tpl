{include "preview/common/header_back.tpl"}


<!-- ▼コンテンツ -->
<div id="p_content_wrap" class="p_m_detail">

<!-- 商品画像 -->
<div class="p_m_d_img">
  <img src="{$item->image_path()}?{time()}" width="225" height="123" alt="トップ画像" />
</div>
<!--  -->

{if ! is_null($item)}
<!-- 商品情報 -->
<div class="p_m_d_date">
  <div class="p_m_d_d_name">
    {$item->title}
  </div>

  <div class="p_m_d_d_text">
    {$item->description|nl2br}
  </div>

  <div class="clearfix">
    <div class="p_m_d_d_price">
      {$item->price}
    </div>
  </div>

  <div class="p_m_d_d_time">
    {$item->updated_at|date_format:"%Y-%m-%d %H:%M:%S"}
  </div>
</div>
<!--  -->

<!-- ホームページへボタン -->
<div class="p_m_d_btns">
  <a href="{$item->url}" class="p_m_d_btn_store" target="_brank">
    ホームページへ
  </a>
</div>
<!--  -->
{/if}

</div>
<!-- ▲ -->


{include "preview/common/footer.tpl"}
