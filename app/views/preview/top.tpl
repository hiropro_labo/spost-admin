{include "preview/common/header.tpl"}


<!-- ▼コンテンツ -->
<div id="p_content_wrap" class="p_top">

<!-- カルーセル -->
<div class="p_top_carousel">
  <div id="p_slides">
{foreach from=$image_list item=image }
    <img src="{$image->image_path()}?{time()}" width="225" height="148" alt="トップ画像" />
{/foreach}
  </div>
  <div id="p_paging" class="p_pagination"></div>
</div>
<!--  -->

<!-- 3つのボタン -->
<div class="p_top_btns clearfix">
  <div class="p_t_b_left_box">
    <div class="p_t_b_left p_btns_box">
      <a href="javascript:void(0);"><img src="/assets/preview/img/topButton/ipost_main_map.png?{time()}" width="61" height="40" alt="トップメニューボタン1" /></a>
    </div>
    <div class="p_t_b_center p_btns_box">
      <a href="/preview/shopdetail"><img src="/assets/preview/img/topButton/ipost_main_shop.png?{time()}" width="61" height="40" alt="トップメニューボタン2" /></a>
    </div>
  </div>
  <div class="p_t_b_right p_btns_box">
    <a href="/preview/onlineshop"><img src="/assets/preview/img/topButton/ipost_main_store.png?{time()}" width="61" height="40" alt="トップメニューボタン3" /></a>
  </div>
</div>
<!--  -->

<!-- Facebook -->
<div class="p_top_url">
  <a href="/preview/webview">ホームページへ</a>
</div>
<!--  -->

<!-- マーキー -->
<div class="p_top_marquee">
  <marquee width="90%" style="margin-left:5%;">{$topic_message}</marquee>
</div>
<!--  -->

</div>
<!-- ▲ -->


{literal}
<script type="text/javascript">
$("#p_slides").carouFredSel({
    circular: false,
    infinite: false,
    auto    : false,
    pagination  : "#p_paging"
});
</script>
{/literal}


{include "preview/common/footer.tpl"}
