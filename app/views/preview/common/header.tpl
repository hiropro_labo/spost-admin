<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

<title>SPost Preview</title>

<!-- Favicon Icon -->
<link href="/assets/img/common/favicon/ipost.png" rel="shortcut icon" type="image/png" />

<meta name="keywords" content="">
<meta name="description" content="">
<meta name="robots" content="" />

<link href="/assets/preview/css/reset.css" rel="stylesheet" type="text/css" />
<link href="/assets/preview/css/base.css" rel="stylesheet" type="text/css" />

<script src="/assets/preview/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="/assets/preview/js/jquery.carouFredSel-6.2.1-packed.js" type="text/javascript"></script>
</head>
<body>
<div id="p_iphone">
  <div id="p_wrap">

<!-- ヘッダ -->
<div id="p_header_wrap">
  <div id="p_h_left">
  </div>
  <div id="p_h_center">
    <h1>SPost</h1>
  </div>
  <div id="p_h_right">
  </div>
</div>
<!--  -->
