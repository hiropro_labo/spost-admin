<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

<title>iPost Preview</title>

<!-- Favicon Icon -->
<link href="/assets/img/common/favicon/ipost.png" rel="shortcut icon" type="image/png" />

<meta name="keywords" content="">
<meta name="description" content="">
<meta name="robots" content="" />

<link href="/assets/preview/css/reset.css" rel="stylesheet" type="text/css" />
<link href="/assets/preview/css/store.css" rel="stylesheet" type="text/css" />

<script src="/assets/preview/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="/assets/preview/js/jquery.carouFredSel-6.2.1-packed.js" type="text/javascript"></script>
</head>
<body>
<div id="iphone">
  <div id="wrap">

<!-- ヘッダ -->
<div id="header" class="store">
</div>
<!--  -->
