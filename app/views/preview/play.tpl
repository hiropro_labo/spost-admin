{include "preview/common/header_play.tpl"}


<!-- アプリ情報 -->
<div id="app_info">
  <div class="clearfix">
    <div id="app_icon" class="fl_l">
      <img src="{$image_icon->image_path()}?{time()}" width="85" height="85" />
    </div>

    <div id="app_text" class="fl_r">
      <div id="app_name">
        <h1><marquee>{$SPONSOR->contents()->store()->app_name()|default:"---"}</marquee></h1>
        <h2>HIROPRO Inc.</h2>
      </div>
      <div id="app_install">
        <a href="javascript:void(0);" class="install_btn">インストール</a>
      </div>
    </div>
  </div>
</div>
<!--  -->

<!-- ▼コンテンツ -->
<div id="contents_wrap">

<div>
  <ul id="list">
    <!-- スクリーンショット -->
    <li id="list_img">
      <div>
{foreach from=$list_sshot1 item=image }
        <img src="{$image->image_path()}?{time()}" width="90" height="160" />
{/foreach}
      </div>
    </li>
    <!--  -->

    <!-- スクリーンショット -->
    <li id="list_data" class="clearfix">
      <div class="fl_l">
        <p>
          ★★★☆☆&nbsp;&nbsp;0<br />
          0件ダウンロード
        </p>
      </div>
      <div class="fl_r">
        <p>
          2013/11/19<br />
          28.0MB
        </p>
      </div>
    </li>

    <hr class="list_border" />

    <!-- Google+ -->
    <li id="list_google">
      <p>0人が+1しました。</p>
    </li>

    <hr class="list_border" />

    <!-- 評価とレビュー -->
    <li id="list_rate" class="clearfix">
      <h4 class="fl_l">評価とレビュー</h4>
      <p class="fl_r">★★★☆☆</p>
    </li>

    <hr class="list_border" />

    <!-- 説明 -->
    <li>
      <h4>説明</h4>
      <p>{$SPONSOR->contents()->store()->description()|default:"---"|nl2br}</p>
    </li>
    <!--  -->

    <hr class="list_border" />

    <!-- デベロッパー -->
    <li class="link_dev">
      <h4>デベロッパー</h4>
    </li>
    <!--  -->

    <hr class="list_border" />

    <!-- ウェブページにアクセス -->
    <li class="link_contents link_web">
      <h4>ウェブページにアクセス</h4>
      <p>http://i-post.jp</p>
    </li>
    <!--  -->

    <hr class="list_border" />

    <!-- メールを送信 -->
    <li class="link_contents link_mail">
      <h4>メールを送信</h4>
      <p>hiropro.co.jp</p>
    </li>
    <!--  -->

    <hr class="list_border" />

    <!-- プライバシーポリシー -->
    <li class="link_contents link_web">
      <h4>Google Playのコンテンツ</h4>
      <p>http://admin.i-post.jp/support/policy</p>
    </li>
    <!--  -->

    <hr class="list_border" />

    <!-- Google Playのコンテンツ -->
    <li class="link_contents">
      <h4>Google Playのコンテンツ</h4>
    </li>
    <!--  -->

    <hr class="list_border" />

    <!-- 不適切なコンテンツとして報告 -->
    <li class="link_contents">
      <h4>不適切なコンテンツとして報告</h4>
      <p>不適切なコンテンツがアプリやこの画面に含まれている場合はGoogleに報告して下さい。</p>
    </li>
    <!--  -->

  </ul>
</div>

</div>
<!-- ▲ -->


{include "preview/common/footer_play.tpl"}
