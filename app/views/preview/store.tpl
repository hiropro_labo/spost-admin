{include "preview/common/header_store.tpl"}


<!-- ▼コンテンツ -->
<div id="contents_wrap">

<div>
<!-- アプリ情報 -->
<div id="app_info">
  <div class="clearfix">
    <div id="app_icon" class="fl_l">
      <img src="{$image_icon->image_path()}?{time()}" width="100" height="100" />
    </div>

    <div id="app_text" class="fl_r">
      <div id="app_name">
        <h1>{$SPONSOR->contents()->store()->app_name()|default:"---"}</h1>
        <h2>HIROPRO Inc.</h2>
      </div>

      <div class="clearfix">
        <div id="app_rate" class="fl_l">
          ★★★☆☆
        </div>

        <div id="app_install" class="fl_r">
          <a href="javascript:void(0);" class="store_btn">無料</a>
        </div>
      </div>
    </div>
  </div>

  <div id="app_menu">
    <ul class="clearfix">
      <li class="current"><a href="javascript:void(0);">詳細</a></li>
      <li><a href="javascript:void(0);">レビュー</a></li>
      <li><a href="javascript:void(0);">関連</a></li>
    </ul>
  </div>
</div>
<!--  -->

<hr />



<ul id="list">
  <!-- スクリーンショット -->
  <li id="list_img">
    <div>
{foreach from=$list_sshot1 item=image }
      <img src="{$image->image_path()}?{time()}" width="140" height="248" />
{/foreach}
    </div>
  </li>
  <!--  -->

  <hr class="list_border" />

  <!-- 説明 -->
  <li>
    <h4>説明</h4>
    <p>{$SPONSOR->contents()->store()->description()|default:"---"|nl2br}</p>
  </li>
  <!--  -->

  <hr class="list_border" />

  <!-- 情報 -->
  <li id="list_info">
    <h4>情報</h4>
    <ul class="clearfix">
      <li><label>販売元</label><span>HIROPRO Inc.</span></li>
      <li><label>カテゴリ</label><span>{$SPONSOR->contents()->store()->category_iphone1()}</span></li>
      <li><label>バージョン</label><span>1.4</span></li>
      <li><label>サイズ</label><span>28.0MB</span></li>
      <li><label>レート</label><span>4+</span></li>
      <li><label>互換性</label><span>iPhone 4S、iPhone、iPod touch (第4世代)、iPod touch (第5世代)、およびiPad に対応。 iOS 6.1 以降が必要</span></li>
    </ul>
  </li>
  <!--  -->

  <hr class="list_border" />

  <!-- バージョン履歴 -->
  <li class="list_link">
    <h4>バージョン履歴</h4>
  </li>
  <!--  -->

  <hr class="list_border" />

  <!-- デベロッパApp -->
  <li class="list_link">
    <h4>デベロッパApp</h4>
  </li>
  <!--  -->

  <hr class="list_border" />

  <!-- デベロッパWebサイト -->
  <li class="list_link">
    <h4>デベロッパWebサイト</h4>
  </li>
  <!--  -->

  <hr class="list_border" />

  <!-- プライバシーポリシー -->
  <li class="list_link">
    <h4>プライバシーポリシー</h4>
  </li>
  <!--  -->

  <hr class="list_border" />

</ul>
</div>

</div>
<!-- ▲ -->


{include "preview/common/footer_store.tpl"}
