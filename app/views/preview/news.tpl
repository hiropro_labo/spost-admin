{include "preview/common/header.tpl"}


<!-- ▼コンテンツ -->
<div id="p_content_wrap" class="p_news">

<!-- ニュース一覧 -->
<div class="p_news_list">
  <ul>
{foreach from=$list item=news}
    <!--  -->
{if $news->notice_status == 1}
    <li>
{else}
    <li class="no_send">
{/if}
      <div class="p_n_title">
        {$news->title}
      </div>
      <div class="p_n_sub">
        <div class="p_n_s_time">
          {$news->updated_at|date_format:"%Y-%m-%d %H:%M:%S"}
        </div>
      </div>
      <div class="p_n_text">
        {$news->body|nl2br}
      </div>
      <img src="{$news->image_path()}?{time()}" width="210" height="140" alt="ニュース画像" />
    </li>
    <!--  -->

{/foreach}
  </ul>
</div>
<!--  -->

</div>
<!-- ▲ -->


{include "preview/common/footer.tpl"}
