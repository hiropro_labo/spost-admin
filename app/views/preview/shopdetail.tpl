{include "preview/common/header_back.tpl"}


<!-- ▼コンテンツ -->
<div id="p_content_wrap" class="p_s_detail">

<!-- お店画像 -->
<div class="p_s_d_img">
  <img src="{$shop_image->image_path()}?{time()}" width="225" height="148" alt="トップ画像" />
</div>
<!--  -->

<!-- お店情報 -->
<div class="p_s_d_date">
  <ul>
    <li>
      <label>お店の名前</label>
      <span>{$shop->shop_name}</span>
    </li>

    <li>
      <label>郵便番号</label>
      <span>{$shop->zip_code1}&nbsp;-&nbsp;{$shop->zip_code2}</span>
    </li>

    <li>
      <label>住所</label>
      <span>
        {$shop_pref} {$shop->city} {$shop->address_opt1} <br />
        {$shop->address_opt2}
      </span>
    </li>

    <li>
      <label>TEL</label>
      <span>{$shop->tel1}&nbsp;-&nbsp;{$shop->tel2}&nbsp;-&nbsp;{$shop->tel3}</span>
    </li>

    <li>
      <label>FAX</label>
      <span>{$shop->fax1}&nbsp;-&nbsp;{$shop->fax2}&nbsp;-&nbsp;{$shop->fax3}</span>
    </li>

    <li>
      <label>Email</label>
{if $shop->email}
      <span>{$shop->email}</span>
{else}
      <span>&nbsp;</span>
{/if}
    </li>

    <li>
      <label>URL</label>
{if $shop->url}
      <span><a href="{$shop->url}">{$shop->url}</a></span>
{else}
      <span>&nbsp;</span>
{/if}
    </li>

    <li>
      <label>営業時間</label>
{if $shop->open_hours}
      <span>{$shop->open_hours}</span>
{else}
      <span>&nbsp;</span>
{/if}
    </li>

    <li>
      <label>定休日</label>
{if $shop->holiday}
      <span>{$shop->holiday}</span>
{else}
      <span>&nbsp;</span>
{/if}
    </li>
  </ul>
</div>
<!--  -->

</div>
<!-- ▲ -->


{include "preview/common/footer.tpl"}
