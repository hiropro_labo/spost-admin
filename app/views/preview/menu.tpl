{if $parent_id == 0}
{include "preview/common/header.tpl"}
{else}
{include "preview/common/header_back.tpl"}
{/if}


<!-- ▼コンテンツ -->
<div id="p_content_wrap" class="p_menu">

<!-- メニュートップ画像 -->
<div>
  <img src="{$image->image_path($SPONSOR->id())}?{time()}" width="240" height="131" alt="メニュートップ画像" />
</div>
<!--  -->

<!-- カテゴリーリスト -->
<div class="p_menu_list">
  <ul>
{if ! is_null($list)}
{foreach from=$list item=entry}
    <!-- リスト表示 -->
    <li class="clearfix">
{if $parent_id == 0}
      <a href="/preview/menu/{$entry->id}">
{else}
      <a href="/preview/menudetail/{$entry->id}">
{/if}
        <div class="p_m_list_img">
          <img src="{$entry->image_path()}?{time()}?{time()}" width="140" height="80" alt="メニュー画像" style="float:left;border-bottom: solid 1px #e0e0e0;" />
        </div>
        <div class="p_m_list_data">
          <div class="clearfix">
            <div class="p_m_list_sub">
              {$entry->title}
            </div>
            <div class="p_m_list_title">
              {$entry->sub_title}
            </div>
          </div>
        </div>
      </a>
    </li>
    <!--  -->
{/foreach}
{/if}
  </ul>
</div>
<!--  -->

</div>
<!-- ▲ -->


{include "preview/common/footer.tpl"}
