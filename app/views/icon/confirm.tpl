{include "common/header.tpl"}

	<h1>Top管理画面</h1>

	{* --------- Content : トップ画像の変更 [Start] --------- *}
	<fieldset>
	   <legend>トップ画像の変更</legend>

	   <div>
	       <p><img src="{$shop_image->tmp_image_path()}?{time()}" width="320" height="210" /></p>
	       <form action="/top/img/shop/update/exe" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">
	           <a href="/top" id="save_btn" class="gray_btn">戻る</a>
               <input type="submit" name="button" value="変更を保存" class="blue_btn" />
	       </form>
	   </div>


	</fieldset>
	{* --------- Content : お店の情報・写真の変更 [End] --------- *}


{include "common/footer_meta/top.tpl"}
{include "common/footer.tpl"}