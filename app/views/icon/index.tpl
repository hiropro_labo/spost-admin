{include "common/header.tpl"}

    <h1>Top管理画面</h1>

    {* --------- Content : アプリアイコンの変更 [Start] --------- *}
    <fieldset>
       <legend>アプリアイコンの更新</legend>

       <div>
           <p><img src="{$shop_image->image_path($client->id)}?{time()}" width="230" /></p>
           <form action="/top/img/shop/update/" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">
               <input type="file" name="upload" id="upload">
               <span class="desc">ファイルサイズ：3MBまで</span><br />
               <a href="/top" id="save_btn" class="gray_btn">戻る</a>
               <input type="submit" name="button" value="確認画面へ" class="blue_btn" />
           </form>
       </div>


    </fieldset>
    {* --------- Content : アプリアイコンの変更 [End] --------- *}

{include "common/footer_meta/top.tpl"}
{include "common/footer.tpl"}