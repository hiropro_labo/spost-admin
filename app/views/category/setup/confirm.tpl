{include "common/header.tpl"}


{include "common/header_meta/category.tpl"}

<div class="contents_box">
  <div class="contents_box_head">カテゴリー選択
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="アプリの利用用途に合わせたカテゴリーを選択してください"></a>
  </div>

  <div>
    <p>選択されたカテゴリー</p>
    {if $category == '1'}
    <p>飲食</p>
    {elseif $category == '2'}
    <p>物販</p>
    {elseif $category == '3'}
    <p>企業</p>
    {/if}
  </div>

  <div>
    <a href="/category/setup">戻る</a>
    <form action="/category/setup/exe/{$category}" method="POST">
        <input type="submit" value=" 登録 " />
    </form>
  </div>

</div>

<div class="last_margin"></div>


{include "common/footer.tpl"}
