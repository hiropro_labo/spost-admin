{include "common/header.tpl"}

{include "common/header_meta/category.tpl"}

<div class="contents_box">
  <div class="contents_box_head">カテゴリー選択</div>
  <h4>カテゴリー選択</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">カテゴリー選択の更新が完了しました</span>
      </li>
    </ul>
    <hr />
    <a href="/" id="save_btn" class="back_btn ml_30">戻る</a>
  </div>
</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}
