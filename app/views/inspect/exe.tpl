{include "common/header.tpl"}


{include "common/header_meta/inspect.tpl"}


<!-- アプリ審査申請 -->
<div class="contents_box">
  <div class="contents_box_head">アプリ審査申請</div>
  <h4>アプリ簡易審査の受付完了</h4>

  <form action="/top/topic/update" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <span class="ml_30">アプリ審査申請を受け付けました。<br />
      ご登録いただきましたコンテンツ内容などを弊社にて簡易審査させていただきます。<br />
      アプリ審査の進捗につきましてはこちらのページとメールにて随時お伝えしてまいります。<br />
      </span>
    </li>
  </ul>

  <hr />

  <a href="/inspect" id="save_btn" class="back_btn ml_30">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
