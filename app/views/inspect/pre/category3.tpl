    <!--左-->
    <div class="fl">

      {if $SPONSOR->contents()->status()->is_pass('top')}
        <div class="inspect_item clearfix">
          <div class="inspect_item_l_off i_icon1_off"><img src="/assets/img/app/spost/inspect/i_icon1_off.png" /></div>
          <div class="inspect_item_r"><h5>トップ</h5>
            <p>アプリトップを編集しましょう</p>
            <p class="inspect_item_off">入力済み</p>
          </div>
        </div>
      {else}
        <a href="/top" class="inspect_item clearfix">
          <div class="inspect_item_l_on i_icon1"><img src="/assets/img/app/spost/inspect/i_icon1_on.png" /></div>
          <div class="inspect_item_r"><h5>トップ</h5>
            <p>アプリトップを編集しましょう</p>
            <p class="inspect_item_on">未入力</p>
          </div>
        </a>
      {/if}

      {if $SPONSOR->contents()->status()->is_pass('news')}
        <div class="inspect_item clearfix">
          <div class="inspect_item_l_off i_icon2_off"><img src="/assets/img/app/spost/inspect/i_icon2_off.png" /></div>
          <div class="inspect_item_r"><h5>ニュース</h5>
            <p>ニュースを作りましょう</p>
            <p class="inspect_item_off">入力済み</p>
          </div>
        </div>
      {else}
        <a href="/news" class="inspect_item clearfix">
          <div class="inspect_item_l_on i_icon2"><img src="/assets/img/app/spost/inspect/i_icon2_on.png" /></div>
          <div class="inspect_item_r"><h5>ニュース</h5>
            <p>ニュースを作りましょう</p>
            <p class="inspect_item_on">未入力</p>
          </div>
        </a>
      {/if}

      {if $SPONSOR->contents()->status()->is_pass('company')}
        <div class="inspect_item clearfix">
          <div class="inspect_item_l_off i_icon5_off"><img src="/assets/img/app/spost/inspect/i_icon5_off.png" /></div>
          <div class="inspect_item_r"><h5>会社概要</h5>
            <p>会社概要を作りましょう</p>
            <p class="inspect_item_off">入力済み</p>
          </div>
        </div>
      {else}
        <a href="/company" class="inspect_item clearfix">
          <div class="inspect_item_l_on i_icon5"><img src="/assets/img/app/spost/inspect/i_icon5_on.png" /></div>
          <div class="inspect_item_r"><h5>会社概要</h5>
            <p>会社概要を作りましょう</p>
            <p class="inspect_item_on">未入力</p>
          </div>
        </a>
      {/if}

    </div>
    <!--/左-->

    <!--右-->
    <div class="fl ml_5p">

      {if $SPONSOR->contents()->status()->is_pass('segments')}
        <div class="inspect_item clearfix">
          <div class="inspect_item_l_off i_icon6_off"><img src="/assets/img/app/spost/inspect/i_icon6_off.png" /></div>
          <div class="inspect_item_r"><h5>事業内容</h5>
            <p>事業内容を作りましょう</p>
            <p class="inspect_item_off">入力済み</p>
          </div>
        </div>
      {else}
        <a href="/segments" class="inspect_item clearfix">
          <div class="inspect_item_l_on i_icon6"><img src="/assets/img/app/spost/inspect/i_icon6_on.png" /></div>
          <div class="inspect_item_r"><h5>事業内容</h5>
            <p>事業内容を作りましょう</p>
            <p class="inspect_item_on">未入力</p>
          </div>
        </a>
      {/if}

      {if $SPONSOR->contents()->status()->is_pass('store')}
        <div class="inspect_item clearfix">
          <div class="inspect_item_l_off i_icon14_off"><img src="/assets/img/app/spost/inspect/i_icon14_off.png" /></div>
          <div class="inspect_item_r"><h5>情報編集</h5>
            <p>アプリアイコンなどを設定しましょう</p>
            <p class="inspect_item_off">入力済み</p>
          </div>
        </div>
      {else}
        <a href="/store" class="inspect_item clearfix">
          <div class="inspect_item_l_on i_icon14"><img src="/assets/img/app/spost/inspect/i_icon14_on.png" /></div>
          <div class="inspect_item_r"><h5>情報編集</h5>
            <p>アプリアイコンなどを設定しましょう</p>
            <p class="inspect_item_on">未入力</p>
          </div>
        </a>
      {/if}

    </div>
    <!--/右-->