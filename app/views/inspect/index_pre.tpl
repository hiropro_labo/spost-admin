{include "common/header_inspect.tpl"}

<div id="container">
    <div id="contents" class="no_preview">

        {include "common/header_meta/inspect.tpl"}

        <div class="inspect_text_box">作業が完了した箇所は灰色に塗り潰されます。<br />全て灰色になればアプリ制作完了となり、公開のための申請を行うことができます。</div>

        <div class="inspect_item_wrap clearfix">
            {include "inspect/pre/category{$SPONSOR->contents()->theme()->model()->category}.tpl"}
        </div>

        {if $SPONSOR->app()->inspect()->is_inspect_allow()}
        <p style="text-align: center; color: #cd4538; font-weight: bold;">※※※　ご注意　※※※<br /><br />
            審査申請ボタンを押されますと、アプリ管理の項目変更にロックが掛かります。<br />
            ロックが掛かりますと、各項目の変更ができなくなります。</p>
        {/if}

        <div class="btn_box">
        {if $SPONSOR->app()->inspect()->is_inspect_allow()}
            <a href="javascript: void(0);" id="smb" class="blue_btn">アプリ申請をする</a>
        {else}
            <p class="glayout_btn">アプリ申請をする</p>
        {/if}
        </div>

        <form id="frm" action="/inspect" method="POST"></form>
    </div>
</div>

<div class="last_margin"></div>

{literal}
<script type="text/javascript">
$(function(){
	$('#smb').click(function(){
		$('#frm').submit();
	});
});
</script>
{/literal}

{include "common/footer.tpl"}
