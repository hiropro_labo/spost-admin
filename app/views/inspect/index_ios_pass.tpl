{include "common/header.tpl"}

{include "common/header_meta/inspect.tpl"}

<div class="contents_box">
  <div class="contents_box_head">アプリ審査申請
    <a href="/support/help" target="_blank"><img src="/assets/img/common/help_tips.png" title=""></a>
  </div>
  <h4 style="color:#900;">お客様のiOSアプリがApple審査を通過しました!!</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">おめでとうございます!!<br />
          iOSアプリの公開手続きを進めてまいりますので今暫くお待ち下さい。<br />
          公開手続きが完了しましたらメールにてお知らせします。</span>
      </li>
    </ul>
  </div>

  <div class="last_margin"></div>

{include "common/footer.tpl"}
