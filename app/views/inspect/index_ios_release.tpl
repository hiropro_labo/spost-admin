{include "common/header.tpl"}

{include "common/header_meta/inspect.tpl"}

<div class="contents_box">
  <div class="contents_box_head">アプリ審査申請
    <a href="/support/help" target="_blank"><img src="/assets/img/common/help_tips.png" title=""></a>
  </div>
  <h4 style="color:#900;">お客様のiOSアプリが公開されました!!</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">おめでとうございます!!<br />
        お客様の制作されたiOSアプリがApp Storeにて公開されました。</span>
      </li>
      <li>
        <span class="ml_30">App Store アプリインストールURL：{$SPONSOR->app()->store_url('apple')}</span>
      </li>
    </ul>
  </div>

  <div class="last_margin"></div>

{include "common/footer.tpl"}
