{include "common/header.tpl"}

{include "common/header_meta/inspect.tpl"}

<div class="contents_box">
  <div class="contents_box_head">アプリ審査申請
    <a href="/support/help" target="_blank"><img src="/assets/img/common/help_tips.png" title=""></a>
  </div>
  <h4 style="color:#900;">お客様のAndroidアプリが公開されました!!</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">おめでとうございます!!<br />
        お客様の制作されたAndroidアプリがGoogle Playにて公開されました。</span>
      </li>
      <li>
        <span class="ml_30">Google Play アプリインストールURL：{$SPONSOR->app()->store_url('google')}</span>
      </li>
      <li>
        <span class="ml_30">また、iOSアプリのApple審査につきましても順次申請手続きを進めてまいります。<br />
        審査終了までの日数につきましては申請完了から最低10営業日程度のお時間が掛かりますことをご了承くださいませ。<br />
        審査結果はメールにて改めてお知らせ致します。</span>
      </li>
    </ul>
  </div>

  <div class="last_margin"></div>

{include "common/footer.tpl"}
