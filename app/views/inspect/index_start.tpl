{include "common/header.tpl"}

{include "common/header_meta/inspect.tpl"}

<div class="contents_box">
  <div class="contents_box_head">アプリ審査申請
    <a href="/support/help" target="_blank"><img src="/assets/img/common/help_tips.png" title=""></a>
  </div>
  <h4 style="color:#900;">現在、アプリ簡易審査中です。審査中は各項目の変更ができなくなります。</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">現在、アプリ審査中です。<br />審査結果のご通知まで暫くお待ち下さい。</span>
      </li>
    </ul>
  </div>

  <div class="last_margin"></div>

{include "common/footer.tpl"}
