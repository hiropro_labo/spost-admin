{include "common/header.tpl"}

{include "common/header_meta/inspect.tpl"}

<div class="contents_box">
  <div class="contents_box_head">アプリ審査申請
    <a href="/support/help" target="_blank"><img src="/assets/img/common/help_tips.png" title=""></a>
  </div>
  <h4 style="color:#900;">お客様のiOSアプリは以下の理由によりApple審査を通過しませんでした。<br />
    お手数ではございますが、下記の審査結果の内容をご確認いただきコンテンツ修正等のご対応をお願い致します。<br />
    ご対応が完了しましたら「Apple審査申請」ボタンを押してください。</h4>

  <form action="/inspect/ios" method="POST">
  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">
          ■Apple審査結果<br />
          審査NG<br />
        </span>
      </li>
      <li>
        <span class="ml_30">
          ■Apple審査NG理由<br />
          {$SPONSOR->app()->inspect()->apple_reject_instance()->comment()|nl2br}
        </span>
      </li>
      <li>
        <span class="ml_30">
          ■審査通過へのヒント<br />
          {$SPONSOR->app()->inspect()->apple_reject_instance()->hint()|nl2br}
        </span>
      </li>
      <li>
        <input type="submit" value="Apple審査申請" class="save_btn ml_30" />
      </li>
    </ul>
  </div>
  </form>

  <div class="last_margin"></div>

{include "common/footer.tpl"}
