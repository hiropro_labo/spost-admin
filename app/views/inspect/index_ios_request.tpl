{include "common/header.tpl"}

{include "common/header_meta/inspect.tpl"}

<div class="contents_box">
  <div class="contents_box_head">アプリ審査申請
  <a href="/support/help" target="_blank"><img src="/assets/img/common/help_tips.png" title=""></a>
  </div>
  <h4 style="color:#900;">iOSアプリのApple審査申請のお申し込みを受け付けました。</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">iOSアプリのApple審査申請を順次進めてまいります。<br />
          審査終了までの日数につきましては申請完了から最低10営業日程度のお時間が掛かりますことをご了承くださいませ。<br />
          審査結果はメールにて改めてお知らせ致します。</span>
        </span>
      </li>
    </ul>
  </div>

  <div class="last_margin"></div>

{include "common/footer.tpl"}
