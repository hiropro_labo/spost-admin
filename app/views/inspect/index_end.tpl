{include "common/header.tpl"}

{include "common/header_meta/inspect.tpl"}

<div class="contents_box">
  <div class="contents_box_head">アプリ審査申請
  <a href="/support/help" target="_blank"><img src="/assets/img/common/help_tips.png" title=""></a>
  </div>
  <h4 style="color:#900;">アプリ簡易審査を通過しました!!</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">只今、Androidアプリの公開手続き、並びにiOSアプリのApple審査申請を進めております。<br />
        進捗につきましては順次お知らせしてまいりますのでしばらくお待ちください。
      </li>
    </ul>
  </div>

  <div class="last_margin"></div>

{include "common/footer.tpl"}
