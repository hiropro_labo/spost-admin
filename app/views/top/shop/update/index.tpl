{include "common/header.tpl"}


{include "common/header_meta/shop.tpl"}


<!-- お店の情報の変更 -->
<div class="contents_box">
  <div class="contents_box_head">お店の情報・写真の変更<a href="/support/manual/top#top_4"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="あなたのお店の情報を変更する事ができます。<br>フォームに入力した後、「確認する」ボタンを押してください。"></a></div>
  <h4>お店の情報</h4>

  <form action="/top/shop/update" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label for="name">お店の名前</label>
      {$fieldset->field('shop_name')->build()}{$fieldset->error_msg('shop_name')}
    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="zip1">郵便番号</label>
    {$fieldset->field('zip_code1')->build()}&nbsp;{$fieldset->field('zip_code2')->build()}
    </li>

    <li>
      <label for="pref">都道府県</label>
      {$fieldset->field('pref')->build()}
    </li>

    <li>
      <label for="pref">市区町村</label>
      {$fieldset->field('city')->build()} {$fieldset->error_msg('city')}
    </li>

    <li>
      <label for="pref">住所（番地）</label>
      {$fieldset->field('address_opt1')->build()} {$fieldset->error_msg('address_opt1')}
    </li>

    <li>
      <label for="pref">住所（建物）</label>
      {$fieldset->field('address_opt2')->build()} {$fieldset->error_msg('address_opt2')}
    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">電話番号</label>
      {$fieldset->field('tel1')->build()}&nbsp;{$fieldset->field('tel2')->build()}&nbsp;{$fieldset->field('tel3')->build()} {$fieldset->description('tel1')}
    </li>

    <li>
      <label for="pref">FAX</label>
      {$fieldset->field('fax1')->build()}&nbsp;{$fieldset->field('fax2')->build()}&nbsp;{$fieldset->field('fax3')->build()} {$fieldset->description('fax1')}
    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">ホームページ</label>
      {$fieldset->field('url')->build()} {$fieldset->description('url')} {$fieldset->error_msg('url')}
    </li>
  </ul>

  <ul>
    <li>
      <label for="pref">オンラインショップ</label>
      {$fieldset->field('online_shop')->build()} {$fieldset->description('online_shop')} {$fieldset->error_msg('online_shop')}
    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">メールアドレス</label>
      {$fieldset->field('email')->build()} {$fieldset->description('email')} {$fieldset->error_msg('email')}
    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">営業時間</label>
      {$fieldset->field('open_hours')->build()} {$fieldset->description('open_hours')} {$fieldset->error_msg('open_hours')}
    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">定休日</label>
      {$fieldset->field('holiday')->build()} {$fieldset->description('holiday')} {$fieldset->error_msg('holiday')}
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/shop" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
