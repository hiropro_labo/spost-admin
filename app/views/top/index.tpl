{include "common/header.tpl"}


{include "common/header_meta/top.tpl"}


<!-- トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">
    {__('images.title')}
    <a href="/support/manual/top" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="{__('images.tips')}"></a>
  </div>

  <h4 id="sortable_h4">{__('images.sub')}</h4>
  <p>{__('images.notes')}</p>


  <ul id="sortable-li">
{foreach from=$image_list item=image }
    <li id="{$image->position}" class="sort_box"><img src="{$image->image_path()}" width="160" height="105" alt="メニュートップ画像" class="d1"/>
{if ! $SPONSOR->app()->is_lock()}
      <div class="contenthover">
        <a href="/top/img/top/delete/{$image->seq}"><img src="/assets/img/common/del_icon.png" class="del_icon"></a>
        <a href="/top/img/top/update/{$image->seq}" class="edit_btn">{__('images.edit_btn')}</a>
      </div>
{/if}
    </li>
{/foreach}
  </ul>

</div>

<div class="last_margin"></div>



{if ! $SPONSOR->app()->is_lock()}
{include "common/footer_meta/top.tpl"}
{/if}
{include "common/footer.tpl"}
