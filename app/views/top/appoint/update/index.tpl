{include "common/header.tpl"}


{include "common/header_meta/appoint.tpl"}


<!-- 予約カレンダーの表示の変更 -->
<div class="contents_box">
  <div class="contents_box_head">予約カレンダーの表示</div>
  <h4>お店情報の画面から予約カレンダーを表示させることが出来ます。</h4>

<form action="/top/appoint/update" method="POST">
  <div class="contents_form ml_30">
    <ul>
      <li>
        <label for="form_appoint_0" class="ml_0"><input type="radio" required="required" id="form_appoint_0" name="appoint" value="0" checked="checked" />&nbsp;表示しない</label>
        <label for="form_appoint_1"><input type="radio" required="required" id="form_appoint_1" name="appoint" value="1" />&nbsp;表示する</label>
      </li>
      <li>
        <p class="desc ml_0">お店情報の画面に予約カレンダーへのボタンを表示させることが出来ます。</p>
        <p class="error">{$fieldset->error_msg('appoint')}</p>
      </li>
    </ul>

  <hr class="ml_0" />

  <input type="submit" name="button" value="変更の確認" class="save_btn ml_0" />
  <a href="/top" id="save_btn" class="back_btn">戻る</a>
  </div>
</form>

</div>

{include "common/footer.tpl"}
