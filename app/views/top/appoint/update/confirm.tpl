{include "common/header.tpl"}


{include "common/header_meta/appoint.tpl"}


<!-- トピックメッセージ -->
<div class="contents_box">
  <div class="contents_box_head">予約カレンダーの表示</div>
  <h4>予約カレンダーの表示</h4>

  <form action="/top/appoint/update/exe" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label class="ml_30">予約カレンダーの表示</label>
      <span>{if $fieldset->value('appoint')=='0'}表示しない{else}表示する{/if}</span>
    </li>
  </ul>

  <hr />

  {* ------- Hidden Data [ Start ] -------- *}
  {foreach from=$fieldset->field() item=field}
  {$field}
  {/foreach}
  {* ------- Hidden Data [ End ] ---------- *}
  <input type="submit" name="button" value="変更の保存" class="save_btn ml_30" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
