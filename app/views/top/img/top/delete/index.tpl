トップ画像の削除{include "common/header.tpl"}


{include "common/header_meta/top.tpl"}


<!-- トップ画像の削除 -->
<div class="contents_box">
  <div class="contents_box_head">
  トップ画像の削除
　<a href="/support/manual/top#top_1"  target="_blank"><img src="/assets/img/common/help_tips.png" 
class="tooltip" title="アプリのトップ画像を削除する事ができます。<br>「この画像を削除」ボタンを押し、削除して下さい。"></a>
  </div>

  <h4>トップ画像の削除</h4>

  <form action="/top/img/top/delete/exe/{$position}" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>トップ画像</label>
      <img src="{$top_image->image_path()}?{time()}" width="160" height="105" alt="トップ画像" class="mb_20 con_img" />
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="この画像を削除" class="save_btn" />
  <a href="/top" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
