{include "common/header.tpl"}


{include "common/header_meta/top.tpl"}


<!-- トップ画像の削除 -->
<div class="contents_box">
  <div class="contents_box_head">お店の情報・写真の変更</div>
  <h4>トップ画像の削除</h4>

  <form action="/top/topic/update" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <span class="ml_30">トップ画像の削除が完了しました</span>
    </li>
  </ul>

  <hr />

  <a href="/top" id="save_btn" class="back_btn ml_30">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
