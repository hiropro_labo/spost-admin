{include "common/header.tpl"}


{include "common/header_meta/top.tpl"}


<!-- トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">トップ画像の変更<a href="/support/manual/top"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="アプリのトップ画像を変更する事ができます。<br>「ファイルを選択」ボタンを押し、お好きな画像をアップロード後、<br>「変更を保存」ボタンを押してください。"></a></div>
  <h4>トップ画像の変更</h4>

  <form action="/top/img/top/update/{$position}" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">画像のアップロード</label>
      <img src="{$top_image->image_path()}?{time()}" width="160" height="105" alt="トップ画像" class="con_img form_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />640px&nbsp;×&nbsp;420px&nbsp;以上の大きさを推奨</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/top" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}