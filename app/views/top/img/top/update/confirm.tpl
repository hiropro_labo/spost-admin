{include "common/header.tpl"}


{include "common/header_meta/top.tpl"}


<!-- トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">お店の情報・写真の変更</div>
  <h4>トップ画像の変更</h4>

  <form action="/top/img/top/update/exe/{$position}" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label>画像のアップロード</label>
      {if $img_upload_flg}
      <img src="{$top_image->tmp_image_path()}?{time()}" width="160" height="105" alt="トップ画像" class="con_img form_img" />
      {else}
      <img src="{$top_image->image_path()}?{time()}" width="160" height="105" alt="トップ画像" class="con_img form_img" />
      {/if}
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
