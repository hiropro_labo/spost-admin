{include "common/header.tpl"}


{include "common/header_meta/shop.tpl"}


<!-- お店の画像変更 -->
<div class="contents_box">
  <div class="contents_box_head">お店の情報・写真の変更
    <a href="/support/manual/top#top_3"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="お店詳細の表示時の画像を変更する事ができます。<br>「ファイルを選択」ボタンを押し、お好きな画像をアップロード後、<br>「変更を保存」ボタンを押してください。"></a>
  </div>
  <h4>お店の画像</h4>

<form action="/top/img/shop/update/" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">画像のアップロード</label>
      <img src="{$SPONSOR->contents()->shop()->image_path()}?{time()}" width="160" height="105" alt="ショップ画像" class="con_img form_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />640px&nbsp;×&nbsp;420px&nbsp;以上の大きさを推奨</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/shop" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
