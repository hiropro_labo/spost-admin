{include "common/header.tpl"}


{include "common/header_meta/top.tpl"}


<!-- トピックメッセージ -->
<div class="contents_box">
  <div class="contents_box_head">トピックメッセージ</div>
  <h4>トピックメッセージ</h4>

	<form action="/top/topic/update" method="POST">

<div class="contents_form">
	<ul>
    <li>
      <label>トピックメッセージ</label>
      {$fieldset->field('message')->build()}
      <p class="desc">最大255文字まで</p>
      <p class="error">{$fieldset->error_msg('message')}</p>
    </li>
	</ul>
 
	<hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/top" id="save_btn" class="back_btn">戻る</a>
</div>

	</form>
</div>
<!---->


<div class="last_margin"></div>

{include "common/footer.tpl"}