{include "common/header.tpl"}


{include "common/header_meta/top.tpl"}


<!-- トピックメッセージ -->
<div class="contents_box">
  <div class="contents_box_head">トピックメッセージ</div>
  <h4>トピックメッセージ</h4>

  <form action="/top/topic/update/exe" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>トピックメッセージ</label>
      <span>{$fieldset->value('message')}</span>
    </li>
  </ul>

  <hr />

  {foreach from=$fieldset->field() item=field}
  {$field}
  {/foreach}
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


{include "common/footer.tpl"}
