{include "common/header.tpl"}

{include "common/header_meta/url.tpl"}

<div class="contents_box">
  <div class="contents_box_head">URL</div>
  <h4>スライドメニューに表示されるURLとメニュータイトルを編集できます</h4>

  <form action="/url/update/exe" method="POST">
    <div class="contents_form">
      <ul>
        <li>
          <label>{$fieldset->label('url_title1')}</label>
          <span>{$fieldset->value('url_title1')|default:"---"}</span>
        </li>
        <li>
          <label>{$fieldset->label('url1')}</label>
          <span>{$fieldset->value('url1')|default:"---"}</span>
        </li>
        <li>
          <label>{$fieldset->label('url_title2')}</label>
          <span>{$fieldset->value('url_title2')|default:"---"}</span>
        </li>
        <li>
          <label>{$fieldset->label('url2')}</label>
          <span>{$fieldset->value('url2')|default:"---"}</span>
        </li>
        <li>
          <label>{$fieldset->label('url_title3')}</label>
          <span>{$fieldset->value('url_title3')|default:"---"}</span>
        </li>
        <li>
          <label>{$fieldset->label('url3')}</label>
          <span>{$fieldset->value('url3')|default:"---"}</span>
        </li>
      </ul>
      <hr />
      {foreach from=$fieldset->field() item=field}
      {$field}
      {/foreach}
      <input type="submit" name="button" value="変更の保存" class="save_btn" />
      <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
    </div>
  </form>
</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}
