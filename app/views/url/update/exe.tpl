{include "common/header.tpl"}

{include "common/header_meta/url.tpl"}

<div class="contents_box">
  <div class="contents_box_head">URL</div>
  <h4>スライドメニューに表示されるURL</h4>
  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">メニューに表示されるURLの更新が完了しました</span>
      </li>
    </ul>

    <hr />

    <a href="/url" id="save_btn" class="back_btn ml_30">戻る</a>
  </div>
</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}
