{include "common/header.tpl"}

{include "common/header_meta/url.tpl"}

<div class="contents_box">
  <div class="contents_box_head">URL</div>
  <h4>スライドメニューに表示されるURLとメニュータイトルを編集できます</h4>

  <form action="/url/update" method="POST">
    <div class="contents_form">
      <ul>
        <li>
          <label>{$fieldset->label('url_title1')}</label>
          {$fieldset->field('url_title1')->build()}
          <p class="desc">最大7文字まで</p>
          <p class="error">{$fieldset->error_msg('url_title1')}</p>
        </li>
        <li>
          <label>{$fieldset->label('url1')}</label>
          {$fieldset->field('url1')->build()}
          <p class="desc">最大255文字まで</p>
          <p class="error">{$fieldset->error_msg('url1')}</p>
        </li>
        <li>
          <label>{$fieldset->label('url_title2')}</label>
          {$fieldset->field('url_title2')->build()}
          <p class="desc">最大7文字まで</p>
          <p class="error">{$fieldset->error_msg('url_title2')}</p>
        </li>
        <li>
          <label>{$fieldset->label('url2')}</label>
          {$fieldset->field('url2')->build()}
          <p class="desc">最大255文字まで</p>
          <p class="error">{$fieldset->error_msg('url2')}</p>
        </li>
        <li>
          <label>{$fieldset->label('url_title3')}</label>
          {$fieldset->field('url_title3')->build()}
          <p class="desc">最大7文字まで</p>
          <p class="error">{$fieldset->error_msg('url_title3')}</p>
        </li>
        <li>
          <label>{$fieldset->label('url3')}</label>
          {$fieldset->field('url3')->build()}
          <p class="desc">最大255文字まで</p>
          <p class="error">{$fieldset->error_msg('url3')}</p>
        </li>
      </ul>
      <hr />
      <input type="submit" name="button" value="変更の確認" class="save_btn" />
      <a href="/url" id="save_btn" class="back_btn">戻る</a>
    </div>
  </form>
</div>

<div class="last_margin"></div>

{include "common/footer.tpl"}