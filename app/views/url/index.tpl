{include "common/header.tpl"}

{include "common/header_meta/url.tpl"}

<div class="contents_box">
  <div class="contents_box_head">
    スライドメニューに表示するURLの変更
    <a href="/support/manual/top#top_3"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="電話番号や営業時間などの、お店の情報を変更する事ができます。<br>「お店の画像」には、あなたのお店の魅力が伝わる写真をアップロードしましょう。。"></a>
  </div>

  <h4>お店の情報</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span><br />
  お店の名前が必要になります。</p>
{if ! $SPONSOR->app()->is_lock()}
  <a href="/url/update" class="edit_btn">編集する</a>
{/if}
  <ul class="cel mt_10">
    {if is_null($model)}
    <li><label></label><span>現在、URLは登録されていません</span></li>
    {else}
    <li class="c_00"><label><img src="/assets/img/common/icon/i20.png" class="mr_5 va_m"><a href="{$model->url1}" target="_blank" class="va_m">{$model->url_title1}</a></label></li>
    <li class="c_00"><label><img src="/assets/img/common/icon/i20.png" class="mr_5 va_m"><a href="{$model->url2}" target="_blank" class="va_m">{$model->url_title2}</a></label></li>
    <li class="c_00"><label><img src="/assets/img/common/icon/i20.png" class="mr_5 va_m"><a href="{$model->url3}" target="_blank" class="va_m">{$model->url_title3}</a></label></li>
    {/if}
  </ul>
</div>

{include "common/footer.tpl"}
