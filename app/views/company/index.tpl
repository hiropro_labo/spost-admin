{include "common/header.tpl"}


{include "common/header_meta/company.tpl"}


<!-- 企業概要トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">企業概要トップ画像の変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="チームトップ画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>企業概要ページのトップ画像を変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/company/top/update" class="edit_btn">変更する</a>
  <a href="/company/top/update" title="変更"><img src="{$image->image_path()}?{time()}" width="160" height="70" alt="企業概要トップ画像" class="mb_20 con_img" /></a>
{else}
  <img src="{$image->image_path()}" width="160" height="70" alt="企業概要トップ画像" class="mb_20 con_img" />
{/if}
</div>
<!---->

<!-- 企業名 -->
<div class="contents_box">
  <div class="contents_box_head lock">
    企業名
    <a href="/support/manual/store#store_1"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playに表示される<br>アプリ名とアプリの説明文が作成できます。<br>表示名はインストールしたアプリの名前です。<br>「編集する」ボタンを押して<br>表示したい内容を記入して下さい。"></a>
  </div>
  <h4></h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span></p>

{if ! $SPONSOR->app()->is_lock()}
  <a href="/company/name/update" class="edit_btn">編集する</a>
{/if}
  <ul class="cel">
    <li><label>日本語</label><span>{$SPONSOR->contents()->company()->name_ja()|default:"---"}</span></li>
    <li><label>英語</label><span>{$SPONSOR->contents()->company()->name_en()|default:"---"}</span></li>
  </ul>
</div>


{* ----- 表示項目一覧 [Start] ----- *}
<div class="contents_box">
  <div class="contents_box_head">表示項目一覧
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="表示項目の詳細が作成できます。"></a>
  </div>

  <h4 class="mb_20">表示項目の登録・編集・削除ができます</h4>

  {if $list}
  {foreach from=$list item=item}
  <ul class="cel cel_recruit">
    <li class="clearfix">
      <div class="cel_recruit_wrap">
        <label><input type="text" id="title{$item->id}" value="{$item->title}" class="w_120"></label>
        <p class="fl"><textarea id="body{$item->id}">{$item->body}</textarea></p>
        <p class="fr"><input type="button" id="upd_{$item->id}" class="mr_3 ml_30 news_edit_btn" value="更新" onclick="update('{$item->id}', this)">&emsp;<input type="button" id="del_{$item->id}" class="mr_3 news_edit_btn" value="削除" onclick="del('{$item->id}', this)"></span>
        <p id="error_msg{$item->id}"></p>
      </div>
    </li>
  </ul>
  {/foreach}
  {/if}

  <ul class="cel cel_recruit">
      <li class="clearfix">
        <div class="cel_recruit_wrap">
          <label><input type="text" id="add_title" value="タイトル" class="w_120"></label>
          <p class="fl"><textarea id="add_body">本文をココに記載してください</textarea></p>
          <p class="di fr mr_13"><input type="button" value="追加" onclick="add(this)" class="news_edit_btn"></span>
          <p id="error_msg_add"></p>
    </li>
  </ul>

</div>


<div class="last_margin"></div>

{include "common/footer_meta/company.tpl"}
{include "common/footer.tpl"}
