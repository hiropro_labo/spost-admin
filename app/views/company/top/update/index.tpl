{include "common/header.tpl"}

{include "common/header_meta/company.tpl"}

<!-- 企業概要トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">企業概要トップ画像の変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="企業概要トップ画像の変更ができます。<br>「ファイルを選択」ボタンを押して<br>画像をアップロードして下さい。"></a>
  </div>
  <h4>企業概要トップ画像の変更</h4>

  <form action="/company/top/update" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">画像のアップロード</label>
      <img src="{$image->image_path()}?{time()}" width="160" height="70" alt="企業概要トップ画像" class="form_img con_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />640px&nbsp;×&nbsp;280px&nbsp;以上の大きさを推奨</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/company" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>

{include "common/footer.tpl"}
