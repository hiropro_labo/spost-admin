{include "common/header.tpl"}

{include "common/header_meta/company.tpl"}

<!-- 企業概要トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">企業概要トップ画像の変更
  </div>
  <h4>企業概要トップ画像の変更</h4>

	<form action="/company/top/update/exe" method="POST">

    <div class="contents_form">
      <ul>
        <li>
          <label>企業概要トップ画像</label>
    	    <img src="{$image->tmp_image_path()}?{time()}" width="160" height="70" alt="企業概要トップ画像" class="form_img con_img" />
        </li>
      </ul>

      <hr />

      <input type="submit" name="button" value="変更の保存" class="save_btn" />
      <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
    </div>

  </form>
</div>


{include "common/footer.tpl"}
