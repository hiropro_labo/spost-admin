{include "common/header.tpl"}


{include "common/header_meta/company.tpl"}


<div class="contents_box">
  <div class="contents_box_head">企業名の変更
    <a href="/support/manual/store#store_1"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="アプリストア(App Store/Google Play)表示項目を編集することができます。<br>フォームに入力した後、「確認する」ボタンを押してください。"></a></div>
  <h4>企業名はこちらで編集できます</h4>

  <form action="/company/name/update" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label for="zip1" class="hisu">日本語</label>
      {$fieldset->field('name_ja')->build()}
      <p class="desc">最大全角255文字</p>
      <p class="error">{$fieldset->error_msg('name_ja')}</p>
    </li>

    <li>
      <label for="pref">英語</label>
      {$fieldset->field('name_en')->build()}
      <p class="desc">最大全角255文字</p>
      <p class="error">{$fieldset->error_msg('name_en')}</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/company" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>


<div class="last_margin"></div>


{include "common/footer.tpl"}
