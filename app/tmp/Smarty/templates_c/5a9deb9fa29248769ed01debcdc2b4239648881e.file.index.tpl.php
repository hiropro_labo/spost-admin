<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-16 17:07:04
         compiled from "/home/spost/admin/app/views/url/add/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:702947740539ea4bde3a614-36207855%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5a9deb9fa29248769ed01debcdc2b4239648881e' => 
    array (
      0 => '/home/spost/admin/app/views/url/add/index.tpl',
      1 => 1402873618,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '702947740539ea4bde3a614-36207855',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539ea4bdeb5375_52663786',
  'variables' => 
  array (
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539ea4bdeb5375_52663786')) {function content_539ea4bdeb5375_52663786($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/url.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<!-- URLの新規登録 -->
<div class="contents_box">
  <div class="contents_box_head">URLの新規登録
　<a href="/support/manual/coupon"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="お客様に特別なURLを提供することが出来ます。<br>「ファイルを選択」ボタンを押して、画像をアップロード<br>利用条件、期間設定後「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>URLの新規登録</h4>

  <form action="/url/add" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

  <div class="contents_form">
    <ul>
      <li>
        <label>メニュータイトル</label>
        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('title')->build();?>

        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('title');?>

      </li>
      <li>
        <label>URL</label>
        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('url')->build();?>

        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('url');?>

      </li>
    </ul>
    <hr />
    <input type="submit" name="button" value="変更の確認" class="save_btn" />
    <a href="/url" id="save_btn" class="back_btn">戻る</a>
  </div>

  </form>
</div>


<div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>