<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-18 19:53:25
         compiled from "/home/spost/admin/app/views/store/copyright/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:204883352653a16fa5090976-47193579%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '88c18d869870cf3f1ca72f6bcee81b779d6990b5' => 
    array (
      0 => '/home/spost/admin/app/views/store/copyright/update/index.tpl',
      1 => 1403056159,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '204883352653a16fa5090976-47193579',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a16fa50ea652_31765073',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a16fa50ea652_31765073')) {function content_53a16fa50ea652_31765073($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/store.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<div class="contents_box">
  <div class="contents_box_head">Copyright表記の変更
    <a href="/support/manual/store#store_1"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="アプリストア(App Store/Google Play)表示項目を編集することができます。<br>フォームに入力した後、「確認する」ボタンを押してください。"></a></div>
  <h4>Copyright表記内容をこちらで編集できます</h4>

  <form action="/store/copyright/update" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label for="zip1" class="hisu">Copyright表記内容</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('copyright')->build();?>

      <p class="desc">最大全角255文字</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('copyright');?>
</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/store" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>