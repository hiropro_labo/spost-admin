<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-03 12:54:35
         compiled from "/home/spost/admin/app/views/store/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1849519952537edd7e65e360-98970314%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd1dc8ed4481999a7a5989853d3f426fb09ab5c01' => 
    array (
      0 => '/home/spost/admin/app/views/store/index.tpl',
      1 => 1404359674,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1849519952537edd7e65e360-98970314',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_537edd7e822c01_07295361',
  'variables' => 
  array (
    'SPONSOR' => 0,
    'image_icon' => 0,
    'image_appicon' => 0,
    'list_sshot1' => 0,
    'image' => 0,
    'list_sshot2' => 0,
    'image_splash' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537edd7e822c01_07295361')) {function content_537edd7e822c01_07295361($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/store.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- お店の写真の変更 -->
<div class="contents_box">
  <div class="contents_box_head lock">
    アイコン画像
    <a href="/support/manual/store"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playに表示される<br>アイコン画像を登録する事ができます。どのアプリよりも目を引く<br>アイコンを表示させましょう。"></a>
  </div>
  <h4>アイコン画像の変更</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span><br />
  <span class="hisu">※簡易申請後、変更することが出来ません。</span><br />
  推奨サイズ : 1024px x 1024px</p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/store/icon/update" class="edit_btn">編集する</a>
<?php }?>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/store/icon/update"><img src="<?php echo $_smarty_tpl->tpl_vars['image_icon']->value->image_path();?>
" width="160" alt="アイコン画像" class="mb_20 con_img" /></a>
<?php }else{ ?>
  <img src="<?php echo $_smarty_tpl->tpl_vars['image_icon']->value->image_path();?>
" width="160" alt="アイコン画像" class="mb_20 con_img" />
<?php }?>
</div>

<!-- アプリ内ステータスバー表示用画像 -->
<div class="contents_box">
  <div class="contents_box_head lock">
    アプリ内ステータスバー表示用画像
    <a href="/support/manual/store#store_1"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playに表示される<br>アプリ名とアプリの説明文が作成できます。<br>表示名はインストールしたアプリの名前です。<br>「編集する」ボタンを押して<br>表示したい内容を記入して下さい。"></a>
  </div>
  <h4>アプリ内ステータスバー表示画像の変更</h4>
  <p><span class="hisu">※未指定の場合はアプリ内のステータスバーには アプリストア->表示名 にて指定されたテキストが表示されます</span><br />
  <span class="hisu">※画像サイズは高さ32pxを基準に縦横比を保ったままリサイズされます</span><br />
  推奨サイズ : 32px x 100px</p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/store/appicon/update" class="edit_btn">編集する</a>
<?php }?>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/store/appicon/update"><img src="<?php echo $_smarty_tpl->tpl_vars['image_appicon']->value->image_path();?>
" width="50" alt="アプリ内ステータスバー表示用画像" class="mt_20 mb_20 con_img" style="background-color: <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->theme()->model()->category==1){?>#d82a1a<?php }elseif($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->theme()->model()->category==2){?>#f7e9c8<?php }elseif($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->theme()->model()->category==3){?>#FFFFFF<?php }elseif($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->theme()->model()->category==4){?>#003077<?php }?>" /></a>
<?php }else{ ?>
  <img src="<?php echo $_smarty_tpl->tpl_vars['image_appicon']->value->image_path();?>
" width="50" alt="アプリ内ステータスバー表示用画像" class="mb_20 con_img" style="background-color: <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->theme()->model()->category==1){?>#d82a1a<?php }elseif($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->theme()->model()->category==2){?>#f7e9c8<?php }elseif($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->theme()->model()->category==3){?>#FFFFFF<?php }elseif($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->theme()->model()->category==4){?>#003077<?php }?>" />
<?php }?>
</div>

<!-- アプリ名 -->
<div class="contents_box">
  <div class="contents_box_head lock">
    アプリストア(App Store/Google Play)表示項目
    <a href="/support/manual/store#store_1"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playに表示される<br>アプリ名とアプリの説明文が作成できます。<br>表示名はインストールしたアプリの名前です。<br>「編集する」ボタンを押して<br>表示したい内容を記入して下さい。"></a>
  </div>
  <h4></h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span><br />
  <span class="hisu">※簡易申請後、変更することが出来ません。</span><br />
  アプリストア(App Store/Google Play)での表示内容を設定してください（※全て登録必須の項目です）</p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/store/text/update" class="edit_btn">編集する</a>
<?php }?>
  <ul class="cel">
    <li><label>アプリ</label><span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->store()->app_name())===null||$tmp==='' ? "---" : $tmp);?>
</span></li>
    <li><label>表示名</label><span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->store()->app_disp_name())===null||$tmp==='' ? "---" : $tmp);?>
</span></li>
    <li><label>説明文</label><span><?php echo nl2br((($tmp = @$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->store()->description())===null||$tmp==='' ? "---" : $tmp));?>
</span></li>
  </ul>
</div>

<!-- スクリーンショット1 -->
<div class="contents_box">
  <div class="contents_box_head">
    スクリーンショット１
    <a href="/support/manual/store#store_2"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playに表示される<br>アプリのイメージ画像が登録できます。<br>ダウンロードしたいと気を引く画像を登録しましょう。"></a>
  </div>
  <h4>iPhone5/Android用（５枚まで）</h4>
  <!-- <p><span class="hisu">※簡易申請時に必須の項目です。</span><br /> -->
  <p><span class="hisu">※簡易申請後、変更することが出来ません。</span><br />
  最低、２枚は必要になっております。<br />
  推奨サイズ 640px x 1136px<br />
  登録されていない場合は、こちらで、<br />
  トップ、クーポン/イベント、メニューのスクリーンショットを、<br />
  1枚ずつご用意します。</p>

  <ul id="sortable-li">
<?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list_sshot1']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value){
$_smarty_tpl->tpl_vars['image']->_loop = true;
?>
    <li id="<?php echo $_smarty_tpl->tpl_vars['image']->value->position;?>
" class="sort_box"><img src="<?php echo $_smarty_tpl->tpl_vars['image']->value->image_path();?>
" width="160" height="284" alt="スクリーンショット２" class="d1"/>
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
      <div class="contenthover">
        <a href="/store/sshot/delete/<?php echo $_smarty_tpl->tpl_vars['image']->value->seq;?>
"><img src="/assets/img/common/del_icon.png" class="del_icon"></a>
        <a href="/store/sshot/update/<?php echo $_smarty_tpl->tpl_vars['image']->value->seq;?>
" class="edit_btn">変更</a>
      </div>
<?php }?>
    </li>
<?php } ?>
  </ul>
</div>

<!-- スクリーンショット2 -->
<div class="contents_box">
  <div class="contents_box_head">
    スクリーンショット２
    <a href="/support/manual/store#store_2"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playに表示される<br>アプリのイメージ画像が登録できます。<br>ダウンロードしたいと気を引く画像を登録しましょう。"></a>
  </div>
  <h4>iPhone 4S以前の機種用（５枚まで）</h4>
  <!-- <p><span class="hisu">※簡易申請時に必須の項目です。</span> -->
  <p><span class="hisu">※簡易申請後、変更することが出来ません。</span><br />
  最低、２枚は必要になっております。<br />
  推奨サイズ 640px x 960px<br />
  登録されていない場合は、こちらで、<br />
  トップ、クーポン/イベント、メニューのスクリーンショットを、<br />
  1枚ずつご用意します。</p>

  <ul id="sortable-li2">
<?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list_sshot2']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value){
$_smarty_tpl->tpl_vars['image']->_loop = true;
?>
    <li id="<?php echo $_smarty_tpl->tpl_vars['image']->value->position;?>
" class="sort_box"><img src="<?php echo $_smarty_tpl->tpl_vars['image']->value->image_path();?>
" width="160" height="240" alt="スクリーンショット２" class="d1"/>
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
      <div class="contenthover">
        <a href="/store/sshot2/delete/<?php echo $_smarty_tpl->tpl_vars['image']->value->seq;?>
"><img src="/assets/img/common/del_icon.png" class="del_icon"></a>
        <a href="/store/sshot2/update/<?php echo $_smarty_tpl->tpl_vars['image']->value->seq;?>
" class="edit_btn">変更</a>
      </div>
<?php }?>
    </li>
<?php } ?>
  </ul>

</div>

<!-- 起動画面用の画像 -->
<div class="contents_box">
  <div class="contents_box_head lock">
    起動画面用の画像
    <a href="/support/manual/store#store_4"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="アプリを起動した時に一番最初に表示される画像です。<br>「編集する」ボタンを押して、表示させたい画像を登録して下さい。"></a>
  </div>
  <h4>アプリ起動時の画像</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span><br />
  <span class="hisu">※簡易申請後、変更することが出来ません。</span><br />
  推奨サイズ 640px x 1136px</p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/store/splash/update" class="edit_btn">編集する</a>
<?php }?>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/store/splash/update"><img src="<?php echo $_smarty_tpl->tpl_vars['image_splash']->value->image_path();?>
" width="160" height="284" alt="起動画面用の画像" class="mb_20 con_img" /></a>
<?php }else{ ?>
  <img src="<?php echo $_smarty_tpl->tpl_vars['image_splash']->value->image_path();?>
" width="160" height="284" alt="起動画面用の画像" class="mb_20 con_img" />
<?php }?>
</div>

<!-- カテゴリー選択 -->
<div class="contents_box">
  <div class="contents_box_head lock">
    App Store / Google Playカテゴリー
    <a href="/support/manual/store#store_5"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playで<br>どのカテゴリーに区分するか選択します。「編集する」ボタンを押して<br>どのカテゴリーにするか選択して下さい。"></a>
  </div>
  <h4>カテゴリーの選択</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span>
  <span class="hisu">※簡易申請後、変更することが出来ません。</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/store/category/update" class="edit_btn">編集する</a>
<?php }?>
  <ul class="cel">
    <li><label>iPhone1</label><span><?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->store()->category_iphone1();?>
</span></li>
    <li><label>iPhone2</label><span><?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->store()->category_iphone2();?>
</span></li>
    <li><label>Android</label><span><?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->store()->category_android();?>
</span></li>
  </ul>
</div>


<!-- Copyright表記 -->
<div class="contents_box">
  <div class="contents_box_head lock">
    Copyright表記
    <a href="/support/manual/store#store_5"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playで<br>どのカテゴリーに区分するか選択します。「編集する」ボタンを押して<br>どのカテゴリーにするか選択して下さい。"></a>
  </div>
  <h4>Copyright表記を編集できます</h4>
  <p><span class="hisu">※未登録の場合は非表示となります</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/store/copyright/update" class="edit_btn">編集する</a>
<?php }?>
  <ul class="cel">
    <li><label>表記内容</label><span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->store()->copyright())===null||$tmp==='' ? "未登録" : $tmp);?>
</span></li>
  </ul>
</div>


<!-- TEL/MAIL -->
<div class="contents_box">
  <div class="contents_box_head lock">
    TEL/MAIL
    <a href="/support/manual/store#store_5"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playで<br>どのカテゴリーに区分するか選択します。「編集する」ボタンを押して<br>どのカテゴリーにするか選択して下さい。"></a>
  </div>
  <h4>アプリ内のスライドメニュー内のTEL/MAILボタンに埋め込む情報を編集できます</h4>
  <p><span class="hisu">※未登録の場合は非表示となります</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/store/info/update" class="edit_btn">編集する</a>
<?php }?>
  <ul class="cel">
    <li><label>TEL</label><span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->store()->tel())===null||$tmp==='' ? "未登録" : $tmp);?>
</span></li>
    <li><label>MAIL</label><span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->store()->email())===null||$tmp==='' ? "未登録" : $tmp);?>
</span></li>
  </ul>
</div>


<div class="last_margin"></div>


<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
<?php echo $_smarty_tpl->getSubTemplate ("common/footer_meta/store.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>