<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-28 16:07:12
         compiled from "/home/spost/admin/app/views/segments/text/message/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:130025399253a018aa2788d4-25827506%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9134d8896ffc7ebdbfeeff159dea66bdba47a8c6' => 
    array (
      0 => '/home/spost/admin/app/views/segments/text/message/update/index.tpl',
      1 => 1403906747,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '130025399253a018aa2788d4-25827506',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a018aa30e1e4_45732050',
  'variables' => 
  array (
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a018aa30e1e4_45732050')) {function content_53a018aa30e1e4_45732050($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/segments.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">Messageのテキスト編集
　<a href="/support/manual/coupon"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title=""></a>
  </div>
  <h4>Messageのテキスト編集</h4>

  <form action="/segments/text/message/update" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

  <div class="contents_form">
    <ul>
      <li>
        <label>タイトル</label>
        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('title_message')->build();?>

        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('title_message');?>

      </li>
      <li>
        <label>本文</label>
        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('body_message')->build();?>

        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('body_message');?>

      </li>
    </ul>
    <hr />
    <input type="submit" name="button" value="変更の確認" class="save_btn" />
    <a href="/segments" id="save_btn" class="back_btn">戻る</a>
  </div>

  </form>
</div>

<div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>