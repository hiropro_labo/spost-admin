<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-28 16:02:35
         compiled from "/home/spost/admin/app/views/segments/text/work/update/confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11524778953a01a2f6a2f11-41925185%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2b21ace2cc61aef163bf50cb3a25432d3f6d963f' => 
    array (
      0 => '/home/spost/admin/app/views/segments/text/work/update/confirm.tpl',
      1 => 1403906511,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11524778953a01a2f6a2f11-41925185',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a01a2f6fed91_69724914',
  'variables' => 
  array (
    'fieldset' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a01a2f6fed91_69724914')) {function content_53a01a2f6fed91_69724914($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/segments.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">Workのテキスト編集 </div>
  <h4>編集内容の確認</h4>

  <form action="/segments/text/work/update/exe" method="POST">

  <div class="contents_form">
    <ul>
      <li>
        <label>タイトル</label>
        <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('title_work');?>
</span>
      </li>
      <li>
        <label>本文</label>
        <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('body_work');?>
</span>
      </li>
    </ul>
    <hr />
    
    <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldset']->value->field(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value){
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>
    <?php echo $_smarty_tpl->tpl_vars['field']->value;?>

    <?php } ?>
    
    <input type="submit" name="button" value="変更の保存" class="save_btn" />
    <a href="javascript:history.back();" class="back_btn">戻る</a>
  </div>

  </form>
</div>

<div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>