<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-29 23:23:05
         compiled from "/home/spost/admin/app/views/segments/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:30596347053a01052c84118-80687650%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '48da8f28d3ef77058183980f19fc85943bac83ab' => 
    array (
      0 => '/home/spost/admin/app/views/segments/index.tpl',
      1 => 1404051783,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '30596347053a01052c84118-80687650',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a01052d3a5f9_14898345',
  'variables' => 
  array (
    'SPONSOR' => 0,
    'image_work' => 0,
    'segments' => 0,
    'image_policy' => 0,
    'image_message' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a01052d3a5f9_14898345')) {function content_53a01052d3a5f9_14898345($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/segments.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">Work 画像の変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="Work画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>Workの画像を変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/segments/img/work/update" class="edit_btn">変更する</a>
  <a href="/segments/img/work/update" title="変更"><img src="<?php echo $_smarty_tpl->tpl_vars['image_work']->value->image_path();?>
?<?php echo time();?>
" width="160" height="70" alt="事業内容Work画像" class="mb_20 con_img" /></a>
<?php }else{ ?>
  <img src="<?php echo $_smarty_tpl->tpl_vars['image_work']->value->image_path();?>
" width="160" height="70" alt="事業内容Work画像" class="mb_20 con_img" />
<?php }?>
</div>


<div class="contents_box">
  <div class="contents_box_head">Work テキストの変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="Work画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>Workのテキストを変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/segments/text/work/update" class="edit_btn">変更する</a>
<?php }?>

  <div class="contents_form">
    <ul>
      <li>
        <label for="name">タイトル</label>
        <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['segments']->value->title_work)===null||$tmp==='' ? "---" : $tmp);?>
</span>
      </li>
      <li>
        <label for="name">本文</label>
        <span><?php echo (($tmp = @nl2br($_smarty_tpl->tpl_vars['segments']->value->body_work))===null||$tmp==='' ? "---" : $tmp);?>
</span>
      </li>
    </ul>
  </div>
</div>


<div class="contents_box">
  <div class="contents_box_head">Policy 画像の変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="Policy画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>Policyの画像を変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/segments/img/policy/update" class="edit_btn">変更する</a>
  <a href="/segments/img/policy/update" title="変更"><img src="<?php echo $_smarty_tpl->tpl_vars['image_policy']->value->image_path();?>
?<?php echo time();?>
" width="160" height="70" alt="事業内容Policy画像" class="mb_20 con_img" /></a>
<?php }else{ ?>
  <img src="<?php echo $_smarty_tpl->tpl_vars['image_policy']->value->image_path();?>
" width="160" height="70" alt="事業内容Policy画像" class="mb_20 con_img" />
<?php }?>
</div>


<div class="contents_box">
  <div class="contents_box_head">Policy テキストの変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="Policy画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>Policyのテキストを変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/segments/text/policy/update" class="edit_btn">変更する</a>
<?php }?>

  <div class="contents_form">
    <ul>
      <li>
        <label for="name">タイトル</label>
        <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['segments']->value->title_policy)===null||$tmp==='' ? "---" : $tmp);?>
</span>
      </li>
      <li>
        <label for="name">本文</label>
        <span><?php echo (($tmp = @nl2br($_smarty_tpl->tpl_vars['segments']->value->body_policy))===null||$tmp==='' ? "---" : $tmp);?>
</span>
      </li>
    </ul>
  </div>
</div>


<div class="contents_box">
  <div class="contents_box_head">Message 画像の変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="Message画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>Messageの画像を変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/segments/img/message/update" class="edit_btn">変更する</a>
  <a href="/segments/img/message/update" title="変更"><img src="<?php echo $_smarty_tpl->tpl_vars['image_message']->value->image_path();?>
?<?php echo time();?>
" width="160" height="70" alt="事業内容Message画像" class="mb_20 con_img" /></a>
<?php }else{ ?>
  <img src="<?php echo $_smarty_tpl->tpl_vars['image_message']->value->image_path();?>
" width="160" height="70" alt="事業内容Message画像" class="mb_20 con_img" />
<?php }?>
</div>


<div class="contents_box">
  <div class="contents_box_head">Message テキストの変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="Message画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>Messageのテキストを変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/segments/text/message/update" class="edit_btn">変更する</a>
<?php }?>

  <div class="contents_form">
    <ul>
      <li>
        <label for="name">タイトル</label>
        <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['segments']->value->title_message)===null||$tmp==='' ? "---" : $tmp);?>
</span>
      </li>
      <li>
        <label for="name">本文</label>
        <span><?php echo (($tmp = @nl2br($_smarty_tpl->tpl_vars['segments']->value->body_message))===null||$tmp==='' ? "---" : $tmp);?>
</span>
      </li>
    </ul>
  </div>
</div>


<div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer_meta/company.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>