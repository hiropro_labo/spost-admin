<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-04 16:33:45
         compiled from "/home/spost/admin/app/views/recruit/info/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:46659226153a14e3af1e2a1-10931279%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0a6c7b2b90487c2fc152f3663e8474e6148d35f0' => 
    array (
      0 => '/home/spost/admin/app/views/recruit/info/index.tpl',
      1 => 1404459193,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '46659226153a14e3af1e2a1-10931279',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a14e3b075d28_08895805',
  'variables' => 
  array (
    'SPONSOR' => 0,
    'title' => 0,
    'list' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a14e3b075d28_08895805')) {function content_53a14e3b075d28_08895805($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/company.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box cel_recruit">
  <div class="contents_box_head">採用情報のタイトル編集
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="チームトップ画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>採用情報のタイトルを変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/recruit/title/update/<?php echo $_smarty_tpl->tpl_vars['title']->value->id;?>
" class="edit_btn">変更する</a>
<?php }?>

  <ul class="cel">
    <li><p class="fw_b di mr_20">タイトル</p><p class="di"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['title']->value->title)===null||$tmp==='' ? "---" : $tmp);?>
</p></li>
  </ul>
</div>
<!---->



<div class="contents_box cel_recruit">
  <div class="contents_box_head">表示項目一覧
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="表示項目の詳細が作成できます。"></a>
  </div>

  <h4>表示項目の登録・編集・削除ができます</h4>
  <p class="mb_20"></p>

  <?php if ($_smarty_tpl->tpl_vars['list']->value){?>
  <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
  <ul class="cel cel_recruit">
    <li class="clearfix">
      <div class="cel_recruit_wrap">
        <label><input type="text" id="title<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->title;?>
" class="w_120"></label>
        <p class="fl"><textarea id="body<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->body;?>
</textarea></p>
        <p class="fr"><input type="button" id="upd_<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" class="mr_3 ml_30 news_edit_btn" value="更新" onclick="update('<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
', this)">&emsp;<input type="button" id="del_<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" class="mr_3 news_edit_btn" value="削除" onclick="del('<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
', this)"></p>
        <p id="error_msg<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
"></p>
      </div>
    </li>
  </ul>
  <?php } ?>
  <?php }?>

  <ul class="cel cel_recruit">
      <li class="clearfix">
        <div class="cel_recruit_wrap">
          <label><input type="text" id="add_title" value="タイトル" class="w_120"></label>
          <p class="fl"><textarea id="add_body">本文をココに記載してください</textarea></p>
          <p class="di fr mr_13"><input type="button" value="追加" onclick="add('<?php echo $_smarty_tpl->tpl_vars['title']->value->id;?>
', this)" class="news_edit_btn"></p>
          <p id="error_msg_add"></p>
        </div>
    </li>
  </ul>

</div>


<div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer_meta/recruit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>