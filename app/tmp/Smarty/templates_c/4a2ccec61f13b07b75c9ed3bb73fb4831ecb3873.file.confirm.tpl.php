<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-07 14:10:28
         compiled from "/home/spost/admin/app/views/news/update/confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:713006590537b2f292670d4-81617336%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4a2ccec61f13b07b75c9ed3bb73fb4831ecb3873' => 
    array (
      0 => '/home/spost/admin/app/views/news/update/confirm.tpl',
      1 => 1404708953,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '713006590537b2f292670d4-81617336',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_537b2f29305b57_67445303',
  'variables' => 
  array (
    'news' => 0,
    'img_upload_flg' => 0,
    'SPONSOR' => 0,
    'fieldset' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537b2f29305b57_67445303')) {function content_537b2f29305b57_67445303($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/news.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- ニュースの編集 -->
<div class="contents_box">
  <div class="contents_box_head">ニュースの編集
  </div>
  <h4>ニュースの編集</h4>

  <form action="/news/update/exe/<?php echo $_smarty_tpl->tpl_vars['news']->value->id;?>
" method="POST">

<div class="contents_form">
  <ul class="cel">
    <li>
      <label>ニュース画像</label>
      <?php if ($_smarty_tpl->tpl_vars['img_upload_flg']->value){?>
      <img src="<?php echo $_smarty_tpl->tpl_vars['news']->value->tmp_image_path($_smarty_tpl->tpl_vars['SPONSOR']->value->id());?>
?<?php echo time();?>
" width="160" height="107" alt="ニュース画像" class="form_img con_img" />
      <?php }else{ ?>
      <img src="<?php echo $_smarty_tpl->tpl_vars['news']->value->image_path($_smarty_tpl->tpl_vars['SPONSOR']->value->id());?>
?<?php echo time();?>
" width="160" height="107" alt="ニュース画像" class="form_img con_img" />
      <?php }?>
    </li>

    <li>
      <label>タイトル</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('title');?>
</span>
    </li>

    <li>
      <label>本文</label>
      <span><?php echo nl2br($_smarty_tpl->tpl_vars['fieldset']->value->value('body'));?>
</span>
    </li>

    <li>
      <label>配信対象</label>
      <span><?php echo (($tmp = @smarty_modifier_replace($_smarty_tpl->tpl_vars['fieldset']->value->value('target'),'1','球団アプリユーザーへも配信する'))===null||$tmp==='' ? "球団アプリユーザーへは配信しない" : $tmp);?>
</span>
    </li>

    <li>
      <label>配信予約日時</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('reserved_at_year');?>
年<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('reserved_at_month');?>
月<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('reserved_at_day');?>
日&emsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('reserved_at_hour');?>
：<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('reserved_at_min');?>
</span>
    </li>
  </ul>

  <hr />

  
  <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldset']->value->field(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value){
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>
  <?php echo $_smarty_tpl->tpl_vars['field']->value;?>

  <?php } ?>
  
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>