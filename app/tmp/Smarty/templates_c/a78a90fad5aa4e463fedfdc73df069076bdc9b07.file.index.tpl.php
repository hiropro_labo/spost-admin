<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-19 18:13:53
         compiled from "/home/spost/admin/app/views/top/appoint/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15866588785379cb51d6f547-17830549%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a78a90fad5aa4e463fedfdc73df069076bdc9b07' => 
    array (
      0 => '/home/spost/admin/app/views/top/appoint/update/index.tpl',
      1 => 1398068781,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15866588785379cb51d6f547-17830549',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5379cb51db1dc7_32178651',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5379cb51db1dc7_32178651')) {function content_5379cb51db1dc7_32178651($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/appoint.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- 予約カレンダーの表示の変更 -->
<div class="contents_box">
  <div class="contents_box_head">予約カレンダーの表示</div>
  <h4>お店情報の画面から予約カレンダーを表示させることが出来ます。</h4>

<form action="/top/appoint/update" method="POST">
  <div class="contents_form ml_30">
    <ul>
      <li>
        <label for="form_appoint_0" class="ml_0"><input type="radio" required="required" id="form_appoint_0" name="appoint" value="0" checked="checked" />&nbsp;表示しない</label>
        <label for="form_appoint_1"><input type="radio" required="required" id="form_appoint_1" name="appoint" value="1" />&nbsp;表示する</label>
      </li>
      <li>
        <p class="desc ml_0">お店情報の画面に予約カレンダーへのボタンを表示させることが出来ます。</p>
        <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('appoint');?>
</p>
      </li>
    </ul>

  <hr class="ml_0" />

  <input type="submit" name="button" value="変更の確認" class="save_btn ml_0" />
  <a href="/top" id="save_btn" class="back_btn">戻る</a>
  </div>
</form>

</div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>