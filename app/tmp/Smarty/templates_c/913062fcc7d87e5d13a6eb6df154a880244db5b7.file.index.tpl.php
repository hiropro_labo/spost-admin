<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-07 15:21:23
         compiled from "/home/spost/admin/app/views/news/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12712509435359f1b3c427c8-17857715%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '913062fcc7d87e5d13a6eb6df154a880244db5b7' => 
    array (
      0 => '/home/spost/admin/app/views/news/index.tpl',
      1 => 1404712456,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12712509435359f1b3c427c8-17857715',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5359f1b3d1af33_78179266',
  'variables' => 
  array (
    'SPONSOR' => 0,
    'pager' => 0,
    'list' => 0,
    'news' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5359f1b3d1af33_78179266')) {function content_5359f1b3d1af33_78179266($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.date_format.php';
?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/news.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- ニュースの編集 -->
<div class="contents_box">
  <div class="contents_box_head">ニュースの編集
    <a href="support/manual/news" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="ニュースの新規作成ができます。<br>あなたのお店の最新情報を配信していきましょう！"></a>
  </div>
  <h4>ニュースの編集</h4>
  <p class="mb_10"><span class="hisu">※簡易申請時に必須の項目です。</span><br />最低、１つのニュースは配信準備が必要になっております。<br />
  PUSH通知機能は、アプリが公開状態になりましたらご利用できます。</p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/news/add" class="edit_btn mt_m35">ニュースの新規作成</a>
<?php }?>
</div>

<!-- ニュース一覧 -->
<div class="contents_box">
  <div class="contents_box_head">ニュース一覧
    <a href="support/manual/news#news_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="ニュースの編集・削除ができます。"></a>
  </div>
  <h4>ニュース一覧</h4>

  <div class="row-fluid mb_20">

    <?php echo $_smarty_tpl->tpl_vars['pager']->value;?>


    <ul id="news_table">

<?php if (!is_null($_smarty_tpl->tpl_vars['list']->value)){?>
<?php  $_smarty_tpl->tpl_vars['news'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['news']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['news']->key => $_smarty_tpl->tpl_vars['news']->value){
$_smarty_tpl->tpl_vars['news']->_loop = true;
?>
      <li class="news_table">
<?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
        <img src="<?php echo $_smarty_tpl->tpl_vars['news']->value->image_path();?>
?<?php echo time();?>
" width="160" height="107" class="mb_20 con_img" alt="ニュース画像" />
<?php }else{ ?>
        <a href="/news/update/<?php echo $_smarty_tpl->tpl_vars['news']->value->id;?>
" title="編集"><img src="<?php echo $_smarty_tpl->tpl_vars['news']->value->image_path();?>
?<?php echo time();?>
" width="160" height="107" class="mb_20 con_img" alt="ニュース画像" /></a>
<?php }?>

        <div class="news_text_box">
          <p id="table_text"><b><?php echo $_smarty_tpl->tpl_vars['news']->value->title;?>
</b><br /><br />
          配信予約日時：<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value->reserved_at,"%Y-%m-%d %H:%M:%S");?>
</p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
          <a href="/news/update/<?php echo $_smarty_tpl->tpl_vars['news']->value->id;?>
" class="mr_3 ml_30 news_edit_btn">編集</a>
          <a href="/news/del/<?php echo $_smarty_tpl->tpl_vars['news']->value->id;?>
" class="mr_3 news_edit_btn"><img src="/assets/img/common/icon/i11.png" width="18" id="i11"/></a>
<?php }?>
        </div>
        <div class="clear"></div>
      </li>

      <hr>
<?php } ?>
<?php }else{ ?>
      <li class="news_table">
      <p>現在、ニュースが登録されていません。</p>
      </li>
<?php }?>

    </ul>

<?php echo $_smarty_tpl->tpl_vars['pager']->value;?>


  </div>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>