<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-10 12:07:28
         compiled from "/home/spost/admin/app/views/branch/update/confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:61374156153abfdbd6ed6f1-19302147%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '66de0507f56fdfc3f5aa151420a6c6c5bd87afbd' => 
    array (
      0 => '/home/spost/admin/app/views/branch/update/confirm.tpl',
      1 => 1404961633,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '61374156153abfdbd6ed6f1-19302147',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53abfdbd7e0813_71457294',
  'variables' => 
  array (
    'b_id' => 0,
    'img_upload_flg' => 0,
    'SPONSOR' => 0,
    'branch' => 0,
    'fieldset' => 0,
    'selected_pref' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53abfdbd7e0813_71457294')) {function content_53abfdbd7e0813_71457294($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/branch.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- カテゴリーの変更 -->
<div class="contents_box">
  <div class="contents_box_head">支店の変更
  </div>

  <form action="/branch/update/exe/<?php echo $_smarty_tpl->tpl_vars['b_id']->value;?>
" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>&nbsp;</label>
      <?php if ($_smarty_tpl->tpl_vars['img_upload_flg']->value){?>
      <img src="<?php echo $_smarty_tpl->tpl_vars['branch']->value->tmp_image_path_by_id($_smarty_tpl->tpl_vars['SPONSOR']->value->id(),'branch');?>
?<?php echo time();?>
" width="160" height="88" alt="支店画像" class="form_img con_img" />
      <?php }else{ ?>
      <img src="<?php echo $_smarty_tpl->tpl_vars['branch']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="支店画像" class="form_img con_img" />
      <?php }?>
    </li>
  </ul>

  <ul>
    <li>
      <label for="name">お店の名前</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('shop_name');?>
</span>
    </li>

    <li>
      <label for="zip1">郵便番号</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('zip_code1');?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('zip_code2');?>
</span>
    </li>

    <li>
      <label for="pref">都道府県</label>
      <span><?php echo $_smarty_tpl->tpl_vars['selected_pref']->value;?>
</span>
    </li>

    <li>
      <label for="pref">市区町村</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('city');?>
</span>
    </li>

    <li>
      <label for="pref">住所（番地）</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('address_opt1');?>
</span>
    </li>

    <li>
      <label for="pref">住所（建物）</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('address_opt2');?>
</span>
    </li>

    <li>
      <label for="pref">電話番号</label><span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('tel1');?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('tel2');?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('tel3');?>
</span>
    </li>

    <li>
      <label for="pref">FAX</label><span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('fax1');?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('fax2');?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('fax3');?>
</span>
    </li>

    <li>
      <label for="pref">ホームページ</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('url');?>
</span>
    </li>

    <li>
      <label for="pref">オンラインショップ</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('online_shop');?>
</span>
    </li>

    <li>
      <label for="pref">メールアドレス</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('email');?>
</span>
    </li>

    <li>
      <label for="pref">営業時間</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('open_hours');?>
</span>
    </li>

    <li>
      <label for="pref">定休日</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('holiday');?>
</span>
    </li>

    <li>
      <label>表示設定</label>
      <span><?php echo smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['fieldset']->value->value('enable'),'0','非表示'),'1','表示');?>
</span>
    </li>

  </ul>

  <hr />

  
  <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldset']->value->field(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value){
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>
  <?php echo $_smarty_tpl->tpl_vars['field']->value;?>

  <?php } ?>
  
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>