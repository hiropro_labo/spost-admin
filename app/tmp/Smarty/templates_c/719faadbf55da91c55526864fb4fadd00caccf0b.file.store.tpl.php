<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-26 14:04:03
         compiled from "/home/spost/admin/app/views/preview/store.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7776675805382cadf3128e2-24667424%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '719faadbf55da91c55526864fb4fadd00caccf0b' => 
    array (
      0 => '/home/spost/admin/app/views/preview/store.tpl',
      1 => 1401080640,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7776675805382cadf3128e2-24667424',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5382cadf3b1374_52733519',
  'variables' => 
  array (
    'image_icon' => 0,
    'SPONSOR' => 0,
    'list_sshot1' => 0,
    'image' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5382cadf3b1374_52733519')) {function content_5382cadf3b1374_52733519($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("preview/common/header_store.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- ▼コンテンツ -->
<div id="contents_wrap">

<div>
<!-- アプリ情報 -->
<div id="app_info">
  <div class="clearfix">
    <div id="app_icon" class="fl_l">
      <img src="<?php echo $_smarty_tpl->tpl_vars['image_icon']->value->image_path();?>
?<?php echo time();?>
" width="100" height="100" />
    </div>

    <div id="app_text" class="fl_r">
      <div id="app_name">
        <h1><?php echo (($tmp = @$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->store()->app_name())===null||$tmp==='' ? "---" : $tmp);?>
</h1>
        <h2>HIROPRO Inc.</h2>
      </div>

      <div class="clearfix">
        <div id="app_rate" class="fl_l">
          ★★★☆☆
        </div>

        <div id="app_install" class="fl_r">
          <a href="javascript:void(0);" class="store_btn">無料</a>
        </div>
      </div>
    </div>
  </div>

  <div id="app_menu">
    <ul class="clearfix">
      <li class="current"><a href="javascript:void(0);">詳細</a></li>
      <li><a href="javascript:void(0);">レビュー</a></li>
      <li><a href="javascript:void(0);">関連</a></li>
    </ul>
  </div>
</div>
<!--  -->

<hr />



<ul id="list">
  <!-- スクリーンショット -->
  <li id="list_img">
    <div>
<?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list_sshot1']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value){
$_smarty_tpl->tpl_vars['image']->_loop = true;
?>
      <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value->image_path();?>
?<?php echo time();?>
" width="140" height="248" />
<?php } ?>
    </div>
  </li>
  <!--  -->

  <hr class="list_border" />

  <!-- 説明 -->
  <li>
    <h4>説明</h4>
    <p><?php echo nl2br((($tmp = @$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->store()->description())===null||$tmp==='' ? "---" : $tmp));?>
</p>
  </li>
  <!--  -->

  <hr class="list_border" />

  <!-- 情報 -->
  <li id="list_info">
    <h4>情報</h4>
    <ul class="clearfix">
      <li><label>販売元</label><span>HIROPRO Inc.</span></li>
      <li><label>カテゴリ</label><span><?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->store()->category_iphone1();?>
</span></li>
      <li><label>バージョン</label><span>1.4</span></li>
      <li><label>サイズ</label><span>28.0MB</span></li>
      <li><label>レート</label><span>4+</span></li>
      <li><label>互換性</label><span>iPhone 4S、iPhone、iPod touch (第4世代)、iPod touch (第5世代)、およびiPad に対応。 iOS 6.1 以降が必要</span></li>
    </ul>
  </li>
  <!--  -->

  <hr class="list_border" />

  <!-- バージョン履歴 -->
  <li class="list_link">
    <h4>バージョン履歴</h4>
  </li>
  <!--  -->

  <hr class="list_border" />

  <!-- デベロッパApp -->
  <li class="list_link">
    <h4>デベロッパApp</h4>
  </li>
  <!--  -->

  <hr class="list_border" />

  <!-- デベロッパWebサイト -->
  <li class="list_link">
    <h4>デベロッパWebサイト</h4>
  </li>
  <!--  -->

  <hr class="list_border" />

  <!-- プライバシーポリシー -->
  <li class="list_link">
    <h4>プライバシーポリシー</h4>
  </li>
  <!--  -->

  <hr class="list_border" />

</ul>
</div>

</div>
<!-- ▲ -->


<?php echo $_smarty_tpl->getSubTemplate ("preview/common/footer_store.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>