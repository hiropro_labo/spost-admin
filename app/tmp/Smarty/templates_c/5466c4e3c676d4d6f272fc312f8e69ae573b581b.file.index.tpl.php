<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-26 19:02:44
         compiled from "/home/spost/admin/app/views/category/setup/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1816334957539a906b54fff1-70809321%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5466c4e3c676d4d6f272fc312f8e69ae573b581b' => 
    array (
      0 => '/home/spost/admin/app/views/category/setup/index.tpl',
      1 => 1403776956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1816334957539a906b54fff1-70809321',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539a906b59ac98_20517964',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539a906b59ac98_20517964')) {function content_539a906b59ac98_20517964($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header_root_new.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!--コンテンツ-->
<div id="contents" class="no_preview option">

  <!-- コンテンツヘッダ -->
  <div id="title_wrap">
    <div id="title_bar" class="top_color"></div>
    <div id="title_box">
      <h2>カテゴリー選択</h2>
      <p id="title_sub">アプリのカテゴリーを選択して下さい。</p>
    </div>
  </div>
  <br class="clear" />
  <!--/コンテンツヘッダー-->

  <!--カテゴリー選択-->
  <div class="category_choose mt_10 clearfix">
      <div class="item">
        <label>
        <div id="cate1" class="restaurant_off"></div>
        <h4>飲食</h4>
        <p class="description">店舗情報、メニュー</p>
        <input name="category" type="radio" value="1" checked id="radio2"><label for="radio2">
        </label>
      </div>
      <div class="item">
        <label>
        <div id="cate2" class="goods_off"></div>
        <h4>物販</h4>
        <p class="description">店舗情報、商品一覧</p>
        <input name="category" type="radio" value="2" id="radio3"><label for="radio3">
        </label>
      </div>
      <div class="item">
        <label>
        <div id="cate3" class="company_off"></div>
        <h4>企業</h4>
        <p class="description">会社概要、事業内容</p>
        <input name="category" type="radio" value="3" id="radio1"><label for="radio1">
        </label>
      </div>
  </div>

  <div class="ta_c lh_18">
    <p>選択したカテゴリーによって、アプリのページ内容が変わります。</p>
    <p class="c_gra mb_20 fs_11">※一度設定したカテゴリーは、以後選びなおすことができません。</p>
    <a href="#" class="blue_btn" onclick="smb();">カテゴリーを決定する</a>
  </div>
  <!--/カテゴリー選択-->
</div>


<script type="text/javascript">
function smb(){
	var category = $('input[type="radio"]:checked').val();
    window.location.href = '/category/setup/exe/' + category;
}
</script>



<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>