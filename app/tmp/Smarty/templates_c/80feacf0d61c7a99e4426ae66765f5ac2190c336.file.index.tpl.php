<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-27 10:38:18
         compiled from "/home/spost/admin/app/views/switcher/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1891869481535a24d0cb4285-61875680%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '80feacf0d61c7a99e4426ae66765f5ac2190c336' => 
    array (
      0 => '/home/spost/admin/app/views/switcher/index.tpl',
      1 => 1403833096,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1891869481535a24d0cb4285-61875680',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_535a24d0d1d344_88123800',
  'variables' => 
  array (
    'category' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535a24d0d1d344_88123800')) {function content_535a24d0d1d344_88123800($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header_root_new.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<!--コンテンツ-->
<div id="contents" class="no_preview option">

  <!-- コンテンツヘッダ -->
  <div id="title_wrap">
    <div id="title_bar" class="top_color"></div>
    <div id="title_box">
      <h2>オプション設定</h2>
      <p id="title_sub">アプリのカテゴリーやオプションページを選択することができます。</p>
    </div>
  </div>
  <br class="clear" />
  <!--/コンテンツヘッダー-->

  <!--カテゴリー選択-->
  <div class="category_choose mt_10">
    <h3>選択カテゴリー</h3>
    <p class="text_sub">選択したカテゴリーが表示されます。</p>
    <?php if ($_smarty_tpl->tpl_vars['category']->value==1){?>
      <div class="box clearfix">
        <div class="restaurant_on fl mr_20"></div>
        <div class="box_fr">
          <h4>飲食</h4>
          <p class="description">店舗情報、メニュー</p>
        </div>
      </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['category']->value==2){?>
      <div class="box clearfix">
        <div class="goods_on fl mr_20"></div>
        <div class="box_fr">
          <h4>物販</h4>
          <p class="description">店舗情報、商品一覧</p>
        </div>
      </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['category']->value==3){?>
      <div class="box clearfix">
        <div class="company_on fl mr_20"></div>
        <div class="box_fr">
          <h4>企業</h4>
          <p class="description">会社概要、事業内容</p>
        </div>
      </div>
    <?php }?>
  </div>
  <br class="clear" />
  <hr />
  <!--/カテゴリー選択-->

  <!--オプション選択-->
  <?php echo $_smarty_tpl->getSubTemplate ("switcher/options/category_".((string)$_smarty_tpl->tpl_vars['category']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

  <!--/オプション選択-->

</div>


<script type="text/javascript">
$(function(){
	$('input').on('ifChanged', function(event){
		var ctl = $('#' + event.currentTarget.id).val();
		var url = '/switcher/update/' + ctl;
		$.get(url, null, function(data, status){
			alert('オプション設定を更新しました');
		});
	});
});
</script>




<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>