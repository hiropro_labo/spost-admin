<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-21 16:15:12
         compiled from "/home/spost/admin/app/views/common/footer_meta/profile.tpl" */ ?>
<?php /*%%SmartyHeaderCode:191407917053a53100544b99-05057760%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9eea066c4c3e535adc4c8b5f9d7309ae5ae451b0' => 
    array (
      0 => '/home/spost/admin/app/views/common/footer_meta/profile.tpl',
      1 => 1403301872,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '191407917053a53100544b99-05057760',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a53100576c99_80838362',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a53100576c99_80838362')) {function content_53a53100576c99_80838362($_smarty_tpl) {?>
<script type="text/javascript">
function update(idx, obj){
    var button = $(obj);
    button.attr("disabled", true);

    var data = {
        title: $("#title" + idx).val(),
        body: $("#body" + idx).val()
    };

    $.ajax({
    	type: "post",
    	url: "/profile/history/update/" + idx,
    	contentType: "application/json",
    	dataType: 'json',
    	data: JSON.stringify(data),
    	success: function(res){
    		if (!res[0]){
    			$("#error_msg" + idx).empty().append("更新処理に失敗しました<br />" + res[1]);
    		}else{
    		    location.reload();
    		}
    	},
        error: function(){
        	$("#error_msg" + idx).empty().append("更新処理に失敗しました[通信エラー発生]");
        },
        complete: function(){
        	button.attr("disabled", false);
        }
    });
}

function add(obj){
	var button = $(obj);
	button.attr("disabled", true);

	var data = {
		title: $("#add_title").val(),
	    body: $("#add_body").val()
	};

	$.ajax({
		type: "post",
		url: "/profile/history/add",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(data),
		success: function(res){
			if (!res[0]){
				$("#error_msg_add").empty().append("更新処理に失敗しました<br />" + res[1]);
			}else{
				location.reload();
			}
		},
		error: function(){
			$("#error_msg_add").empty().append("更新処理に失敗しました[通信エラー発生]");
		},
		complete: function(){
			button.attr("disabled", false);
		}
	});
}

function del(idx, obj){
    if (!confirm("指定された項目を削除します\nよろしいですか？")) return false;

    var button = $(obj);
	button.attr("disabled", true);

    $.ajax({
    	type: "post",
    	url: "/profile/history/del/" + idx,
    	success: function(res){
    		if (!res[0]){
    			$("#error_msg" + idx).empty().append("削除処理に失敗しました<br />" + res[1]);
    		}else{
    			location.reload();
    		}
    	},
        error: function(){
        	$("#error_msg" + idx).empty().append("更新処理に失敗しました[通信エラー発生]");
        },
        complete: function(){
        	button.attr("disabled", false);
        }
    });
}
</script>
<?php }} ?>