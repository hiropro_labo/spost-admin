<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-05 20:01:39
         compiled from "/home/spost/admin/app/views/common/header_inspect.tpl" */ ?>
<?php /*%%SmartyHeaderCode:175909037653b7db138b4711-85576081%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4dc5683d360f369f3a75b27ba850eb0fd64efaa9' => 
    array (
      0 => '/home/spost/admin/app/views/common/header_inspect.tpl',
      1 => 1404525668,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '175909037653b7db138b4711-85576081',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53b7db138d1099_12826105',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53b7db138d1099_12826105')) {function content_53b7db138d1099_12826105($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<title><?php echo __('site.title');?>
</title>

<!-- Favicon Icon -->
<link href="/assets/img/common/favicon/ipost.png" rel="shortcut icon" type="image/png" />

<link rel="stylesheet" type="text/css" href="/assets/css/top/bootstrap.tutorial.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/root.css" media="all" />
<link rel="stylesheet" type="text/css" href="/assets/css/top/bass.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/sub.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/tooltipster.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/spost.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/spost_a.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/blue.css">


<?php echo $_smarty_tpl->getSubTemplate ("common/load_css.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<script src="/assets/js/jquery-1.8.0.min.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery-ui.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/bootstrap.min.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.contenthover.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.dropdown.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.tooltipster.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.cookie.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.carouFredSel-6.2.1-packed.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/modal/popup.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/modal/tutorial.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/icheck.js" type="text/javascript" language="javascript"></script>

<script>
    $(function(){
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '10%', // optional
            cursor: true // optional
        });
    });
</script>



<?php echo $_smarty_tpl->getSubTemplate ("common/load_js.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



</head>
<body>


<?php echo $_smarty_tpl->getSubTemplate ("common/content_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/pan/index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>