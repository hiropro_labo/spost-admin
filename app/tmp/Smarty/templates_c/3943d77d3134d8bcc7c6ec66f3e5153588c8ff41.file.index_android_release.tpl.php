<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-29 22:39:54
         compiled from "/home/spost/admin/app/views/inspect/index_android_release.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6322712553b016c6c1ff57-75393340%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3943d77d3134d8bcc7c6ec66f3e5153588c8ff41' => 
    array (
      0 => '/home/spost/admin/app/views/inspect/index_android_release.tpl',
      1 => 1404049193,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6322712553b016c6c1ff57-75393340',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53b016c6c6d635_73315026',
  'variables' => 
  array (
    'SPONSOR' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53b016c6c6d635_73315026')) {function content_53b016c6c6d635_73315026($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/inspect.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">アプリ審査申請
    <a href="/support/help" target="_blank"><img src="/assets/img/common/help_tips.png" title=""></a>
  </div>
  <h4 style="color:#900;">お客様のAndroidアプリが公開されました!!</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">おめでとうございます!!<br />
        お客様の制作されたAndroidアプリがGoogle Playにて公開されました。</span>
      </li>
      <li>
        <span class="ml_30">Google Play アプリインストールURL：<?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->app()->store_url('google');?>
</span>
      </li>
      <li>
        <span class="ml_30">また、iOSアプリのApple審査につきましても順次申請手続きを進めてまいります。<br />
        審査終了までの日数につきましては申請完了から最低10営業日程度のお時間が掛かりますことをご了承くださいませ。<br />
        審査結果はメールにて改めてお知らせ致します。</span>
      </li>
    </ul>
  </div>

  <div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>