<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-10 12:05:42
         compiled from "/home/spost/admin/app/views/top/shop/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6904259555379c438501862-45532333%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ddedc62535223db87980b1a8272909329e54ccdd' => 
    array (
      0 => '/home/spost/admin/app/views/top/shop/update/index.tpl',
      1 => 1404961521,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6904259555379c438501862-45532333',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5379c4386248b9_98373990',
  'variables' => 
  array (
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5379c4386248b9_98373990')) {function content_5379c4386248b9_98373990($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/shop.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- お店の情報の変更 -->
<div class="contents_box">
  <div class="contents_box_head">お店の情報・写真の変更<a href="/support/manual/top#top_4"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="あなたのお店の情報を変更する事ができます。<br>フォームに入力した後、「確認する」ボタンを押してください。"></a></div>
  <h4>お店の情報</h4>

  <form action="/top/shop/update" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label for="name">お店の名前</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('shop_name')->build();?>
<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('shop_name');?>

    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="zip1">郵便番号</label>
    <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('zip_code1')->build();?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('zip_code2')->build();?>

    </li>

    <li>
      <label for="pref">都道府県</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('pref')->build();?>

    </li>

    <li>
      <label for="pref">市区町村</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('city')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('city');?>

    </li>

    <li>
      <label for="pref">住所（番地）</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('address_opt1')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('address_opt1');?>

    </li>

    <li>
      <label for="pref">住所（建物）</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('address_opt2')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('address_opt2');?>

    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">電話番号</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('tel1')->build();?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('tel2')->build();?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('tel3')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('tel1');?>

    </li>

    <li>
      <label for="pref">FAX</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('fax1')->build();?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('fax2')->build();?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('fax3')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('fax1');?>

    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">ホームページ</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('url')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('url');?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('url');?>

    </li>
  </ul>

  <ul>
    <li>
      <label for="pref">オンラインショップ</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('online_shop')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('online_shop');?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('online_shop');?>

    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">メールアドレス</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('email')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('email');?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('email');?>

    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">営業時間</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('open_hours')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('open_hours');?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('open_hours');?>

    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">定休日</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('holiday')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('holiday');?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('holiday');?>

    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/shop" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>