<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-23 15:09:54
         compiled from "/home/spost/admin/app/views/url/update/confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1692942207539ea80e557d19-57693977%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cca92e7352162c42dc408305a213807f240ab935' => 
    array (
      0 => '/home/spost/admin/app/views/url/update/confirm.tpl',
      1 => 1403470480,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1692942207539ea80e557d19-57693977',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539ea80e5e7572_31961469',
  'variables' => 
  array (
    'fieldset' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539ea80e5e7572_31961469')) {function content_539ea80e5e7572_31961469($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/url.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">URL</div>
  <h4>スライドメニューに表示されるURLとメニュータイトルを編集できます</h4>

  <form action="/url/update/exe" method="POST">
    <div class="contents_form">
      <ul>
        <li>
          <label><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->label('url_title1');?>
</label>
          <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('url_title1'))===null||$tmp==='' ? "---" : $tmp);?>
</span>
        </li>
        <li>
          <label><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->label('url1');?>
</label>
          <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('url1'))===null||$tmp==='' ? "---" : $tmp);?>
</span>
        </li>
        <li>
          <label><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->label('url_title2');?>
</label>
          <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('url_title2'))===null||$tmp==='' ? "---" : $tmp);?>
</span>
        </li>
        <li>
          <label><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->label('url2');?>
</label>
          <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('url2'))===null||$tmp==='' ? "---" : $tmp);?>
</span>
        </li>
        <li>
          <label><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->label('url_title3');?>
</label>
          <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('url_title3'))===null||$tmp==='' ? "---" : $tmp);?>
</span>
        </li>
        <li>
          <label><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->label('url3');?>
</label>
          <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('url3'))===null||$tmp==='' ? "---" : $tmp);?>
</span>
        </li>
      </ul>
      <hr />
      <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldset']->value->field(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value){
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>
      <?php echo $_smarty_tpl->tpl_vars['field']->value;?>

      <?php } ?>
      <input type="submit" name="button" value="変更の保存" class="save_btn" />
      <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
    </div>
  </form>
</div>

<div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>