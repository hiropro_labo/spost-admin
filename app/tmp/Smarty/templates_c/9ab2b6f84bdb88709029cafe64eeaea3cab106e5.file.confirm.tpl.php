<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-28 16:07:18
         compiled from "/home/spost/admin/app/views/segments/text/message/update/confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:102376427153a018c1ab3ac0-22686506%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9ab2b6f84bdb88709029cafe64eeaea3cab106e5' => 
    array (
      0 => '/home/spost/admin/app/views/segments/text/message/update/confirm.tpl',
      1 => 1403906730,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '102376427153a018c1ab3ac0-22686506',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a018c1b18a88_32222650',
  'variables' => 
  array (
    'fieldset' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a018c1b18a88_32222650')) {function content_53a018c1b18a88_32222650($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/segments.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">Messageのテキスト編集 </div>
  <h4>編集内容の確認</h4>

  <form action="/segments/text/message/update/exe" method="POST">

  <div class="contents_form">
    <ul>
      <li>
        <label>タイトル</label>
        <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('title_message');?>
</span>
      </li>
      <li>
        <label>本文</label>
        <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('body_message');?>
</span>
      </li>
    </ul>
    <hr />
    
    <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldset']->value->field(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value){
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>
    <?php echo $_smarty_tpl->tpl_vars['field']->value;?>

    <?php } ?>
    
    <input type="submit" name="button" value="変更の保存" class="save_btn" />
    <a href="javascript:history.back();" class="back_btn">戻る</a>
  </div>

  </form>
</div>

<div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>