<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-21 19:47:55
         compiled from "/home/spost/admin/app/views/banner/del/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:77462064053a562ae05f536-22833159%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd996abec0f692e23c8ece7478eefa0aa41c32389' => 
    array (
      0 => '/home/spost/admin/app/views/banner/del/index.tpl',
      1 => 1403315271,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '77462064053a562ae05f536-22833159',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a562ae0c8954_89531544',
  'variables' => 
  array (
    'model' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a562ae0c8954_89531544')) {function content_53a562ae0c8954_89531544($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/banner.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">バナーの削除
    <a href="/support/manual/menu#menu_2" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="登録したバナーを削除できます。<br>「削除」ボタンを押して、削除して下さい。"></a>
  </div>
  <h4>バナーの削除</h4>

  <form action="/banner/del/exe/<?php echo $_smarty_tpl->tpl_vars['model']->value->id;?>
" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>アップロード画像</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['model']->value->image_path();?>
?<?php echo time();?>
" width="320" height="71" alt="バナー画像" class="form_img con_img" />
    </li>

    <li>
      <label>URL</label>
      <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('url'))===null||$tmp==='' ? "-----" : $tmp);?>
</span>
    </li>

    <li>
      <label>表示設定</label>
      <span><?php echo smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['fieldset']->value->value('enable'),'0','非表示'),'1','表示');?>
</span>
    </li>
  </ul>

  <hr />

   <input type="submit" name="button" value="削除" class="save_btn" />
 <a href="/profile" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>