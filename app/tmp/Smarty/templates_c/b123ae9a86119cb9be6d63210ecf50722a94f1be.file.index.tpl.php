<?php /* Smarty version Smarty-3.1-DEV, created on 2014-04-22 18:10:17
         compiled from "/home/spost/admin/app/views/support/policy/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1130235717535631f9992500-19464870%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b123ae9a86119cb9be6d63210ecf50722a94f1be' => 
    array (
      0 => '/home/spost/admin/app/views/support/policy/index.tpl',
      1 => 1398068608,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1130235717535631f9992500-19464870',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_535631f9a04963_33422676',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535631f9a04963_33422676')) {function content_535631f9a04963_33422676($_smarty_tpl) {?><!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>iPost利用規約</title>

<!-- Favicon Icon -->
<link href="/assets/img/common/favicon/ipost.png" rel="shortcut icon" type="image/png" />

<style type="text/css">
body {
font-size: 13px;
line-height: 1.5;
}
#main_box {
  max-width: 640px;
  margin: 0px auto 30px;
  width: 100%;
}
.box {
  margin: 20px 0 0;
  border: 1px;
}
.box p {
  font-size: 12px;
  color: #666;
  margin: 5px;
  line-height: 1.5;
  margin-top: 20px;
}
.box h3 {
  margin-bottom: 23px;
  margin-top: -7px;
  border-bottom: solid 2px #0D8DBB;
  padding-bottom: 7px;
  color: #4b4b4b;
  font-size: 13px;
}
.privacy table {
  width: 598px;
  margin: 20px;
}
.privacy table .table_l {
  width: 150px;
}
.privacy table th, .privacy table td {
  width: auto;
  height: auto;
  padding: 5px;
  color: rgb(102, 102, 102);
  vertical-align: baseline;
  text-align: left;
  border: solid 1px rgb(102, 102, 102);
}
.privacy table th {
  color: rgb(102, 102, 102);
  background-color: rgb(222, 222, 222);
}
table {
border-collapse: collapse;
border-spacing: 0;
}
.win input[type="text"], .win input[type="password"], .win textarea, .win select {
  font-size: 14px;
  color: #333;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  border-radius: 4px;
  background-color: #f3f1f1;
  box-shadow: inset 1px 1px 1px hsla(0,0%,0%,0.35), 0 1px 0 hsla(0,0%,100%,0.55);
  border-radius: 4px;
  border: 1px solid #ccc;
}
.mac input[type="text"], .mac input[type="password"], .mac textarea, .mac select {
  font-size: 14px;
  color: #333;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  border-radius: 4px;
  background-color: #f3f1f1;
  box-shadow: inset 2px 2px 1px hsla(0,0%,0%,0.35), 0 1px 0 hsla(0,0%,100%,0.55);
  border-radius: 4px;
  border: 1px solid #ccc;
}
.help_view_wrap, .form_wrap {
  clear: both;
  float: none;
  margin: 0 auto;
  margin-top: 15px;
  width: 93.5%;
  display: block;
  border: 1px solid #EBEBEB;
  background: #FFF;
  box-shadow: 0 1px 1px 0 rgba(0,0,0,0.1), inset 0px 1px 1px 0px #ffffff;
  -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,0.1), inset 0px 1px 1px 0px #ffffff;
  -moz-box-shadow: 0 1px 1px 0 rgba(0,0,0,0.1), inset 0px 1px 1px 0px #ffffff;
  border-radius: 3px;
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  overflow: hidden;
  padding-top: 10px;
  padding-bottom: 17px;
}
.help_view_wrap {
  padding-left: 3%;
  padding-right: 3%;
}
.nav {
  overflow: hidden;
  border: solid 1px #ccc;
  border-radius: 3px;
  box-shadow: 2px 2px 4px rgba(0,0,0,0.1), inset 0px 1px 1px 0px #ffffff;
  margin-top: 14px;
}
.nav li {
  border-top: solid 1px #cacaca;
  margin-top: -1px;
}
.nav li a {
  display: block;
  width: 100%;
  height: 100%;
  padding: 10px;
  text-decoration: none;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  background-color: #fff;
  background: url(../../../../images/user/arrow_basic_s.png), -webkit-gradient(linear, left top, left bottom, from(#fff), to(#ebebeb));
  background-repeat: no-repeat;
  background-position: 94% center;
  padding-left: 20px;
  color: #4b4b4b;
  padding-right: 30px;
}
/* yoshitaka 2013-04-16
Select Style */
.nav li.dis a {
  color: #fccf49;
  background: url(../../../../images/user/arrow_basic_s.png), -webkit-gradient(linear, left top, left bottom, from(#393838), to(#1f1e1e));
  background-repeat: no-repeat;
  background-position: 94% center;
}
/* Select Style END */

.nav li a:hover {
  opacity: 0.8;
  filter: alpha(opacity=80);
  -ms-filter: “alpha( opacity=80 )”;
}
.nav ul {
  padding: 0px;
  margin-top: 0;
  margin-bottom: 0;
}
/*
右カラム
*/
#main_box_r {
  width: 94%;
  margin-top: 0px;
  display: block;
  margin: 0 auto;
  background: #fff;
}
.Qes, .Ans {
  font-size: 13px;
  color: #4b4b4b;
  line-height: 1.5;
}
.Qes {
  font-weight: bold;
  margin-top: 5px;
  margin-bottom: 10px;
}
.Ans {
  padding: 20px 10px;
  background: #f0f0f0;
}
.help_view_wrap hr {
  height: 0;
  border: 0;
  margin: 0 auto;
  border-top: 1px solid #D1D1D1;
  border-bottom: 1px solid #fff;
  width: 100%;
  margin-top: 5px;
  margin-bottom: 5px;
}
/*
コンタクト
*/

#left_contact_box {
  margin-right: 44px;
  margin-bottom: 20px;
}
/*
ヘルプ
*/
#topicpath {
  margin: 10px 0;
  padding-right: 0px;
  padding-left: 3px;
  color: #4b4b4b;
}
#topicpath li {
  display: inline;
  line-height: 110%;
  list-style-type: none;
}
#topicpath li a {
  color: #900;
  padding-right: 10px;
  background: url(../../../images/index/topicpath_icon.gif) no-repeat right center;
  text-decoration: none;
}
#topicpath a:hover {
  text-decoration: underline;
}
#topicpath a:visited {
  text-decoration: none;
}
#main_box_r a {
  color: #900;
  float: right;
  margin-top: 10px;
  text-decoration: none;
}
#main_box_r a:hover {
  text-decoration: underline;
}
#main_box_r a:visited {
  text-decoration: none;
}
.main_box_l {
  margin-bottom: 40px;
}

/*iPhone横*/
@media screen and (min-width: 481px) {
/*
ヘルプ
*/
.main_box_l {
  float: left;
  max-width: 320px;
  width: 32%;
}
#main_box_r {
  width: 66%;
  margin-top: 0px;
  float: right;
  max-width: 640px;
  display: block;
  margin: 0 auto;
  background: #fff;
}
.help_view_wrap {
  padding-left: 20px;
  padding-right: 20px;
}
.box h3 {
  margin-bottom: 3px;
  padding: 3px;
}
}
/*iPad*/
@media screen and (min-width: 768px) {
}
/*PC*/
@media screen and (min-width: 1024px) {
.box h3 {
  margin-bottom: 23px;
  margin-top: -7px;
  border-bottom: solid 2px #0D8DBB;
  padding-bottom: 7px;
  color: #4b4b4b;
}
}
/* yoshitaka 2013-04-29
Privacy Style */
.privacy.box {
  margin: 20px 0 0;
  border: solid 1px #aaa;
  box-shadow: 0 1px 1px 0 rgba(0,0,0,0.1), inset 0px 1px 1px 0px #ffffff;
  -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,0.1), inset 0px 1px 1px 0px #ffffff;
  -moz-box-shadow: 0 1px 1px 0 rgba(0,0,0,0.1), inset 0px 1px 1px 0px #ffffff;
  border-radius: 3px;
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
}
.privacy p {
  margin: 20px;
  background: #FFF;
}
.support_window_r {
  float: right;
  width: 16em;
}
.support_window {
  clear: both;
  margin: 10px;
}

/*
tos
*/

.box p span{
font-weight:bold;
}
.box p a {
color: #008cb4;
text-decoration: none;
}
.box p a:hover {
color: #005580;
text-decoration: underline;
}
.ta_r{
text-align: right;
}

</style>
</head>


<body>
<!-- コンテンツ -->
<div id="main_box">
  <div class="box"><!-- コメント左側に挿絵を入れるためのdiv -->
    <h3>iPost利用規約</h3>
  </div>
  <div class="box privacy">
    <p><span>第一条　利用規約</span><br>
iPost利用規約（以下、「本規約」という）は、株式会社ヒロ企画（以下「当社」という）が提供するスマートフォン用アプリ作成・管理ツール（以下「本サービス」という）は本規約を承認したうえで本サービスを利用するものとします。</p>
    <p><span>第二条　ユーザーへの通知</span><br>
１．当社からユーザーへの通知は、特別な定めのない限り、通知内容を電子メール、書面又は当社のウェブサイトに掲載するなど、当社が適当と判断する方法により行います。

２．前項の規定に基づき、当社からユーザーへの通知を電子メールの送信又は当社のウェブサイトへの掲載がなされた時点から効力を生じるものとします。</p>
    <p><span>第三条　規約の変更</span><br>
１．当社はユーザーの了承を得ることなく本規約を変更することがあります。原則として変更日の１週間以上前に変更内容を本サービスのウェブサイト内に掲載することによりユーザーへ通知します。緊急を要する場合は予告なく変更することがありますが、その場合はウェブサイト内にて事後掲載することによりユーザーへ通知します。ユーザーが閲覧可能となった時点で変更の効力が生じ、変更の効力発生後もユーザーが本サービスの利用を継続する場合、変更後の本規約を承認したものとみなします。
２．変更後の規約は、前条の規定によりユーザーへ通知するものとし、ユーザーからの異議申し立てがない限り通知日をもってユーザーが同通知の内容に同意したとみなします。 </p>
    <p><span>第四条　本サービスの申込方法</span><br>
１．本サービスの申込者は、代理店のウェブサイトより申込みを行うものとします。
２．代理店のウェブサイトから申し込む場合には、ウェブサイト上の申込フォームのすべての項目を漏れなく入力したうえ、画面に表示される手順に従って送信の操作を行ってください。</p>
    <p><span>第五条　ユーザー登録</span><br>
１．当社は、前条に定める方法にて利用申込みを受付け、必要な審査・手順等を経た後に利用を承認します。
２．当社は、利用申込者が以下のいずれかに該当する場合、その者の利用を承認いたしません。
（1） 利用申込者が実在しない場合
（2） 利用申込の際の申告事項に、虚偽の記載、誤記入または記入漏れがあった場合
（3） 利用申込者が過去に本規約の違反を行っている場合
（4） その他当社が不適切と判断した場合
３．ユーザーは、当社への届出内容に変更があった場合、速やかに変更の届出を行うものとします。届出を行わなかったことによりユーザーが不利益を被った場合、当社は一切その責任を負いません。
４．当社は、ユーザー情報の取り扱いには最大限の注意を払い、別途定めるプライバシーポリシーを遵守します。</p>
    <p><span>第六条　本サービスの申請代行</span><br>
本サービスは、前条に定めるユーザー情報を元にスマートフォンアプリ（以下「アプリ」という）を生成し、当社がアプリを米Apple Inc.（以下「アップル社」という）の運営するAppStoreおよび米Google Inc.（以下「グーグル社」という）の運営するGooglePlayに申請代行し、掲載されるよう手続きを行います。
但し、申請したアプリは、アップル社およびグーグル社による審査が行われるため、当社は、掲載について一切保証いたしません。
２．AppStoreへの申請を円滑にするため、ユーザーは当社がアプリ紹介文等の申請内容を編集することを認めるものとします。</p>
    <p><span>第七条　本サービスの利用開始</span><br>
１．本サービスは、当社から管理画面・ID・パスワードが発行された時点でサービス利用開始とし、公開のお知らせを電子メールの送信または当社が適切と判断する方法により通知します。
２．ユーザーは、本サービスを本規約に定める条件に従って利用できるものとします。</p>
    <p><span>第八条 利用料金</span><br>
１．当社の提供する本サービスの各サービスプラン、追加オプションの料金については予めその額を定め、本サービスのウェブサイトへ掲載するものとします。ユーザーは、次の各号に掲げる料金を当社に支払うものとします。
（１） 月額利用料金
２．当社は、法制度の改正、経済情勢の変動により、前項に定めた料金の額を変更することがあります。変更された料金額は、本サービスのウェブサイトへの掲載等、当社が適切と判断する方法でユーザーに通知します。
３． ユーザーは、当社が指定する期日までに利用料金を次の各号のいずれかの方法で支払うものとします。
（1）クレジットカード決済
<Strike>（2）ユーザーの銀行預金口座又は郵便貯金口座からの自動引落</Strike></p>
    <p><span>第九条　本サービスのアップグレード</span><br>
当社は、随時本サービスのアップグレード版を提供します。その際、アップグレード版への切り替え作業に、別途費用が必要になる場合があります。アップグレードとは、古いバージョンから最新のバージョンに入れ替えることを指します。</p>
    <p><span>第十条　本サービスの利用中止</span><br>
ユーザーは、本サービスを中止する場合は、当社所定の届出方法に従い届出を行うことにより、本サービスを中止することができるものとします。ただし、初めの一年は「一年契約」となり、一年契約の途中で本サービスの利用を中止する場合は、利用中止年の利用料金を支払うものとします。
２．当社は次の場合、本サービスの解約を行うことがあります。
（1）当社が指定する期日までに本サービスの利用料金の入金が確認できなかった場合
（2）当社から電子メールの送信で連絡を行っても１週間以内に返信がない場合、又は連絡がとれなくなった場合
（3）アプリがAppStoreおよびGooglePlayの両方が掲載対象外となり、再掲載の見込みがない場合、当社より電子メールの送信または当社が適切と判断する方法で通知し、本サービスの利用を中止するものとします。但し、この場合、ユーザーが既に支払った利用料金は当社は返金の義務がないものとし、ユーザーも返金の請求をしないものとします。
３．前項の規定に基づき、当社が本サービスの解約を行った場合、直ちに当該ユーザーのアカウントを停止し、ユーザーへ通知するものとします。通知後１ヶ月以内にユーザーより何ら連絡がない場合、当社は当該ユーザーのアプリデータを削除できるものとします。</p>
    <p><span>第十一条　IDおよびパスワードの管理責任</span><br>
１．ユーザーは、自己のID、パスワードの使用および管理について一切の責任を負うものとします。
2. 当社は、ユーザーのID、パスワードが第三者に使用されたことによって当該ユーザーが被った損害について、当該ユーザーの故意過失の有無にかかわらず一切責任を負いません。</p>
    <p><span>第十二条　禁止事項</span><br>
ユーザーは以下の各号に該当する禁止行為をしてはいけません。
（1）著作権、特許権等の知的財産権を侵害する行為
（2）名誉毀損行為、侮辱行為や他者の業務妨害となる行為
（3）他人のプライバシーを侵害する行為や個人情報保護法に抵触する行為
（4）不正アクセスをするなど不正アクセス禁止法に抵触する行為
（5）犯罪に関わる行為あるいは法令に違反する行為
（6）公序良俗に反する行為、あるいは社会的に不適切な行動と解される行為
（7）当社が不適切と判断した広告バナー、広告テキストまたは広告ハイパーリンクの表示
（8）本サービスを管理するサーバーなどに過負荷を与えるプログラムなどの設置および行為
（9）本サービスの利用にあたり、当社が不適切と判断する行為</p>
    <p><span>第十三条　免責事項</span><br>
ユーザーが前条に定める禁止事項を行った場合、または本規約および関連する規約の一部もしくは全部に違反した場合、当社は直接的にも間接的にも一切の責任を負わないものとします。ユーザーの行為によって刑事・民事・行政上の責任が生じた場合、ユーザー自身においてその解決を図ることとします。
２．当社は本サービスの円滑な運営のため、ユーザーが前項に抵触する場合、またはその恐れがある場合、もしくは当社が必要と判断した場合、当社は予告なしに当該ユーザーに対する本サービスを停止し、当社サーバーに保存したデータを削除する事ができるものとします。その場合、当社は削除されたデータを復元する責任を負わないものとし、利用停止および保存データの削除に起因する直接的または間接的な損害に関して一切責任を負わないものとします。
３．当社は、本サービスにおいて、欠陥、一時停止、削除、変更、終了及びそれらに起因したユーザーまたは第三者の損害に対し、一切責任を負いません。
４．当社は本サービスにおいて開示されたコンテンツ及び同コンテンツのリンク先が提供するサービスの合法性、道徳性、著作権の許諾の有無、信頼性、正確性について一切責任を負いません。
５．当社はユーザーが自ら開示した情報により、他のユーザーまたは第三者との間における紛争、誹謗中傷、嫌がらせ、詐欺、ストーカー行為等の被害を受けた場合、同被害に関する損害について、一切責任を負いません。</p>
    <p><span>第十四条　サービスの一時的な中断</span><br>
当社は、以下のいずれかの事由が生じた場合には、ユーザーに事前に通知することなく一時的に本サービスを中断することがあります。
（1） 本サービス用設備等の保守を緊急に行う場合。
（2） 火災、停電等により本サービスの提供ができなくなった場合。
（3） 地震、噴火、洪水、津波等の天災地変により本サービスの提供ができなくなった場合。
（4） 戦争、動乱、暴動、騒乱、労働争議等により本サービスの提供ができなくなった場合。
（5） その他、運用上または技術上当社が本サービスの一時的な中断が必要と判断した場合。</p>
    <p><span>第十五条　サービス提供の中止</span><br>
当社は、事前通知をした上で本サービスの全部または一部の提供を中止することがあります。ただし、緊急やむを得ない場合は事前通知をせず中止することがあります。
２．当社は、前条またはその他の事由により本サービスの提供の遅延または中断等が発生したとしても、これに起因するユーザーまたは第三者が被った損害について一切責任を負いません。</p>
    <p><span>第16条　当社の財産権</span><br>
ユーザーが送信（発信）したものならびに予めユーザーより支給された固有デザイン、ロゴ、写真、名称の範囲、付随する意匠デザインの著作権を除き、サービスに含まれているコンテンツ、個々の情報（データ）および情報（データ）の集合体に関する財産権は当社に帰属しています。
２．本サービスに使用されているすべてのソフトウェア（以下「ソフトウェア」という）は知的財産権に関する法令等により保護されている財産権および営業秘密を包含しています。
３．本サービスの提供、利用促進および本サービスの広告・宣伝の目的のために、当社は本サービスへ送信された情報を、無償かつ非独占的にインターネットサイトおよび紙面媒体へ掲載することができるものとし、ユーザーはこれを許諾するものとします。</p>
    <p><span>第十七条　秘密保持義務</span><br>
ユーザーおよび当社は、相手方の書面による承諾なくして、本サービス約に関連して相手方から開示された相手方固有の技術上、販売上その他業務上の秘密を、本サービス期間中はもとより、本サービス終了後も第三者に対しては開示、漏洩しないものとします。
２．前項にかかわらず、ユーザーおよび当社は、裁判所の決定、行政機関等の命令・指示等により秘密情報の開示を要求された場合または法令等に定める場合は、必要な範囲内と認められる部分のみを開示することが出来るものとします。
３．第１項の規定にかかわらず、次の各号のいずれかに該当する情報は秘密情報から除外するものとします。
（1） 開示の時点で既に公知のもの、または開示後情報を受領した当事者の責によらずして公知となったもの
（2） 開示を行った時点で既に相手方が保有しているもの
（3） 第三者から秘密保持義務を負うことなく正当に入手したもの
（4） 相手方からの開示以降に開発されたもので、相手方からの情報によらないもの</p>
    <p><span>第十八条　反社会的勢力の排除</span><br>
当社およびユーザーは、相手方に対して、ユーザーが本規約に同意した日および将来にわたって、自己又は自己の役職員が次の各号に掲げる者（本規約において、「反社会的勢力」という。）でないことを表明し、保証します。
(1) 暴力団、暴力団の構成員（準構成員を含む。）又は暴力団の構成員でなくなった日から５年を経過しない者。
(2) 暴力団関係企業。
(3) 総会屋、社会運動標榜ゴロ、政治活動標榜ゴロ、特殊知能暴力集団またはこれらの団体の構成員。
(4) 前各号に準じるもの。
２． 当社およびユーザーは、次の各号のいずれかに該当する行為若しくは該当するおそれのある行為を行わず、または第三者をして行わせしめないことを相手方に対して表明し、保証します。
(1) 暴力的な要求行為。
(2) 法的な責任を超えた不当な要求行為。
(3) 取引に関して、脅迫的な言動を行い、又は暴力を用いる行為。
(4) 風説の流布、偽計若しくは威力を用いて信用を毀損し、又は業務を妨害する行為
(5) 前各号に準じる行為。
３． 当社およびユーザーは、相手方が前２項に定める表明保証に反した場合は、将来に向かって直ちに本サービスの解除を行うことができます。
４． 当社またはユーザーが本条に定める解除を行ったときは、本サービスは、その解除の通知を相手方に発信した日をもって終了します。
５． 当社およびユーザーは、本条に定める解除を行った場合であっても、相手方に対する損害賠償請求権を失わないものとします。なお、解除された当事者は、解除した当事者に対して損害賠償を請求することはできません。</p>
    <p><span>第十九条　当社への連絡</span><br>
ＩＤやパスワードを紛失した場合、本サービス利用に伴うユーザー遵守事項に第三者が違反しているのを発見した場合、その他当社に対する問合せが必要な場合は、<a href="mailto:support@hiropro.co.jp">お問い合わせフォーム</a>よりご連絡ください。</p>
    <p><span>第二十条　全般</span><br>
本規約の準拠法は日本法とします。
２．本サービスの利用にあたり、本規約および当社の指導により解決できない問題が生じた場合、ユーザーと当社との間で誠意を持って話し合い、これを解決するものとします。
３．本サービスまたは本規約に関し、訴訟の必要が生じた場合、当社の本店所在地を管轄する地方裁判所を専属的合意管轄裁判所とします。</p>

    <p>以上</p>
    <p class="ta_r">2013年11月1日制定</p>

</div>
<!-- /コンテンツ -->
</body>
</html>
<?php }} ?>