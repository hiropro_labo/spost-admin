<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-07 13:31:29
         compiled from "/home/spost/admin/app/views/menu/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:917176338537b466b01ee15-10560683%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2c9b03f6fad40f21d14d27d0c8d2aded4873a933' => 
    array (
      0 => '/home/spost/admin/app/views/menu/index.tpl',
      1 => 1404707486,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '917176338537b466b01ee15-10560683',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_537b466b2149e3_35561883',
  'variables' => 
  array (
    'SPONSOR' => 0,
    'image' => 0,
    'list' => 0,
    'entry' => 0,
    'menu' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537b466b2149e3_35561883')) {function content_537b466b2149e3_35561883($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- メニュートップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">メニュートップ画像の変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="メニュートップ画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>メニュートップ画像の変更</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/menu/top/update" class="edit_btn">変更する</a>
  <a href="/menu/top/update" title="変更"><img src="<?php echo $_smarty_tpl->tpl_vars['image']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="メニュートップ画像" class="mb_20 con_img" /></a>
<?php }else{ ?>
  <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value->image_path();?>
" width="160" height="88" alt="メニュートップ画像" class="mb_20 con_img" />
<?php }?>
</div>
<!---->


<!-- カテゴリーの編集 -->
<div class="contents_box">
  <div class="contents_box_head">カテゴリーの編集
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="カテゴリーが追加できます。<br>「カテゴリーの新規登録」ボタンを押して、カテゴリーを追加して下さい。"></a>
  </div>
  <h4>カテゴリーの編集</h4>
  <p class="mb_20"><span class="hisu">※簡易申請時に必須の項目です。</span><br />最低、１個のカテゴリーと、１個の商品は必要になっております。<br />
  <span class="hisu">商品の登録はカテゴリーの登録後、「メニュー作成」を押して下さい。</span>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/menu/category/add" class="edit_btn mt_m12">カテゴリーの新規登録</a>
<?php }?>
  </p>
</div>



<div class="contents_box">
  <div class="contents_box_head">メニュー一覧
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="カテゴリー商品の詳細が作成できます。<br>商品の魅力が伝わる画像と内容をご紹介しましょう。"></a>
  </div>
  <h4>メニュー一覧</h4>

<?php if (!is_null($_smarty_tpl->tpl_vars['list']->value)&&count($_smarty_tpl->tpl_vars['list']->value)>0){?>

<?php  $_smarty_tpl->tpl_vars['entry'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['entry']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['entry']->key => $_smarty_tpl->tpl_vars['entry']->value){
$_smarty_tpl->tpl_vars['entry']->_loop = true;
?>
<div class="row-fluid mb_20 mt_20 br_4 row-fluid_wrap"<?php if ($_smarty_tpl->tpl_vars['entry']->value['category']->enable==0){?> style="background-color:#cccccc;"<?php }?>>
  <dl class="menu_ac">
    <dt>
      <div class="menu_cate_box">
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
        <a href="/menu/category/update/<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->id;?>
" title="変更"><img src="<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="メニューカテゴリー画像" class="mb_20 con_img"></a>
<?php }else{ ?>
        <img src="<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="メニューカテゴリー画像" class="mb_20 con_img">
<?php }?>

        <p class="menu_table_text">
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
          <a href="/menu/category/update/<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->id;?>
"><?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->title;?>
</a><br />
<?php }else{ ?>
          <?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->title;?>
<br />

<?php }?>
          <?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->sub_title;?>

        </p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
        <a href="/menu/category/update/<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->id;?>
" class="gray_btn mr_5 ml_20">カテゴリの編集</a>

        <a href="/menu/category/del/<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->id;?>
" class="gray_btn mr_5"><img src="/assets/img/common/icon/i11.png" width="18" id="i11" style="margin-left:0;"></a>
<?php }?>

        <a href="javascript:void(0);" class="blue_btn">メニュー作成</a>
      </div>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
      <div class="up_down">
        <?php if (!is_null($_smarty_tpl->tpl_vars['entry']->value['category']->file_name)&&count($_smarty_tpl->tpl_vars['list']->value)>1){?>
          <?php if ($_smarty_tpl->tpl_vars['entry']->value['category']->position!=1){?>
          <a href="/menu/category/order/up/<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->position;?>
" class="gray_btn ml_20 mb_10"><img src="/assets/img/common/icon/i16.png" width="14" style="margin-left: 0;"/ ></a>
          <?php }?>

          <?php if ($_smarty_tpl->tpl_vars['entry']->value['category']->position<count($_smarty_tpl->tpl_vars['list']->value)){?>
          <a href="/menu/category/order/down/<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->position;?>
" class="gray_btn ml_20"><img src="/assets/img/common/icon/i17.png" width="14" style="margin-left:0;"/ ></a>
          <?php }?>
        <?php }?>
      </div>
<?php }?>
      <br class="clear">
    </dt>

    
    <dd>
      <?php  $_smarty_tpl->tpl_vars['menu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['menu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['entry']->value['menu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['menu']->key => $_smarty_tpl->tpl_vars['menu']->value){
$_smarty_tpl->tpl_vars['menu']->_loop = true;
?>
      <div class="menu_inline">
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
        <a href="/menu/item/update/<?php echo $_smarty_tpl->tpl_vars['menu']->value->id;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['menu']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="メニュー商品画像" class="mb_20 con_img" /></a>
<?php }else{ ?>
        <img src="<?php echo $_smarty_tpl->tpl_vars['menu']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="メニュー商品画像" class="mb_20 con_img" />
<?php }?>

        <p class="menu_table_text">
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
          <a href="/menu/item/update/<?php echo $_smarty_tpl->tpl_vars['menu']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['menu']->value->title;?>
</a><br />
<?php }else{ ?>
          <?php echo $_smarty_tpl->tpl_vars['menu']->value->title;?>
<br />
<?php }?>
          <?php echo $_smarty_tpl->tpl_vars['menu']->value->sub_title;?>

        </p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
        <a href="/menu/item/update/<?php echo $_smarty_tpl->tpl_vars['menu']->value->id;?>
" class="gray_btn mr_5 ml_20">商品の編集</a>

        <a href="/menu/item/del/<?php echo $_smarty_tpl->tpl_vars['menu']->value->id;?>
" class="gray_btn mr_5"><img src="/assets/img/common/icon/i11.png" width="18" style="margin-left:0px;"/></a>
        <a href="javascript:pickup('<?php echo $_smarty_tpl->tpl_vars['menu']->value->id;?>
');" id="pickup_<?php echo $_smarty_tpl->tpl_vars['menu']->value->id;?>
" class="gray_btn"><?php if ($_smarty_tpl->tpl_vars['menu']->value->pickup=='0'){?>☆<?php }else{ ?>★<?php }?></a>
<?php }?>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
        <div class="up_down" id="up_down_inside">
        <?php if (!is_null($_smarty_tpl->tpl_vars['menu']->value->file_name)&&$_smarty_tpl->tpl_vars['entry']->value['category']->menu_max_cnt>1){?>
          <?php if ($_smarty_tpl->tpl_vars['menu']->value->position!=1){?>
          <a href="/menu/item/order/up/<?php echo $_smarty_tpl->tpl_vars['menu']->value->position;?>
/<?php echo $_smarty_tpl->tpl_vars['menu']->value->parent_id;?>
" class="gray_btn ml_20 mb_10"><img src="/assets/img/common/icon/i16.png" width="14" style="margin-left:0;"></a>
          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['menu']->value->position<$_smarty_tpl->tpl_vars['entry']->value['category']->menu_max_cnt){?>
          <a href="/menu/item/order/down/<?php echo $_smarty_tpl->tpl_vars['menu']->value->position;?>
/<?php echo $_smarty_tpl->tpl_vars['menu']->value->parent_id;?>
" class="gray_btn ml_20"><img src="/assets/img/common/icon/i17.png" width="14" style="margin-left:0;"></a>
          <?php }?>
        <?php }?>
        </div>
<?php }?>
      </div>
      <?php } ?>
      <div class="clear"></div>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
      <div class="menu_inline add_box">
        <a href="/menu/item/add/<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->id;?>
" class="blue_btn add_btn" id="new_edit">商品の追加</a>
      </div>
<?php }?>
    </dd>
  </dl>

  <div class="clear"></div>
</div>


<?php } ?>

<?php }else{ ?>
  <div class="menu_list row-fluid mb_20 mt_20 br_4 row-fluid_wrap">
    <p>現在、カテゴリー、商品の登録がされていません。</p>
  </div>
<?php }?>

</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer_meta/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>