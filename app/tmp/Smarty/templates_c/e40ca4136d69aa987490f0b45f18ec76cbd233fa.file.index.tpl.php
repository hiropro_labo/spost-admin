<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-19 16:46:29
         compiled from "/home/spost/admin/app/views/top/img/top/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1647665395379b6d55bdd90-95737328%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e40ca4136d69aa987490f0b45f18ec76cbd233fa' => 
    array (
      0 => '/home/spost/admin/app/views/top/img/top/update/index.tpl',
      1 => 1400485539,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1647665395379b6d55bdd90-95737328',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'position' => 0,
    'top_image' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5379b6d5620923_34239837',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5379b6d5620923_34239837')) {function content_5379b6d5620923_34239837($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">トップ画像の変更<a href="/support/manual/top"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="アプリのトップ画像を変更する事ができます。<br>「ファイルを選択」ボタンを押し、お好きな画像をアップロード後、<br>「変更を保存」ボタンを押してください。"></a></div>
  <h4>トップ画像の変更</h4>

  <form action="/top/img/top/update/<?php echo $_smarty_tpl->tpl_vars['position']->value;?>
" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">画像のアップロード</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['top_image']->value->image_path();?>
?<?php echo time();?>
" width="160" height="105" alt="トップ画像" class="con_img form_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />640px&nbsp;×&nbsp;420px&nbsp;以上の大きさを推奨</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/top" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>