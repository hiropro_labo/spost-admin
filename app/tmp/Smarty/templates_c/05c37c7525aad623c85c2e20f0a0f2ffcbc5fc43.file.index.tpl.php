<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-13 19:46:26
         compiled from "/home/spost/admin/app/views/goods/item/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1077903629539ad6825c1128-42067221%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '05c37c7525aad623c85c2e20f0a0f2ffcbc5fc43' => 
    array (
      0 => '/home/spost/admin/app/views/goods/item/update/index.tpl',
      1 => 1402623892,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1077903629539ad6825c1128-42067221',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'c_id' => 0,
    'item' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539ad6826e4eb7_30740912',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539ad6826e4eb7_30740912')) {function content_539ad6826e4eb7_30740912($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/goods.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- 商品の変更 -->
<div class="contents_box">
  <div class="contents_box_head">商品の変更
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="商品の内容を変更できます。<br>変更したい内容を変更後、「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>商品の変更</h4>

  <form action="/goods/item/update/<?php echo $_smarty_tpl->tpl_vars['c_id']->value;?>
" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">画像のアップロード</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['item']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="商品トップ画像" class="form_img con_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />560px&nbsp;×&nbsp;320px&nbsp;以上の大きさを推奨</p>
    </li>

    <li>
      <label class="hisu">カテゴリー</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('parent_id')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('parent_id');?>
</p>
    </li>
    <li>
      <label class="hisu">商品名</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('title')->build();?>

      <p class="desc">全角20文字まで</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('title');?>
</p>
    </li>
    <li>
      <label class="hisu">サブタイトル名</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('sub_title')->build();?>

      <p class="desc">全角28文字まで</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('sub_title');?>
</p>
    </li>
    <li>
      <label class="hisu">商品説明</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('description')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('description');?>
</p>
    </li>
    <li>
      <label class="hisu">価格</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('price')->build();?>
<span class="ml_5">円</span>
      <p class="desc mt_m15">半角英数字で入力して下さい。<br />
      価格の非表示は、「0」を設定して下さい。</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('price');?>
</p>
    </li>
    <li>
      <label>URL</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('url')->build();?>

      <p class="desc">オンラインショプなどへのリンクを貼ることができます。</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('url');?>
</p>
    </li>
    <li>
      <label>&nbsp;</label>
      <label for="form_enable_1"><input type="radio" required="required" value="1" id="form_enable_1" name="enable" <?php if ($_smarty_tpl->tpl_vars['fieldset']->value->value('enable')=='1'){?>checked="checked"<?php }?> />表示</label>

      <label for="form_enable_0"><input type="radio" required="required" value="0" id="form_enable_0" name="enable" <?php if ($_smarty_tpl->tpl_vars['fieldset']->value->value('enable')=='0'){?>checked="checked"<?php }?> />非表示</label>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('enable');?>
</p>
    </li>
    <li>
      <p class="desc">お客様に見せるかどうかを選ぶことができます。</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/goods" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>