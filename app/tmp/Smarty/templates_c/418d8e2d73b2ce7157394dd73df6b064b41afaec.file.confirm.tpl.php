<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-07 14:31:03
         compiled from "/home/spost/admin/app/views/news/add/confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1277727506537b26cc852af5-12874187%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '418d8e2d73b2ce7157394dd73df6b064b41afaec' => 
    array (
      0 => '/home/spost/admin/app/views/news/add/confirm.tpl',
      1 => 1404708939,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1277727506537b26cc852af5-12874187',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_537b26cc8ca5f5_21830199',
  'variables' => 
  array (
    'SPONSOR' => 0,
    'news' => 0,
    'fieldset' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537b26cc8ca5f5_21830199')) {function content_537b26cc8ca5f5_21830199($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/news.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- ニュースの新規作成 -->
<div class="contents_box">
  <div class="contents_box_head">ニュースの新規作成

  </div>
  <h4>ニュースの新規作成</h4>

  <form action="/news/add/exe" method="POST">

<div class="contents_form">
  <ul class="cel">
    <li>
      <label>ニュース画像</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['news']->value->tmp_image_path_by_id($_smarty_tpl->tpl_vars['SPONSOR']->value->id(),'news');?>
?<?php echo time();?>
" width="160" height="107" alt="ニュース画像" class="form_img con_img" />
    </li>

    <li>
      <label>タイトル</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('title');?>
</span>
    </li>

    <li>
      <label>本文</label>
      <span><?php echo nl2br($_smarty_tpl->tpl_vars['fieldset']->value->value('body'));?>
</span>
    </li>

    <li>
      <label>配信対象</label>
      <span><?php echo (($tmp = @smarty_modifier_replace($_smarty_tpl->tpl_vars['fieldset']->value->value('target'),'1','球団アプリユーザーへも配信する'))===null||$tmp==='' ? "球団アプリユーザーへは配信しない" : $tmp);?>
</span>
    </li>

    <li>
      <label>配信予約日時</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('reserved_at_year');?>
年<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('reserved_at_month');?>
月<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('reserved_at_day');?>
日&emsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('reserved_at_hour');?>
：<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('reserved_at_min');?>
</span>
    </li>

  </ul>

  <hr />

  
  <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldset']->value->field(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value){
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>
  <?php echo $_smarty_tpl->tpl_vars['field']->value;?>

  <?php } ?>
  
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>