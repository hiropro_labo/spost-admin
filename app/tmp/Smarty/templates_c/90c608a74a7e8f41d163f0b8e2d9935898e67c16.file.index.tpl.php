<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-07 14:27:33
         compiled from "/home/spost/admin/app/views/news/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1079448610537b2f194e77f9-81589493%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '90c608a74a7e8f41d163f0b8e2d9935898e67c16' => 
    array (
      0 => '/home/spost/admin/app/views/news/update/index.tpl',
      1 => 1404710844,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1079448610537b2f194e77f9-81589493',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_537b2f195633f9_10261987',
  'variables' => 
  array (
    'news' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537b2f195633f9_10261987')) {function content_537b2f195633f9_10261987($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/news.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- ニュースの編集 -->
<div class="contents_box">
  <div class="contents_box_head">ニュースの編集
    <a href="/support/manual/news#news_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="ニュースの編集をすることが出来ます。<br>「ファイルを選択」ボタンを押し、お好きな画像をアップロード・<br>
    テキストの記入・PUSH通知選択後、「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>ニュースの編集</h4>

  <form action="/news/update/<?php echo $_smarty_tpl->tpl_vars['news']->value->id;?>
" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

  <div class="contents_form">
  <ul>
    <li>
      <label>画像のアップロード</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['news']->value->image_path();?>
?<?php echo time();?>
"  width="160" height="107" alt="ニュース画像" class="form_img con_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />600px&nbsp;×&nbsp;380px&nbsp;以上の大きさを推奨</p>
    </li>

    <li>
      <label class="hisu">タイトル</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('title')->build();?>

      <p class="desc">文字数：全角20文字以内を推奨</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('title');?>
</p>
    </li>

    <li>
      <label class="hisu">本文</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('body')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('body');?>
</p>
    </li>

    <li>
      <label>配信オプション</label>
      <input type="checkbox" id="form_notice_1" name="target" value="1" <?php if ($_smarty_tpl->tpl_vars['news']->value->target==1){?>checked="checked"<?php }?> />球団アプリユーザーへも配信
    </li>
    <li>
      <p class="desc">チェックすると球団アプリのユーザーへもニュース配信します</p>
    </li>

    <li>
      <label class="hisu">配信予約日時</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('reserved_at_year')->build();?>
&nbsp;年&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('reserved_at_month')->build();?>
&nbsp;月&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('reserved_at_day')->build();?>
&nbsp;日&emsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('reserved_at_hour')->build();?>
&nbsp;：&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('reserved_at_min')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('reserved_at_year');?>
</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('reserved_at_month');?>
</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('reserved_at_day');?>
</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('reserved_at_hour');?>
</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('reserved_at_min');?>
</p>
    </li>
    <li>
      <p class="desc">指定しない場合、または、過去の日時を指定した場合は即時配信となります。<br />
      また、アプリ公開前に登録されたニュースについてはPUSH配信されませんのでご了承ください。</p>
    </li>
  </ul>
  <hr />

  <input type="submit" name="button" value="変更の確認" id="save_btn" class="save_btn" onclick="DisableButton(this);" />
  <a href="/news" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>