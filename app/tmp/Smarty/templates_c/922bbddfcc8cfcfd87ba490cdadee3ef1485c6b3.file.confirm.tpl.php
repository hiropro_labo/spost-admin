<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-21 15:53:21
         compiled from "/home/spost/admin/app/views/profile/info/update/confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:210647501253a5296499c524-15534868%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '922bbddfcc8cfcfd87ba490cdadee3ef1485c6b3' => 
    array (
      0 => '/home/spost/admin/app/views/profile/info/update/confirm.tpl',
      1 => 1403333416,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '210647501253a5296499c524-15534868',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a52964a290b1_99259489',
  'variables' => 
  array (
    'fieldset' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a52964a290b1_99259489')) {function content_53a52964a290b1_99259489($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/profile.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">プロフィールの変更
  </div>
  <h4>プロフィールの変更</h4>

  <form action="/profile/info/update/exe" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>お名前</label>
      <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('name'))===null||$tmp==='' ? "-----" : $tmp);?>
</span>
    </li>

    <li>
      <label>肩書き</label>
      <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('position'))===null||$tmp==='' ? "-----" : $tmp);?>
</span>
    </li>

    <li>
      <label>生年月日</label>
      <span>昭和<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('birth_year');?>
年<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('birth_month');?>
月<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('birth_date');?>
日</span>
    </li>

    <li>
      <label>血液型</label>
      <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('blood'))===null||$tmp==='' ? "-----" : $tmp);?>
</span>
    </li>

    <li>
      <label>趣味</label>
      <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('hobby'))===null||$tmp==='' ? "-----" : $tmp);?>
</span>
    </li>

  </ul>

  <hr />

  
  <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldset']->value->field(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value){
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>
  <?php echo $_smarty_tpl->tpl_vars['field']->value;?>

  <?php } ?>
  
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>