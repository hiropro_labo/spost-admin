<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-13 19:18:04
         compiled from "/home/spost/admin/app/views/goods/category/update/confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:38253061539acfdc6faf21-65642266%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bbd3520c1988fe6c7740d243c4659fc228c00c8d' => 
    array (
      0 => '/home/spost/admin/app/views/goods/category/update/confirm.tpl',
      1 => 1402622229,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '38253061539acfdc6faf21-65642266',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'c_id' => 0,
    'img_upload_flg' => 0,
    'category' => 0,
    'fieldset' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539acfdc79cb91_47960692',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539acfdc79cb91_47960692')) {function content_539acfdc79cb91_47960692($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/goods.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- カテゴリーの変更 -->
<div class="contents_box">
  <div class="contents_box_head">カテゴリーの変更
  </div>
  <h4>カテゴリーの変更</h4>

  <form action="/goods/category/update/exe/<?php echo $_smarty_tpl->tpl_vars['c_id']->value;?>
" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>&nbsp;</label>
      <?php if ($_smarty_tpl->tpl_vars['img_upload_flg']->value){?>
      <img src="<?php echo $_smarty_tpl->tpl_vars['category']->value->tmp_image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="商品カテゴリー画像" class="form_img con_img" />
      <?php }else{ ?>
      <img src="<?php echo $_smarty_tpl->tpl_vars['category']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="商品カテゴリー画像" class="form_img con_img" />
      <?php }?>
    </li>

    <li>
      <label>カテゴリー名</label>
      <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('title'))===null||$tmp==='' ? "-----" : $tmp);?>
</span>
    </li>

    <li>
      <label>サブタイトル名</label>
      <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('sub_title'))===null||$tmp==='' ? "-----" : $tmp);?>
</span>
    </li>

    <li>
      <label>&nbsp;</label>
      <span><?php echo smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['fieldset']->value->value('enable'),'0','非表示'),'1','表示');?>
</span>
    </li>
  </ul>

  <hr />

  
  <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldset']->value->field(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value){
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>
  <?php echo $_smarty_tpl->tpl_vars['field']->value;?>

  <?php } ?>
  
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>