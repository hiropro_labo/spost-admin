<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-20 18:44:45
         compiled from "/home/spost/admin/app/views/preview/news.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6084296355359f1b46331d4-04866333%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '82fa75cb532beb14051e9897755cc533fed106e1' => 
    array (
      0 => '/home/spost/admin/app/views/preview/news.tpl',
      1 => 1400561673,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6084296355359f1b46331d4-04866333',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5359f1b46a3b26_64123427',
  'variables' => 
  array (
    'list' => 0,
    'news' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5359f1b46a3b26_64123427')) {function content_5359f1b46a3b26_64123427($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.date_format.php';
?><?php echo $_smarty_tpl->getSubTemplate ("preview/common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- ▼コンテンツ -->
<div id="p_content_wrap" class="p_news">

<!-- ニュース一覧 -->
<div class="p_news_list">
  <ul>
<?php  $_smarty_tpl->tpl_vars['news'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['news']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['news']->key => $_smarty_tpl->tpl_vars['news']->value){
$_smarty_tpl->tpl_vars['news']->_loop = true;
?>
    <!--  -->
<?php if ($_smarty_tpl->tpl_vars['news']->value->notice_status==1){?>
    <li>
<?php }else{ ?>
    <li class="no_send">
<?php }?>
      <div class="p_n_title">
        <?php echo $_smarty_tpl->tpl_vars['news']->value->title;?>

      </div>
      <div class="p_n_sub">
        <div class="p_n_s_time">
          <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value->updated_at,"%Y-%m-%d %H:%M:%S");?>

        </div>
      </div>
      <div class="p_n_text">
        <?php echo nl2br($_smarty_tpl->tpl_vars['news']->value->body);?>

      </div>
      <img src="<?php echo $_smarty_tpl->tpl_vars['news']->value->image_path();?>
?<?php echo time();?>
" width="210" height="140" alt="ニュース画像" />
    </li>
    <!--  -->

<?php } ?>
  </ul>
</div>
<!--  -->

</div>
<!-- ▲ -->


<?php echo $_smarty_tpl->getSubTemplate ("preview/common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>