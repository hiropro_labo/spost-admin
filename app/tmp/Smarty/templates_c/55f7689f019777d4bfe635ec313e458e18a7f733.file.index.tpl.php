<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-13 19:13:01
         compiled from "/home/spost/admin/app/views/goods/category/add/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:910055152539acead6d6422-61840777%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '55f7689f019777d4bfe635ec313e458e18a7f733' => 
    array (
      0 => '/home/spost/admin/app/views/goods/category/add/index.tpl',
      1 => 1402621910,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '910055152539acead6d6422-61840777',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'category' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539acead745973_43610538',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539acead745973_43610538')) {function content_539acead745973_43610538($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/goods.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- カテゴリーの新規作成 -->
<div class="contents_box">
  <div class="contents_box_head">カテゴリーの新規作成
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="カテゴリーの新規作成ができます。<br>「ファイルを選択」ボタンを押して、画像をアップロード<br>テキストの記入、表示・非表示選択後<br>「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>カテゴリーの新規作成</h4>

  <form action="/goods/category/add" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

  <div class="contents_form">
    <ul>
      <li>
        <label class="hisu">画像のアップロード</label>
        <img src="<?php echo $_smarty_tpl->tpl_vars['category']->value->blank_image();?>
?<?php echo time();?>
" width="160" height="88" alt="メニュートップ画像" class="form_img con_img" />
        <input type="file" name="upload" id="upload">
        <p class="desc">ファイルサイズ：3MBまで<br />640px&nbsp;×&nbsp;350px&nbsp;以上の大きさを推奨</p>
      </li>

      <li>
        <label class="hisu">カテゴリー名</label>
        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('title')->build();?>

        <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('title');?>
</p>
      </li>

      <li>
        <label class="hisu">サブタイトル名</label>
        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('sub_title')->build();?>

        <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('sub_title');?>
</p>
      </li>

      <li>
        <label>&nbsp;</label>
        <label for="form_enable_1"><input type="radio" required="required" value="1" id="form_enable_1" name="enable" checked="checked" />表示</label>

        <label for="form_enable_0"><input type="radio" required="required" value="0" id="form_enable_0" name="enable" />非表示</label>
        <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('enable');?>
</p>
      </li>
    </ul>

    <p class="desc">お客様に見せるかどうかを選ぶことができます。</p>

    <hr />

    <input type="submit" name="button" value="変更の確認" class="save_btn" />
    <a href="/goods" id="save_btn" class="back_btn">戻る</a>
  </div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>