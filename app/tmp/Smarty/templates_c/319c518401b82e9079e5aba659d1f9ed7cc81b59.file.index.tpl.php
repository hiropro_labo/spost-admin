<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-13 19:20:12
         compiled from "/home/spost/admin/app/views/goods/category/del/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1078504876539ad05c1ce3f3-27869140%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '319c518401b82e9079e5aba659d1f9ed7cc81b59' => 
    array (
      0 => '/home/spost/admin/app/views/goods/category/del/index.tpl',
      1 => 1402622368,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1078504876539ad05c1ce3f3-27869140',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'category' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539ad05c255391_96991641',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539ad05c255391_96991641')) {function content_539ad05c255391_96991641($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/goods.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- カテゴリーの削除 -->
<div class="contents_box">
  <div class="contents_box_head">カテゴリーの削除
    <a href="/support/manual/menu#menu_2" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="登録したカテゴリーを削除できます。<br>「削除」ボタンを押して、削除して下さい。"></a>
  </div>
  <h4>カテゴリーの削除</h4>

  <form action="/goods/category/del/exe/<?php echo $_smarty_tpl->tpl_vars['category']->value->id;?>
" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>カテゴリー画像</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['category']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="商品カテゴリー画像" class="form_img con_img" />
    </li>

    <li>
      <label>カテゴリー名</label>
      <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('title'))===null||$tmp==='' ? "-----" : $tmp);?>
</span>
    </li>

    <li>
      <label>サブタイトル名</label>
      <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('sub_title'))===null||$tmp==='' ? "-----" : $tmp);?>
</span>
    </li>

    <li>
      <label>&nbsp;</label>
      <span><?php echo smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['fieldset']->value->value('enable'),'0','非表示'),'1','表示');?>
</span>
    </li>
  </ul>

  <hr />

   <input type="submit" name="button" value="削除" class="save_btn" />
 <a href="/goods" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>