<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-23 12:03:19
         compiled from "/home/spost/admin/app/views/menu/category/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1342635894537eba77c51329-54806293%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '595fdef45e761ad067c7ded49773648d54130f29' => 
    array (
      0 => '/home/spost/admin/app/views/menu/category/update/index.tpl',
      1 => 1400814136,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1342635894537eba77c51329-54806293',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'c_id' => 0,
    'category' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_537eba77cd7994_98751554',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537eba77cd7994_98751554')) {function content_537eba77cd7994_98751554($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- カテゴリーの変更 -->
<div class="contents_box">
  <div class="contents_box_head">カテゴリーの変更
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="新規追加したカテゴリーの内容が変更できます。<br>変更したい内容を変更後、「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>カテゴリーの変更</h4>

  <form action="/menu/category/update/<?php echo $_smarty_tpl->tpl_vars['c_id']->value;?>
" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label>画像のアップロード</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['category']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="メニュートップ画像" class="form_img con_img" />
    </li>
    <li>
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br>640px&nbsp;×&nbsp;350px&nbsp;以上の大きさを推奨</p>
    </li>

    <li>
      <label>カテゴリー名</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('title')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('title');?>
</p>
    </li>

    <li>
      <label>サブタイトル名</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('sub_title')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('sub_title');?>
</p>
    </li>

    <li>
      <label>&nbsp;</label>
      <label for="form_enable_1"><input type="radio" required="required" value="1" id="form_enable_1" name="enable" <?php if ($_smarty_tpl->tpl_vars['fieldset']->value->value('enable')=='1'){?>checked="checked"<?php }?> />表示</label>
      <label for="form_enable_0"><input type="radio" required="required" value="0" id="form_enable_0" name="enable" <?php if ($_smarty_tpl->tpl_vars['fieldset']->value->value('enable')=='0'){?>checked="checked"<?php }?> />非表示</label>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('enable');?>
</p>
    </li>
  </ul>
      <p class="desc">お客様に見せるかどうかを選ぶことができます。</p>
  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/menu" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>