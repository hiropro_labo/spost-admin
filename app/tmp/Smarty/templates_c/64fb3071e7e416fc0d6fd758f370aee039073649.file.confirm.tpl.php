<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-23 13:06:24
         compiled from "/home/spost/admin/app/views/menu/item/add/confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1683580530537ec84bca1835-95278322%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '64fb3071e7e416fc0d6fd758f370aee039073649' => 
    array (
      0 => '/home/spost/admin/app/views/menu/item/add/confirm.tpl',
      1 => 1400817980,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1683580530537ec84bca1835-95278322',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_537ec84bd3d6c5_63604364',
  'variables' => 
  array (
    'c_id' => 0,
    'SPONSOR' => 0,
    'item' => 0,
    'category' => 0,
    'fieldset' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537ec84bd3d6c5_63604364')) {function content_537ec84bd3d6c5_63604364($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- 商品の新規作成 -->
<div class="contents_box">
  <div class="contents_box_head">商品の新規作成
  </div>
  <h4>商品の新規作成</h4>

  <form action="/menu/item/add/exe/<?php echo $_smarty_tpl->tpl_vars['c_id']->value;?>
" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label>画像のアップロード</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['item']->value->tmp_image_path_by_id($_smarty_tpl->tpl_vars['SPONSOR']->value->id(),'menu_item');?>
?<?php echo time();?>
" width="160" height="88" alt="メニュー商品画像" class="mb_20 con_img" />
    </li>

    <li>
      <label>カテゴリー</label>
      <span><?php echo $_smarty_tpl->tpl_vars['category']->value->title;?>
</span>
    </li>

    <li>
      <label>メニュー名</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('title');?>
</span>
    </li>

    <li>
      <label>サブタイトル名</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('sub_title');?>
</span>
    </li>

    <li>
      <label>メニュー説明</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('description');?>
</span>
    </li>

    <li>
      <label>価格</label>
      <span><?php echo number_format($_smarty_tpl->tpl_vars['fieldset']->value->value('price'));?>
円</span>
    </li>

    <li>
      <label>URL</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('url');?>
</span>
    </li>

    <li>
      <label>&nbsp;</label>
      <span><?php echo smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['fieldset']->value->value('enable'),'0','非表示'),'1','表示');?>
</span>
     </li>
  </ul>

  <hr />

  
  <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldset']->value->field(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value){
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>
  <?php echo $_smarty_tpl->tpl_vars['field']->value;?>

  <?php } ?>
  
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>