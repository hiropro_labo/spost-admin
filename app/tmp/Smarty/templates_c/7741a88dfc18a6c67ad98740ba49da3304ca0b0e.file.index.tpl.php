<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-18 19:32:20
         compiled from "/home/spost/admin/app/views/topics/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:212477888753a16ab481c4a0-59992675%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7741a88dfc18a6c67ad98740ba49da3304ca0b0e' => 
    array (
      0 => '/home/spost/admin/app/views/topics/update/index.tpl',
      1 => 1403055030,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '212477888753a16ab481c4a0-59992675',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'topics' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a16ab487fd60_01584534',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a16ab487fd60_01584534')) {function content_53a16ab487fd60_01584534($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/topics.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- トピックスの編集 -->
<div class="contents_box">
  <div class="contents_box_head">トピックスの編集
    <a href="/support/manual/news#news_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="トピックスの編集をすることが出来ます。<br>「ファイルを選択」ボタンを押し、お好きな画像をアップロード・<br>
    テキストの記入・PUSH通知選択後、「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>トピックスの編集</h4>

  <form action="/topics/update/<?php echo $_smarty_tpl->tpl_vars['topics']->value->id;?>
" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

  <div class="contents_form">
  <ul>
    <li>
      <label>画像のアップロード</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['topics']->value->image_path();?>
?<?php echo time();?>
"  width="160" height="107" alt="トピックス画像" class="form_img con_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />600px&nbsp;×&nbsp;380px&nbsp;以上の大きさを推奨</p>
    </li>

    <li>
      <label class="hisu">タイトル</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('title')->build();?>

      <p class="desc">文字数：全角20文字以内を推奨</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('title');?>
</p>
    </li>

    <li>
      <label class="hisu">本文</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('body')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('body');?>
</p>
    </li>
  </ul>
  <hr />

  <input type="submit" name="button" value="変更の確認" id="save_btn" class="save_btn" onclick="DisableButton(this);" />
  <a href="/topics" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>