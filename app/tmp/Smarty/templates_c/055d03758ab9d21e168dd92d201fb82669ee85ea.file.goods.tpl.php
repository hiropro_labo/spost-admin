<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-27 18:55:00
         compiled from "/home/spost/admin/app/views/common/footer_meta/goods.tpl" */ ?>
<?php /*%%SmartyHeaderCode:398778214539acb5cd572a8-50055770%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '055d03758ab9d21e168dd92d201fb82669ee85ea' => 
    array (
      0 => '/home/spost/admin/app/views/common/footer_meta/goods.tpl',
      1 => 1403862892,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '398778214539acb5cd572a8-50055770',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539acb5cd5a068_47461197',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539acb5cd5a068_47461197')) {function content_539acb5cd5a068_47461197($_smarty_tpl) {?>
<script type="text/javascript">
// カテゴリーのアコーディオン
$(function(){
  $(".menu_ac dt .blue_btn").hover(function(){
    $(this).css("cursor","pointer");
  },function(){
    $(this).css("cursor","default");
  });
  $(".menu_ac dd .menu_inline").css("display","none");
  $(".menu_ac dt .blue_btn:not(:animated)").click(function(){
    $(this).parent().parent().next().children().slideToggle("fast");
  });
});

function pickup(id){
    var data = {c_id: id};

    $.ajax({
        type: "post",
        url: '/goods/item/pickup/' + id,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(data),
        success: function(res){
            if (!res[0]){
                alert("おすすめの更新処理に失敗しました");
            }else{
                $('#pickup_' + id).html(res[1]);
                alert('指定された商品のおすすめ設定を更新しました');
            }
        },
        error: function(){
            alert('おすすめの更新処理に失敗しました');
        }
    });
}
</script>
<?php }} ?>