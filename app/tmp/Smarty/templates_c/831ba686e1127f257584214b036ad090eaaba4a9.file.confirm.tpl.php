<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-18 19:33:15
         compiled from "/home/spost/admin/app/views/topics/update/confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:85523035853a16ac7555455-78835044%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '831ba686e1127f257584214b036ad090eaaba4a9' => 
    array (
      0 => '/home/spost/admin/app/views/topics/update/confirm.tpl',
      1 => 1403087583,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '85523035853a16ac7555455-78835044',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a16ac7602c71_15589819',
  'variables' => 
  array (
    'topics' => 0,
    'img_upload_flg' => 0,
    'SPONSOR' => 0,
    'fieldset' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a16ac7602c71_15589819')) {function content_53a16ac7602c71_15589819($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/topics.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- トピックスの編集 -->
<div class="contents_box">
  <div class="contents_box_head">トピックスの編集
  </div>
  <h4>トピックスの編集</h4>

  <form action="/topics/update/exe/<?php echo $_smarty_tpl->tpl_vars['topics']->value->id;?>
" method="POST">

<div class="contents_form">
  <ul class="cel">
    <li>
      <label>トピックス画像</label>
      <?php if ($_smarty_tpl->tpl_vars['img_upload_flg']->value){?>
      <img src="<?php echo $_smarty_tpl->tpl_vars['topics']->value->tmp_image_path($_smarty_tpl->tpl_vars['SPONSOR']->value->id());?>
?<?php echo time();?>
" width="160" height="107" alt="トピックス画像" class="form_img con_img" />
      <?php }else{ ?>
      <img src="<?php echo $_smarty_tpl->tpl_vars['topics']->value->image_path($_smarty_tpl->tpl_vars['SPONSOR']->value->id());?>
?<?php echo time();?>
" width="160" height="107" alt="トピックス画像" class="form_img con_img" />
      <?php }?>
    </li>

  <li>
    <label>タイトル</label>
    <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('title');?>
</span>
  </li>

  <li>
    <label>本文</label>
    <span><?php echo nl2br($_smarty_tpl->tpl_vars['fieldset']->value->value('body'));?>
</span>
  </li>

  </ul>

  <hr />

  
  <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldset']->value->field(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value){
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>
  <?php echo $_smarty_tpl->tpl_vars['field']->value;?>

  <?php } ?>
  
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>