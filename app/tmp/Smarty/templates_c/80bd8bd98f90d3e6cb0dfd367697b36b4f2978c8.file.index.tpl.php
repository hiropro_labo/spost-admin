<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-13 19:09:19
         compiled from "/home/spost/admin/app/views/goods/top/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1597375119539acdcfbc3d40-69466782%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '80bd8bd98f90d3e6cb0dfd367697b36b4f2978c8' => 
    array (
      0 => '/home/spost/admin/app/views/goods/top/update/index.tpl',
      1 => 1402621681,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1597375119539acdcfbc3d40-69466782',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539acdcfc0e032_27452614',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539acdcfc0e032_27452614')) {function content_539acdcfc0e032_27452614($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/goods.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- 商品トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">商品トップ画像の変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="商品トップ画像の変更ができます。<br>「ファイルを選択」ボタンを押して<br>画像をアップロードして下さい。"></a>
  </div>
  <h4>商品トップ画像の変更</h4>

  <form action="/goods/top/update" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">画像のアップロード</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="商品トップ画像" class="form_img con_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />640px&nbsp;×&nbsp;350px&nbsp;以上の大きさを推奨</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/goods" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>