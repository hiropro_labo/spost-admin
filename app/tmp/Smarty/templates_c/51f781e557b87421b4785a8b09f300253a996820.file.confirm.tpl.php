<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-23 22:02:50
         compiled from "/home/spost/admin/app/views/store/info/update/confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:68718250853a8257a6b2b41-81292488%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '51f781e557b87421b4785a8b09f300253a996820' => 
    array (
      0 => '/home/spost/admin/app/views/store/info/update/confirm.tpl',
      1 => 1403495613,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '68718250853a8257a6b2b41-81292488',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'fieldset' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a8257a71d7b0_99026478',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a8257a71d7b0_99026478')) {function content_53a8257a71d7b0_99026478($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/store.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">TEL/MAIL</div>
  <h4>アプリ内のスライドメニュー内のTEL/MAILボタンに埋め込む情報を編集できます</h4>

  <form action="/store/info/update/exe" method="POST">
    <div class="contents_form">
      <ul>
        <li>
          <label for="pref">電話番号</label>
          <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('tel1');?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('tel2');?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('tel3');?>
</span>
        </li>
        <li>
          <label for="pref">メールアドレス</label>
          <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('email');?>
</span>
        </li>
      </ul>
      <hr />
      <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldset']->value->field(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value){
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>
      <?php echo $_smarty_tpl->tpl_vars['field']->value;?>

      <?php } ?>
      <input type="submit" name="button" value="変更の保存" class="save_btn" />
      <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
    </div>
  </form>
</div>

<div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>