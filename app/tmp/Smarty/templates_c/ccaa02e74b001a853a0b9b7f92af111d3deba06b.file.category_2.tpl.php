<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-09 10:13:04
         compiled from "/home/spost/admin/app/views/switcher/options/category_2.tpl" */ ?>
<?php /*%%SmartyHeaderCode:60910443353bc97205371a5-61793948%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ccaa02e74b001a853a0b9b7f92af111d3deba06b' => 
    array (
      0 => '/home/spost/admin/app/views/switcher/options/category_2.tpl',
      1 => 1403801731,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '60910443353bc97205371a5-61793948',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'SPONSOR' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53bc9720599cd9_49448741',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53bc9720599cd9_49448741')) {function content_53bc9720599cd9_49448741($_smarty_tpl) {?>  <!--オプション選択-->
  <div class="option_choose mt_40 clearfix">
    <h3>オプション選択</h3>
    <p class="text_sub">必要に応じて、アプリ内にお好きなページを追加することができます。</p>


    <div class="box">
      <label><input type="checkbox" id="check1" value="branch" <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->switcher()->is_active("branch")){?>checked<?php }?>>
      <img src="/assets/img/app/spost/option/o_list2.png"></label>
      <div class="box_right">
        <h4>多店舗</h4>
        <p>チェーン店や複数の店舗がある方向けの機能です。</p>
      </div>
    </div>

    <div class="box">
      <label><input type="checkbox" id="check2" value="coupon" <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->switcher()->is_active("coupon")){?>checked<?php }?>>
      <img src="/assets/img/app/spost/option/o_list3.png"></label>
      <div class="box_right">
        <h4>クーポン</h4>
        <p>ユーザーに向けてクーポンを発行することができます。</p>
      </div>
    </div>


    <div class="box">
      <label><input type="checkbox" id="check3" value="recruit" <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->switcher()->is_active("recruit")){?>checked<?php }?>>
      <img src="/assets/img/app/spost/option/o_list4.png"></label>
      <div class="box_right">
        <h4>採用情報</h4>
        <p>採用情報のページをアプリに追加します。</p>
      </div>
    </div>

  </div>
  <!--/オプション選択-->
<?php }} ?>