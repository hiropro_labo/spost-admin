<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-26 17:10:43
         compiled from "/home/spost/admin/app/views/support/manual/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4970207575382f70344e445-76763697%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a5697cc051a1981c2a4bd1d1d0490c3664259204' => 
    array (
      0 => '/home/spost/admin/app/views/support/manual/index.tpl',
      1 => 1398068607,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4970207575382f70344e445-76763697',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5382f7034a6441_45509972',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5382f7034a6441_45509972')) {function content_5382f7034a6441_45509972($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header_manual.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!--<div class="phone">-->
  <div class="main_width km">
  <div class="top menu_box km">
  <img src="/assets/img/common/manual/top_title.png" class="menu_title">
  <hr class="menu_line">
  <ul class="menu">
  <li><a href="/support/manual/top">アプリのトップ画像を登録(変更)する</a></li>
  <li><a href="/support/manual/top#top_1">アプリのトップ画像を削除する</a></li>
  <li><a href="/support/manual/top#top_2">トピックメッセージを変更する</a></li>
  <li><a href="/support/manual/top#top_3">お店の画像を登録(変更)する</a></li>
  <li><a href="/support/manual/top#top_4">お店の情報を登録(変更)する</a></li>
  </ul>
  </div><!-- (/top) -->

  <div class="coupon menu_box km">
  <img src="/assets/img/common/manual/coupon_title.png" class="menu_title">
  <hr class="menu_line">
  <ul class="menu">
  <li><a href="/support/manual/coupon">クーポンを登録(変更)する</a></li>
  <li><a href="/support/manual/coupon#coupon_1">クーポンを削除する</a></li>
  </ul>
  </div><!-- (/coupon) -->

  <div class="news menu_box km">
  <img src="/assets/img/common/manual/news_title.png" class="menu_title">
  <hr class="menu_line">
  <ul class="menu">
  <li><a href="/support/manual/news">ニュースを登録する</a></li>
  <li><a href="/support/manual/news#news_1">ニュースを編集する</a></li>
  <li><a href="/support/manual/news#news_2">ニュースを削除する</a></li>
  <li><a href="/support/manual/news#news_3">ニュースを配信する</a></li>
  </ul>
  </div><!-- (/news) -->

  <div class="menu menu_box km">
  <img src="/assets/img/common/manual/menu_title.png" class="menu_title">
  <hr class="menu_line">
  <ul class="menu">
  <li><a href="/support/manual/menu">メニューのトップ画像を登録(変更)する</a></li>
  <li><a href="/support/manual/menu#menu_1">メニューのカテゴリーを登録(変更)する</a></li>
  <li><a href="/support/manual/menu#menu_2">メニューのカテゴリーを削除する</a></li>
  <li><a href="/support/manual/menu#menu_3">メニューの商品画像を登録(変更)する</a></li>
  <li><a href="/support/manual/menu#menu_4">メニューの商品画像を削除する</a></li>
  </ul>
  </div><!-- (/menu) -->

  <div class="info menu_box km">
  <img src="/assets/img/common/manual/info_title.png" class="menu_title">
  <hr class="menu_line">
  <ul class="menu">
  <li><a href="/support/manual/store">アプリのアイコンを登録する</a></li>
  <li><a href="/support/manual/store#store_1">アプリの名前を登録する</a></li>
  <li><a href="/support/manual/store#store_2">アプリのスクリーンショットを登録する</a></li>
  <li><a href="/support/manual/store#store_3">アプリのスクリーンショットを削除する</a></li>
  <li><a href="/support/manual/store#store_4">アプリ起動時の画像を登録する</a></li>
  <li><a href="/support/manual/store#store_5">アプリのカテゴリーを登録する</a></li>
  </ul>
  </div><!-- (/info) -->

  <div class="pdf">
  <p><img src="/assets/img/common/manual/pdf_title.png" class="pdf_title"></p>
  <hr>
  <ul>
  <li><a href="/assets/pdf/manual/ipost_manual.pdf" target="_blank">iPostアプリ制作マニュアル</a></li>
  </ul>
  </div><!-- (/pdf") -->

<div class="clear"></div>

</div><!-- (/main_width) -->


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>