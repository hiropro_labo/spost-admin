<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-05 20:46:19
         compiled from "/home/spost/admin/app/views/inspect/pre/category1.tpl" */ ?>
<?php /*%%SmartyHeaderCode:183159486253b7e58b05c7b4-30821487%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4b7a63119cf3cb7229d0d8eb87d511e4e9a8f2ac' => 
    array (
      0 => '/home/spost/admin/app/views/inspect/pre/category1.tpl',
      1 => 1404528267,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '183159486253b7e58b05c7b4-30821487',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'SPONSOR' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53b7e58b0f2468_20623698',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53b7e58b0f2468_20623698')) {function content_53b7e58b0f2468_20623698($_smarty_tpl) {?>    <!--左-->
    <div class="fl">

      <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->status()->is_pass('top')){?>
        <div class="inspect_item clearfix">
          <div class="inspect_item_l_off i_icon1_off"><img src="/assets/img/app/spost/inspect/i_icon1_off.png" /></div>
          <div class="inspect_item_r"><h5>トップ</h5>
            <p>アプリトップを編集しましょう</p>
            <p class="inspect_item_off">入力済み</p>
          </div>
        </div>
      <?php }else{ ?>
        <a href="/top" class="inspect_item clearfix">
          <div class="inspect_item_l_on i_icon1"><img src="/assets/img/app/spost/inspect/i_icon1_on.png" /></div>
          <div class="inspect_item_r"><h5>トップ</h5>
            <p>アプリトップを編集しましょう</p>
            <p class="inspect_item_on">未入力</p>
          </div>
        </a>
      <?php }?>

      <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->status()->is_pass('news')){?>
        <div class="inspect_item clearfix">
          <div class="inspect_item_l_off i_icon2_off"><img src="/assets/img/app/spost/inspect/i_icon2_off.png" /></div>
          <div class="inspect_item_r"><h5>ニュース</h5>
            <p>ニュースを作りましょう</p>
            <p class="inspect_item_off">入力済み</p>
          </div>
        </div>
      <?php }else{ ?>
        <a href="/news" class="inspect_item clearfix">
          <div class="inspect_item_l_on i_icon2"><img src="/assets/img/app/spost/inspect/i_icon2_on.png" /></div>
          <div class="inspect_item_r"><h5>ニュース</h5>
            <p>ニュースを作りましょう</p>
            <p class="inspect_item_on">未入力</p>
          </div>
        </a>
      <?php }?>

      <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->status()->is_pass('shop')){?>
        <div class="inspect_item clearfix">
          <div class="inspect_item_l_off i_icon3_off"><img src="/assets/img/app/spost/inspect/i_icon3_off.png" /></div>
          <div class="inspect_item_r"><h5>店舗情報</h5>
            <p>店舗情報を作りましょう</p>
            <p class="inspect_item_off">入力済み</p>
          </div>
        </div>
      <?php }else{ ?>
        <a href="/shop" class="inspect_item clearfix">
          <div class="inspect_item_l_on i_icon3"><img src="/assets/img/app/spost/inspect/i_icon3_on.png" /></div>
          <div class="inspect_item_r"><h5>店舗情報</h5>
            <p>店舗情報を作りましょう</p>
            <p class="inspect_item_on">未入力</p>
          </div>
        </a>
      <?php }?>

    </div>
    <!--/左-->

    <!--右-->
    <div class="fl ml_5p">

      <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->status()->is_pass('menu')){?>
        <div class="inspect_item clearfix">
          <div class="inspect_item_l_off i_icon7_off"><img src="/assets/img/app/spost/inspect/i_icon7_off.png" /></div>
          <div class="inspect_item_r"><h5>メニュー</h5>
            <p>メニューを作りましょう</p>
            <p class="inspect_item_off">入力済み</p>
          </div>
        </div>
      <?php }else{ ?>
        <a href="/menu" class="inspect_item clearfix">
          <div class="inspect_item_l_on i_icon7"><img src="/assets/img/app/spost/inspect/i_icon7_on.png" /></div>
          <div class="inspect_item_r"><h5>メニュー</h5>
            <p>メニューを作りましょう</p>
            <p class="inspect_item_on">未入力</p>
          </div>
        </a>
      <?php }?>

      <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->status()->is_pass('store')){?>
        <div class="inspect_item clearfix">
          <div class="inspect_item_l_off i_icon14_off"><img src="/assets/img/app/spost/inspect/i_icon14_off.png" /></div>
          <div class="inspect_item_r"><h5>情報編集</h5>
            <p>アプリアイコンなどを設定しましょう</p>
            <p class="inspect_item_off">入力済み</p>
          </div>
        </div>
      <?php }else{ ?>
        <a href="/store" class="inspect_item clearfix">
          <div class="inspect_item_l_on i_icon14"><img src="/assets/img/app/spost/inspect/i_icon14_on.png" /></div>
          <div class="inspect_item_r"><h5>情報編集</h5>
            <p>アプリアイコンなどを設定しましょう</p>
            <p class="inspect_item_on">未入力</p>
          </div>
        </a>
      <?php }?>

    </div>
    <!--/右--><?php }} ?>