<?php /* Smarty version Smarty-3.1-DEV, created on 2014-04-30 15:48:14
         compiled from "/home/spost/admin/app/views/support/help/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:125733972453609cae9614b7-48830188%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '79508e722fb02081f2e6f9337eccf4ba30ff3f1d' => 
    array (
      0 => '/home/spost/admin/app/views/support/help/index.tpl',
      1 => 1398068606,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '125733972453609cae9614b7-48830188',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'current' => 0,
    'sub_menu' => 0,
    'item' => 0,
    'ipost_list' => 0,
    'list' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53609caea22074_01248883',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53609caea22074_01248883')) {function content_53609caea22074_01248883($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<div class="gridContainer clearfix">
  <div id="mypage_header"></div>

  <!-- ヘルプナビゲーション -->
  <div class="main_box_l">
    <div class="nav">
      <ul>
      <?php if (empty($_smarty_tpl->tpl_vars['current']->value)||!is_numeric($_smarty_tpl->tpl_vars['current']->value)){?>
        <li><a href="/support/help/" id="carrent">よくある質問</a></li>
      <?php }else{ ?>
        <li><a href="/support/help/">よくある質問</a></li>
      <?php }?>

<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sub_menu']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
  <?php if ($_smarty_tpl->tpl_vars['item']->value->id!=1&&$_smarty_tpl->tpl_vars['item']->value->id!=2){?>
    <?php if ($_smarty_tpl->tpl_vars['current']->value==$_smarty_tpl->tpl_vars['item']->value->id){?>
          <li><a href="/support/help/<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" id="carrent"><?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
</a></li>
    <?php }else{ ?>
        <li><a href="/support/help/<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
</a></li>
    <?php }?>
  <?php }?>
<?php } ?>
      </ul>
    </div>
  </div>
  <!--  -->


  <div id="main_box_r">
    <div class="box">
<?php if (empty($_smarty_tpl->tpl_vars['current']->value)||!is_numeric($_smarty_tpl->tpl_vars['current']->value)){?>
      <h3>よくある質問</h3>
<?php }else{ ?>
      <h3><?php echo $_smarty_tpl->tpl_vars['sub_menu']->value[$_smarty_tpl->tpl_vars['current']->value-1]->name;?>
</h3>
<?php }?>
    </div>

<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ipost_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
    <!--  -->
    <ul class="help_view_wrap">
      <li class="Qes">Q：<?php echo nl2br($_smarty_tpl->tpl_vars['item']->value->question);?>
</li>
      <hr>
      <li  class="Ans"><?php echo nl2br($_smarty_tpl->tpl_vars['item']->value->answer);?>
</li>
    </ul>
    <!--  -->
<?php } ?>

<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
    <!--  -->
    <ul class="help_view_wrap">
      <li class="Qes">Q：<?php echo nl2br($_smarty_tpl->tpl_vars['item']->value->question);?>
</li>
      <hr>
      <li  class="Ans"><?php echo nl2br($_smarty_tpl->tpl_vars['item']->value->answer);?>
</li>
    </ul>
    <!--  -->
<?php } ?>

    <a href="#mypage_header" id="to_top">▲ページトップに戻る</a>
  </div>

  <div class="clear"></div>
</div>



<script type="text/javascript">
//イージング
$(function(){
  $('a[href^=#]').click(function(){
    var speed = 500;
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    var body = 'body';
    if (navigator.userAgent.match(/MSIE/)){
      body = 'html';
    }
    $(body).animate({scrollTop:position}, speed, 'swing');
    return false;
  });
});
</script>



<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>