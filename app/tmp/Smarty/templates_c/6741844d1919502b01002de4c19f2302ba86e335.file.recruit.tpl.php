<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-18 17:52:35
         compiled from "/home/spost/admin/app/views/common/footer_meta/recruit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:205593780153a14e53494f33-30859864%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6741844d1919502b01002de4c19f2302ba86e335' => 
    array (
      0 => '/home/spost/admin/app/views/common/footer_meta/recruit.tpl',
      1 => 1403081552,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '205593780153a14e53494f33-30859864',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a14e534c6993_30211867',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a14e534c6993_30211867')) {function content_53a14e534c6993_30211867($_smarty_tpl) {?>
<script type="text/javascript">
function update(idx, obj){
    var button = $(obj);
    button.attr("disabled", true);

    var data = {
        title: $("#title" + idx).val(),
        body: $("#body" + idx).val()
    };

    $.ajax({
    	type: "post",
    	url: "/recruit/info/update/" + idx,
    	contentType: "application/json",
    	dataType: 'json',
    	data: JSON.stringify(data),
    	success: function(res){
    		if (!res[0]){
    			$("#error_msg" + idx).empty().append("更新処理に失敗しました<br />" + res[1]);
    		}else{
    		    location.reload();
    		}
    	},
        error: function(){
        	$("#error_msg" + idx).empty().append("更新処理に失敗しました[通信エラー発生]");
        },
        complete: function(){
        	button.attr("disabled", false);
        }
    });
}

function add(id, obj){
	var button = $(obj);
	button.attr("disabled", true);

	var data = {
		p_id: id,
		title: $("#add_title").val(),
	    body: $("#add_body").val()
	};

	$.ajax({
		type: "post",
		url: "/recruit/info/add",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(data),
		success: function(res){
			if (!res[0]){
				$("#error_msg_add").empty().append("更新処理に失敗しました<br />" + res[1]);
			}else{
				location.reload();
			}
		},
		error: function(){
			$("#error_msg_add").empty().append("更新処理に失敗しました[通信エラー発生]");
		},
		complete: function(){
			button.attr("disabled", false);
		}
	});
}

function del(idx, obj){
    if (!confirm("指定された項目を削除します\nよろしいですか？")) return false;

    var button = $(obj);
	button.attr("disabled", true);

    $.ajax({
    	type: "post",
    	url: "/recruit/info/del/" + idx,
    	success: function(res){
    		if (!res[0]){
    			$("#error_msg" + idx).empty().append("削除処理に失敗しました<br />" + res[1]);
    		}else{
    			location.reload();
    		}
    	},
        error: function(){
        	$("#error_msg" + idx).empty().append("更新処理に失敗しました[通信エラー発生]");
        },
        complete: function(){
        	button.attr("disabled", false);
        }
    });
}
</script>
<?php }} ?>