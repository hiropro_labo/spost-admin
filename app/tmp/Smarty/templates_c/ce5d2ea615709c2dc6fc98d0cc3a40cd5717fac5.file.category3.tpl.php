<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-05 20:49:48
         compiled from "/home/spost/admin/app/views/inspect/pre/category3.tpl" */ ?>
<?php /*%%SmartyHeaderCode:82726986953b7e65c032391-55267768%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ce5d2ea615709c2dc6fc98d0cc3a40cd5717fac5' => 
    array (
      0 => '/home/spost/admin/app/views/inspect/pre/category3.tpl',
      1 => 1404528318,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '82726986953b7e65c032391-55267768',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'SPONSOR' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53b7e65c0f7c14_86344412',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53b7e65c0f7c14_86344412')) {function content_53b7e65c0f7c14_86344412($_smarty_tpl) {?>    <!--左-->
    <div class="fl">

      <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->status()->is_pass('top')){?>
        <div class="inspect_item clearfix">
          <div class="inspect_item_l_off i_icon1_off"><img src="/assets/img/app/spost/inspect/i_icon1_off.png" /></div>
          <div class="inspect_item_r"><h5>トップ</h5>
            <p>アプリトップを編集しましょう</p>
            <p class="inspect_item_off">入力済み</p>
          </div>
        </div>
      <?php }else{ ?>
        <a href="/top" class="inspect_item clearfix">
          <div class="inspect_item_l_on i_icon1"><img src="/assets/img/app/spost/inspect/i_icon1_on.png" /></div>
          <div class="inspect_item_r"><h5>トップ</h5>
            <p>アプリトップを編集しましょう</p>
            <p class="inspect_item_on">未入力</p>
          </div>
        </a>
      <?php }?>

      <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->status()->is_pass('news')){?>
        <div class="inspect_item clearfix">
          <div class="inspect_item_l_off i_icon2_off"><img src="/assets/img/app/spost/inspect/i_icon2_off.png" /></div>
          <div class="inspect_item_r"><h5>ニュース</h5>
            <p>ニュースを作りましょう</p>
            <p class="inspect_item_off">入力済み</p>
          </div>
        </div>
      <?php }else{ ?>
        <a href="/news" class="inspect_item clearfix">
          <div class="inspect_item_l_on i_icon2"><img src="/assets/img/app/spost/inspect/i_icon2_on.png" /></div>
          <div class="inspect_item_r"><h5>ニュース</h5>
            <p>ニュースを作りましょう</p>
            <p class="inspect_item_on">未入力</p>
          </div>
        </a>
      <?php }?>

      <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->status()->is_pass('company')){?>
        <div class="inspect_item clearfix">
          <div class="inspect_item_l_off i_icon5_off"><img src="/assets/img/app/spost/inspect/i_icon5_off.png" /></div>
          <div class="inspect_item_r"><h5>会社概要</h5>
            <p>会社概要を作りましょう</p>
            <p class="inspect_item_off">入力済み</p>
          </div>
        </div>
      <?php }else{ ?>
        <a href="/company" class="inspect_item clearfix">
          <div class="inspect_item_l_on i_icon5"><img src="/assets/img/app/spost/inspect/i_icon5_on.png" /></div>
          <div class="inspect_item_r"><h5>会社概要</h5>
            <p>会社概要を作りましょう</p>
            <p class="inspect_item_on">未入力</p>
          </div>
        </a>
      <?php }?>

    </div>
    <!--/左-->

    <!--右-->
    <div class="fl ml_5p">

      <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->status()->is_pass('segments')){?>
        <div class="inspect_item clearfix">
          <div class="inspect_item_l_off i_icon6_off"><img src="/assets/img/app/spost/inspect/i_icon6_off.png" /></div>
          <div class="inspect_item_r"><h5>事業内容</h5>
            <p>事業内容を作りましょう</p>
            <p class="inspect_item_off">入力済み</p>
          </div>
        </div>
      <?php }else{ ?>
        <a href="/segments" class="inspect_item clearfix">
          <div class="inspect_item_l_on i_icon6"><img src="/assets/img/app/spost/inspect/i_icon6_on.png" /></div>
          <div class="inspect_item_r"><h5>事業内容</h5>
            <p>事業内容を作りましょう</p>
            <p class="inspect_item_on">未入力</p>
          </div>
        </a>
      <?php }?>

      <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->status()->is_pass('store')){?>
        <div class="inspect_item clearfix">
          <div class="inspect_item_l_off i_icon14_off"><img src="/assets/img/app/spost/inspect/i_icon14_off.png" /></div>
          <div class="inspect_item_r"><h5>情報編集</h5>
            <p>アプリアイコンなどを設定しましょう</p>
            <p class="inspect_item_off">入力済み</p>
          </div>
        </div>
      <?php }else{ ?>
        <a href="/store" class="inspect_item clearfix">
          <div class="inspect_item_l_on i_icon14"><img src="/assets/img/app/spost/inspect/i_icon14_on.png" /></div>
          <div class="inspect_item_r"><h5>情報編集</h5>
            <p>アプリアイコンなどを設定しましょう</p>
            <p class="inspect_item_on">未入力</p>
          </div>
        </a>
      <?php }?>

    </div>
    <!--/右--><?php }} ?>