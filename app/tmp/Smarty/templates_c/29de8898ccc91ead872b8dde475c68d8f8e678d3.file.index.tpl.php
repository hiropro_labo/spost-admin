<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-23 17:14:16
         compiled from "/home/spost/admin/app/views/auth/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8300308535354fa43eb75d3-27935379%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '29de8898ccc91ead872b8dde475c68d8f8e678d3' => 
    array (
      0 => '/home/spost/admin/app/views/auth/index.tpl',
      1 => 1403511221,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8300308535354fa43eb75d3-27935379',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5354fa43f38d89_58346950',
  'variables' => 
  array (
    'img_common_path' => 0,
    'fieldset' => 0,
    'message' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5354fa43f38d89_58346950')) {function content_5354fa43f38d89_58346950($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header_login.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



	<div id="login_title_wrap">
	    <img src="<?php echo $_smarty_tpl->tpl_vars['img_common_path']->value;?>
logo/login_title.png" width="192" height="97"  />
	</div>

	<form method="POST" accept-charset="utf-8" action="/auth/login">
		<div id="login_box">
			<div id="head_title">Login</div>


			<div id="login_box_wrap">
				<label for="login"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->label('login_id');?>
</label>
				<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('login_id')->build();?>

				<span class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('login_id');?>
</span>

				<label for="secret"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->label('password');?>
</label>
				<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('password')->build();?>

				<span class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('password');?>
</span>

				<input type="hidden" name="commit" id="commit" class="commit" value="1" />
				<input type="submit" id="login_btn" class="btn_red" value="Login" />
            
            <?php if (isset($_smarty_tpl->tpl_vars['message']->value)){?>
            <div class="error_message_wrapper">
                <?php echo nl2br($_smarty_tpl->tpl_vars['message']->value);?>

            </div>
            <?php }?>
            
				<div class="clear"></div>
			</div>
		</div>
	</form>

	<p id="copy">Copyright © 2013 HIROPRO, Inc. All Rights Reserved.</p>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer_login.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>