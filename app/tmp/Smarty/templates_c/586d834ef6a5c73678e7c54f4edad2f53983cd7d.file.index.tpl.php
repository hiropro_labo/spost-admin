<?php /* Smarty version Smarty-3.1-DEV, created on 2014-04-30 15:40:21
         compiled from "/home/spost/admin/app/views/inspect/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:101243841153609ad5876241-39674214%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '586d834ef6a5c73678e7c54f4edad2f53983cd7d' => 
    array (
      0 => '/home/spost/admin/app/views/inspect/index.tpl',
      1 => 1398068558,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '101243841153609ad5876241-39674214',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'inspect' => 0,
    'contents' => 0,
    'inspect_pass' => 0,
    'shop' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53609ad58d4300_02418352',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53609ad58d4300_02418352')) {function content_53609ad58d4300_02418352($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/inspect.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- お支払いの確認 -->
<div class="contents_box">
  <div class="contents_box_head">アプリ審査申請
  <a href="/support/help" target="_blank"><img src="/assets/img/common/help_tips.png" title=""></a>
  </div>
  <h4 style="color:#900;">審査申請ボタンを押されますと、アプリ管理の項目変更にロックが掛かります。<br />
  ロックが掛かりますと、各項目の変更ができなくなります。
  </h4>

  <form action="/inspect" method="POST">

<div class="contents_form">
  <ul>
    <li>
<?php if ($_smarty_tpl->tpl_vars['inspect']->value){?>
  <?php if ($_smarty_tpl->tpl_vars['contents']->value){?>
      <input type="submit" value="審査申請" class="save_btn ml_30" />
  <?php }else{ ?>
      <span class="ml_30">アプリ制作で未登録のものがあります。<br />アプリ審査の申請をするためには各コンテンツへ情報を入力・登録して下さい。</span>
  <?php }?>
<?php }else{ ?>
  <?php if (!$_smarty_tpl->tpl_vars['inspect_pass']->value){?>
     <span class="ml_30">現在、アプリ審査中です。<br />審査結果のご通知まで暫くお待ち下さい。</span>
  <?php }else{ ?>
     <?php if ($_smarty_tpl->tpl_vars['shop']->value){?>
     <span class="ml_30">現在、アプリ公開中です。</span>
     <?php }else{ ?>
     <span class="ml_30">現在公開処理の手続き中です。<br />公開のご通知まで暫くお待ち下さい。</span>
     <?php }?>
  <?php }?>
<?php }?>
    </li>
  </ul>

  </form>
</div>


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>