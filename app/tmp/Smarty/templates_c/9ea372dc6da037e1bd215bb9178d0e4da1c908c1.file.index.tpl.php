<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-18 19:18:51
         compiled from "/home/spost/admin/app/views/topics/add/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:114355672153a1676e8dfcd7-87581315%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9ea372dc6da037e1bd215bb9178d0e4da1c908c1' => 
    array (
      0 => '/home/spost/admin/app/views/topics/add/index.tpl',
      1 => 1403086730,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '114355672153a1676e8dfcd7-87581315',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a1676e969178_47864336',
  'variables' => 
  array (
    'topics' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a1676e969178_47864336')) {function content_53a1676e969178_47864336($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/topics.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- トピックスの新規作成 -->
<div class="contents_box">
  <div class="contents_box_head">トピックスの新規作成
    <a href="/support/manual/news" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="トピックスの配信内容を設定することが出来ます。<br>「ファイルを選択」ボタンを押し、お好きな画像をアップロード・<br>
    テキストの記入・PUSH通知選択後、「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>トピックスの新規作成</h4>

  <form action="/topics/add" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label>画像のアップロード</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['topics']->value->blank_image();?>
?<?php echo time();?>
" width="160" height="107" alt="トピックス画像" class="form_img con_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />600px&nbsp;×&nbsp;380px&nbsp;以上の大きさを推奨</p>
    </li>
    <li>
      <label class="hisu">タイトル</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('title')->build();?>

      <p class="desc">文字数：全角20文字以内を推奨</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('title');?>
</p>
    </li>
    <li>
      <label class="hisu">本文</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('body')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('body');?>
</p>
    </li>
  </ul>
  <hr />

  <input type="submit" name="button" value="変更の確認" id="save_btn" class="save_btn" onclick="DisableButton(this);" />
  <a href="/topics" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>