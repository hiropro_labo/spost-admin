<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-26 15:53:17
         compiled from "/home/spost/admin/app/views/store/text/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18487724115382e4920676a6-67833916%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'be96f5a8093ba3c70796a34bd86c1925a74faf37' => 
    array (
      0 => '/home/spost/admin/app/views/store/text/update/index.tpl',
      1 => 1401087194,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18487724115382e4920676a6-67833916',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5382e4920ed9a2_16439196',
  'variables' => 
  array (
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5382e4920ed9a2_16439196')) {function content_5382e4920ed9a2_16439196($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/store.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<div class="contents_box">
  <div class="contents_box_head">アプリストア(App Store/Google Play)表示項目の変更
    <a href="/support/manual/store#store_1"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="アプリストア(App Store/Google Play)表示項目を編集することができます。<br>フォームに入力した後、「確認する」ボタンを押してください。"></a></div>
  <h4>アプリストア(App Store/Google Play)表示項目をこちらで編集できます</h4>

  <form action="/store/text/update" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label for="zip1" class="hisu">アプリ名</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('app_name')->build();?>

      <p class="desc">最大全角112文字／推奨全角9文字以内</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('app_name');?>
</p>
    </li>

    <li>
      <label for="pref" class="hisu">表示名</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('app_disp_name')->build();?>

      <p class="desc">最大全角６文字</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('app_disp_name');?>
</p>
    </li>

    <li>
      <label for="pref" class="hisu">説明文</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('description')->build();?>

      <p class="desc">全角100 〜 2,000文字</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('description');?>
</p>
      <p class="desc_margin">審査に通過しやすい説明文<br><br><br>
      例文：<br><br>
      みつの里の公式アプリです。<br>
      このアプリでは、アプリユーザー限定の様々なサービスを受けることができます。<br><br><br>
      例えば…<br>
      ・お店情報や最新の商品情報を入手することができます。<br>
      ・標準アプリのマップを起動させてお店の位置を確認することができます。<br>
      ・お店から最新情報やお得なクーポンなどを受け取ることができます。<br>
      ・オンラインショップや公式ホームページなどもすぐにチェックできます。<br>
      ・オーダーエントリーがお使いのスマートフォンからできるようになります。（導入店に限る）<br><br><br>
      このようにアプリで実際に提供できる内容を記載すると、審査に通過しやすいポイントになります。
      </p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/store" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>