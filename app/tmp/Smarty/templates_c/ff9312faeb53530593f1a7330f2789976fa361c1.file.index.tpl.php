<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-21 19:10:35
         compiled from "/home/spost/admin/app/views/banner/add/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:123955268053a555db1632a0-47019767%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ff9312faeb53530593f1a7330f2789976fa361c1' => 
    array (
      0 => '/home/spost/admin/app/views/banner/add/index.tpl',
      1 => 1403313027,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '123955268053a555db1632a0-47019767',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a555db225a49_08694595',
  'variables' => 
  array (
    'model' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a555db225a49_08694595')) {function content_53a555db225a49_08694595($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/banner.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- バナーの新規作成 -->
<div class="contents_box">
  <div class="contents_box_head">バナーの新規登録
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="チームメンバーの新規登録ができます。<br>「ファイルを選択」ボタンを押して、画像をアップロード<br>テキストの記入、表示・非表示選択後<br>「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>バナーの新規登録</h4>

  <form action="/banner/add" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

  <div class="contents_form">
    <ul>
      <li>
        <label class="hisu">画像のアップロード</label>
        <img src="<?php echo $_smarty_tpl->tpl_vars['model']->value->blank_image();?>
?<?php echo time();?>
" width="320" height="71" alt="バナー画像" class="form_img con_img" />
        <input type="file" name="upload" id="upload">
        <p class="desc">ファイルサイズ：3MBまで<br />640px&nbsp;×&nbsp;142px&nbsp;以上の大きさを推奨</p>
      </li>

      <li>
        <label class="hisu">URL</label>
        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('url')->build();?>

        <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('url');?>
</p>
      </li>

      <li>
        <label>&nbsp;</label>
        <label for="form_enable_1"><input type="radio" required="required" value="1" id="form_enable_1" name="enable" checked="checked" />表示</label>
        <label for="form_enable_0"><input type="radio" required="required" value="0" id="form_enable_0" name="enable" />非表示</label>
        <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('enable');?>
</p>
      </li>
    </ul>

    <p class="desc">お客様に見せるかどうかを選ぶことができます。</p>

    <hr />

    <input type="submit" name="button" value="変更の確認" class="save_btn" />
    <a href="/profile" id="save_btn" class="back_btn">戻る</a>
  </div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>