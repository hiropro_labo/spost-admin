<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-28 14:02:46
         compiled from "/home/spost/admin/app/views/common/footer_meta/menu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1291777509537b48ace179d7-09108590%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'de31eb53c1869450b47efac281c574cdabc582fe' => 
    array (
      0 => '/home/spost/admin/app/views/common/footer_meta/menu.tpl',
      1 => 1403862896,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1291777509537b48ace179d7-09108590',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_537b48ace4b802_95719185',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537b48ace4b802_95719185')) {function content_537b48ace4b802_95719185($_smarty_tpl) {?>
<script type="text/javascript">
// カテゴリーのアコーディオン
$(function(){
  $(".menu_ac dt .blue_btn").hover(function(){
    $(this).css("cursor","pointer");
  },function(){
    $(this).css("cursor","default");
  });
  $(".menu_ac dd .menu_inline").css("display","none");
  $(".menu_ac dt .blue_btn:not(:animated)").click(function(){
    $(this).parent().parent().next().children().slideToggle("fast");
  });
});

function pickup(id){
	    var data = {c_id: id};

	    $.ajax({
	        type: "post",
	        url: '/menu/item/pickup/' + id,
	        contentType: "application/json",
	        dataType: 'json',
	        data: JSON.stringify(data),
	        success: function(res){
	            if (!res[0]){
	            	alert("おすすめの更新処理に失敗しました");
	            }else{
	            	$('#pickup_' + id).html(res[1]);
	            	alert('指定されたメニューのおすすめ設定を更新しました');
	            }
	        },
	        error: function(){
	        	alert('おすすめの更新処理に失敗しました');
	        }
	    });
}
</script>
<?php }} ?>