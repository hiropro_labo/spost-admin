<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-18 11:33:25
         compiled from "/home/spost/admin/app/views/store/appicon/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:159738265953a0fa54cbf886-48813086%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f3aa261db2ff494b007b313f16c7acea3aa00ebd' => 
    array (
      0 => '/home/spost/admin/app/views/store/appicon/update/index.tpl',
      1 => 1403026393,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '159738265953a0fa54cbf886-48813086',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a0fa54d0d6c5_26316548',
  'variables' => 
  array (
    'image_icon' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a0fa54d0d6c5_26316548')) {function content_53a0fa54d0d6c5_26316548($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/store.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">アプリ情報<a href="/support/manual/store"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playに表示される<br>アイコン画像を登録する事ができます。「ファイルを選択」ボタンを押して<br>アイコン画像をアップロードして下さい。"></a></div>
  <h4>アプリ内ステータスバー表示画像の変更</h4>

  <form action="/store/appicon/update/" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">画像のアップロード</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['image_icon']->value->image_path();?>
" width="50" height="50" alt="トップ画像" class="con_img form_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />40px x 65px 以上の大きさを推奨<br /><span></span></p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/store" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>