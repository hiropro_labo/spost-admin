<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-26 20:06:28
         compiled from "/home/spost/admin/app/views/branch/del/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:61148482353abfeb4e8c604-30816013%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '25c56eac66202b27cc7da9d3a75e2b8b1456c336' => 
    array (
      0 => '/home/spost/admin/app/views/branch/del/index.tpl',
      1 => 1403780786,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '61148482353abfeb4e8c604-30816013',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'branch' => 0,
    'fieldset' => 0,
    'selected_pref' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53abfeb50547e6_37492824',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53abfeb50547e6_37492824')) {function content_53abfeb50547e6_37492824($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/branch.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- 支店の削除 -->
<div class="contents_box">
  <div class="contents_box_head">支店の削除
    <a href="/support/manual/menu#menu_2" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="登録されている支店データを削除できます。"></a>
  </div>

  <form action="/branch/del/exe/<?php echo $_smarty_tpl->tpl_vars['branch']->value->id;?>
" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>支店画像</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['branch']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="支店画像" class="form_img con_img" />
    </li>
  </ul>


  <ul>
    <li>
      <label for="name">お店の名前</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('shop_name');?>
</span>
    </li>

    <li>
      <label for="zip1">郵便番号</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('zip_code1');?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('zip_code2');?>
</span>
    </li>

    <li>
      <label for="pref">都道府県</label>
      <span><?php echo $_smarty_tpl->tpl_vars['selected_pref']->value;?>
</span>
    </li>

    <li>
      <label for="pref">市区町村</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('city');?>
</span>
    </li>

    <li>
      <label for="pref">住所（番地）</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('address_opt1');?>
</span>
    </li>

    <li>
      <label for="pref">住所（建物）</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('address_opt2');?>
</span>
    </li>

    <li>
      <label for="pref">電話番号</label><span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('tel1');?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('tel2');?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('tel3');?>
</span>
    </li>

    <li>
      <label for="pref">FAX</label><span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('fax1');?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('fax2');?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('fax3');?>
</span>
    </li>

    <li>
      <label for="pref">ホームページアドレス</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('url');?>
</span>
    </li>

    <li>
      <label for="pref">オンラインショップ</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('online_shop');?>
</span>
    </li>

    <li>
      <label for="pref">メールアドレス</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('email');?>
</span>
    </li>

    <li>
      <label for="pref">営業時間</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('open_hours');?>
</span>
    </li>

    <li>
      <label for="pref">定休日</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('holiday');?>
</span>
    </li>
  </ul>

  <hr />

   <input type="submit" name="button" value="削除" class="save_btn" />
 <a href="/branch" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>