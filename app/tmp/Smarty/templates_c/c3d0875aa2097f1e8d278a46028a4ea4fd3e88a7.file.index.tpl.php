<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-03 17:43:59
         compiled from "/home/spost/admin/app/views/branch/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:234745732535a20f13db2f6-49905793%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c3d0875aa2097f1e8d278a46028a4ea4fd3e88a7' => 
    array (
      0 => '/home/spost/admin/app/views/branch/index.tpl',
      1 => 1404377007,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '234745732535a20f13db2f6-49905793',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_535a20f14ca214_11353286',
  'variables' => 
  array (
    'SPONSOR' => 0,
    'list' => 0,
    'branch' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535a20f14ca214_11353286')) {function content_535a20f14ca214_11353286($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/branch.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<!-- 支店の編集 -->
<div class="contents_box">
  <div class="contents_box_head">支店の編集
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="支店データの登録や編集ができます。"></a>
  </div>
  <h4>支店の編集</h4>
  <p class="mb_20"><span class="hisu">※簡易申請時の必須項目ではありません。</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/branch/add" class="edit_btn mt_m50">支店の新規登録</a>
<?php }?>
</div>



<div class="contents_box">
  <div class="contents_box_head">支店一覧
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="支店データを登録できます。"></a>
  </div>

<?php if (!is_null($_smarty_tpl->tpl_vars['list']->value)&&count($_smarty_tpl->tpl_vars['list']->value)>0){?>

<?php  $_smarty_tpl->tpl_vars['branch'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['branch']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['branch']->key => $_smarty_tpl->tpl_vars['branch']->value){
$_smarty_tpl->tpl_vars['branch']->_loop = true;
?>
<div class="row-fluid mb_20 mt_20 br_4 row-fluid_wrap"<?php if ($_smarty_tpl->tpl_vars['branch']->value->enable=='0'){?> style="background-color:#cccccc;"<?php }?>>
  <dl class="menu_ac">
    <dt>
      <div class="menu_cate_box">
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
        <a href="/branch/update/<?php echo $_smarty_tpl->tpl_vars['branch']->value->id;?>
" title="変更"><img src="<?php echo $_smarty_tpl->tpl_vars['branch']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="支店画像" class="mb_20 con_img"></a>
<?php }else{ ?>
        <img src="<?php echo $_smarty_tpl->tpl_vars['branch']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="支店画像" class="mb_20 con_img">
<?php }?>

        <p class="menu_table_text">
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
          <a href="/branch/update/<?php echo $_smarty_tpl->tpl_vars['branch']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['branch']->value->shop_name;?>
</a><br />
<?php }else{ ?>
          <?php echo $_smarty_tpl->tpl_vars['branch']->value->shop_name;?>
<br />

<?php }?>
        </p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
        <a href="/branch/update/<?php echo $_smarty_tpl->tpl_vars['branch']->value->id;?>
" class="gray_btn mr_5 ml_20">支店の編集</a>

        <a href="/branch/del/<?php echo $_smarty_tpl->tpl_vars['branch']->value->id;?>
" class="gray_btn mr_5"><img src="/assets/img/common/icon/i11.png" width="18" id="i11" style="margin-left:0;"></a>
<?php }?>

      </div>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
      <div class="up_down">
        <?php if (!is_null($_smarty_tpl->tpl_vars['branch']->value->file_name)&&count($_smarty_tpl->tpl_vars['list']->value)>1){?>
          <?php if ($_smarty_tpl->tpl_vars['branch']->value->position!=1){?>
          <a href="/branch/order/up/<?php echo $_smarty_tpl->tpl_vars['branch']->value->position;?>
" class="gray_btn ml_20 mb_10"><img src="/assets/img/common/icon/i16.png" width="14" style="margin-left: 0;"/ ></a>
          <?php }?>

          <?php if ($_smarty_tpl->tpl_vars['branch']->value->position<count($_smarty_tpl->tpl_vars['list']->value)){?>
          <a href="/branch/order/down/<?php echo $_smarty_tpl->tpl_vars['branch']->value->position;?>
" class="gray_btn ml_20"><img src="/assets/img/common/icon/i17.png" width="14" style="margin-left:0;"/ ></a>
          <?php }?>
        <?php }?>
      </div>
<?php }?>
      <br class="clear">
    </dt>
  </dl>
</div>

<?php } ?>

<?php }else{ ?>
  <div class="menu_list row-fluid mb_20 mt_20 br_4 row-fluid_wrap">
    <p>現在、支店のデータはありません</p>
  </div>
<?php }?>

</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>