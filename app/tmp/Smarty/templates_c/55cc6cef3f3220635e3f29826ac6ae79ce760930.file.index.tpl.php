<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-16 13:37:19
         compiled from "/home/spost/admin/app/views/coupon/del/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2134496925379e2f675ad70-61775143%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '55cc6cef3f3220635e3f29826ac6ae79ce760930' => 
    array (
      0 => '/home/spost/admin/app/views/coupon/del/index.tpl',
      1 => 1402856687,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2134496925379e2f675ad70-61775143',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5379e2f67f46a2_45930318',
  'variables' => 
  array (
    'coupon' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5379e2f67f46a2_45930318')) {function content_5379e2f67f46a2_45930318($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/coupon.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- クーポンの削除 -->
<div class="contents_box">
  <div class="contents_box_head">クーポンの削除
 <a href="/support/manual/coupon#coupon_1"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="クーポンの削除ができます。<br>「このクーポンを削除」ボタンを押して、削除して下さい。"></a>
  </div>
  <h4>クーポンの削除</h4>

  <form action="/coupon/del/exe/<?php echo $_smarty_tpl->tpl_vars['coupon']->value->id;?>
" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>タイトル</label>
      <?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('title'))===null||$tmp==='' ? "-----" : $tmp);?>

    </li>

    <li>
      <label>クーポン内容</label>
      <?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('body'))===null||$tmp==='' ? "-----" : $tmp);?>

    </li>

    <li>
      <label>利用条件</label>
      <?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('policy'))===null||$tmp==='' ? "-----" : $tmp);?>

    </li>

    <li>
      <label>期間指定</label>
      <?php echo smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['fieldset']->value->value('term_flg'),'0','指定なし'),'1','指定あり');?>

    </li>

    <?php if ($_smarty_tpl->tpl_vars['fieldset']->value->value('term_flg')=='1'){?>
    <li>
      <label>表示期間</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('start_year');?>
年<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('start_month');?>
月<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('start_day');?>
日
      〜<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('end_year');?>
年<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('end_month');?>
月<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('end_day');?>
日
    </li>
    <?php }?>
  </ul>

  <hr />

  <input type="submit" name="button" value="このクーポンを削除" class="save_btn" />
  <a href="/coupon" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>