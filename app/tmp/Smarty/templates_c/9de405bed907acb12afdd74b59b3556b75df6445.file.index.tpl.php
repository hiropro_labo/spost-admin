<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-16 13:02:38
         compiled from "/home/spost/admin/app/views/coupon/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:478189794535df5977c9bb0-53553597%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9de405bed907acb12afdd74b59b3556b75df6445' => 
    array (
      0 => '/home/spost/admin/app/views/coupon/update/index.tpl',
      1 => 1402891357,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '478189794535df5977c9bb0-53553597',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_535df59788ea13_61427468',
  'variables' => 
  array (
    'coupon' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535df59788ea13_61427468')) {function content_535df59788ea13_61427468($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/coupon.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- クーポンの編集 -->
<div class="contents_box">
  <div class="contents_box_head">クーポンの編集
　<a href="/support/manual/coupon"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="お客様に特別なクーポンを提供することが出来ます。<br>「ファイルを選択」ボタンを押して、画像をアップロード<br>利用条件、期間設定後「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>クーポンの編集</h4>

  <form action="/coupon/update/<?php echo $_smarty_tpl->tpl_vars['coupon']->value->id;?>
" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label>タイトル</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('title')->build();?>

      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('title');?>

    </li>

    <li>
      <label>クーポン内容</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('body')->build();?>

      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('body');?>

    </li>

    <li>
      <label>利用条件</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('policy')->build();?>

      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('policy');?>

    <p class="desc">実際のスマホアプリでは、クーポン画像をタップすると表示されます。<br />※左の簡易プレビューには表示されません。</p>
    </li>

    <li class="mb_20">
      <label>期間指定</label>
      <label for="form_term_flg_0"><input type="radio" required="required" id="form_term_flg_0" name="term_flg" value="0" <?php if ($_smarty_tpl->tpl_vars['fieldset']->value->value('term_flg')=='0'){?>checked="checked"<?php }?> />指定しない</label>
      <label for="form_term_flg_1"><input type="radio" required="required" id="form_term_flg_1" name="term_flg" value="1" <?php if ($_smarty_tpl->tpl_vars['fieldset']->value->value('term_flg')=='1'){?>checked="checked"<?php }?> />指定する</label>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('term_flg');?>
</p>
    </li>

    <li>
      <label>表示開始日</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('start_year')->build();?>
年<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('start_month')->build();?>
月<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('start_day')->build();?>
日
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('start_year');?>
</p>
    </li>

    <li>
      <label>表示終了日</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('end_year')->build();?>
年<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('end_month')->build();?>
月<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('end_day')->build();?>
日
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('end_year');?>
</p>
    </li>
  </ul>


  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/coupon" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>



<script type="text/javascript">
$(function(){
  if($('#form_policy').val() != "")
  {
    $('#form_policy').css({"height":"300px"});
  }
  $('#form_policy').focus(function(){
    $(this).animate({"height":"300px"}, "swing");
  });
});
</script>



<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>