<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-16 12:55:58
         compiled from "/home/spost/admin/app/views/coupon/add/confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2143181369539e6ace90d5e3-92906477%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '90f3a8520a3b05d91f449c352935e87e7610bc3c' => 
    array (
      0 => '/home/spost/admin/app/views/coupon/add/confirm.tpl',
      1 => 1402858312,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2143181369539e6ace90d5e3-92906477',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'fieldset' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539e6ace9fea30_15804117',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539e6ace9fea30_15804117')) {function content_539e6ace9fea30_15804117($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/coupon.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- クーポンの新規登録 -->
<div class="contents_box">
  <div class="contents_box_head">クーポンの新規登録</div>
  <h4>クーポンの新規登録</h4>

  <form action="/coupon/add/exe" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>タイトル</label>
      <span><?php echo nl2br((($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('title'))===null||$tmp==='' ? "-----" : $tmp));?>
</span>
    </li>

    <li>
      <label>クーポン内容</label>
      <span><?php echo nl2br((($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('body'))===null||$tmp==='' ? "-----" : $tmp));?>
</span>
    </li>

    <li>
      <label>利用条件</label>
      <span><?php echo nl2br((($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('policy'))===null||$tmp==='' ? "-----" : $tmp));?>
</span>
    </li>

    <li>
      <label>利用期間</label>
      <span><?php echo smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['fieldset']->value->value('term_flg'),'0','指定なし'),'1','指定あり');?>
</span>
    </li>

    <?php if ($_smarty_tpl->tpl_vars['fieldset']->value->value('term_flg')=='1'){?>
    <li>
      <label>表示期間</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('start_year');?>
年<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('start_month');?>
月<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('start_day');?>
日
      〜<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('end_year');?>
年<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('end_month');?>
月<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('end_day');?>
日</span>
    </li>
    <?php }?>
  </ul>

  <hr />

  
  <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldset']->value->field(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value){
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>
  <?php echo $_smarty_tpl->tpl_vars['field']->value;?>

  <?php } ?>
  
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>