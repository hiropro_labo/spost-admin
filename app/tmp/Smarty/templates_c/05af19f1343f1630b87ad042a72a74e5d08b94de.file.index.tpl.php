<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-18 12:41:55
         compiled from "/home/spost/admin/app/views/company/top/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1556743607539ffe6c2ea3e7-86932846%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '05af19f1343f1630b87ad042a72a74e5d08b94de' => 
    array (
      0 => '/home/spost/admin/app/views/company/top/update/index.tpl',
      1 => 1402962002,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1556743607539ffe6c2ea3e7-86932846',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539ffe6c338b74_71198356',
  'variables' => 
  array (
    'image' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539ffe6c338b74_71198356')) {function content_539ffe6c338b74_71198356($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/company.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<!-- 企業概要トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">企業概要トップ画像の変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="企業概要トップ画像の変更ができます。<br>「ファイルを選択」ボタンを押して<br>画像をアップロードして下さい。"></a>
  </div>
  <h4>企業概要トップ画像の変更</h4>

  <form action="/company/top/update" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">画像のアップロード</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value->image_path();?>
?<?php echo time();?>
" width="160" height="70" alt="企業概要トップ画像" class="form_img con_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />640px&nbsp;×&nbsp;280px&nbsp;以上の大きさを推奨</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/company" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>