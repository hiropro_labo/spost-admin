<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-16 19:21:14
         compiled from "/home/spost/admin/app/views/preview/top.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6641009455356043b1ab5d5-74373334%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4b0c3ed28a029c8d23970cb6f90e8d3afba8eec7' => 
    array (
      0 => '/home/spost/admin/app/views/preview/top.tpl',
      1 => 1400235661,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6641009455356043b1ab5d5-74373334',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5356043b211ff4_35945207',
  'variables' => 
  array (
    'image_list' => 0,
    'image' => 0,
    'topic_message' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5356043b211ff4_35945207')) {function content_5356043b211ff4_35945207($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("preview/common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- ▼コンテンツ -->
<div id="p_content_wrap" class="p_top">

<!-- カルーセル -->
<div class="p_top_carousel">
  <div id="p_slides">
<?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['image_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value){
$_smarty_tpl->tpl_vars['image']->_loop = true;
?>
    <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value->image_path();?>
?<?php echo time();?>
" width="225" height="148" alt="トップ画像" />
<?php } ?>
  </div>
  <div id="p_paging" class="p_pagination"></div>
</div>
<!--  -->

<!-- 3つのボタン -->
<div class="p_top_btns clearfix">
  <div class="p_t_b_left_box">
    <div class="p_t_b_left p_btns_box">
      <a href="javascript:void(0);"><img src="/assets/preview/img/topButton/ipost_main_map.png?<?php echo time();?>
" width="61" height="40" alt="トップメニューボタン1" /></a>
    </div>
    <div class="p_t_b_center p_btns_box">
      <a href="/preview/shopdetail"><img src="/assets/preview/img/topButton/ipost_main_shop.png?<?php echo time();?>
" width="61" height="40" alt="トップメニューボタン2" /></a>
    </div>
  </div>
  <div class="p_t_b_right p_btns_box">
    <a href="/preview/onlineshop"><img src="/assets/preview/img/topButton/ipost_main_store.png?<?php echo time();?>
" width="61" height="40" alt="トップメニューボタン3" /></a>
  </div>
</div>
<!--  -->

<!-- Facebook -->
<div class="p_top_url">
  <a href="/preview/webview">ホームページへ</a>
</div>
<!--  -->

<!-- マーキー -->
<div class="p_top_marquee">
  <marquee width="90%" style="margin-left:5%;"><?php echo $_smarty_tpl->tpl_vars['topic_message']->value;?>
</marquee>
</div>
<!--  -->

</div>
<!-- ▲ -->



<script type="text/javascript">
$("#p_slides").carouFredSel({
    circular: false,
    infinite: false,
    auto    : false,
    pagination  : "#p_paging"
});
</script>



<?php echo $_smarty_tpl->getSubTemplate ("preview/common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>