<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-03 14:41:00
         compiled from "/home/spost/admin/app/views/company/name/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:176352484353b4ec70143397-58393440%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f3493db62d5686396e94310be07c7234137b23f4' => 
    array (
      0 => '/home/spost/admin/app/views/company/name/update/index.tpl',
      1 => 1404366054,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '176352484353b4ec70143397-58393440',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53b4ec701d0f10_05642757',
  'variables' => 
  array (
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53b4ec701d0f10_05642757')) {function content_53b4ec701d0f10_05642757($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/company.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<div class="contents_box">
  <div class="contents_box_head">企業名の変更
    <a href="/support/manual/store#store_1"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="アプリストア(App Store/Google Play)表示項目を編集することができます。<br>フォームに入力した後、「確認する」ボタンを押してください。"></a></div>
  <h4>企業名はこちらで編集できます</h4>

  <form action="/company/name/update" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label for="zip1" class="hisu">日本語</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('name_ja')->build();?>

      <p class="desc">最大全角255文字</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('name_ja');?>
</p>
    </li>

    <li>
      <label for="pref">英語</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('name_en')->build();?>

      <p class="desc">最大全角255文字</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('name_en');?>
</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/company" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>