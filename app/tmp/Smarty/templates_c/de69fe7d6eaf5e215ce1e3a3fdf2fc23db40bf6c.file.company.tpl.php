<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-17 15:47:06
         compiled from "/home/spost/admin/app/views/common/footer_meta/company.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1530659786539edc54556295-63661169%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'de69fe7d6eaf5e215ce1e3a3fdf2fc23db40bf6c' => 
    array (
      0 => '/home/spost/admin/app/views/common/footer_meta/company.tpl',
      1 => 1402987624,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1530659786539edc54556295-63661169',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539edc5455ba07_53073848',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539edc5455ba07_53073848')) {function content_539edc5455ba07_53073848($_smarty_tpl) {?>
<script type="text/javascript">
function update(idx, obj){
    var button = $(obj);
    button.attr("disabled", true);

    var data = {
        title: $("#title" + idx).val(),
        body: $("#body" + idx).val()
    };

    $.ajax({
    	type: "post",
    	url: "/company/update/" + idx,
    	contentType: "application/json",
    	dataType: 'json',
    	data: JSON.stringify(data),
    	success: function(res){
    		if (!res[0]){
    			$("#error_msg" + idx).empty().append("更新処理に失敗しました<br />" + res[1]);
    		}else{
    		    location.reload();
    		}
    	},
        error: function(){
        	$("#error_msg" + idx).empty().append("更新処理に失敗しました[通信エラー発生]");
        },
        complete: function(){
        	button.attr("disabled", false);
        }
    });
}

function add(obj){
	var button = $(obj);
	button.attr("disabled", true);

	var data = {
		title: $("#add_title").val(),
	    body: $("#add_body").val()
	};

	$.ajax({
		type: "post",
		url: "/company/add",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(data),
		success: function(res){
			if (!res[0]){
				$("#error_msg_add").empty().append("更新処理に失敗しました<br />" + res[1]);
			}else{
				location.reload();
			}
		},
		error: function(){
			$("#error_msg_add").empty().append("更新処理に失敗しました[通信エラー発生]");
		},
		complete: function(){
			button.attr("disabled", false);
		}
	});
}

function del(idx, obj){
    if (!confirm("指定された項目を削除します\nよろしいですか？")) return false;

    var button = $(obj);
	button.attr("disabled", true);

    $.ajax({
    	type: "post",
    	url: "/company/del/" + idx,
    	success: function(res){
    		if (!res[0]){
    			$("#error_msg" + idx).empty().append("削除処理に失敗しました<br />" + res[1]);
    		}else{
    			location.reload();
    		}
    	},
        error: function(){
        	$("#error_msg" + idx).empty().append("更新処理に失敗しました[通信エラー発生]");
        },
        complete: function(){
        	button.attr("disabled", false);
        }
    });
}
</script>
<?php }} ?>