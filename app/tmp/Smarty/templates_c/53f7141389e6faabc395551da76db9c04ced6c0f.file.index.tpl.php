<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-26 14:08:58
         compiled from "/home/spost/admin/app/views/store/icon/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11889036635382cc6a05b4b6-79804570%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '53f7141389e6faabc395551da76db9c04ced6c0f' => 
    array (
      0 => '/home/spost/admin/app/views/store/icon/update/index.tpl',
      1 => 1401048460,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11889036635382cc6a05b4b6-79804570',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image_icon' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5382cc6a0a4194_59936223',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5382cc6a0a4194_59936223')) {function content_5382cc6a0a4194_59936223($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/store.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">アプリ情報<a href="/support/manual/store"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playに表示される<br>アイコン画像を登録する事ができます。「ファイルを選択」ボタンを押して<br>アイコン画像をアップロードして下さい。"></a></div>
  <h4>アプリアイコン画像の変更</h4>

  <form action="/store/icon/update/" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">画像のアップロード</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['image_icon']->value->image_path();?>
" width="160" height="160" alt="トップ画像" class="con_img form_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />1024px x 1024px 以上の大きさを推奨<br /><span>※1度審査申請したアプリアイコンは変更することができませんのでご注意下さい。</span></p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/store" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>