<?php /* Smarty version Smarty-3.1-DEV, created on 2014-04-30 16:10:54
         compiled from "/home/spost/admin/app/views/pop/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12598038705360a1febcfaf2-24601043%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0c6befda825d1ca23c0f7c589469a2d7eaf19aac' => 
    array (
      0 => '/home/spost/admin/app/views/pop/index.tpl',
      1 => 1398068561,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12598038705360a1febcfaf2-24601043',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5360a1fec0f2b7_50193258',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5360a1fec0f2b7_50193258')) {function content_5360a1fec0f2b7_50193258($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/pop.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- ポスター・ポップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">ポスター&nbsp;A3サイズ</div>
  <div>
    <img src="/assets/img/common/pop/poster_a3.jpg" width="284" height="100%" alt="ポスター A3サイズ" class="ml_30"/>
  </div>
  <div>
    <ul>
      <li><label>用紙規格</label><span>A3サイズ</span></li>
      <li><label>用紙サイズ</label><span>297mm x 420mm</span></li>
    </ul>
    <a href="/assets/pdf/pop/pop_order.pdf" class="download_btn" title="ポスター A3サイズ" target="_brank">注文はこちら</a>
    <span class="tyui">※&nbsp;pdfファイルになります。</span>
    <span class="tyui">ポスター・ポップは、アート印刷株式会社が販売しています。</span>
  </div>
  <div class="clear"></div>
</div>


<div class="contents_box">
  <div class="contents_box_head">ポスター&nbsp;A4サイズ</div>
  <div>
    <img src="/assets/img/common/pop/poster_a4.jpg" width="284" height="100%" alt="ポスター A4サイズ" class="ml_30"/>
  </div>
  <div>
    <ul>
      <li><label>用紙規格</label><span>A4サイズ</span></li>
      <li><label>用紙サイズ</label><span>210mm x 297mm</span></li>
    </ul>
    <a href="/assets/pdf/pop/pop_order.pdf" class="download_btn" title="ポスター A4サイズ" target="_brank">注文はこちら</a>
    <span class="tyui">※&nbsp;pdfファイルになります。</span>
    <span class="tyui">ポスター・ポップは、アート印刷株式会社が販売しています。</span>
    
  </div>
  <div class="clear"></div>
</div>



<div class="contents_box">
  <div class="contents_box_head">三角POP</div>
  <div>
    <img src="/assets/img/common/pop/triangle_pop.jpg" width="284" height="100%" alt="三角POP" class="ml_30"/>
</div>
  <div>
    <ul>
      <li><label>用紙規格</label><span>A3サイズ</span></li>
      <li><label>用紙サイズ</label><span>&nbsp;</span></li>
      <li><label>組み立て前</label><span>100mm x 160mm</span></li>
      <li><label>組み立て後</label><span>306mm x 160mm</span></li>
    </ul>
    <a href="/assets/pdf/pop/pop_order.pdf" class="download_btn" title="三角POP" target="_brank">注文はこちら</a>
    <span class="tyui">※&nbsp;pdfファイルになります。</span>
    <span class="tyui">ポスター・ポップは、アート印刷株式会社が販売しています。</span>
  </div>
<div class="clear"></div>
</div>


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>