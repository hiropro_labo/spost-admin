<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-29 23:17:06
         compiled from "/home/spost/admin/app/views/common/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9606372505356043a6aa418-80314772%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'aa89dfacf5164b8c51db6e96a1449919ece7b8ca' => 
    array (
      0 => '/home/spost/admin/app/views/common/header.tpl',
      1 => 1404051385,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9606372505356043a6aa418-80314772',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5356043a6dc5f0_01929543',
  'variables' => 
  array (
    'current_page' => 0,
    'not_preview' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5356043a6dc5f0_01929543')) {function content_5356043a6dc5f0_01929543($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<title><?php echo __('site.title');?>
</title>

<!-- Favicon Icon -->
<link href="/assets/img/common/favicon/ipost.png" rel="shortcut icon" type="image/png" />

<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.tutorial.css">
<link rel="stylesheet" type="text/css" href="/assets/css/bass.css">
<link rel="stylesheet" type="text/css" href="/assets/css/path/sub.css">
<link rel="stylesheet" type="text/css" href="/assets/css/tooltipster.css">


<?php echo $_smarty_tpl->getSubTemplate ("common/load_css.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<script src="/assets/js/jquery-1.8.0.min.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery-ui.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/bootstrap.min.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.contenthover.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.dropdown.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.tooltipster.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.cookie.js" type="text/javascript" language="javascript"></script>


<?php echo $_smarty_tpl->getSubTemplate ("common/load_js.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


</head>


<body>

<?php echo $_smarty_tpl->getSubTemplate ("common/content_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/pan/index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<!--▼コンテナ-->
<div id="container">

<?php if (!(in_array($_smarty_tpl->tpl_vars['current_page']->value,$_smarty_tpl->tpl_vars['not_preview']->value))){?>
  <!-- モーダルウィンドウ本体 -->
  <div id="dialog-preview" title="アプリプレビュー">
    <iframe src="/preview/<?php echo $_smarty_tpl->tpl_vars['current_page']->value;?>
" id="preview" name="preview" width="300" height="630">
     この部分はインラインフレームを使用しています。
    }
    </iframe>
<?php if ($_smarty_tpl->tpl_vars['current_page']->value=='store'||$_smarty_tpl->tpl_vars['current_page']->value=='play'){?>
    <div class="preview_radio_group">
      <input id="previewStore" type="radio" name="check[]" value="0" onclick="previewChange(0);" checked="checked" />
      <label id="label_play" for="previewStore">App Store</label>

      <input id="previewPlay" type="radio" name="check[]" value="1" onclick="previewChange(1);" />
      <label id="label_store" for="previewPlay">Google Play</label>
    </div>
<?php }?>
  </div>
  <!---->

  <!--コンテンツ-->
  <div id="contents">
<?php }else{ ?>
  <!--コンテンツ-->
  <div id="contents" class="no_preview">
<?php }?>



<?php }} ?>