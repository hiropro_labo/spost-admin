<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-28 15:59:08
         compiled from "/home/spost/admin/app/views/recruit/title/add/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:206094977553a141ff40ad11-15809742%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '100009924720f819f6d1cc2a918539a4ab36e07c' => 
    array (
      0 => '/home/spost/admin/app/views/recruit/title/add/index.tpl',
      1 => 1403044749,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '206094977553a141ff40ad11-15809742',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a141ff462097_91905199',
  'variables' => 
  array (
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a141ff462097_91905199')) {function content_53a141ff462097_91905199($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/recruit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">採用情報の新規作成
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="カテゴリーの新規作成ができます。<br>「ファイルを選択」ボタンを押して、画像をアップロード<br>テキストの記入、表示・非表示選択後<br>「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>採用情報の新規作成</h4>

  <form action="/recruit/title/add" method="POST" name="form1" id="form1" class="form1">

  <div class="contents_form">

    <ul>
      <li>
        <label for="name" class="hisu">タイトル</label>
        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('title')->build();?>

        <p class="desc">例）[正][ア][パ]キッチンスタッフ・ホールスタッフ</p>
        <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('title');?>
</p>
      </li>
    </ul>
    <ul>
      <li>
        <label>表示設定</label>
        <label for="form_enable_1"><input type="radio" required="required" value="1" id="form_enable_1" name="enable" checked="checked" />表示</label>
        <label for="form_enable_0"><input type="radio" required="required" value="0" id="form_enable_0" name="enable" />非表示</label>
        <p class="desc">お客様に見せるかどうかを選ぶことができます。</p>
        <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('enable');?>
</p>
      </li>
    </ul>

    <hr />

    <input type="submit" name="button" value="変更の確認" class="save_btn" />
    <a href="/recruit" id="save_btn" class="back_btn">戻る</a>
  </div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>