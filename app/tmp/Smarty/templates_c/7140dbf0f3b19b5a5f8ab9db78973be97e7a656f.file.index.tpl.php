<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-23 14:12:24
         compiled from "/home/spost/admin/app/views/menu/item/del/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:513933404537ed8b8e78fa0-33875164%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7140dbf0f3b19b5a5f8ab9db78973be97e7a656f' => 
    array (
      0 => '/home/spost/admin/app/views/menu/item/del/index.tpl',
      1 => 1400784661,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '513933404537ed8b8e78fa0-33875164',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item' => 0,
    'category' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_537ed8b8f0db53_58606623',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537ed8b8f0db53_58606623')) {function content_537ed8b8f0db53_58606623($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- メニュートップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">商品の削除
    <a href="/support/manual/menu#menu_4" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="カテゴリー商品の削除ができます。<br>「削除」ボタンを押して、削除して下さい。"></a>
  </div>
  <h4>商品の削除</h4>

  <form action="/menu/item/del/exe/<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label>メニュートップ画像</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['item']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="メニュートップ画像" class="mb_20 con_img" />
    </li>

    <li>
      <label>カテゴリー</label>
      <span><?php echo $_smarty_tpl->tpl_vars['category']->value->title;?>
</span>
    </li>

    <li>
      <label>メニュー名</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('title');?>
</span>
    </li>

    <li>
      <label>サブタイトル名</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('sub_title');?>
</span>
    </li>

    <li>
      <label>メニュー説明</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('description');?>
</span>
    </li>

    <li>
      <label>価格</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('price');?>
</span>
    </li>

    <li>
      <label>URL</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('url');?>
</span>
    </li>

    <li>
      <label>&nbsp;</label>
      <span><?php echo smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['fieldset']->value->value('enable'),'0','非表示'),'1','表示');?>
</span>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="削除" class="save_btn" />
  <a href="/menu" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>