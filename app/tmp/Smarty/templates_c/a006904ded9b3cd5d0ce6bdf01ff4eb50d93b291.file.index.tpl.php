<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-28 16:13:11
         compiled from "/home/spost/admin/app/views/segments/text/work/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:187632120953a01a281c0819-98259792%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a006904ded9b3cd5d0ce6bdf01ff4eb50d93b291' => 
    array (
      0 => '/home/spost/admin/app/views/segments/text/work/update/index.tpl',
      1 => 1403939507,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '187632120953a01a281c0819-98259792',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a01a2821b8d1_03144147',
  'variables' => 
  array (
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a01a2821b8d1_03144147')) {function content_53a01a2821b8d1_03144147($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/segments.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">Workのテキスト編集
　<a href="/support/manual/coupon"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title=""></a>
  </div>
  <h4>Workのテキスト編集</h4>

  <form action="/segments/text/work/update" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

  <div class="contents_form">
    <ul>
      <li>
        <label>タイトル</label>
        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('title_work')->build();?>

        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('title_work');?>

      </li>
      <li>
        <label>本文</label>
        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('body_work')->build();?>

        <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('body_work');?>

      </li>
    </ul>
    <hr />
    <input type="submit" name="button" value="変更の確認" class="save_btn" />
    <a href="/segments" id="save_btn" class="back_btn">戻る</a>
  </div>

  </form>
</div>

<div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>