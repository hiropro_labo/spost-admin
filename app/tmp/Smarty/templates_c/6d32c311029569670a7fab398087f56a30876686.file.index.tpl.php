<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-26 20:22:14
         compiled from "/home/spost/admin/app/views/shop/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:44774548153ac0266ca9f72-97772443%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6d32c311029569670a7fab398087f56a30876686' => 
    array (
      0 => '/home/spost/admin/app/views/shop/index.tpl',
      1 => 1403749245,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '44774548153ac0266ca9f72-97772443',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'SPONSOR' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53ac0266df10f1_00317334',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53ac0266df10f1_00317334')) {function content_53ac0266df10f1_00317334($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/shop.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<!-- お店の写真の変更 -->
<div class="contents_box" id="shop_info">
  <div class="contents_box_head">
    お店の情報・写真の変更
    <a href="/support/manual/top#top_3"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="電話番号や営業時間などの、お店の情報を変更する事ができます。<br>「お店の画像」には、あなたのお店の魅力が伝わる写真をアップロードしましょう。。"></a>
  </div>

  <h4>お店の写真の変更</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span></p>
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/top/img/shop/update" class="edit_btn">編集する</a>
  <a href="/top/img/shop/update"><img src="<?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop()->image_path();?>
?<?php echo time();?>
" width="160" alt="トップ画像" class="mb_20 con_img" /></a>
<?php }else{ ?>
  <img src="<?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop()->image_path();?>
?<?php echo time();?>
" width="160" alt="トップ画像" class="mb_20 con_img" />
<?php }?>
  <hr />

  <h4>お店の情報</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span><br />
  お店の名前が必要になります。</p>
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/top/shop/update" class="edit_btn">編集する</a>
<?php }?>
  <ul class="cel">
    <li><label>お店の名前</label><span><?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->shop_name();?>
</span></li>
    <li><label>郵便番号</label><span><?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->zip_code1();?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->zip_code2();?>
</span></li>
    <li><label>住所</label><span><?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->pref_name();?>
 <?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->city();?>
 <?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->address_opt1();?>
 <br /><?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->address_opt2();?>
</span></li>
    <li><label>電話番号</label><span><?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->tel1();?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->tel2();?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->tel3();?>
</span></li>
    <li><label>FAX</label><span><?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->fax1();?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->fax2();?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->fax3();?>
</span></li>
    <li><label>メールアドレス</label><span><?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->email();?>
</span></li>
    <li><label>ホームページアドレス</label><a href="<?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->url();?>
" target="_blank"><span style="color:#08C;"><?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->url();?>
</span></a></li>
    <li><label>オンラインショップ</label><a href="<?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->online_shop();?>
" target="_blank"><span style="color:#08C;"><?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->online_shop();?>
</span></a></li>
    <li><label>営業時間</label><span><?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->open_hours();?>
</span></li>
    <li><label>定休日</label><span><?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop_profile()->holiday();?>
</span></li>
  </ul>
</div>

<div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>