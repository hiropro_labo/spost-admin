<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-05 21:26:02
         compiled from "/home/spost/admin/app/views/inspect/index_pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13713661965382f97d6e4626-01468805%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '05f944279a5d4f7c617f35568e109f3017b1ef4d' => 
    array (
      0 => '/home/spost/admin/app/views/inspect/index_pre.tpl',
      1 => 1404563139,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13713661965382f97d6e4626-01468805',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5382f97d7326b3_49507959',
  'variables' => 
  array (
    'SPONSOR' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5382f97d7326b3_49507959')) {function content_5382f97d7326b3_49507959($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header_inspect.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="container">
    <div id="contents" class="no_preview">

        <?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/inspect.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


        <div class="inspect_text_box">作業が完了した箇所は灰色に塗り潰されます。<br />全て灰色になればアプリ制作完了となり、公開のための申請を行うことができます。</div>

        <div class="inspect_item_wrap clearfix">
            <?php echo $_smarty_tpl->getSubTemplate ("inspect/pre/category".((string)$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->theme()->model()->category).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

        </div>

        <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->app()->inspect()->is_inspect_allow()){?>
        <p style="text-align: center; color: #cd4538; font-weight: bold;">※※※　ご注意　※※※<br /><br />
            審査申請ボタンを押されますと、アプリ管理の項目変更にロックが掛かります。<br />
            ロックが掛かりますと、各項目の変更ができなくなります。</p>
        <?php }?>

        <div class="btn_box">
        <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->app()->inspect()->is_inspect_allow()){?>
            <a href="javascript: void(0);" id="smb" class="blue_btn">アプリ申請をする</a>
        <?php }else{ ?>
            <p class="glayout_btn">アプリ申請をする</p>
        <?php }?>
        </div>

        <form id="frm" action="/inspect" method="POST"></form>
    </div>
</div>

<div class="last_margin"></div>


<script type="text/javascript">
$(function(){
	$('#smb').click(function(){
		$('#frm').submit();
	});
});
</script>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>