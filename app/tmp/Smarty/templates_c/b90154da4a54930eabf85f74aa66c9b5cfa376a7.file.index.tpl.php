<?php /* Smarty version Smarty-3.1-DEV, created on 2014-04-25 18:56:17
         compiled from "/home/spost/admin/app/views/top/img/top/delete/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1678836942535a3141e0ea41-12273464%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b90154da4a54930eabf85f74aa66c9b5cfa376a7' => 
    array (
      0 => '/home/spost/admin/app/views/top/img/top/delete/index.tpl',
      1 => 1398068965,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1678836942535a3141e0ea41-12273464',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'position' => 0,
    'top_image' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_535a3141e5de48_62088983',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535a3141e5de48_62088983')) {function content_535a3141e5de48_62088983($_smarty_tpl) {?>トップ画像の削除<?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- トップ画像の削除 -->
<div class="contents_box">
  <div class="contents_box_head">
  トップ画像の削除
　<a href="/support/manual/top#top_1"  target="_blank"><img src="/assets/img/common/help_tips.png" 
class="tooltip" title="アプリのトップ画像を削除する事ができます。<br>「この画像を削除」ボタンを押し、削除して下さい。"></a>
  </div>

  <h4>トップ画像の削除</h4>

  <form action="/top/img/top/delete/exe/<?php echo $_smarty_tpl->tpl_vars['position']->value;?>
" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>トップ画像</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['top_image']->value->image_path();?>
?<?php echo time();?>
" width="160" height="105" alt="トップ画像" class="mb_20 con_img" />
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="この画像を削除" class="save_btn" />
  <a href="/top" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>