<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-07 15:44:37
         compiled from "/home/spost/admin/app/views/url/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1501694511539ea463bf7fa9-38953455%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '043c000d4999a703b4a31513ae9c254d9279e573' => 
    array (
      0 => '/home/spost/admin/app/views/url/index.tpl',
      1 => 1404715476,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1501694511539ea463bf7fa9-38953455',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539ea463cce8f2_52391972',
  'variables' => 
  array (
    'SPONSOR' => 0,
    'model' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539ea463cce8f2_52391972')) {function content_539ea463cce8f2_52391972($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/url.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">
    スライドメニューに表示するURLの変更
    <a href="/support/manual/top#top_3"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="電話番号や営業時間などの、お店の情報を変更する事ができます。<br>「お店の画像」には、あなたのお店の魅力が伝わる写真をアップロードしましょう。。"></a>
  </div>

  <h4>お店の情報</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span><br />
  お店の名前が必要になります。</p>
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/url/update" class="edit_btn">編集する</a>
<?php }?>
  <ul class="cel mt_10">
    <?php if (is_null($_smarty_tpl->tpl_vars['model']->value)){?>
    <li><label></label><span>現在、URLは登録されていません</span></li>
    <?php }else{ ?>
    <li class="c_00"><label><img src="/assets/img/common/icon/i20.png" class="mr_5 va_m"><a href="<?php echo $_smarty_tpl->tpl_vars['model']->value->url1;?>
" target="_blank" class="va_m"><?php echo $_smarty_tpl->tpl_vars['model']->value->url_title1;?>
</a></label></li>
    <li class="c_00"><label><img src="/assets/img/common/icon/i20.png" class="mr_5 va_m"><a href="<?php echo $_smarty_tpl->tpl_vars['model']->value->url2;?>
" target="_blank" class="va_m"><?php echo $_smarty_tpl->tpl_vars['model']->value->url_title2;?>
</a></label></li>
    <li class="c_00"><label><img src="/assets/img/common/icon/i20.png" class="mr_5 va_m"><a href="<?php echo $_smarty_tpl->tpl_vars['model']->value->url3;?>
" target="_blank" class="va_m"><?php echo $_smarty_tpl->tpl_vars['model']->value->url_title3;?>
</a></label></li>
    <?php }?>
  </ul>
</div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>