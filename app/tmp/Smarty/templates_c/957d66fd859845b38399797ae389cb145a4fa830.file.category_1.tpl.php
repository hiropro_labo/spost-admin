<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-27 12:12:03
         compiled from "/home/spost/admin/app/views/switcher/options/category_1.tpl" */ ?>
<?php /*%%SmartyHeaderCode:156363878053ac25d342c391-84385005%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '957d66fd859845b38399797ae389cb145a4fa830' => 
    array (
      0 => '/home/spost/admin/app/views/switcher/options/category_1.tpl',
      1 => 1403838712,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '156363878053ac25d342c391-84385005',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53ac25d3452f54_70098970',
  'variables' => 
  array (
    'SPONSOR' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53ac25d3452f54_70098970')) {function content_53ac25d3452f54_70098970($_smarty_tpl) {?>  <!--オプション選択-->
  <div class="option_choose mt_40 clearfix">
    <h3>オプション選択</h3>
    <p class="text_sub">必要に応じて、アプリ内にお好きなページを追加することができます。</p>

    <div class="box">
      <label><input type="checkbox" id="check1" value="branch" <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->switcher()->is_active("branch")){?>checked<?php }?>>
      <img src="/assets/img/app/spost/option/o_list2.png"></label>
      <div class="box_right">
        <h4>多店舗</h4>
        <p>チェーン店や複数の店舗がある方向けの機能です。</p>
      </div>
    </div>

    <div class="box">
      <label><input type="checkbox" id="check2" value="coupon" <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->switcher()->is_active("coupon")){?>checked<?php }?>>
      <img src="/assets/img/app/spost/option/o_list3.png"></label>
      <div class="box_right">
        <h4>クーポン</h4>
        <p>ユーザーに向けてクーポンを発行することができます。</p>
      </div>
    </div>

    <div class="box">
      <label><input type="checkbox" id="check3" value="recruit" <?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->switcher()->is_active("recruit")){?>checked<?php }?>>
      <img src="/assets/img/app/spost/option/o_list4.png"></label>
      <div class="box_right">
        <h4>採用情報</h4>
        <p>採用情報のページをアプリに追加します。</p>
      </div>
    </div>

  </div>
  <!--/オプション選択-->
<?php }} ?>