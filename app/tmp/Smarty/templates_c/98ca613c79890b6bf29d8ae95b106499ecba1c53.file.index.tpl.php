<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-09 11:36:22
         compiled from "/home/spost/admin/app/views/goods/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1883553858539acb5cb4e226-08194235%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '98ca613c79890b6bf29d8ae95b106499ecba1c53' => 
    array (
      0 => '/home/spost/admin/app/views/goods/index.tpl',
      1 => 1404873380,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1883553858539acb5cb4e226-08194235',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539acb5cd20547_52358209',
  'variables' => 
  array (
    'SPONSOR' => 0,
    'image' => 0,
    'list' => 0,
    'entry' => 0,
    'goods' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539acb5cd20547_52358209')) {function content_539acb5cd20547_52358209($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/goods.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- 商品トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">商品トップ画像の変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="商品トップ画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>商品トップ画像の変更</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/goods/top/update" class="edit_btn">変更する</a>
  <a href="/goods/top/update" title="変更"><img src="<?php echo $_smarty_tpl->tpl_vars['image']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="商品トップ画像" class="mb_20 con_img" /></a>
<?php }else{ ?>
  <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value->image_path();?>
" width="160" height="88" alt="商品トップ画像" class="mb_20 con_img" />
<?php }?>
</div>
<!---->


<!-- カテゴリーの編集 -->
<div class="contents_box">
  <div class="contents_box_head">カテゴリーの編集
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="カテゴリーが追加できます。<br>「カテゴリーの新規登録」ボタンを押して、カテゴリーを追加して下さい。"></a>
  </div>
  <h4>カテゴリーの編集</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span><br />最低、１個のカテゴリーと、１個の商品は必要になっております。<br />
  <span class="hisu">商品の登録はカテゴリーの登録後、「商品作成」を押して下さい。</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/goods/category/add" class="edit_btn mt_m40">カテゴリーの新規登録</a>
<?php }?>
</div>



<div class="contents_box">
  <div class="contents_box_head">商品一覧
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="カテゴリー商品の詳細が作成できます。<br>商品の魅力が伝わる画像と内容をご紹介しましょう。"></a>
  </div>
  <h4>商品一覧</h4>

<?php if (!is_null($_smarty_tpl->tpl_vars['list']->value)&&count($_smarty_tpl->tpl_vars['list']->value)>0){?>

<?php  $_smarty_tpl->tpl_vars['entry'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['entry']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['entry']->key => $_smarty_tpl->tpl_vars['entry']->value){
$_smarty_tpl->tpl_vars['entry']->_loop = true;
?>
<div class="row-fluid mb_20 mt_20 br_4 row-fluid_wrap"<?php if ($_smarty_tpl->tpl_vars['entry']->value['category']->enable==0){?> style="background-color:#cccccc;"<?php }?>>
  <dl class="menu_ac">
    <dt>
      <div class="menu_cate_box">
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
        <a href="/goods/category/update/<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->id;?>
" title="変更"><img src="<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="商品カテゴリー画像" class="mb_20 con_img"></a>
<?php }else{ ?>
        <img src="<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="商品カテゴリー画像" class="mb_20 con_img">
<?php }?>

        <p class="menu_table_text">
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
          <a href="/goods/category/update/<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->id;?>
"><?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->title;?>
</a><br />
<?php }else{ ?>
          <?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->title;?>
<br />

<?php }?>
          <?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->sub_title;?>

        </p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
        <a href="/goods/category/update/<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->id;?>
" class="gray_btn mr_5 ml_20">カテゴリの編集</a>

        <a href="/goods/category/del/<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->id;?>
" class="gray_btn mr_5"><img src="/assets/img/common/icon/i11.png" width="18" id="i11" style="margin-left:0;"></a>
<?php }?>

        <a href="javascript:void(0);" class="blue_btn">商品作成</a>
      </div>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
      <div class="up_down">
        <?php if (!is_null($_smarty_tpl->tpl_vars['entry']->value['category']->file_name)&&count($_smarty_tpl->tpl_vars['list']->value)>1){?>
          <?php if ($_smarty_tpl->tpl_vars['entry']->value['category']->position!=1){?>
          <a href="/goods/category/order/up/<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->position;?>
" class="gray_btn ml_20 mb_10"><img src="/assets/img/common/icon/i16.png" width="14" style="margin-left: 0;"/ ></a>
          <?php }?>

          <?php if ($_smarty_tpl->tpl_vars['entry']->value['category']->position<count($_smarty_tpl->tpl_vars['list']->value)){?>
          <a href="/goods/category/order/down/<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->position;?>
" class="gray_btn ml_20"><img src="/assets/img/common/icon/i17.png" width="14" style="margin-left:0;"/ ></a>
          <?php }?>
        <?php }?>
      </div>
<?php }?>
      <br class="clear">
    </dt>

    
    <dd>
      <?php  $_smarty_tpl->tpl_vars['goods'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['goods']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['entry']->value['goods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['goods']->key => $_smarty_tpl->tpl_vars['goods']->value){
$_smarty_tpl->tpl_vars['goods']->_loop = true;
?>
      <div class="menu_inline">
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
        <a href="/goods/item/update/<?php echo $_smarty_tpl->tpl_vars['goods']->value->id;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['goods']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="商品商品画像" class="mb_20 con_img" /></a>
<?php }else{ ?>
        <img src="<?php echo $_smarty_tpl->tpl_vars['goods']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="商品商品画像" class="mb_20 con_img" />
<?php }?>

        <p class="menu_table_text">
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
          <a href="/goods/item/update/<?php echo $_smarty_tpl->tpl_vars['goods']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['goods']->value->title;?>
</a><br />
<?php }else{ ?>
          <?php echo $_smarty_tpl->tpl_vars['goods']->value->title;?>
<br />
<?php }?>
          <?php echo $_smarty_tpl->tpl_vars['goods']->value->sub_title;?>

        </p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
        <a href="/goods/item/update/<?php echo $_smarty_tpl->tpl_vars['goods']->value->id;?>
" class="gray_btn mr_5 ml_20">商品の編集</a>

        <a href="/goods/item/del/<?php echo $_smarty_tpl->tpl_vars['goods']->value->id;?>
" class="gray_btn mr_5"><img src="/assets/img/common/icon/i11.png" width="18" style="margin-left:0px;"/></a>
        <a href="javascript:pickup('<?php echo $_smarty_tpl->tpl_vars['goods']->value->id;?>
');" id="pickup_<?php echo $_smarty_tpl->tpl_vars['goods']->value->id;?>
" class="gray_btn"><?php if ($_smarty_tpl->tpl_vars['goods']->value->pickup=='0'){?>☆<?php }else{ ?>★<?php }?></a>
<?php }?>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
        <div class="up_down" id="up_down_inside">
        <?php if (!is_null($_smarty_tpl->tpl_vars['goods']->value->file_name)&&$_smarty_tpl->tpl_vars['entry']->value['category']->goods_max_cnt>1){?>
          <?php if ($_smarty_tpl->tpl_vars['goods']->value->position!=1){?>
          <a href="/goods/item/order/up/<?php echo $_smarty_tpl->tpl_vars['goods']->value->position;?>
/<?php echo $_smarty_tpl->tpl_vars['goods']->value->parent_id;?>
" class="gray_btn ml_20 mb_10"><img src="/assets/img/common/icon/i16.png" width="14" style="margin-left:0;"></a>
          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['goods']->value->position<$_smarty_tpl->tpl_vars['entry']->value['category']->goods_max_cnt){?>
          <a href="/goods/item/order/down/<?php echo $_smarty_tpl->tpl_vars['goods']->value->position;?>
/<?php echo $_smarty_tpl->tpl_vars['goods']->value->parent_id;?>
" class="gray_btn ml_20"><img src="/assets/img/common/icon/i17.png" width="14" style="margin-left:0;"></a>
          <?php }?>
        <?php }?>
        </div>
<?php }?>
      </div>
      <?php } ?>
      <div class="clear"></div>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
      <div class="menu_inline add_box">
        <a href="/goods/item/add/<?php echo $_smarty_tpl->tpl_vars['entry']->value['category']->id;?>
" class="blue_btn add_btn" id="new_edit">商品の追加</a>
      </div>
<?php }?>
    </dd>
  </dl>

  <div class="clear"></div>
</div>


<?php } ?>

<?php }else{ ?>
  <div class="menu_list row-fluid mb_20 mt_20 br_4 row-fluid_wrap">
    <p>現在、カテゴリー、商品の登録がされていません。</p>
  </div>
<?php }?>

</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer_meta/goods.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>