<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-07 14:15:20
         compiled from "/home/spost/admin/app/views/news/del/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:843287008537b306e1da6e7-79444922%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '81325f682a50522b3bcfaca1ce4bae23c7ffa6a7' => 
    array (
      0 => '/home/spost/admin/app/views/news/del/index.tpl',
      1 => 1404708948,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '843287008537b306e1da6e7-79444922',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_537b306e24a9b9_07555257',
  'variables' => 
  array (
    'news' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537b306e24a9b9_07555257')) {function content_537b306e24a9b9_07555257($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/news.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- ニュースの削除 -->
<div class="contents_box">
  <div class="contents_box_head">ニュースの削除
    <a href="/support/manual/news#news_2" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="ニュースの削除をすることが出来ます。<br>内容を確認後、「削除」ボタンを押して下さい。"></a>
  </div>
  <h4>ニュースの削除</h4>

  <form action="/news/del/exe/<?php echo $_smarty_tpl->tpl_vars['news']->value->id;?>
" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>ニュース画像</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['news']->value->image_path();?>
?<?php echo time();?>
" width="160" height="105" alt="ニュース画像" class="form_img con_img" />
    </li>

    <li>
      <label>タイトル</label>
      <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('title'))===null||$tmp==='' ? "-----" : $tmp);?>
</span>
    </li>
    <li>
      <label>本文</label>
      <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('body'))===null||$tmp==='' ? "-----" : $tmp);?>
</span>
    </li>
    <li>
      <label>配信対象</label>
      <span><?php echo (($tmp = @smarty_modifier_replace($_smarty_tpl->tpl_vars['fieldset']->value->value('target'),'1','球団アプリユーザーへも配信する'))===null||$tmp==='' ? "球団アプリユーザーへは配信しない" : $tmp);?>
</span>
    </li>
    <li>
      <label>配信予約日時</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('reserved_at_year');?>
年<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('reserved_at_month');?>
月<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('reserved_at_day');?>
日&emsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('reserved_at_hour');?>
：<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('reserved_at_min');?>
</span>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="削除" class="save_btn" />
  <a href="/news" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>