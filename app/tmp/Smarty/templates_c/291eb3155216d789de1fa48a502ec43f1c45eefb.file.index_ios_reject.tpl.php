<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-07 11:24:27
         compiled from "/home/spost/admin/app/views/inspect/index_ios_reject.tpl" */ ?>
<?php /*%%SmartyHeaderCode:134607745353ba04db4946e4-77605638%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '291eb3155216d789de1fa48a502ec43f1c45eefb' => 
    array (
      0 => '/home/spost/admin/app/views/inspect/index_ios_reject.tpl',
      1 => 1404016934,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '134607745353ba04db4946e4-77605638',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'SPONSOR' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53ba04db4f5781_26887968',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53ba04db4f5781_26887968')) {function content_53ba04db4f5781_26887968($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/inspect.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">アプリ審査申請
    <a href="/support/help" target="_blank"><img src="/assets/img/common/help_tips.png" title=""></a>
  </div>
  <h4 style="color:#900;">お客様のiOSアプリは以下の理由によりApple審査を通過しませんでした。<br />
    お手数ではございますが、下記の審査結果の内容をご確認いただきコンテンツ修正等のご対応をお願い致します。<br />
    ご対応が完了しましたら「Apple審査申請」ボタンを押してください。</h4>

  <form action="/inspect/ios" method="POST">
  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">
          ■Apple審査結果<br />
          審査NG<br />
        </span>
      </li>
      <li>
        <span class="ml_30">
          ■Apple審査NG理由<br />
          <?php echo nl2br($_smarty_tpl->tpl_vars['SPONSOR']->value->app()->inspect()->apple_reject_instance()->comment());?>

        </span>
      </li>
      <li>
        <span class="ml_30">
          ■審査通過へのヒント<br />
          <?php echo nl2br($_smarty_tpl->tpl_vars['SPONSOR']->value->app()->inspect()->apple_reject_instance()->hint());?>

        </span>
      </li>
      <li>
        <input type="submit" value="Apple審査申請" class="save_btn ml_30" />
      </li>
    </ul>
  </div>
  </form>

  <div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>