<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-09 11:52:31
         compiled from "/home/spost/admin/app/views/company/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1805446511539ebdedcdb1c0-24784157%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e51e487c4b6997df4e5318fc2764a9ebef35e963' => 
    array (
      0 => '/home/spost/admin/app/views/company/index.tpl',
      1 => 1404874350,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1805446511539ebdedcdb1c0-24784157',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539ebdedd63149_38044518',
  'variables' => 
  array (
    'SPONSOR' => 0,
    'image' => 0,
    'list' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539ebdedd63149_38044518')) {function content_539ebdedd63149_38044518($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/company.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- 企業概要トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">企業概要トップ画像の変更
    <a href="/support/manual/menu" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="チームトップ画像の変更ができます。<br>紹介したいものが伝わりやすい画像をアップロードしましょう。"></a>
  </div>
  <h4>企業概要ページのトップ画像を変更できます</h4>
  <p><span class="hisu">&nbsp;</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/company/top/update" class="edit_btn">変更する</a>
  <a href="/company/top/update" title="変更"><img src="<?php echo $_smarty_tpl->tpl_vars['image']->value->image_path();?>
?<?php echo time();?>
" width="160" height="70" alt="企業概要トップ画像" class="mb_20 con_img" /></a>
<?php }else{ ?>
  <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value->image_path();?>
" width="160" height="70" alt="企業概要トップ画像" class="mb_20 con_img" />
<?php }?>
</div>
<!---->

<!-- 企業名 -->
<div class="contents_box">
  <div class="contents_box_head lock">
    企業名
    <a href="/support/manual/store#store_1"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="App Store/Google Playに表示される<br>アプリ名とアプリの説明文が作成できます。<br>表示名はインストールしたアプリの名前です。<br>「編集する」ボタンを押して<br>表示したい内容を記入して下さい。"></a>
  </div>
  <h4></h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/company/name/update" class="edit_btn">編集する</a>
<?php }?>
  <ul class="cel">
    <li><label>日本語</label><span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->company()->name_ja())===null||$tmp==='' ? "---" : $tmp);?>
</span></li>
    <li><label>英語</label><span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->company()->name_en())===null||$tmp==='' ? "---" : $tmp);?>
</span></li>
  </ul>
</div>



<div class="contents_box">
  <div class="contents_box_head">表示項目一覧
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="表示項目の詳細が作成できます。"></a>
  </div>

  <h4 class="mb_20">表示項目の登録・編集・削除ができます</h4>

  <?php if ($_smarty_tpl->tpl_vars['list']->value){?>
  <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
  <ul class="cel cel_recruit">
    <li class="clearfix">
      <div class="cel_recruit_wrap">
        <label><input type="text" id="title<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->title;?>
" class="w_120"></label>
        <p class="fl"><textarea id="body<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->body;?>
</textarea></p>
        <p class="fr"><input type="button" id="upd_<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" class="mr_3 ml_30 news_edit_btn" value="更新" onclick="update('<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
', this)">&emsp;<input type="button" id="del_<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" class="mr_3 news_edit_btn" value="削除" onclick="del('<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
', this)"></span>
        <p id="error_msg<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
"></p>
      </div>
    </li>
  </ul>
  <?php } ?>
  <?php }?>

  <ul class="cel cel_recruit">
      <li class="clearfix">
        <div class="cel_recruit_wrap">
          <label><input type="text" id="add_title" value="タイトル" class="w_120"></label>
          <p class="fl"><textarea id="add_body">本文をココに記載してください</textarea></p>
          <p class="di fr mr_13"><input type="button" value="追加" onclick="add(this)" class="news_edit_btn"></span>
          <p id="error_msg_add"></p>
    </li>
  </ul>

</div>


<div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer_meta/company.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>