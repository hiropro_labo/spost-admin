<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-21 15:50:39
         compiled from "/home/spost/admin/app/views/profile/info/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:138169241553a5294b655110-89682280%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '14f28e1f7c7bf39d1eca09c5c4c87bdac2b0acb2' => 
    array (
      0 => '/home/spost/admin/app/views/profile/info/update/index.tpl',
      1 => 1403333412,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '138169241553a5294b655110-89682280',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a5294b6df098_62030328',
  'variables' => 
  array (
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a5294b6df098_62030328')) {function content_53a5294b6df098_62030328($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/profile.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">プロフィールの変更
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="新規追加したカテゴリーの内容が変更できます。<br>変更したい内容を変更後、「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>プロフィールの変更</h4>

  <form action="/profile/info/update/" method="POST" name="form1" id="form1" class="form1">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">お名前</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('name')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('name');?>
</p>
    </li>
    <li>
      <label class="hisu">肩書き</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('position')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('position');?>
</p>
    </li>
    <li>
      <label >生年月日</label>
      昭和<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('birth_year')->build();?>
&nbsp;年&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('birth_month')->build();?>
&nbsp;月&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('birth_date')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('birth_year');?>
</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('birth_month');?>
</p>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('birth_date');?>
</p>
    </li>
    <li>
      <label >血液型</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('blood')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('blood');?>
</p>
    </li>
    <li>
      <label >趣味</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('hobby')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('hobby');?>
</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/profile" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>