<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-07 11:43:04
         compiled from "/home/spost/admin/app/views/inspect/index_ios_request.tpl" */ ?>
<?php /*%%SmartyHeaderCode:192142539553ba0938080e48-52899576%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bf84d8257cacd1b41b3681804ff307916171296d' => 
    array (
      0 => '/home/spost/admin/app/views/inspect/index_ios_request.tpl',
      1 => 1399154787,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '192142539553ba0938080e48-52899576',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53ba09380d7952_71779821',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53ba09380d7952_71779821')) {function content_53ba09380d7952_71779821($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/inspect.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">アプリ審査申請
  <a href="/support/help" target="_blank"><img src="/assets/img/common/help_tips.png" title=""></a>
  </div>
  <h4 style="color:#900;">iOSアプリのApple審査申請のお申し込みを受け付けました。</h4>

  <div class="contents_form">
    <ul>
      <li>
        <span class="ml_30">iOSアプリのApple審査申請を順次進めてまいります。<br />
          審査終了までの日数につきましては申請完了から最低10営業日程度のお時間が掛かりますことをご了承くださいませ。<br />
          審査結果はメールにて改めてお知らせ致します。</span>
        </span>
      </li>
    </ul>
  </div>

  <div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>