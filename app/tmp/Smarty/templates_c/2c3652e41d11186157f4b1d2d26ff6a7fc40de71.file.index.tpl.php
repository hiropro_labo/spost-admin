<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-18 19:39:34
         compiled from "/home/spost/admin/app/views/topics/del/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:164132260253a16c6665eba2-55438418%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2c3652e41d11186157f4b1d2d26ff6a7fc40de71' => 
    array (
      0 => '/home/spost/admin/app/views/topics/del/index.tpl',
      1 => 1403055326,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '164132260253a16c6665eba2-55438418',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'topics' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a16c666ca5c7_48815282',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a16c666ca5c7_48815282')) {function content_53a16c666ca5c7_48815282($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/topics.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- トピックスの削除 -->
<div class="contents_box">
  <div class="contents_box_head">トピックスの削除
    <a href="/support/manual/news#news_2" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="トピックスの削除をすることが出来ます。<br>内容を確認後、「削除」ボタンを押して下さい。"></a>
  </div>
  <h4>トピックスの削除</h4>

  <form action="/topics/del/exe/<?php echo $_smarty_tpl->tpl_vars['topics']->value->id;?>
" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>トピックス画像</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['topics']->value->image_path();?>
?<?php echo time();?>
" width="160" height="105" alt="トピックス画像" class="form_img con_img" />
    </li>

    <li>
      <label>タイトル</label>
      <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('title'))===null||$tmp==='' ? "-----" : $tmp);?>
</span>
    </li>
    <li>
      <label>本文</label>
      <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('body'))===null||$tmp==='' ? "-----" : $tmp);?>
</span>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="削除" class="save_btn" />
  <a href="/topics" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>