<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-19 20:07:26
         compiled from "/home/spost/admin/app/views/preview/shopdetail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14043158495379e5cbcbe841-04362920%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8093d95733418cbc72beb8726f7d3d5dbbb96525' => 
    array (
      0 => '/home/spost/admin/app/views/preview/shopdetail.tpl',
      1 => 1400497638,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14043158495379e5cbcbe841-04362920',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5379e5cbdcf301_01149957',
  'variables' => 
  array (
    'shop_image' => 0,
    'shop' => 0,
    'shop_pref' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5379e5cbdcf301_01149957')) {function content_5379e5cbdcf301_01149957($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("preview/common/header_back.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- ▼コンテンツ -->
<div id="p_content_wrap" class="p_s_detail">

<!-- お店画像 -->
<div class="p_s_d_img">
  <img src="<?php echo $_smarty_tpl->tpl_vars['shop_image']->value->image_path();?>
?<?php echo time();?>
" width="225" height="148" alt="トップ画像" />
</div>
<!--  -->

<!-- お店情報 -->
<div class="p_s_d_date">
  <ul>
    <li>
      <label>お店の名前</label>
      <span><?php echo $_smarty_tpl->tpl_vars['shop']->value->shop_name;?>
</span>
    </li>

    <li>
      <label>郵便番号</label>
      <span><?php echo $_smarty_tpl->tpl_vars['shop']->value->zip_code1;?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['shop']->value->zip_code2;?>
</span>
    </li>

    <li>
      <label>住所</label>
      <span>
        <?php echo $_smarty_tpl->tpl_vars['shop_pref']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['shop']->value->city;?>
 <?php echo $_smarty_tpl->tpl_vars['shop']->value->address_opt1;?>
 <br />
        <?php echo $_smarty_tpl->tpl_vars['shop']->value->address_opt2;?>

      </span>
    </li>

    <li>
      <label>TEL</label>
      <span><?php echo $_smarty_tpl->tpl_vars['shop']->value->tel1;?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['shop']->value->tel2;?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['shop']->value->tel3;?>
</span>
    </li>

    <li>
      <label>FAX</label>
      <span><?php echo $_smarty_tpl->tpl_vars['shop']->value->fax1;?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['shop']->value->fax2;?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['shop']->value->fax3;?>
</span>
    </li>

    <li>
      <label>Email</label>
<?php if ($_smarty_tpl->tpl_vars['shop']->value->email){?>
      <span><?php echo $_smarty_tpl->tpl_vars['shop']->value->email;?>
</span>
<?php }else{ ?>
      <span>&nbsp;</span>
<?php }?>
    </li>

    <li>
      <label>URL</label>
<?php if ($_smarty_tpl->tpl_vars['shop']->value->url){?>
      <span><a href="<?php echo $_smarty_tpl->tpl_vars['shop']->value->url;?>
"><?php echo $_smarty_tpl->tpl_vars['shop']->value->url;?>
</a></span>
<?php }else{ ?>
      <span>&nbsp;</span>
<?php }?>
    </li>

    <li>
      <label>営業時間</label>
<?php if ($_smarty_tpl->tpl_vars['shop']->value->open_hours){?>
      <span><?php echo $_smarty_tpl->tpl_vars['shop']->value->open_hours;?>
</span>
<?php }else{ ?>
      <span>&nbsp;</span>
<?php }?>
    </li>

    <li>
      <label>定休日</label>
<?php if ($_smarty_tpl->tpl_vars['shop']->value->holiday){?>
      <span><?php echo $_smarty_tpl->tpl_vars['shop']->value->holiday;?>
</span>
<?php }else{ ?>
      <span>&nbsp;</span>
<?php }?>
    </li>
  </ul>
</div>
<!--  -->

</div>
<!-- ▲ -->


<?php echo $_smarty_tpl->getSubTemplate ("preview/common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>