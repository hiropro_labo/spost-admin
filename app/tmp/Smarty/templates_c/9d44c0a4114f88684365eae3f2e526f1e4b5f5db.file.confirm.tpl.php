<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-26 15:53:25
         compiled from "/home/spost/admin/app/views/store/text/update/confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11667514915382e4e5b9d4a4-00035033%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9d44c0a4114f88684365eae3f2e526f1e4b5f5db' => 
    array (
      0 => '/home/spost/admin/app/views/store/text/update/confirm.tpl',
      1 => 1398068780,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11667514915382e4e5b9d4a4-00035033',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'fieldset' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5382e4e5c0e610_77086156',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5382e4e5c0e610_77086156')) {function content_5382e4e5c0e610_77086156($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/store.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<div class="contents_box">
  <div class="contents_box_head">アプリストア(App Store/Google Play)表示項目の変更
  </div>
  <h4>アプリストア(App Store/Google Play)表示項目の変更</h4>

  <form action="/store/text/update/exe" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label for="name">アプリ名</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('app_name');?>
</span>
    </li>
    <li>
      <label for="name">表示名</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('app_disp_name');?>
</span>
    </li>
    <li>
      <label for="name">説明文</label>
      <span><?php echo nl2br($_smarty_tpl->tpl_vars['fieldset']->value->value('description'));?>
</span>
    </li>
  </ul>

  <hr />

  
  <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldset']->value->field(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value){
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>
  <?php echo $_smarty_tpl->tpl_vars['field']->value;?>

  <?php } ?>
  
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>