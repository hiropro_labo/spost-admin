<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-26 16:28:26
         compiled from "/home/spost/admin/app/views/store/sshot2/delete/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7689725715382ed1a196120-51673042%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b7ae17574b2bdcac16b0ac7ca2afef518e2b118a' => 
    array (
      0 => '/home/spost/admin/app/views/store/sshot2/delete/index.tpl',
      1 => 1397786736,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7689725715382ed1a196120-51673042',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'position' => 0,
    'image' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5382ed1a200f82_02155061',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5382ed1a200f82_02155061')) {function content_5382ed1a200f82_02155061($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/store.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- トップ画像の変更 -->
<div class="contents_box">
  <div class="contents_box_head">スクリーンショット２の削除
   <a href="/support/manual/store#store_3"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="スクリーンショット１の画像を削除できます。<br>「この画像を削除」ボタンを押して、削除して下さい。"></a>
  </div>
  <h4>スクリーンショット２の画像を削除します</h4>

  <form action="/store/sshot2/delete/exe/<?php echo $_smarty_tpl->tpl_vars['position']->value;?>
" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value->image_path();?>
?<?php echo time();?>
" width="160" height="240" alt="スクリーンショット画像" class="mb_20 con_img" />
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="この画像を削除" class="save_btn" />
  <a href="/store" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>