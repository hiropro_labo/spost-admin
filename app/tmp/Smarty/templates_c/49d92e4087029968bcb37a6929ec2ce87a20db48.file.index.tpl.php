<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-04 14:03:51
         compiled from "/home/spost/admin/app/views/recruit/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:43431334553a13be677ab84-50854567%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '49d92e4087029968bcb37a6929ec2ce87a20db48' => 
    array (
      0 => '/home/spost/admin/app/views/recruit/index.tpl',
      1 => 1404450230,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '43431334553a13be677ab84-50854567',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a13be68771b4_54733426',
  'variables' => 
  array (
    'SPONSOR' => 0,
    'list' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a13be68771b4_54733426')) {function content_53a13be68771b4_54733426($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/company.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>




<div class="contents_box">
  <div class="contents_box_head">採用情報一覧
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="採用情報データを登録できます。"></a>
  </div>
  <h4>採用情報を新規に追加できます</h4>
  <p class="mb_20"><span class="hisu">※簡易申請時の必須項目ではありません。</span>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/recruit/title/add" class="edit_btn mt_m12">採用情報の追加</a>
<?php }?>

</p>

<?php if (!is_null($_smarty_tpl->tpl_vars['list']->value)&&count($_smarty_tpl->tpl_vars['list']->value)>0){?>

<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
<div class="row-fluid br_4 row-fluid_wrap">
  <dl class="menu_ac_recruit">
    <dt>
      <div class="menu_cate_box_recruit">
        <p class="menu_table_text_recruit clearfix">
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
          <a href="/recruit/info/<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->title;?>
</a><br />
<?php }else{ ?>
          <?php echo $_smarty_tpl->tpl_vars['item']->value->title;?>
<br />
<?php }?>
        </p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
       <p class="fr">
        <a href="/recruit/info/<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" class="gray_btn mr_5 ml_20">編集</a>
        <a href="/recruit/del/<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" class="gray_btn mr_5 bucket"></a>
       </p>
<?php }?>
      </div>

    </dt>
  </dl>
</div>

<?php } ?>

<?php }else{ ?>
  <div class="menu_list row-fluid mb_20 mt_20 br_4 row-fluid_wrap">
    <p>現在、採用情報のデータはありません</p>
  </div>
<?php }?>

</div>


<div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>