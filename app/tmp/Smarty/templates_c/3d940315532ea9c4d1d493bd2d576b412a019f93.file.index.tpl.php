<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-23 12:09:14
         compiled from "/home/spost/admin/app/views/menu/category/del/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9567435537ebbda673ed9-03772414%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3d940315532ea9c4d1d493bd2d576b412a019f93' => 
    array (
      0 => '/home/spost/admin/app/views/menu/category/del/index.tpl',
      1 => 1400814534,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9567435537ebbda673ed9-03772414',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'category' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_537ebbda6e3b34_98201707',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537ebbda6e3b34_98201707')) {function content_537ebbda6e3b34_98201707($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- カテゴリーの削除 -->
<div class="contents_box">
  <div class="contents_box_head">カテゴリーの削除
    <a href="/support/manual/menu#menu_2" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="登録したカテゴリーを削除できます。<br>「削除」ボタンを押して、削除して下さい。"></a>
  </div>
  <h4>カテゴリーの削除</h4>

  <form action="/menu/category/del/exe/<?php echo $_smarty_tpl->tpl_vars['category']->value->id;?>
" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label>カテゴリー画像</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['category']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="メニューカテゴリー画像" class="form_img con_img" />
    </li>

    <li>
      <label>カテゴリー名</label>
      <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('title'))===null||$tmp==='' ? "-----" : $tmp);?>
</span>
    </li>

    <li>
      <label>サブタイトル名</label>
      <span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['fieldset']->value->value('sub_title'))===null||$tmp==='' ? "-----" : $tmp);?>
</span>
    </li>

    <li>
      <label>&nbsp;</label>
      <span><?php echo smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['fieldset']->value->value('enable'),'0','非表示'),'1','表示');?>
</span>
    </li>
  </ul>

  <hr />

   <input type="submit" name="button" value="削除" class="save_btn" />
 <a href="/menu" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>