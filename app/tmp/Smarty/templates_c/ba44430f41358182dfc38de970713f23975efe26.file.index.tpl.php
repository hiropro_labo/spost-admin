<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-07 12:24:06
         compiled from "/home/spost/admin/app/views/coupon/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1863756083535a313434d052-35026673%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ba44430f41358182dfc38de970713f23975efe26' => 
    array (
      0 => '/home/spost/admin/app/views/coupon/index.tpl',
      1 => 1404703444,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1863756083535a313434d052-35026673',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_535a31343ca223_08976010',
  'variables' => 
  array (
    'SPONSOR' => 0,
    'list' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535a31343ca223_08976010')) {function content_535a31343ca223_08976010($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/coupon.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- クーポンの変更 -->
<div class="contents_box">
  <div class="contents_box_head">クーポンの変更
    <a href="/support/manual/coupon" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="お客様に特別なクーポンを提供する事ができます。<br>「編集」ボタンから画像や内容を変更することができます。<br>クーポンは、全部で５枚まで登録可能です。"></a></div>

  <h4>クーポンの登録・変更</h4>
  <p class="mb_20"><span class="hisu">※簡易申請時に必須の項目です。</span><br />最低、１枚は必要になっております。

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()&&!$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->coupon()->is_entry_max()){?>
  <a href="/coupon/add" class="edit_btn">クーポンの新規登録</a>
<?php }?>
  </p>

<?php if (!is_null($_smarty_tpl->tpl_vars['list']->value)&&count($_smarty_tpl->tpl_vars['list']->value)>0){?>

<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
  <div class="row-fluid br_4 row-fluid_wrap"style="max-height:100px;">
      <dl class="menu_ac_recruit">
        <dt>
          <div class="menu_cate_box_recruit">
            <p class="menu_table_text_recruit">
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
            <a href="/coupon/update/<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->title;?>
</a><br />
<?php }else{ ?>
            <?php echo $_smarty_tpl->tpl_vars['item']->value->title;?>
<br />
<?php }?>
            </p>

  <div class="fr">
    <?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
                
                <a href="/coupon/update/<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" class="gray_btn mr_5 ml_20">編集</a>
                
                <a href="/coupon/del/<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" class="gray_btn mr_5"><img src="/assets/img/common/icon/i11.png" width="18" id="i11" style="margin-left:0;"></a>
    <?php }?>
    
          
    
    <?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
          
            <?php if (!is_null($_smarty_tpl->tpl_vars['item']->value->file_name)&&count($_smarty_tpl->tpl_vars['list']->value)>1){?>
              
              <?php if ($_smarty_tpl->tpl_vars['item']->value->position!=1){?>
              <a href="/coupon/order/up/<?php echo $_smarty_tpl->tpl_vars['item']->value->position;?>
" class="gray_btn ml_20 mb_10"><img src="/assets/img/common/icon/i16.png" width="14" style="margin-left: 0;"/ ></a>
              <?php }?>
    
              
              <?php if ($_smarty_tpl->tpl_vars['item']->value->position<count($_smarty_tpl->tpl_vars['list']->value)){?>
              <a href="/coupon/order/down/<?php echo $_smarty_tpl->tpl_vars['item']->value->position;?>
" class="gray_btn ml_20"><img src="/assets/img/common/icon/i17.png" width="14" style="margin-left:0;"/ ></a>
              <?php }?>
            <?php }?>
          
    <?php }?>
  </div>
  </div>
      
      </dt>
    </dl>
  </div>

<?php } ?>

<?php }else{ ?>
  <div class="menu_list row-fluid mb_20 mt_20 br_4 row-fluid_wrap">
    <p>現在、クーポンが登録されていません。</p>
  </div>
<?php }?>

</div>







</div>
<!---->


<div class="last_margin"></div>


<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
<?php echo $_smarty_tpl->getSubTemplate ("common/footer_meta/coupon.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>