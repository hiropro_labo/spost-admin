<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-23 15:09:34
         compiled from "/home/spost/admin/app/views/url/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1578714876539ea805309844-96161146%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7fb499736983659d30595bf4de4bf018546862ba' => 
    array (
      0 => '/home/spost/admin/app/views/url/update/index.tpl',
      1 => 1403470485,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1578714876539ea805309844-96161146',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539ea805384b04_04558813',
  'variables' => 
  array (
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539ea805384b04_04558813')) {function content_539ea805384b04_04558813($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/url.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">URL</div>
  <h4>スライドメニューに表示されるURLとメニュータイトルを編集できます</h4>

  <form action="/url/update" method="POST">
    <div class="contents_form">
      <ul>
        <li>
          <label><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->label('url_title1');?>
</label>
          <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('url_title1')->build();?>

          <p class="desc">最大7文字まで</p>
          <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('url_title1');?>
</p>
        </li>
        <li>
          <label><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->label('url1');?>
</label>
          <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('url1')->build();?>

          <p class="desc">最大255文字まで</p>
          <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('url1');?>
</p>
        </li>
        <li>
          <label><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->label('url_title2');?>
</label>
          <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('url_title2')->build();?>

          <p class="desc">最大7文字まで</p>
          <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('url_title2');?>
</p>
        </li>
        <li>
          <label><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->label('url2');?>
</label>
          <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('url2')->build();?>

          <p class="desc">最大255文字まで</p>
          <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('url2');?>
</p>
        </li>
        <li>
          <label><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->label('url_title3');?>
</label>
          <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('url_title3')->build();?>

          <p class="desc">最大7文字まで</p>
          <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('url_title3');?>
</p>
        </li>
        <li>
          <label><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->label('url3');?>
</label>
          <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('url3')->build();?>

          <p class="desc">最大255文字まで</p>
          <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('url3');?>
</p>
        </li>
      </ul>
      <hr />
      <input type="submit" name="button" value="変更の確認" class="save_btn" />
      <a href="/url" id="save_btn" class="back_btn">戻る</a>
    </div>
  </form>
</div>

<div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>