<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-10 12:08:35
         compiled from "/home/spost/admin/app/views/branch/add/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:74200726853abf8f366b653-72382217%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '61851a59360daf049883b7de288d0825013c9544' => 
    array (
      0 => '/home/spost/admin/app/views/branch/add/index.tpl',
      1 => 1404961685,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '74200726853abf8f366b653-72382217',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53abf8f37b8764_94818171',
  'variables' => 
  array (
    'branch' => 0,
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53abf8f37b8764_94818171')) {function content_53abf8f37b8764_94818171($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- カテゴリーの新規作成 -->
<div class="contents_box">
  <div class="contents_box_head">カテゴリーの新規作成
    <a href="/support/manual/menu#menu_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="カテゴリーの新規作成ができます。<br>「ファイルを選択」ボタンを押して、画像をアップロード<br>テキストの記入、表示・非表示選択後<br>「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>カテゴリーの新規作成</h4>

  <form action="/branch/add" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">画像のアップロード</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['branch']->value->blank_image();?>
?<?php echo time();?>
" width="160" height="88" alt="メニュートップ画像" class="form_img con_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />640px&nbsp;×&nbsp;350px&nbsp;以上の大きさを推奨</p>
    </li>
  </ul>

  <ul>
    <li>
      <label for="name" class="hisu">お店の名前</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('shop_name')->build();?>
<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('shop_name');?>

    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="zip1">郵便番号</label>
    <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('zip_code1')->build();?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('zip_code2')->build();?>

    </li>

    <li>
      <label for="pref">都道府県</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('pref')->build();?>

    </li>

    <li>
      <label for="pref">市区町村</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('city')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('city');?>

    </li>

    <li>
      <label for="pref">住所（番地）</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('address_opt1')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('address_opt1');?>

    </li>

    <li>
      <label for="pref">住所（建物）</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('address_opt2')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('address_opt2');?>

    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">電話番号</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('tel1')->build();?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('tel2')->build();?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('tel3')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('tel1');?>

    </li>

    <li>
      <label for="pref">FAX</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('fax1')->build();?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('fax2')->build();?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('fax3')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('fax1');?>

    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">ホームページ</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('url')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('url');?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('url');?>

    </li>
  </ul>

  <ul>
    <li>
      <label for="pref">オンラインショップ</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('online_shop')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('online_shop');?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('online_shop');?>

    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">メールアドレス</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('email')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('email');?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('email');?>

    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">営業時間</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('open_hours')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('open_hours');?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('open_hours');?>

    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label for="pref">定休日</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('holiday')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('holiday');?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('holiday');?>

    </li>
  </ul>

  <hr />

  <ul>
    <li>
      <label>&nbsp;</label>
      <label for="form_enable_1"><input type="radio" required="required" value="1" id="form_enable_1" name="enable" checked="checked" />表示</label>

      <label for="form_enable_0"><input type="radio" required="required" value="0" id="form_enable_0" name="enable" />非表示</label>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('enable');?>
</p>
    </li>
  </ul>
      <p class="desc">お客様に見せるかどうかを選ぶことができます。</p>
  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/branch" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>