<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-16 12:55:32
         compiled from "/home/spost/admin/app/views/coupon/add/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1398215118539e6989746148-40042453%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1aa0381d7d2ba4db44271a796be4ba3aa96a2264' => 
    array (
      0 => '/home/spost/admin/app/views/coupon/add/index.tpl',
      1 => 1402890930,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1398215118539e6989746148-40042453',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539e69898034c7_74580261',
  'variables' => 
  array (
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539e69898034c7_74580261')) {function content_539e69898034c7_74580261($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/coupon.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- クーポンの新規登録 -->
<div class="contents_box">
  <div class="contents_box_head">クーポンの新規登録
　<a href="/support/manual/coupon"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="お客様に特別なクーポンを提供することが出来ます。<br>「ファイルを選択」ボタンを押して、画像をアップロード<br>利用条件、期間設定後「変更の確認」ボタンを押して下さい。"></a>
  </div>
  <h4>クーポンの新規登録</h4>

  <form action="/coupon/add" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label>タイトル</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('title')->build();?>

      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('title');?>

    </li>

    <li>
      <label>クーポン内容</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('body')->build();?>

      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('body');?>

    </li>

    <li>
      <label>利用条件</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('policy')->build();?>

      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('policy');?>

    <p class="desc">実際のスマホアプリでは、クーポン画像をタップすると表示されます。<br />※左の簡易プレビューには表示されません。</p>
    </li>

    <li class="mb_20">
      <label>期間指定</label>
      <!-- <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('term_flg')->build();?>
 -->
      <label for="form_term_flg_0"><input type="radio" required="required" id="form_term_flg_0" name="term_flg" value="0" checked="checked" />指定しない</label>
      <label for="form_term_flg_1"><input type="radio" required="required" id="form_term_flg_1" name="term_flg" value="1" />指定する</label>
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('term_flg');?>
</p>
    </li>

    <li>
      <label>表示開始日</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('start_year')->build();?>
年<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('start_month')->build();?>
月<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('start_day')->build();?>
日
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('start_year');?>
</p>
    </li>

    <li>
      <label>表示終了日</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('end_year')->build();?>
年<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('end_month')->build();?>
月<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('end_day')->build();?>
日
      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('end_year');?>
</p>
    </li>
  </ul>


  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/coupon" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>



<script type="text/javascript">
$(function(){
	  if($('#form_body').val() != "")
	  {
	    $('#form_body').css({"height":"300px"});
	  }
	  $('#form_body').focus(function(){
	    $(this).animate({"height":"300px"}, "swing");
	  });
	  if($('#form_policy').val() != "")
	  {
	    $('#form_policy').css({"height":"300px"});
	  }
	  $('#form_policy').focus(function(){
	    $(this).animate({"height":"300px"}, "swing");
	  });
});
</script>



<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>