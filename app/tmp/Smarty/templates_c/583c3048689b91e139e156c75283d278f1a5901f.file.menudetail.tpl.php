<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-23 14:18:57
         compiled from "/home/spost/admin/app/views/preview/menudetail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:150022398537ed9c79e1414-64713666%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '583c3048689b91e139e156c75283d278f1a5901f' => 
    array (
      0 => '/home/spost/admin/app/views/preview/menudetail.tpl',
      1 => 1400822330,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '150022398537ed9c79e1414-64713666',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_537ed9c7a82299_33196364',
  'variables' => 
  array (
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537ed9c7a82299_33196364')) {function content_537ed9c7a82299_33196364($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.date_format.php';
?><?php echo $_smarty_tpl->getSubTemplate ("preview/common/header_back.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- ▼コンテンツ -->
<div id="p_content_wrap" class="p_m_detail">

<!-- 商品画像 -->
<div class="p_m_d_img">
  <img src="<?php echo $_smarty_tpl->tpl_vars['item']->value->image_path();?>
?<?php echo time();?>
" width="225" height="123" alt="トップ画像" />
</div>
<!--  -->

<?php if (!is_null($_smarty_tpl->tpl_vars['item']->value)){?>
<!-- 商品情報 -->
<div class="p_m_d_date">
  <div class="p_m_d_d_name">
    <?php echo $_smarty_tpl->tpl_vars['item']->value->title;?>

  </div>

  <div class="p_m_d_d_text">
    <?php echo nl2br($_smarty_tpl->tpl_vars['item']->value->description);?>

  </div>

  <div class="clearfix">
    <div class="p_m_d_d_price">
      <?php echo $_smarty_tpl->tpl_vars['item']->value->price;?>

    </div>
  </div>

  <div class="p_m_d_d_time">
    <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value->updated_at,"%Y-%m-%d %H:%M:%S");?>

  </div>
</div>
<!--  -->

<!-- ホームページへボタン -->
<div class="p_m_d_btns">
  <a href="<?php echo $_smarty_tpl->tpl_vars['item']->value->url;?>
" class="p_m_d_btn_store" target="_brank">
    ホームページへ
  </a>
</div>
<!--  -->
<?php }?>

</div>
<!-- ▲ -->


<?php echo $_smarty_tpl->getSubTemplate ("preview/common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>