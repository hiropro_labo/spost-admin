<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-26 16:46:49
         compiled from "/home/spost/admin/app/views/store/category/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7909453335382f169f19177-24667731%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1594da96b26864fd008068d59ae37480b6448ca8' => 
    array (
      0 => '/home/spost/admin/app/views/store/category/update/index.tpl',
      1 => 1398068775,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7909453335382f169f19177-24667731',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5382f16a045d05_59308940',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5382f16a045d05_59308940')) {function content_5382f16a045d05_59308940($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/store.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<div class="contents_box">
  <div class="contents_box_head">アプリのカテゴリーの変更
    <a href="/support/manual/store#store_5"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="アプリストア(App Store/Google Play)表示項目を編集することができます。<br>フォームに入力した後、「確認する」ボタンを押してください。"></a></div>
  <h4>アプリのカテゴリーを選択してください</h4>

  <form action="/store/category/update" method="POST">

<div class="contents_form">
  <ul>
    <li>
      <label for="zip1" class="hisu">iPhone１</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('category_iphone1')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('category_iphone1');?>
</p>
    </li>

    <li>
      <label for="zip1" class="hisu">iPhone２</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('category_iphone2')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('category_iphone2');?>
</p>
    </li>

    <li>
      <label for="zip1" class="hisu">Android</label>
      <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('category_android')->build();?>

      <p class="error"><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('category_android');?>
</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/store" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>