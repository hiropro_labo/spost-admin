<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-23 22:01:04
         compiled from "/home/spost/admin/app/views/store/info/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:196905040253a82510d51b65-37719179%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd43063d364802789ea3bd5c3ab112bc1ba5f1ed0' => 
    array (
      0 => '/home/spost/admin/app/views/store/info/update/index.tpl',
      1 => 1403495691,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '196905040253a82510d51b65-37719179',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'fieldset' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a82510de5f46_44530364',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a82510de5f46_44530364')) {function content_53a82510de5f46_44530364($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/store.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="contents_box">
  <div class="contents_box_head">TEL/MAIL</div>
  <h4>アプリ内のスライドメニュー内のTEL/MAILボタンに埋め込む情報を編集できます</h4>

  <form action="/store/info/update" method="POST">
    <div class="contents_form">
      <ul>
        <li>
          <label for="pref">電話番号</label>
          <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('tel1')->build();?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('tel2')->build();?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('tel3')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('tel1');?>

        </li>
        <li>
          <label for="pref">メールアドレス</label>
          <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->field('email')->build();?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->description('email');?>
 <?php echo $_smarty_tpl->tpl_vars['fieldset']->value->error_msg('email');?>

        </li>
      </ul>
      <hr />
      <input type="submit" name="button" value="変更の確認" class="save_btn" />
      <a href="/store" id="save_btn" class="back_btn">戻る</a>
    </div>
  </form>
</div>

<div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>