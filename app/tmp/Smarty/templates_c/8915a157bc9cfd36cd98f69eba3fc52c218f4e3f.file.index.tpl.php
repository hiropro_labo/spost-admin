<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-26 20:29:57
         compiled from "/home/spost/admin/app/views/top/img/shop/update/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8831199015379a892e183c5-81333395%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8915a157bc9cfd36cd98f69eba3fc52c218f4e3f' => 
    array (
      0 => '/home/spost/admin/app/views/top/img/shop/update/index.tpl',
      1 => 1403749783,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8831199015379a892e183c5-81333395',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5379a892e6e209_60011752',
  'variables' => 
  array (
    'SPONSOR' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5379a892e6e209_60011752')) {function content_5379a892e6e209_60011752($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/shop.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- お店の画像変更 -->
<div class="contents_box">
  <div class="contents_box_head">お店の情報・写真の変更
    <a href="/support/manual/top#top_3"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="お店詳細の表示時の画像を変更する事ができます。<br>「ファイルを選択」ボタンを押し、お好きな画像をアップロード後、<br>「変更を保存」ボタンを押してください。"></a>
  </div>
  <h4>お店の画像</h4>

<form action="/top/img/shop/update/" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label class="hisu">画像のアップロード</label>
      <img src="<?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->shop()->image_path();?>
?<?php echo time();?>
" width="160" height="105" alt="ショップ画像" class="con_img form_img" />
      <input type="file" name="upload" id="upload">
      <p class="desc">ファイルサイズ：3MBまで<br />640px&nbsp;×&nbsp;420px&nbsp;以上の大きさを推奨</p>
    </li>
  </ul>

  <hr />

  <input type="submit" name="button" value="変更の確認" class="save_btn" />
  <a href="/shop" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>