<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-07 15:21:02
         compiled from "/home/spost/admin/app/views/common/header_root_new.tpl" */ ?>
<?php /*%%SmartyHeaderCode:112811055253abcdf5175447-69984348%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9ca795b83e2c21262997d44b95c925a9078ff708' => 
    array (
      0 => '/home/spost/admin/app/views/common/header_root_new.tpl',
      1 => 1404714021,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '112811055253abcdf5175447-69984348',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53abcdf5191279_98665137',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53abcdf5191279_98665137')) {function content_53abcdf5191279_98665137($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<title><?php echo __('site.title');?>
</title>

<!-- Favicon Icon -->
<link href="/assets/img/common/favicon/ipost.png" rel="shortcut icon" type="image/png" />

<link rel="stylesheet" type="text/css" href="/assets/css/top/bootstrap.tutorial.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/root.css" media="all" />
<link rel="stylesheet" type="text/css" href="/assets/css/top/bass.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/sub.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/tooltipster.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/spost.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/assets/css/top/blue.css">


<?php echo $_smarty_tpl->getSubTemplate ("common/load_css.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<script src="/assets/js/jquery-1.8.0.min.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery-ui.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/bootstrap.min.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.contenthover.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.dropdown.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.tooltipster.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.cookie.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/jquery.carouFredSel-6.2.1-packed.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/modal/popup.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/modal/tutorial.js" type="text/javascript" language="javascript"></script>
<script src="/assets/js/icheck.js" type="text/javascript" language="javascript"></script>

<script>
    $(function(){
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '10%', // optional
            cursor: true // optional
        });
    });
</script>



<?php echo $_smarty_tpl->getSubTemplate ("common/load_js.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



</head>
<body>


<?php echo $_smarty_tpl->getSubTemplate ("common/content_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/pan/index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>