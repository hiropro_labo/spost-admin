<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-13 19:50:34
         compiled from "/home/spost/admin/app/views/goods/item/update/confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1694357433539ad77a6fe377-56116809%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '14f94b2112e638207e781a2df5db670427eb22f6' => 
    array (
      0 => '/home/spost/admin/app/views/goods/item/update/confirm.tpl',
      1 => 1402623916,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1694357433539ad77a6fe377-56116809',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'c_id' => 0,
    'img_upload_flg' => 0,
    'item' => 0,
    'category' => 0,
    'fieldset' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_539ad77a7a6ca8_88009261',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539ad77a7a6ca8_88009261')) {function content_539ad77a7a6ca8_88009261($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/spost/fuel/vendor/smarty/smarty/distribution/libs/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/goods.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- 商品の変更 -->
<div class="contents_box">
  <div class="contents_box_head">商品の変更
  </div>
  <h4>商品の変更</h4>

  <form action="/goods/item/update/exe/<?php echo $_smarty_tpl->tpl_vars['c_id']->value;?>
" method="POST" name="form1" id="form1" class="form1" enctype="multipart/form-data">

<div class="contents_form">
  <ul>
    <li>
      <label>画像のアップロード</label>
      <?php if ($_smarty_tpl->tpl_vars['img_upload_flg']->value){?>
      <img src="<?php echo $_smarty_tpl->tpl_vars['item']->value->tmp_image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="商品画像" class="mb_20 con_img" />
      <?php }else{ ?>
      <img src="<?php echo $_smarty_tpl->tpl_vars['item']->value->image_path();?>
?<?php echo time();?>
" width="160" height="88" alt="商品画像" class="mb_20 con_img" />
      <?php }?>
    </li>

    <li>
      <label>カテゴリー</label>
      <span><?php echo $_smarty_tpl->tpl_vars['category']->value->title;?>
</span>
    </li>

    <li>
      <label>商品名</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('title');?>
</span>
    </li>

    <li>
      <label>サブタイトル名</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('sub_title');?>
</span>
    </li>

    <li>
      <label>商品説明</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('description');?>
</span>
    </li>

    <li>
      <label>価格</label>
      <span><?php echo number_format($_smarty_tpl->tpl_vars['fieldset']->value->value('price'));?>
円</span>
    </li>

    <li>
      <label>URL</label>
      <span><?php echo $_smarty_tpl->tpl_vars['fieldset']->value->value('url');?>
</span>
    </li>

    <li>
      <label>&nbsp;</label>
      <span><?php echo smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['fieldset']->value->value('enable'),'0','非表示'),'1','表示');?>
</span>
     </li>
  </ul>

  <hr />

  
  <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldset']->value->field(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value){
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>
  <?php echo $_smarty_tpl->tpl_vars['field']->value;?>

  <?php } ?>
  
  <input type="submit" name="button" value="変更の保存" class="save_btn" />
  <a href="javascript:history.back();" id="save_btn" class="back_btn">戻る</a>
</div>

  </form>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>