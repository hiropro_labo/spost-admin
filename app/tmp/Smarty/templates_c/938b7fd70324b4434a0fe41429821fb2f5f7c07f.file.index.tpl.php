<?php /* Smarty version Smarty-3.1-DEV, created on 2014-06-21 19:50:46
         compiled from "/home/spost/admin/app/views/profile/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:46474950253a519ec351c37-20704820%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '938b7fd70324b4434a0fe41429821fb2f5f7c07f' => 
    array (
      0 => '/home/spost/admin/app/views/profile/index.tpl',
      1 => 1403347841,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '46474950253a519ec351c37-20704820',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a519ec4cc284_09169891',
  'variables' => 
  array (
    'SPONSOR' => 0,
    'profile_history' => 0,
    'item' => 0,
    'list_banner' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a519ec4cc284_09169891')) {function content_53a519ec4cc284_09169891($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/profile.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<div class="contents_box">
  <div class="contents_box_head">
    プロフィール
    <a href="/support/manual/top#top_3"  target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="電話番号や営業時間などの、お店の情報を変更する事ができます。<br>「お店の画像」には、あなたのお店の魅力が伝わる写真をアップロードしましょう。。"></a>
  </div>

  <h4>写真の変更</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span></p>
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/profile/img/update" class="edit_btn">編集する</a>
  <a href="/profile/img/update"><img src="<?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->image_profile()->image_path();?>
?<?php echo time();?>
" width="240" alt="プロフィール画像" class="mb_20 con_img" /></a>
<?php }else{ ?>
  <img src="<?php echo $_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->image_profile()->image_path();?>
?<?php echo time();?>
" width="240" alt="プロフィール画像" class="mb_20 con_img" />
<?php }?>

  <hr />

  <h4>プロフィール情報</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span></p>
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/profile/info/update" class="edit_btn">編集する</a>
<?php }?>
  <ul class="cel">
    <li><label>お名前</label><span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->profile()->name())===null||$tmp==='' ? "---" : $tmp);?>
</span></li>
    <li><label>肩書き</label><span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->profile()->position())===null||$tmp==='' ? "---" : $tmp);?>
</span></li>
    <li><label>生年月日</label><span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->profile()->birth_day())===null||$tmp==='' ? "---" : $tmp);?>
</span></li>
    <li><label>血液型</label><span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->profile()->blood())===null||$tmp==='' ? "---" : $tmp);?>
</span></li>
    <li><label>趣味</label><span><?php echo (($tmp = @$_smarty_tpl->tpl_vars['SPONSOR']->value->contents()->profile()->hobby())===null||$tmp==='' ? "---" : $tmp);?>
</span></li>
  </ul>
</div>




<div class="contents_box">
  <div class="contents_box_head">略　歴
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="表示項目の詳細が作成できます。"></a>
  </div>

  <h4>略歴の登録・編集・削除ができます</h4>
  <p><span class="hisu"></span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/profile/history" class="edit_btn">編集する</a>
<?php }?>

  <?php if ($_smarty_tpl->tpl_vars['profile_history']->value){?>
  <ul class="cel">
  <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['profile_history']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
    <li><label><?php echo $_smarty_tpl->tpl_vars['item']->value->title;?>
</label><span><?php echo $_smarty_tpl->tpl_vars['item']->value->body;?>
</span></li>
  <?php } ?>
  </ul>
  <?php }else{ ?>
  <div class="menu_list row-fluid mb_20 mt_20 br_4 row-fluid_wrap">
    <p>現在、略歴が登録されていません。</p>
  </div>
  <?php }?>
</div>




<div class="contents_box">
  <div class="contents_box_head">バナー一覧
    <a href="/support/manual/menu#menu_3" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="バナー関連の編集ができます。"></a>
  </div>

  <h4>バナーの登録・編集・削除ができます</h4>
  <p><span class="hisu">※簡易申請時に必須の項目です。</span></p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/banner/add" class="edit_btn">バナーの新規登録</a>
<?php }?>

<?php if (!is_null($_smarty_tpl->tpl_vars['list_banner']->value)&&count($_smarty_tpl->tpl_vars['list_banner']->value)>0){?>

<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list_banner']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
  <div class="row-fluid mb_20 mt_20 br_4 row-fluid_wrap" <?php if ($_smarty_tpl->tpl_vars['item']->value>'enable'==0){?> style="background-color:#cccccc;max-height:100px;"<?php }else{ ?>style="max-height:100px;"<?php }?>>
      <dl class="menu_ac">
        <dt>
          <div class="menu_cate_box">
<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
            <a href="/banner/update/<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" title="編集"><img src="<?php echo $_smarty_tpl->tpl_vars['item']->value->image_path();?>
?<?php echo time();?>
" width="320" height="71" alt="バナー画像" class="mb_20 con_img"></a>
<?php }else{ ?>
            <img src="<?php echo $_smarty_tpl->tpl_vars['item']->value->image_path();?>
?<?php echo time();?>
" width="320" height="71" alt="バナー画像" class="mb_20 con_img">
<?php }?>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
            
            <a href="/banner/update/<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" class="gray_btn mr_5 ml_20">編集</a>
            
            <a href="/banner/del/<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" class="gray_btn mr_5"><img src="/assets/img/common/icon/i11.png" width="18" id="i11" style="margin-left:0;"></a>
<?php }?>

      </div>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
      <div class="up_down">
        <?php if (!is_null($_smarty_tpl->tpl_vars['item']->value->file_name)&&count($_smarty_tpl->tpl_vars['list_banner']->value)>1){?>
          
          <?php if ($_smarty_tpl->tpl_vars['item']->value->position!=1){?>
          <a href="/banner/order/up/<?php echo $_smarty_tpl->tpl_vars['item']->value->position;?>
" class="gray_btn ml_20 mb_10"><img src="/assets/img/common/icon/i16.png" width="14" style="margin-left: 0;"/ ></a>
          <?php }?>

          
          <?php if ($_smarty_tpl->tpl_vars['item']->value->position<count($_smarty_tpl->tpl_vars['list_banner']->value)){?>
          <a href="/banner/order/down/<?php echo $_smarty_tpl->tpl_vars['item']->value->position;?>
" class="gray_btn ml_20"><img src="/assets/img/common/icon/i17.png" width="14" style="margin-left:0;"/ ></a>
          <?php }?>
        <?php }?>
      </div>
<?php }?>
      <div class="clear"></div>
      </dt>
    </dl>
  </div>

<?php } ?>

<?php }else{ ?>
  <div class="menu_list row-fluid mb_20 mt_20 br_4 row-fluid_wrap">
    <p>現在、バナーが登録されていません。</p>
  </div>
<?php }?>

</div>

<div class="last_margin"></div>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer_meta/recruit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>