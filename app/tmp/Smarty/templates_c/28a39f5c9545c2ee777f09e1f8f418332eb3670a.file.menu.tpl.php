<?php /* Smarty version Smarty-3.1-DEV, created on 2014-05-22 12:55:39
         compiled from "/home/spost/admin/app/views/preview/menu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:804360090537d75168bfee7-30323900%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '28a39f5c9545c2ee777f09e1f8f418332eb3670a' => 
    array (
      0 => '/home/spost/admin/app/views/preview/menu.tpl',
      1 => 1400730936,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '804360090537d75168bfee7-30323900',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_537d7516951b43_35736652',
  'variables' => 
  array (
    'parent_id' => 0,
    'SPONSOR' => 0,
    'image' => 0,
    'list' => 0,
    'entry' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537d7516951b43_35736652')) {function content_537d7516951b43_35736652($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['parent_id']->value==0){?>
<?php echo $_smarty_tpl->getSubTemplate ("preview/common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }else{ ?>
<?php echo $_smarty_tpl->getSubTemplate ("preview/common/header_back.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }?>


<!-- ▼コンテンツ -->
<div id="p_content_wrap" class="p_menu">

<!-- メニュートップ画像 -->
<div>
  <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value->image_path($_smarty_tpl->tpl_vars['SPONSOR']->value->id());?>
?<?php echo time();?>
" width="240" height="131" alt="メニュートップ画像" />
</div>
<!--  -->

<!-- カテゴリーリスト -->
<div class="p_menu_list">
  <ul>
<?php if (!is_null($_smarty_tpl->tpl_vars['list']->value)){?>
<?php  $_smarty_tpl->tpl_vars['entry'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['entry']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['entry']->key => $_smarty_tpl->tpl_vars['entry']->value){
$_smarty_tpl->tpl_vars['entry']->_loop = true;
?>
    <!-- リスト表示 -->
    <li class="clearfix">
<?php if ($_smarty_tpl->tpl_vars['parent_id']->value==0){?>
      <a href="/preview/menu/<?php echo $_smarty_tpl->tpl_vars['entry']->value->id;?>
">
<?php }else{ ?>
      <a href="/preview/menudetail/<?php echo $_smarty_tpl->tpl_vars['entry']->value->id;?>
">
<?php }?>
        <div class="p_m_list_img">
          <img src="<?php echo $_smarty_tpl->tpl_vars['entry']->value->image_path();?>
?<?php echo time();?>
?<?php echo time();?>
" width="140" height="80" alt="メニュー画像" style="float:left;border-bottom: solid 1px #e0e0e0;" />
        </div>
        <div class="p_m_list_data">
          <div class="clearfix">
            <div class="p_m_list_sub">
              <?php echo $_smarty_tpl->tpl_vars['entry']->value->title;?>

            </div>
            <div class="p_m_list_title">
              <?php echo $_smarty_tpl->tpl_vars['entry']->value->sub_title;?>

            </div>
          </div>
        </div>
      </a>
    </li>
    <!--  -->
<?php } ?>
<?php }?>
  </ul>
</div>
<!--  -->

</div>
<!-- ▲ -->


<?php echo $_smarty_tpl->getSubTemplate ("preview/common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>