<?php /* Smarty version Smarty-3.1-DEV, created on 2014-07-09 11:38:20
         compiled from "/home/spost/admin/app/views/topics/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:80534959753a165e266d1e5-26273200%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f2aea16e8a57f7bd14c2b1267fb3dd320be555b7' => 
    array (
      0 => '/home/spost/admin/app/views/topics/index.tpl',
      1 => 1404873498,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '80534959753a165e266d1e5-26273200',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_53a165e2753157_71147259',
  'variables' => 
  array (
    'SPONSOR' => 0,
    'pager' => 0,
    'list' => 0,
    'topics' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a165e2753157_71147259')) {function content_53a165e2753157_71147259($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ("common/header_meta/topics.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<!-- トピックスの編集 -->
<div class="contents_box">
  <div class="contents_box_head">トピックスの編集
    <a href="support/manual/news" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="トピックスの新規作成ができます。<br>あなたのお店の最新情報を配信していきましょう！"></a>
  </div>
  <h4>トピックスの編集</h4>
  <p class="mb_10"><span class="hisu">※簡易申請時に必須の項目です。</span><br />最低、１つのトピックスは配信準備が必要になっております。<br />
  PUSH通知機能は、アプリが公開状態になりましたらご利用できます。</p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
  <a href="/topics/add" class="edit_btn mt_m40">トピックスの新規作成</a>
<?php }?>
</div>

<!-- トピックス一覧 -->
<div class="contents_box">
  <div class="contents_box_head">トピックス一覧
    <a href="support/manual/news#news_1" target="_blank"><img src="/assets/img/common/help_tips.png" class="tooltip" title="トピックスの編集・削除ができます。"></a>
  </div>
  <h4>トピックス一覧</h4>

  <div class="row-fluid mb_20">

    <?php echo $_smarty_tpl->tpl_vars['pager']->value;?>


    <ul id="news_table">

<?php if (!is_null($_smarty_tpl->tpl_vars['list']->value)){?>
<?php  $_smarty_tpl->tpl_vars['topics'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['topics']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['topics']->key => $_smarty_tpl->tpl_vars['topics']->value){
$_smarty_tpl->tpl_vars['topics']->_loop = true;
?>
      <li class="news_table">
<?php if ($_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
        <img src="<?php echo $_smarty_tpl->tpl_vars['topics']->value->image_path();?>
?<?php echo time();?>
" width="160" height="107" class="mb_20 con_img" alt="トピックス画像" />
<?php }else{ ?>
        <a href="/topics/update/<?php echo $_smarty_tpl->tpl_vars['topics']->value->id;?>
" title="編集"><img src="<?php echo $_smarty_tpl->tpl_vars['topics']->value->image_path();?>
?<?php echo time();?>
" width="160" height="107" class="mb_20 con_img" alt="トピックス画像" /></a>
<?php }?>

        <div class="news_text_box">
          <p id="table_text"><b><?php echo $_smarty_tpl->tpl_vars['topics']->value->title;?>
</b><br /><?php echo $_smarty_tpl->tpl_vars['topics']->value->body;?>
</p>

<?php if (!$_smarty_tpl->tpl_vars['SPONSOR']->value->app()->is_lock()){?>
          <a href="/topics/update/<?php echo $_smarty_tpl->tpl_vars['topics']->value->id;?>
" class="mr_3 ml_30 news_edit_btn">編集</a>
          <a href="/topics/del/<?php echo $_smarty_tpl->tpl_vars['topics']->value->id;?>
" class="mr_3 news_edit_btn"><img src="/assets/img/common/icon/i11.png" width="18" id="i11"/></a>
<?php }?>
        </div>
        <div class="clear"></div>
      </li>

      <hr>
<?php } ?>
<?php }else{ ?>
      <li class="news_table">
      <p>現在、配信準備、または配信中のトピックスが登録されていません。</p>
      </li>
<?php }?>

    </ul>

<?php echo $_smarty_tpl->tpl_vars['pager']->value;?>


  </div>
</div>
<!---->


<div class="last_margin"></div>


<?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>