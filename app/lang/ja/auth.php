<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * ログイン画面
 */

return array(
    'btn_login'    => 'ログイン',
    'lbl_login_id' => 'E-Mail',
    'lbl_secret'   => 'パスワード',
);
