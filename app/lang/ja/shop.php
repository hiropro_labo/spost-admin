<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * トップ管理画面
 */

return array(
    // header_meta / top
    'header' => array(
        'title' => '店舗情報',
        'sub'   => '店舗情報の編集を行うことが出来ます。',
        ),
);
