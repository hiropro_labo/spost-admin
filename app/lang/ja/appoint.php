<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * メニュー管理画面
 */

return array(
    // header_meta / top
    'header' => array(
        'title' => '予約カレンダーの表示',
        'sub'   => 'お店情報の画面から予約カレンダーを表示させることが出来ます。',
        ),
);
