<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * メニュー管理画面
 */

return array(
    // header_meta / top
    'header' => array(
        'title' => 'カテゴリー選択',
        'sub'   => 'アプリの利用用途に合わせたカテゴリーを選択してください',
        ),
);
