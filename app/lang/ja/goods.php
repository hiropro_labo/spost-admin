<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * 商品管理画面
 */

return array(
    // header_meta / top
    'header' => array(
        'title' => '商品',
        'sub'   => '商品の編集を行うことが出来ます。',
        ),
);
