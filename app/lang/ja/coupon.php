<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * クーポン管理画面
 */

return array(
    // header_meta / top
    'header' => array(
        'title' => 'クーポン',
        'sub'   => 'クーポンの編集を行うことが出来ます。',
        ),
);
