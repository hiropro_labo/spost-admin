<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * 審査管理画面
 */

return array(
    // header_meta / top
    'header' => array(
        'title' => 'アプリ審査申請',
        'sub'   => 'アプリ審査申請を行うことが出来ます。',
        ),
);
