<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * クーポン管理画面
 */

return array(
    // header_meta / top
    'header' => array(
        'title' => 'プロフィール情報',
        'sub'   => 'プロフィール情報の編集を行うことが出来ます。',
        ),
);
