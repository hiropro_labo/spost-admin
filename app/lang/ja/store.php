<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * ストア情報管理画面
 */

return array(
    // header_meta / top
    'header' => array(
        'title' => 'アプリの情報編集',
        'sub'   => 'アプリ情報などの変更を行うことが出来ます。',
        ),
);
