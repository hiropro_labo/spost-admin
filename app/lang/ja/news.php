<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * ニュース管理画面
 */

return array(
    // header_meta / top
    'header' => array(
        'title' => 'ニュース',
        'sub'   => 'ニュースの編集を行うことが出来ます。',
        ),
);
