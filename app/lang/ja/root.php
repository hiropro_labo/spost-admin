<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * ログイン画面
 */

return array(
    // アプリの作り方
    'howto_app' => array(
        'howto_way'    => 'もっと詳しく',
        'step_top'     => 'トップ画像の作成',
        'step_coupon'  => 'クーポン/イベント画面の作成',
        'step_news'    => '>ニュース画面の作成',
        'step_menu'    => 'メニュー画面の作成',
        'step_info'    => 'アプリの情報編集',
        'step_apply'   => 'アプリの審査申請',
        'step_payment' => 'お支払い手続き',
        'step_expose'  => '公開',
        ),
    'howto_app_desc' => array(
        'step_top'     => 'アプリトップを編集しましょう',
        'step_coupon'  => 'クーポンやイベント情報を入れましょう',
        'step_news'    => 'ニュースを作りましょう',
        'step_menu'    => 'メニューを作りましょう',
        'step_info'    => 'アプリアイコンなどを設定しましょう',
        'step_apply'   => 'アプリ作成が終わったら審査申請しましょう',
        'step_payment' => '月額利用料金のお支払い手続き',
        'step_expose'  => 'お支払い確認後アプリが正式公開されます！',
        ),
    // お知らせ
    'info' => array(
        'info_ja' => 'お知らせ',
        'info_en' => 'information',
        ),
);
