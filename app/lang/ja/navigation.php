<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * ログイン画面
 */

return array(
    'site'     => array(
        'title' => 'SPost管理画面 Admin',
        ),
    'nav_home' => 'ホーム',
    'app_edit' => array(
        'nav_edit' => 'アプリ管理',
        'nav_top'      => 'トップ',
        'nav_coupon'   => 'クーポン',
        'nav_news'     => 'ニュース',
        'nav_menu'     => 'メニュー',
        'nav_goods'    => '商品',
        'nav_url'      => 'URL',
        'nav_company'  => '企業概要',
        'nav_segments' => '事業内容',
        'nav_recruit'  => '採用情報',
        'nav_topics'   => 'トピックス',
        'nav_profile'  => 'プロフィール',
        'nav_banner'   => 'バナー',
        'nav_shop'     => '店舗情報',
        'nav_branch'   => '多店舗',
        ),

    'app_info' => array(
        'nav_info'    => 'アプリ情報',
        'nav_edit'    => '情報編集',
        'nav_inspect' => '審査申請',
        'nav_payment' => '支払い',
        'nav_expire'  => '有効期限',
        ),

    'breadlist' => array(
        'bread_home'    => 'ホーム',
        'bread_edit_app'    => 'アプリ管理',
        'bread_app_info'    => 'アプリ情報',
        'bread_support' => 'サポート',
        'bread_top'     => 'トップ',
        'bread_coupon'  => 'クーポン',
        'bread_news'    => 'ニュース',
        'bread_menu'    => 'メニュー',
        'bread_edit'    => '情報編集',
        'bread_inspect' => '審査申請',
        'bread_payment' => '支払い',
        'bread_pop'     => 'ポスター・ポップ',
        'bread_info'    => 'お知らせ',
        'bread_reject'  => 'リジェクト',
        'bread_help'    => 'ヘルプ',
        'bread_contact' => 'お問い合わせ',
        ),

    'login' => array(
        'client_id' => 'クライアントID',
        'logout'    => 'ログアウト',
        'san'       => 'さん',
        'login_now' => 'でログイン中',
        ),

    'footer' => array(
        'help'          => 'ヘルプ',
        'terms_of_user' => '利用規約',
        'contact'       => 'お問い合わせ',
        'postar_pop'    => 'ポスター・ポップ',
        ),

    'page' => array(
        'guide' => '使い方ガイド',
        ),
);
