<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * メニュー管理画面
 */

return array(
    // header_meta / top
    'header' => array(
        'title' => 'メニュー',
        'sub'   => 'メニューの編集を行うことが出来ます。',
        ),
);
