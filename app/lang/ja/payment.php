<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * 支払い管理画面
 */

return array(
    // header_meta / top
    'header' => array(
        'title' => 'お支払い',
        'sub'   => 'お支払いの確認等を行うことが出来ます。',
        ),
);
