<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * ストア情報管理画面
 */

return array(
    // header_meta / top
    'header' => array(
        'title' => 'ポスター・ポップ',
        'sub'   => 'ポスター・ポップの見本を見ることが出来ます。',
        ),
);
