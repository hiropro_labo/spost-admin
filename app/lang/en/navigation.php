<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * ログイン画面
 */

return array(
    'site'     => array(
        'title' => 'SPost Admin page',
        ),

    'nav_home'    => 'Home',
    'app_edit' => array(
        'nav_edit' => 'Edit App',
        'nav_top'      => 'TOP',
        'nav_coupon'   => 'COUPON',
        'nav_news'     => 'NEWS',
        'nav_menu'     => 'MENU',
        'nav_goods'    => 'GOODS',
        'nav_url'      => 'URL',
        'nav_company'  => 'COMPANY',
        'nav_segments' => 'SEGMENTS',
        'nav_recruit'  => 'RECRUIT',
        'nav_topics'   => 'TOPICS',
        'nav_profile'  => 'PROFILE',
        'nav_banner'   => 'BANNER',
        'nav_shop'     => 'SHOP',
        'nav_branch'   => 'BRANCH',
        ),

    'app_info' => array(
        'nav_info'    => 'App info',
        'nav_edit'    => 'Edit info',
        'nav_inspect' => 'Apply for exam',
        'nav_payment' => 'Payment',
        'nav_expire'  => 'Expire',
        ),

    'breadlist' => array(
        'bread_home'    => 'Home',
        'bread_edit_app'    => 'Edit App',
        'bread_app_info'    => 'App info',
        'bread_support' => 'Support',
        'bread_top'     => 'Top',
        'bread_coupon'  => 'Coupon',
        'bread_news'    => 'News',
        'bread_menu'    => 'Menu',
        'bread_edit'    => 'Edit info',
        'bread_inspect' => 'Apply for exam',
        'bread_payment' => 'Payment',
        'bread_pop'     => 'poster / pop',
        'bread_info'    => 'Infomation',
        'bread_reject'  => 'Reject',
        'bread_help'    => 'Help',
        'bread_contact' => 'Inquiry',
        ),

    'login' => array(
        'client_id' => 'Client ID',
        'logout'    => 'Logout',
        'san'       => '',
        'login_now' => ''
        ),

    'footer' => array(
        'help'          => 'HELP',
        'terms_of_user' => 'Terms of Use',
        'contact'       => 'Contact',
        'postar_pop'    => 'poster / pop',
        ),

    'page' => array(
        'guide' => 'How to use',
        ),
);
