<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * トップ管理画面
 */

return array(
    // header_meta / top
    'header' => array(
        'title' => 'PROFILE',
        'sub'   => 'プロフィール情報の編集を行うことが出来ます。',
        ),
);
