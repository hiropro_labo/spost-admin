<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * メニュー管理画面
 */

return array(
    // header_meta / top
    'header' => array(
        'title' => 'Category Choice',
        'sub'   => 'Please select the category that matches the use use of application.',
        ),
);
