<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * メニュー管理画面
 */

return array(
    // header_meta / top
    'header' => array(
        'title' => 'オプション設定',
        'sub'   => '各種オプションの機能設定を行うことが出来ます。',
        ),
);
