<?php
/**
 * Languege 設定ファイル：Japanese
 *
 * ログイン画面
 */

return array(
    // アプリの作り方
    'howto_app' => array(
        'howto_way'    => 'The detailed way',
        'step_top'     => 'Add Image on TOP.',
        'step_coupon'  => 'Create coupon/event page.',
        'step_news'    => 'Create NEWS page.',
        'step_menu'    => 'Create MENU page.',
        'step_info'    => 'Edit App info',
        'step_apply'   => 'Apply for examination',
        'step_payment' => 'Payment',
        'step_expose'  => 'App Expose',
        ),
    'howto_app_desc' => array(
        'step_top'     => 'Edit App TOP.',
        'step_coupon'  => 'Input coupon/event info.',
        'step_news'    => 'Write NEWS.',
        'step_menu'    => 'edit MENU.',
        'step_info'    => 'Set App icon.etc.',
        'step_apply'   => 'After completed, Apply App.',
        'step_payment' => 'Please pay the charges.',
        'step_expose'  => 'After payment, App be available',
        ),
    // お知らせ
    'info' => array(
        'info_ja' => 'information',
        'info_en' => '',
        ),
);
