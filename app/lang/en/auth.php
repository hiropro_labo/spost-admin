<?php
/**
 * Languege 設定ファイル：English
 *
 * ログイン画面
 */

return array(
    'btn_login'    => 'Login',
    'lbl_login_id' => 'E-Mail',
    'lbl_secret'   => 'Secret',
);
