<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>iPost｜年内限定１か月無料キャンペーン｜銀行振込での支払いの場合</title>

<link rel="stylesheet" type="text/css" href="/assets/css/path/lp/cp.css">
<link rel="stylesheet" type="text/css" href="/assets/css/bass.css">

</head>
<body>

<div id="contents" class="no_preview">

   <div class="contents_box">
      <div class="widget_head_2 contents_box_head">銀行振込でのお支払い</div>
      <div class="row-fluid zero_camp_box">
            <p>銀行振込みによる利用料金のお支払いが可能です。<br>銀行振り込みをご希望のお客様は、下記の口座へお振り込み下さい。</p>
              <dl>
                <dt>銀行名</dt><dd>三井住友銀行</dd>
                <dt>店番号</dt><dd>651</dd>
                <dt>支店名</dt><dd>岡山支店</dd>
                <dt>口座番号</dt><dd>6976158</dd>
                <dt>口座名義</dt><dd>ｶﾌﾞｼｷｶﾞｲｼｬ　ﾋﾛｷｶｸ</dd>
              </dl>
            <p><span id="zero_camp_caution">※振込名義人はiPost申し込み時のお名前でお願い致します。また、振込手数料はお客様のご負担となります。</span><br>※2013年内にiPostの初月利用料金をお支払いいただいた時点で、「１か月無料キャンペーン」への参加が完了いたします。</p>
          <div class="clear"></div>
      </div>
      </div>

  <img src="/assets/img/common/lp/cp1/camp_img_4.png" class="mt_30 ml_40">
  <img src="/assets/img/common/lp/cp1/camp_img_3.png" class="mt_20 ml_40 mb_60">

</div>

</body>
</html>
