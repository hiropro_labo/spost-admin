;(function($) {
$.fn.select_to_children=function(config){
  var defaults={
    file:"./script/update_data.1.0.php",
    table:"cities",
    column:"code, name",
    where:"prefectures_code",
    target:"#city_id"
  };
  var options=$.extend(defaults, config);

  this.change(function(){
   val=this.value;
    if(val!=""){
      $.post(options.file, {"param1":options.table, "param2":val, "param3":options.column, "param4":options.where}, function(json){

        res=$.parseJSON(json);
        data=[];
        if(json){
          for(i=0;i<json.length;i++){
            data.push(json[i]);
          }
        }

        $(options.target).html("<option value=\"0\">選択してください</option>\n");
        for(i=0;i<data.length;i++){
          dat=data[i];
          $(options.target).append("<option value=\""+ dat.code +"\">"+ dat.name +"</option>\n");
        }
      }, "json");
    }else{
      $(options.target).html("<option value=\"0\">選択してください</option>\n");
    }
  });
};})(jQuery);
