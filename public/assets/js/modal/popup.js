/**
 * POPUP NEWS
 **/
function popup_open(str)
{
    if($.cookie('admin.ipost.popup.cp1') != 1)
    {
        $.cookie('admin.ipost.popup.cp1', '1', { expires: 31 });
        window.open('/lp/cp');

        // $('#popupModal').modal('show');
    }
}

/***
 * Modal OK
 **/
function popup_ok()
{
    $('#popupModal').modal('hide');
    $.cookie('admin.ipost.popup.cp1', '1', { expires: 31 });
    window.open('/lp/cp');
}

/**
 * Modal Close
 **/
function popup_close()
{
    $('#popupModal').modal('hide');
}

