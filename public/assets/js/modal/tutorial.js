/**
 * チュートリアル
 **/
function tutorial(client, num)
{
    if(num != 0 || num != "")
    {
        switch(num)
        {
            case 1: // Root Top
                $('div.list_carousel ul#foo2 li:nth-of-type(1) a').append('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click" alt="ここをクリック！" />');
                break;

            case 2: // Top Carousel
                tutorial_download('top_images');

                $('ul#sortable-li').prepend('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click" alt="ここをクリック！" />');
                break;

            case 3: // Top Topic
                $('div#contents div.contents_box:nth-of-type(3)').append('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click" alt="ここをクリック！" />');
                break;

            case 4: // Top Shop Image
                tutorial_download('top_shop');

                $('div#contents div.contents_box:nth-of-type(4)').prepend('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-img-click" alt="ここをクリック！" />');
                break;

            case 5: // Top Shop
                $('#tutorialModal').modal('show');

                $('div#contents div.contents_box:nth-of-type(4)').prepend('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-shop-click" alt="ここをクリック！" />');
                break;

            case 6: // Coupon
                tutorial_download('coupon');

                $('ul#sortable-li').prepend('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click" alt="ここをクリック！" />');
                break;

            case 7: // News
                tutorial_download('news');

                $('div#contents div.contents_box:nth-of-type(2)').append('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click" alt="ここをクリック！" />');
                break;

            case 8: // Menu Top Image
                tutorial_download('menu_top');

                $('div#contents div.contents_box:nth-of-type(2)').append('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click menu-top" alt="ここをクリック！" />');
                break;

            case 9: // Menu Category Add
                tutorial_download('menu_category');

                $('div#contents div.contents_box:nth-of-type(3)').append('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click menu-category" alt="ここをクリック！" />');

                break;

            case 10: // Menu Item Add
                tutorial_download('menu_item');

                $('div#contents div.contents_box:nth-of-type(4)').prepend('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click menu-item" alt="ここをクリック！" />');

                $('div#contents div.contents_box:nth-of-type(4) dl.menu_ac dd div.menu_inline').prepend('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click menu-item" alt="ここをクリック！" />');
                break;

            case 11: // Store Application Icon Image
                tutorial_download('store_icon');

                $('div#contents div.contents_box:nth-of-type(2)').append('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click store-icon" alt="ここをクリック！" />');
                break;

            case 12: // Store Application Date
                $('div#contents div.contents_box:nth-of-type(4)').prepend('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click store-date" alt="ここをクリック！" />');
                break;

            case 13: // Store Application Splash Image
                tutorial_download('store_splash');

                $('div#contents div.contents_box:nth-of-type(7)').prepend('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click store-splash" alt="ここをクリック！" />');
                break;

            case 14: // Store Category
                $('div#contents div.contents_box:nth-of-type(8)').prepend('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click store-splash" alt="ここをクリック！" />');
                break;
        }
        // 画像指定
        tutorial_img(num);

        $('#tutorialModal').modal('show');

        tutorial_send(client, num);
    }
}

/**
 * Tutorial Root Click
 **/
function tutorial_root(client, num)
{
    // Root Top Click
    if(num > 0 && num < 6)
    {
        $('div.list_carousel ul#foo2 li:nth-of-type(1) a').append('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click root-top" alt="ここをクリック！" />');
    }
    // Root Coupon Click
    if(num == 6)
    {
        $('div.list_carousel ul#foo2 li:nth-of-type(2) a').append('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click root-coupon" alt="ここをクリック！" />');
    }
    // Root News Click
    if(num == 7)
    {
        $('div.list_carousel ul#foo2 li:nth-of-type(3) a').append('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click root-news" alt="ここをクリック！" />');
    }
    // Root Menu Click
    if(num > 7 && num < 11)
    {
        $('div.list_carousel ul#foo2 li:nth-of-type(4) a').append('<img src="/assets/img/common/tutorial/click.png" width="152" height="110" class="tutorial-click root-menu" alt="ここをクリック！" />');
    }
    // Root Store Click
    if(num > 10 && num < 15)
    {
        $('div#head ul#nav').append('<img src="/assets/img/common/tutorial/click_leftup.png" width="152" height="70" class="tutorial-click root-store" alt="ここをクリック！" />');
    }
}

/**
 * Top Tutorial Step
 **/
function tutorial_top()
{
    return false;
}

/**
 * Send Tutorial Image
 **/
function tutorial_img(num)
{
    if(num == 3 || num == 5 || num == 12 || num == 14)
    {
        $('a.tutorial-sample-open img').addClass('hidden');
    }

    $('div.modal-body.tutorial img.tutorial-step').attr({
        src: '/assets/img/common/tutorial/'+ num +'.png'
    });
}

/**
 * Send Tutorial Step
 **/
function tutorial_send(client, step)
{
    $.post('/tutorial/step/index', {
        'client':client,
        'step':step
    }, function(res){
        // alert('ok : ' + client +' : '+ step);
    }, 'json');
}

/**
 * Tutorial Step OK
 **/
function tutorial_ok()
{
    $('div.modal-body.tutorial a.tutorial-ok img').removeClass('hidden');
}

/**
 * Tutorial Step Next
 **/
function tutorial_back(back)
{
    $('div.modal-body.tutorial a.tutorial-back img').removeClass('hidden');

    back --;
    if(back < 1) back = 1;
    $('div.modal-body.tutorial a.tutorial-back').attr({
        href: 'javascript:tutorial_img('+ back +');'
    });
}

/**
 * Tutorial Step Next
 **/
function tutorial_next(next)
{
    $('div.modal-body.tutorial a.tutorial-next img').removeClass('hidden');

    next ++;
    $('div.modal-body.tutorial a.tutorial-next').attr({
        href: 'javascript:tutorial_img('+ next +');'
    });
}

/**
 * Tutorial Step Sample Download
 **/
function tutorial_download(sample)
{
    $('div.modal-body.tutorial a.tutorial-sample-open').attr({
        href:'/assets/img/common/tutorial/sample/'+ sample +'.jpg'
    });
}

/**
 * Mocal Close
 **/
function tutorial_close()
{
    $('#tutorialModal').modal('hide');
}

